theory Fibonacci_StateT
  imports
    "../StateT_Heap/StateT_Heap_Main"
    Example_Misc
begin

fun fib :: "nat \<Rightarrow> int" where
  "fib 0 = 0"
| "fib (Suc 0) = 1"
| "fib (Suc (Suc n)) = fib (Suc n) + fib n"
thm Divides.eucl_rel_nat_unique_div

context
  fixes n :: nat
begin

print_locale dp_consistency_dmc_indicies
dp_fun fib\<^sub>T: fib
  memoizes dp_consistency_dmc_indicies
    where row_bound = "Bound 0 3"
      and col_bound = "Bound 0 1"
      and split_key = "\<lambda>n. (n div 3, n mod 3, 0::nat)"
      and indicies = "[0..<Suc n]"
    by standard (auto
        simp del: One_nat_def
        simp: index_ord.in_bound_def
        intro: injI div_mod_eq_iff[THEN iffD1]
        )
  monadifies (StateT_Heap) fib.simps
  dp_correctness
    by dp_match

  thm fib\<^sub>T.filled_memoized

definition "fib_impl = fib\<^sub>T.inited_filled fib\<^sub>T' n"
end
find_theorems StateT_Heap_Monad_Loop.sequence' mem_subtype.inv

term fib_impl

lemmas [code] =
  init_filled_mem_def.inited_filled_def
  direct_map_cache_heap_def.init_mem_def
  init_empty_def.inited_def
  filled_mem_def.filled_def
  (*filled_indicies_mem_def.fill_def*)

lemmas [code_unfold] =
  lazy_list_code

code_reflect fib_test functions fib_impl nat_of_integer
ML_val \<open>timeit (fib_test.fib_impl (fib_test.nat_of_integer 300000)) |> fst\<close>
  (* Naive Fibonacci is not linear but quadratic! *)

(*export_code open fib_impl in SML_imp module_name fib_test file "fib_test.ML"*)

end