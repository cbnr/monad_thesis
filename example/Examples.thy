(*<*)
theory Examples
imports
  LaTeXsugar
  OptBST
  CYK
  Min_Ed_Dist0
begin

declare [[names_short]]

no_notation Leaf ("\<langle>\<rangle>")
no_notation Node ("(1\<langle>_,/ _,/ _\<rangle>)")

translations ("type") "('n, 't) prods" <= ("type") "('n \<times> ('m, 't) rhs) list"

lemma cyk_ia_def2: "cyk_ia P xs = (let a = IArray xs in cyk_ix P (\<lambda>i. a !! hideid i) (0,length xs))"
by(simp add: cyk_ia_def hideid_def)
(*>*)
text \<open>
\section{Examples}

FIXME Intro to HOL - where? Incl \<open>`\<close> interval sets and lists.
@{term"[i..<j]"} is the list @{text"[i,...,j-1]"}.
Isabelle supports list comprehension syntax like in Haskell, except that \<open>[e | P]\<close> is written \<open>[e. P]\<close>.

This section presents five canonical but quite different examples of dynamic programming.
In each case we present a development that follows this template: we start with a recursive
function, prove its correctness and refine it to an imperative memoized algorithm
with the help of the automation described above.
Because all examples are well-known (in the context of imperative programming)
we mostly omit literature references.

There are usually two versions of each problem: computing the optimal cost of
some object and computing the object as well.
Below we describe, for lack of space, only the basic version of each problem.
FIXME We have formalized most of the extended versions as well.

All the main functions presented in this section are executable,
which means that Isabelle can generate executable code
in SML and other functional languages from those function definitions
\cite{BerghoferN-TYPES00,HaftmannN-FLOPS2010,HaftmannKKN-ITP13}.

\<close>
text\<open>\subsection{The Cocke-Younger-Kasami Algorithm}

Given a grammar in Chomsky normal form, the CYK algorithm computes the set of nonterminals that
produce (yield) some input string. We model
productions in Chomsky normal form as pairs \<open>(A,r)\<close> of a nonterminal \<open>A\<close> and a r.h.s. \<open>r\<close>
that is either of the form \<open>T a\<close>, where \<open>a\<close> is a terminal (of type \<open>'t\<close>), or \<open>NN B C\<close>, where \<open>B\<close> and \<open>C\<close>
are nonterminals (of type \<open>'n\<close>). Below \noquotes{@{term[source] "P :: ('n,'t) prods"}} is a list of productions.
The \emph{yield} of a nonterminal is defined inductively as a relation:
\medskip

\noindent
@{thm [mode=Rule] yield.intros(1)} \quad
@{thm [mode=Rule] yield.intros(2)}
\medskip

A functional programmer will start out with an implementation @{const_typ cyk} of the CYK algorithm
defined by recursion on lists and prove its correctness: @{thm cyk_correct}.
However, memoizing the list argument leads to an inefficient implementation.
Hence one would first refine @{const cyk} to a function @{const_typ cyk2} where the list argument is
constant and the two @{typ nat} arguments index the list and can be memoized efficiently.

We start with an indexed version and still leave open how indexing is implemented
by passing a function. This is the result:
\begin{quote}
@{const_typ cyk_ix}\\
@{thm cyk_ix.simps(1)}\\
\noquotes{@{prop[source]"cyk_ix P w (i,Suc 0) = [A . (A, T a) \<leftarrow> P, a = w i]"}}\\
@{text"cyk_ix P w (i,n) ="}\\
@{text"[A. k \<leftarrow> [1..<n], B \<leftarrow> cyk_ix P w (i,k), C \<leftarrow> cyk_ix P w (i+k,n-k),"}\\
\mbox{}\qquad@{text"(A, NN B' C') \<leftarrow> P, B' = B, C' = C]"}
\end{quote}
Correctness (proved by induction) explains the meaning of the argument \<open>(i,n)\<close>:
\begin{quote}
@{thm cyk_ix_correct}\\
where @{abbrev "slice w i j"}.
\end{quote}

Functional Memoization: FIXME should be moved to Bellman-Ford and adjusted

Now we invoke the memoization command that generates from @{const cyk_ix} a memoized version
@{const cyk_ix\<^sub>T} of type @{typ "nat \<times> nat \<Rightarrow> ((nat \<times> nat, 'n list) mapping, 'n list) state"}
where @{typ "('a,'b)mapping"} is some implementation of finite mappings from @{typ 'a} to @{typ 'b};
in this and all other examples we have chosen red-black trees.
The memoization also proves that @{const cyk_ix} can be computed by running @{const cyk_ix\<^sub>T}:
@{thm cyk_ix_cyk_ixT}. Isabelle can be instructed to use this equation when generating code for @{const cyk_ix}.

In another step we implement parameter \noquotes{@{term[source] "w :: nat \<Rightarrow> 't"}}.
Instead of indexing into a list (which takes linear time) we employ Isabelle's theory of
immutable arrays. It defines a function @{const IArray}
that maps a list into an immutable array and with the infix \<open>!!\<close> array subscript function.
Thus we can transform a list into an immutable array first and then call @{const cyk_ix}:
\begin{quote}
@{thm cyk_ia_def2}
\end{quote}
Isabelle's code generator maps immutable arrays to type \<open>vector\<close> in SML.


As before we obtain a memoized version @{const cyk_ix\<^sub>T} and the correctness theorem
@{thm cyk_ix_cyk_ixT}.

Imperative Memoization: FIXME


Related work: AFP article~\cite{CYK-AFP} presents a formalization of the CYK algorithm
where the memoizing version (using HOL functions as tables!) is defined and verified by hand.
In total it requires 1000 lines of Isabelle text. Our version is a mere 100 FIXME lines
and yields efficient imperative code.
\<close>
text\<open>\subsection{Minimum Edit Distance}

The minimum edit distance between two lists \<open>xs\<close> and \<open>ys\<close> of type @{typ "'a list"} is the
minimum cost of converting \<open>xs\<close> to \<open>ys\<close> by means of a sequence of the edit operations
copy, replace, insert and delete:
\begin{quote}
@{datatype ed}
\end{quote}
The cost of @{const Copy} is 0, all other operations have cost 1.
Function @{const edit} defines how an @{typ "'a ed list"} transform one @{typ "'a list"}
into another:
\begin{quote}
@{thm (dummy_pats) edit.simps(1-4) edit.simps(8)}
\end{quote}
We have omitted the cases where the second list becomes empty before the first.

This time we start from two functions defined by recursion on lists:
\begin{quote}
@{const_typ min_ed}\\
@{const_typ min_eds}
\end{quote}
Function @{const min_ed} computes the minimum edit distance and @{const_typ min_eds}
computes a list of edits with minimum cost. We omit their definitions.
The relationship between them is trivial to prove: @{thm min_ed_min_eds}.
Therefore the following easy correctness and minimality theorems about @{const min_eds}
also imply correctness and minimality of @{const min_ed}:
\begin{quote}
@{thm min_eds_correct} \qquad
@{thm min_eds_minimal[OF refl]}
\end{quote}
As for CYK, we define a function by recursion on indices
\begin{quote}
@{const_typ min_ed_ix}\\
@{thm [break] min_ed_ix.simps}
\end{quote}
and prove that it correctly refines @{const min_ed}:
@{thm min_ed_ix_min_ed}.
Although one can prove correctness of this indexed version directly, the route via
the recursive functions on lists is simpler.

As before we obtain a memoized version @{const min_ed_ix\<^sub>T} and the correctness theorem
@{thm min_ed_ix_min_ed_ixT}.

FIXME imperative memoization

\<close>text\<open>\subsection{Optimal Binary Search Tree}

We are given a fixed list of keys $k_0 = -\infty < k_1 < k_2 < \cdots < k_n < k_{n+1} = \infty$
and a fixed frequency distribution for these keys
and we want to construct an optimum binary search tree for these keys w.r.t their distribution.
The latter are given by $\alpha_i$ ($0 \le i \le n$), the frequency of searching for a key
in the interval $(k_i,k_{i+1})$ (i.e.\ failing searches) and $\beta_i$ ($1 \le i \le n$),
the frequency of searching for $k_i$ \cite{Knuth71}. Instead of using $\alpha$ and $\beta$ directly we
combine them into $W\;i\;j = \sum_{l=i-1}^j \alpha_l + \sum_{l=i}^j \beta_l$,
the frequency of searching for a key in the interval $(k_{i-1}, k_{j+1})$.
In our formalization \<open>W\<close> has type @{typ "int \<Rightarrow> int \<Rightarrow> nat"}.
The keys themselves are irrelevant and we work with their indices instead.

Our data type of binary trees (type @{typ "'a tree"}) has two constructors: leaves @{const Leaf}
and nodes @{term "Node l x r"} where \<open>l\<close> and \<open>r\<close> are the two children and \<open>x :: 'a\<close> is the
label of the node. Function @{const inorder} returns the list of all labels in symmetric order.
Thus we are interested in trees \noquotes{@{term[source] "t :: int tree"}} where
@{prop"inorder t = [i..j]"}.
Among those we want one with minimum \emph{weighted path length}:
\begin{quote}
@{thm wpl.simps}
\end{quote}
This definition of @{term "wpl i j t"} implicitly assumes that @{prop"inorder t = [i..j]"}.
Then @{term "wpl i j t"} is the cost (number of comparisons) of searching \<open>t\<close> for all the keys
in the interval $(k_{i-1},k_{j+1})$ with their given frequencies.
The minimum cost among all search trees is computed as follows:
\begin{quote}
@{thm[break] min_wpl.simps}
\end{quote}
For lack of space we do not present the (analogous) computation of the tree itself
by function @{const_typ opt_bst} (nor the simultaneous computation of the cost and the tree).
We proved correctness and optimality of @{const min_wpl} and @{const opt_bst}:
\begin{quote}
@{thm min_wpl_minimal}\\
@{thm opt_bst_correct}\\
@{thm wpl_opt_bst}
\end{quote}
When memoized, both @{const min_wpl} and @{const opt_bst} take cubic time. We do not present
Knuth's optimization to quadratic time as it is orthogonal to our aims.

FIXME memoization


\<close>
(*<*)
end
(*>*)
