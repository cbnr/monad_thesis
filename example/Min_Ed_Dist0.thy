section "Minimum Edit Distance purely functionally"

theory Min_Ed_Dist0
imports
  "~~/src/HOL/Library/IArray"
  "../state_monad/State_Main"
  "../heap_monad/Heap_Reader_Main"
  Example_Misc
  "HOL-Library.Code_Target_Numeral"
  "~~/src/HOL/Library/Product_Lexorder"
begin

subsection "Executable argmin"

fun argmin :: "('a \<Rightarrow> 'b::order) \<Rightarrow> 'a list \<Rightarrow> 'a" where
"argmin f [a] = a" |
"argmin f (a#as) = (let m = argmin f as in if f a \<le> f m then a else m)"
(* end rm *)

(* Ex: Optimization of argmin *)
fun argmin2 :: "('a \<Rightarrow> 'b::order) \<Rightarrow> 'a list \<Rightarrow> 'a * 'b" where
"argmin2 f [a] = (a, f a)" |
"argmin2 f (a#as) = (let fa = f a; (am,m) = argmin2 f as in if fa \<le> m then (a, fa) else (am,m))"


subsection "Edit Distance"

datatype 'a ed = Copy | Repl 'a | Ins 'a | Del

fun edit :: "'a ed list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"edit (Copy # es) (x # xs) = x # edit es xs" |
"edit (Repl a # es) (x # xs) = a # edit es xs" |
"edit (Ins a # es) xs = a # edit es xs" |
"edit (Del # es) (x # xs) = edit es xs" |
"edit (Copy # es) [] = edit es []" |
"edit (Repl a # es) [] = edit es []" |
"edit (Del # es) [] = edit es []" |
"edit [] xs = xs"

abbreviation cost where
"cost es \<equiv> length [e <- es. e \<noteq> Copy]"


subsection "Minimum edit sequence"

fun min_eds :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a ed list" where
"min_eds [] [] = []" |
"min_eds [] (y#ys) = Ins y # min_eds [] ys" |
"min_eds (x#xs) [] = Del # min_eds xs []" |
"min_eds (x#xs) (y#ys) =
  argmin cost [Ins y # min_eds (x#xs) ys, Del # min_eds xs (y#ys),
     (if x=y then Copy else Repl y) # min_eds xs ys]"

lemma "min_eds ''vintner'' ''writers'' =
  [Ins CHR ''w'', Repl CHR ''r'', Copy, Del, Copy, Del, Copy, Copy, Ins CHR ''s'']"
by eval
(*
value "min_eds ''madagascar'' ''bananas''"

value "min_eds ''madagascaram'' ''banananas''"
*)
lemma min_eds_correct: "edit (min_eds xs ys) xs = ys"
by (induction xs ys rule: min_eds.induct) auto

lemma min_eds_same: "min_eds xs xs = replicate (length xs) Copy"
by (induction xs) auto

lemma min_eds_eq_Nil_iff: "min_eds xs ys = [] \<longleftrightarrow> xs = [] \<and> ys = []"
by (induction xs ys rule: min_eds.induct) auto

lemma min_eds_Nil: "min_eds [] ys = map Ins ys"
by (induction ys) auto

lemma min_eds_Nil2: "min_eds xs [] = replicate (length xs) Del"
by (induction xs) auto

lemma if_edit_Nil2: "edit es ([]::'a list) = ys \<Longrightarrow> length ys \<le> cost es"
apply(induction es "[]::'a list" arbitrary: ys rule: edit.induct)
apply auto
 apply fastforce
apply fastforce
done

lemma if_edit_eq_Nil: "edit es xs = [] \<Longrightarrow> length xs \<le> cost es"
by (induction es xs rule: edit.induct) auto

lemma min_eds_minimal: "edit es xs = ys \<Longrightarrow> cost(min_eds xs ys) \<le> cost es"
proof(induction xs ys arbitrary: es rule: min_eds.induct)
  case 1 thus ?case by simp
next
  case 2 thus ?case by (auto simp add: min_eds_Nil dest: if_edit_Nil2)
next
  case 3
  thus ?case by(auto simp add: min_eds_Nil2 dest: if_edit_eq_Nil)
next
  case 4
  show ?case
  proof (cases "es")
    case Nil then show ?thesis using "4.prems" by (auto simp: min_eds_same)
  next
    case [simp]: (Cons e es')
    show ?thesis
    proof (cases e)
      case Copy
      thus ?thesis using "4.prems" "4.IH"(3)[of es'] by simp
    next
      case (Repl a)
      thus ?thesis using "4.prems" "4.IH"(3)[of es']
        using [[simp_depth_limit=1]] by simp
    next
      case (Ins a)
      thus ?thesis using "4.prems" "4.IH"(1)[of es']
        using [[simp_depth_limit=1]] by auto
    next
      case Del
      thus ?thesis using "4.prems" "4.IH"(2)[of es']
        using [[simp_depth_limit=1]] by auto
    qed
  qed
qed


subsection "Minimum edit distance"

fun min_ed :: "'a list \<Rightarrow> 'a list \<Rightarrow> nat" where
"min_ed [] [] = 0" |
"min_ed [] (y#ys) = 1 + min_ed [] ys" |
"min_ed (x#xs) [] = 1 + min_ed xs []" |
"min_ed (x#xs) (y#ys) =
  Min {1 + min_ed (x#xs) ys, 1 + min_ed xs (y#ys), (if x=y then 0 else 1) + min_ed xs ys}"

lemma min_ed_min_eds: "min_ed xs ys = cost(min_eds xs ys)"
apply(induction xs ys rule: min_ed.induct)
apply (auto split!: if_splits)
done

lemma "min_ed ''madagascar'' ''bananas'' = 6"
by eval
(*
value "min_ed ''madagascaram'' ''banananas''"
*)

subsubsection "Exercise: Optimization of the Copy case"

fun min_eds2 :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a ed list" where
"min_eds2 [] [] = []" |
"min_eds2 [] (y#ys) = Ins y # min_eds2 [] ys" |
"min_eds2 (x#xs) [] = Del # min_eds2 xs []" |
"min_eds2 (x#xs) (y#ys) =
  (if x=y then Copy # min_eds2 xs ys
   else argmin cost
     [Ins y # min_eds2 (x#xs) ys, Del # min_eds2 xs (y#ys), Repl y # min_eds2 xs ys])"

value "min_eds2 ''madagascar'' ''bananas''"

lemma cost_Copy_Del: "cost(min_eds xs ys) \<le> cost (min_eds xs (x#ys)) + 1"
apply(induction xs ys rule: min_eds.induct)
apply(auto simp del: filter_True filter_False split!: if_splits)
done

lemma cost_Copy_Ins: "cost(min_eds xs ys) \<le> cost (min_eds (x#xs) ys) + 1"
apply(induction xs ys rule: min_eds.induct)
apply(auto simp del: filter_True filter_False split!: if_splits)
done

lemma "cost(min_eds2 xs ys) = cost(min_eds xs ys)"
proof(induction xs ys rule: min_eds2.induct)
  case (4 x xs y ys) thus ?case
    apply (auto split!: if_split)
      apply (metis (mono_tags, lifting) Suc_eq_plus1 Suc_leI cost_Copy_Del cost_Copy_Ins le_imp_less_Suc le_neq_implies_less not_less)
     apply (metis Suc_eq_plus1 cost_Copy_Del le_antisym)
    by (metis Suc_eq_plus1 cost_Copy_Ins le_antisym)
qed simp_all

lemma "min_eds2 xs ys = min_eds xs ys"
oops
(* Not proveable because Copy comes last in min_eds but first in min_eds2.
   Can reorder, but the proof still requires the same two lemmas cost_*_* above.
*)


subsection "Indexing"

subsubsection "Indexing lists"

context
fixes xs ys :: "'a list"
fixes m n :: nat
begin

function (sequential)
  min_ed_ix' :: "nat * nat \<Rightarrow> nat" where
"min_ed_ix' (i,j) =
  (if i \<ge> m then
     if j \<ge> n then 0 else 1 + min_ed_ix' (i,j+1) else
   if j \<ge> n then 1 + min_ed_ix' (i+1, j)
   else
   Min {1 + min_ed_ix' (i,j+1), 1 + min_ed_ix' (i+1, j),
       (if xs!i = ys!j then 0 else 1) + min_ed_ix' (i+1,j+1)})"
by pat_completeness auto
termination by(relation "measure(\<lambda>(i,j). (m - i) + (n - j))") auto


declare min_ed_ix'.simps[simp del]

end

lemma min_ed_ix'_min_ed:
  "min_ed_ix' xs ys (length xs) (length ys) (i, j) = min_ed (drop i xs) (drop j ys)"
apply(induction "(i,j)" arbitrary: i j rule: min_ed_ix'.induct[of "length xs" "length ys"])
apply(subst min_ed_ix'.simps)
apply(simp add: Cons_nth_drop_Suc[symmetric])
done


subsubsection "Indexing functions"

context
fixes xs ys :: "nat \<Rightarrow> 'a"
fixes m n :: nat
begin

function (sequential)
  min_ed_ix :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"min_ed_ix i j =
  (if i \<ge> m then
     if j \<ge> n then 0 else n-j else
   if j \<ge> n then m-i
   else
   min_list [1 + min_ed_ix i (j+1), 1 + min_ed_ix (i+1) j,
       (if xs i = ys j then 0 else 1) + min_ed_ix (i+1) (j+1)])"
by pat_completeness auto
termination by(relation "measure(\<lambda>(i,j). (m - i) + (n - j))") auto


text \<open>Functional Memoization\<close>

dp_fun min_ed_ix\<^sub>m: min_ed_ix memoizes dp_consistency_rbt monadifies (state) min_ed_ix.simps
thm min_ed_ix\<^sub>m'.simps

dp_correctness
  by dp_match
print_theorems

lemmas [code] = min_ed_ix\<^sub>m.memoized_correct

declare min_ed_ix.simps[simp del]


text \<open>Imperative Memoization\<close>

dp_fun min_ed_ix\<^sub>h: min_ed_ix
  memoizes (standard auto) dp_consistency_reader_array_pair'
  where bound = "Bound 0 (n+1)"
    and key1="fst :: nat \<times> nat \<Rightarrow> nat" and key2="snd :: nat \<times> nat \<Rightarrow> nat"
    and k1="m :: nat" and k2="m + 1 :: nat"
monadifies (heap_reader) min_ed_ix.simps

dp_correctness
  by dp_match

lemmas memoized_empty = min_ed_ix\<^sub>h.memoized_correct

definition "min_ed_ix_impl l r = min_ed_ix\<^sub>h.inited (\<lambda>(l, r). min_ed_ix\<^sub>h' l r) (l, r)"
lemmas min_ed_ix_impl_def_alt = min_ed_ix_impl_def[unfolded prod.case]

end

abbreviation "slice xs i j \<equiv> map xs [i..<j]"

lemma min_ed_Nil1: "min_ed [] ys = length ys"
by (induction ys) auto

lemma min_ed_Nil2: "min_ed xs [] = length xs"
by (induction xs) auto

(* prove correctness of min_ed_ix directly ? *)
lemma min_ed_ix_min_ed: "min_ed_ix xs ys m n i j = min_ed (slice xs i m) (slice ys j n)"
apply(induction i j rule: min_ed_ix.induct[of m n])
apply(simp add: min_ed_ix.simps upt_conv_Cons min_ed_Nil1 min_ed_Nil2 Suc_diff_Suc)
done


text \<open>Functional Test Cases\<close>

definition "min_ed_list xs ys = min_ed_ix (\<lambda>i. xs!i) (\<lambda>i. ys!i) (length xs) (length ys) 0 0"
lemma "min_ed_list ''madagascar'' ''bananas'' = 6"
by eval

definition "min_ed_ia xs ys = (let a = IArray xs; b = IArray ys
  in min_ed_ix (\<lambda>i. a!!i) (\<lambda>i. b!!i) (length xs) (length ys) 0 0)"

lemma "min_ed_ia ''madagascar'' ''bananas'' = 6"
by eval



text \<open>Extracting an Executable Constant for the Imperative Implementation\<close>

text \<open>Imperative Test Case\<close>

definition
  "min_ed_ia\<^sub>h xs ys = (let a = IArray xs; b = IArray ys
  in min_ed_ix_impl (\<lambda>i. a!!i) (\<lambda>i. b!!i) (length xs) (length ys) 0 0)"

definition
  "test_case = min_ed_ia\<^sub>h ''madagascar'' ''bananas''"

export_code min_ed_ix in SML module_name Test

code_reflect Test functions test_case

ML \<open>Test.test_case ()\<close>

end
