(*  Title:      HOL/Library/LaTeXsugar.thy
    Author:     Gerwin Klein, Tobias Nipkow, Norbert Schirmer
    Copyright   2005 NICTA and TUM
*)

(*<*)
theory LaTeXsugar
imports Main
begin

notation (Rule output)
  Pure.imp  ("\<^latex>\<open>\\mbox{}\\inferrule{\\mbox{\<close>_\<^latex>\<open>}}\<close>\<^latex>\<open>{\\mbox{\<close>_\<^latex>\<open>}}\<close>")

syntax (Rule output)
  "_bigimpl" :: "asms \<Rightarrow> prop \<Rightarrow> prop"
  ("\<^latex>\<open>\\mbox{}\\inferrule{\<close>_\<^latex>\<open>}\<close>\<^latex>\<open>{\\mbox{\<close>_\<^latex>\<open>}}\<close>")

  "_asms" :: "prop \<Rightarrow> asms \<Rightarrow> asms" 
  ("\<^latex>\<open>\\mbox{\<close>_\<^latex>\<open>}\\\\\<close>/ _")

  "_asm" :: "prop \<Rightarrow> asms" ("\<^latex>\<open>\\mbox{\<close>_\<^latex>\<open>}\<close>")

(*
syntax
  "fun2" :: "type \<Rightarrow> type \<Rightarrow> type" (infixr "\<rightarrow>" 0)
translations
  ("type") "'x \<rightarrow> 'y" <= ("type") "'x \<Rightarrow> 'y"

translations
  ("prop") "P \<and> Q \<Longrightarrow> R" <= ("prop") "P \<Longrightarrow> Q \<Longrightarrow> R"

notation (latex output)
  Pure.imp  (infixr "\<longrightarrow>" 4)

translations
  "x = y" <= ("prop") "x == y"

syntax (latex output)
  "_andL" :: "bool \<Rightarrow> bool \<Rightarrow> bool" (infixl "\<and>" 35)
translations
  "_andL x y" <= "x \<and> y"
  "_andL (_andL x y) z" <= "_andL x (_andL y z)"

syntax (latex output)
  "_lele" :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("(_ \<le>/ _/ \<le> _)" [51, 51, 51] 50)
print_translation \<open>
  [(@{const_syntax HOL.conj}, fn _ =>
      fn [Const(@{const_syntax "less_eq"}, _) $ x $ y,
          Const(@{const_syntax "less_eq"}, _) $ y' $ z] =>
      if y = y' then Syntax.const @{syntax_const "_lele"} $ x $ y $ z
      else raise Match)]
\<close>
*)
notation (latex output)
  If  ("(\<^latex>\<open>{\\sf \<close>if\<^latex>\<open>}\<close> (_)/ \<^latex>\<open>{\\sf \<close>then\<^latex>\<open>}\<close> (_)/ \<^latex>\<open>{\\sf \<close>else\<^latex>\<open>}\<close> (_))" [0, 0, 10] 10)

syntax (latex output)

  "_Let"        :: "[letbinds, 'a] => 'a"
  ("(\<^latex>\<open>{\\sf \<close>let\<^latex>\<open>}\<close> (_)/ \<^latex>\<open>{\\sf \<close>in\<^latex>\<open>}\<close> (_))" 10)

  "_binds"      :: "[letbind, letbinds] \<Rightarrow> letbinds"     ("_;//_")

  "_case_syntax":: "['a, cases_syn] => 'b"
  ("(\<^latex>\<open>{\\sf \<close>case\<^latex>\<open>}\<close> _ \<^latex>\<open>{\\sf \<close>of\<^latex>\<open>}\<close>//  _)" 10)

  "_case2" :: "[case_syn, cases_syn] \<Rightarrow> cases_syn"  ("_//| _")
(*
translations 
  "n + 1" <= "CONST Suc n"
*)

translations 
  "1" <= "CONST Suc 0"

notation (nospace output) plus ("_+_" [65,66] 65)
notation (nospace output) minus ("_-_" [65,66] 65)
(*
notation (latex output)
  length ("|_|")

(* SETS *)

(* empty set *)
notation (latex)
  "Set.empty" ("\<emptyset>")

(* insert *)
translations 
  "{x} \<union> A" <= "CONST insert x A"
  "{x,y}" <= "{x} \<union> {y}"
  "{x,y} \<union> A" <= "{x} \<union> ({y} \<union> A)"
  "{x}" <= "{x} \<union> \<emptyset>"
*)
(* set comprehension *)
syntax (latex output)
  "_Collect" :: "pttrn => bool => 'a set"              ("(1{_ | _})")
  "_CollectIn" :: "pttrn => 'a set => bool => 'a set"   ("(1{_ \<in> _ | _})")
translations
  "_Collect p P"      <= "{p. P}"
  "_Collect p P"      <= "{p|xs. P}"
  "_CollectIn p A P"  <= "{p : A. P}"

(* LISTS *)

(* Cons *)
notation (latex output)
  Cons  ("_ \<cdot>/ _" [66,65] 65)

(* DUMMY *)
consts DUMMY :: 'a ("\<^latex>\<open>\\_\<close>")

definition "hideid x = x"
translations "x" <= "CONST hideid x"

setup{*
let
  fun dummy_pats (wrap $ (eq $ lhs $ rhs)) =
    let
      val rhs_vars = Term.add_vars rhs [];
      fun dummy (v as Var (ixn as (_, T))) =
            if member (op = ) rhs_vars ixn then v else Var (("_",0), T)
        | dummy (t $ u) = dummy t $ dummy u
        | dummy (Abs (n, T, b)) = Abs (n, T, dummy b)
        | dummy t = t;
    in wrap $ (eq $ dummy lhs $ rhs) end
in
  Term_Style.setup @{binding dummy_pats} (Scan.succeed (K dummy_pats))
end
*}

setup\<open>
  let
    fun pretty ctxt c =
      let val tc = Proof_Context.read_const {proper = true, strict = false} ctxt c
      in Pretty.block [Thy_Output.pretty_term ctxt tc, Pretty.str " ::",
            Pretty.brk 1, Syntax.pretty_typ ctxt (fastype_of tc)]
      end
  in
    Thy_Output.antiquotation @{binding "const_typ"}
     (Scan.lift Args.embedded_inner_syntax)
       (fn {source = src, context = ctxt, ...} => fn arg =>
          Thy_Output.output ctxt
            (Thy_Output.maybe_pretty_source pretty ctxt src [arg]))
  end;
\<close>

end
(*>*)
