theory Locale_Tests
  imports "../state_monad/State_Monad_Ext" "../Index1"
begin

locale L = fixes x begin lemma t: \<open>x=x\<close> .. end

interpretation a: L 0 .
thm a.t (* 0 = 0 *)

interpretation b: L 0 .
thm b.t (* DOES NOT EXIST *)

interpretation c: L 1 .
thm c.t (* 1 = 1 *)

interpretation d: L x for x .
thm d.t (* ?x = ?x *)

interpretation e: L 1 .
thm e.t (* DOES NOT EXIST *)

end