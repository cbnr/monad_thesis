theory Scratch
  imports Main
    "Lazy_Sequence"
begin

find_consts name: Lazy_Sequence "'a lazy_sequence"

term "Lazy_Sequence.iterate_upto id (0::int)"

term list_of_lazy_sequence

value "list_of_lazy_sequence (Lazy_Sequence.empty::int lazy_sequence)"
value "(Lazy_Sequence.empty::int lazy_sequence)"
end