theory Test_Heap
  imports
    "HOL-Library.Extended"
    "../transform/Transform_Cmd2"
    "../heap_monad/Heap_Main"
    "../state_monad/State_Main"
begin

instance extended :: (heap) heap
proof
  obtain to_nat :: "'a \<Rightarrow> nat" where inj0: "inj to_nat"
    by auto

  define to_nat' :: "'a extended \<Rightarrow> nat" where
    "to_nat' e \<equiv> case e of
      \<infinity> \<Rightarrow> 0
    | -\<infinity> \<Rightarrow> 1
    | Fin x \<Rightarrow> to_nat x+2" for e

  have "to_nat' x = to_nat' y \<Longrightarrow> x = y" for x y
    apply (cases y)
    using inj0 by (auto simp: to_nat'_def split: extended.splits dest: injD)
  hence *: "inj to_nat'"
    by (auto intro: injI)
  thus "\<exists>to_nat::('a extended \<Rightarrow> nat). inj to_nat"
    by auto
qed


context
  fixes n :: nat and W :: "nat \<Rightarrow> nat \<Rightarrow> int"
begin

context
  fixes t :: nat -- "Final node"
begin

fun bf :: "nat \<Rightarrow> nat \<Rightarrow> int extended" where
  "bf 0 j = (if t = j then Fin 0 else \<infinity>)"
| "bf (Suc k) j = fold min [Fin (W j i) + bf k i. i \<leftarrow> [0..<Suc n]] (bf k j)"

dp_fun bf\<^sub>T: bf memoizes dp_consistency_heap_default where bound = "(n+1, n+1)" and mem="undefined::int extended option array"
  monadifies (heap) bf.simps

context includes heap_monad_syntax begin
thm bf\<^sub>T'.simps
end

dp_correctness
  by dp_match

dp_fun bf1\<^sub>T: bf memoizes dp_consistency_rbt
  monadifies (state) bf.simps

end
end
