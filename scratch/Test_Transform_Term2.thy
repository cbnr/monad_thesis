theory Test_Transform_Term2
  imports Main "../DP_CRelVS_Ext"
begin
notation fun_app_lifted (infixl "." 999)
notation Transfer.Rel ("Rel")

ML_file \<open>../transform/Transform_Misc.ML\<close>
ML_file \<open>../transform/Transform_Const.ML\<close>
ML_file \<open>../transform/Transform_Term2.ML\<close>

ML \<open>
val cx = @{context};
fun prt tm = tm |> Syntax.pretty_term cx |> Pretty.writeln;
fun tr tm = tm |> Transform_Term.lift_term cx [];
fun trp tm = tm |> tr |> Syntax.check_term cx |> prt
fun trm tm = tm |> Transform_Term.monadify cx |> prt
\<close>

ML_command \<open>
val tm = @{term Nil};
val ctxt = @{context};

tm |> Transform_Term.lift_term ctxt [] |> Syntax.pretty_term ctxt |> Pretty.writeln;
@{term "a+b"} |> Transform_Term.lift_term ctxt [] |> Syntax.pretty_term ctxt |> Pretty.writeln;
\<close>

ML_command \<open>@{term "id 3"} |> trp\<close>
ML_command \<open>@{term "id id 3"} |> trp\<close>
ML_command \<open>@{term "id id id id 3"} |> trp\<close>
term case_guard

ML_command \<open>@{term "case x of (a, b) \<Rightarrow> (Suc a + b)"} |> trp\<close>
ML_command \<open>@{term "case x of Nil \<Rightarrow> 0 | (x#xs) \<Rightarrow> x+1"} |> trp\<close>
ML_command \<open>@{term "if x>0 then x+1 else x"} |> trp\<close>
ML_command \<open>@{term "\<lambda>x. x"} |> trp\<close>
ML_command \<open>@{term "\<lambda>x. x + x"} |> trp\<close>
ML_command \<open>@{term "\<lambda>x. case x of (a, b) \<Rightarrow> (Suc a + b)"} |> trp\<close>

ML_command \<open>@{term "id 3"} |> trm\<close>
ML_command \<open>@{term "id id 3"} |> trm\<close>
ML_command \<open>@{term "id id id id id id 3"} |> trm\<close>
ML_command \<open>@{term "case x of (a, b) \<Rightarrow> (Suc a + b)"} |> trm\<close>
ML_command \<open>@{term "case x of Nil \<Rightarrow> 0 | (x#xs) \<Rightarrow> x+1"} |> trm\<close>
ML_command \<open>@{term "if x>0 then x+1 else x"} |> trm\<close>
ML_command \<open>@{term "\<lambda>x. x"} |> trm\<close>
ML_command \<open>@{term "\<lambda>x. x + x"} |> trm\<close>
ML_command \<open>@{term "\<lambda>x. case x of (a, b) \<Rightarrow> (Suc a + b)"} |> trm\<close>
ML_command \<open>@{term "\<lambda>f x. id f x"} |> trp\<close>

context dp_consistency begin
context includes lifting_syntax begin
context
  fixes xx :: nat
  fixes yy :: "'a \<Rightarrow> 'b \<Rightarrow> 'a"
begin
ML_val \<open>Proof_Context.read_const {proper=true, strict=true} @{context} "map"\<close>
ML_val \<open>Proof_Context.read_term_pattern @{context} "Nil"\<close>

lemma "yy = yy" ..
lemma "crel_vs (op= ===>\<^sub>T op= ===>\<^sub>T op=) yy (#%a. (#%b. (#yy a b#) #) #)"
  by transfer_prover

lemma "crel_vs op= (yy a b) ((#%a. (#%b. (#yy a b#) #) #) . (#a#) . (#b#))"
  by transfer_prover
end (*context*)
ML_val \<open>head_of; chop; Term.strip_abs_eta; op o; absdummy; list_comb; map_range\<close>
ML_val \<open>rev (map_range Bound 5)\<close>
ML \<open>
open Transform_Term

\<close>
thm list.map[no_vars]

ML_command \<open>
val ctxt = @{context}
val equ = @{term "f (x#xs) = x + f xs"}
val equ = @{term "Map f [] = []"}
val equ = @{term "Map f (x21 # x22) = f x21 # Map f x22"}
val (lhs, rhs) = equ |> HOLogic.dest_eq;

lift_equation ctxt (lhs, rhs) |> prt
\<close>
thm list.map
ML_val \<open>@{term "a.b"}\<close>
ML \<open>
fun add_function bind defs =
  let
    val fixes = [(bind, NONE, NoSyn)];
    val specs = map (fn def => (((Binding.empty, []), def), [], [])) defs
    val pat_completeness_auto = fn ctxt =>
      Pat_Completeness.pat_completeness_tac ctxt 1
      THEN auto_tac ctxt
  in
    Function.add_function fixes specs Function_Fun.fun_config pat_completeness_auto
  end
\<close>

ML \<open>
val webind = Binding.qualify true "map.@ @.haha" @{binding "e"};
val wedefs = [
  (Free ("e", @{typ "nat\<Rightarrow>int"}) $ Free ("x", @{typ nat}),
  @{term "0::int"}) |> HOLogic.mk_eq |> HOLogic.mk_Trueprop]
\<close>
ML_val \<open>
val x = @{binding "x"}
val ba  = @{binding "a.x"}
val bb = Binding.qualify true "a" x;
val bc = Binding.qualify true "a" x;
bb=bc
\<close>
term upto
thm upto.simps
term upto_graph
declare [[function_internals]]
fun fff and g where
"fff x = g x"
| "g x = 0"
thm fff_g.induct
term fff_g_graph
term fff_g_sumC
local_setup \<open>
  Local_Theory.define ((@{binding ta}, NoSyn), ((@{binding tb}, []), @{term "3+Suc 4"})) #> snd
\<close>
term fff_g_graph
ML_val {*Thm.make_def_binding; Local_Theory.define; op ||>; op |>>*}
ML_val \<open>Local_Theory.define_internal; type a = Proof.context\<close>
ML_val \<open>Proof_Context.full_name @{context} @{binding map}\<close>
term w.e
local_setup \<open>
add_function webind wedefs #> snd
\<close>
print_theorems
ML_val \<open>
Local_Theory.abbrev
\<close>

term "map.@@.haha.e"
thm map.haha.e.pelims

ML_val \<open>@{const map.haha.e}\<close>
term Mapping.update
thm update_def
thm RBT_Impl.fold_def
term RBT_Impl.fold
thm List.fold_def
(*
dp_monadify map (list.map)
  mapT' .. = ...
  mapT' .. = ...

dp_monadify fold (List.fold_def / fold.simps)
dp_monadify rbt_fold = RBT_Impl.fold (RBT_Impl.fold_def)
dp_monadify update1 = Mapping.update (update_def)
dp_monadify Cons (simps and termination retrieved by Function_Common.get_info)

crel_vs ... 
*)
ML_val {*Transfer.transfer_rule_of_lhs @{context} @{term "map"}*}
ML_val \<open>Syntax.read_terms\<close>

schematic_goal "crel_vs (?R ===>\<^sub>T ?R) (\<lambda>x. x) \<langle>\<lambda>x. \<langle>x\<rangle>\<rangle>"
  apply (rule transfer_raw)
  ML_val \<open>Proof.theorem; Method.text; `\<close>
  

ML_val \<open>map_types; HOLogic.mk_eq; HOLogic.mk_Trueprop\<close>
ML_val \<open>Term.add_frees; map_types; Transform_Term.lift_type @{context}\<close>
end
