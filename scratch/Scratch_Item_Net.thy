theory Scratch_Item_Net
  imports Main
begin

lemma True
  by transfer simp

ML \<open>
structure Data = Generic_Data (
  type T = {
    monadified_terms: (term * term) Item_Net.T
  }

  val empty = {
    monadified_terms = Item_Net.init (op aconv o apply2 snd) (single o fst)
  }

  val extend = I

  fun merge (
    {monadified_terms = m0},
    {monadified_terms = m1}
  ) = {
    monadified_terms = Item_Net.merge (m0, m1)
  }
)
\<close>

ML_val \<open>
(Data.get (Context.Proof @{context}) |> #monadified_terms |> Item_Net.retrieve) @{term a} 
\<close>