theory Scratch_Monadify_Defs
  imports "../DP_Lifting"
begin

term "(#3#)"
ML_val \<open>Local_Defs.unfold\<close>
ML_val \<open>Conv.no_conv; Thm.reflexive @{cterm 3}\<close>
ML_val \<open>Term.type_of\<close>

fun f and g where
"f x = g x"
| "g 0  = 0"
| "g (Suc x) = f x"

ML \<open>val finfo = Function.get_info @{context} @{term f}\<close>
ML \<open>val ginfo = Function.get_info @{context} @{term g}\<close>
ML_val \<open>finfo |> #fnames; finfo |> #defname; finfo |> #fs\<close>
term g
ML_val \<open>Parse_Spec.specification; Parse.vars; Parse.binding; type x = Binding.binding\<close>
ML_val \<open>finfo |> #fs |> hd |> Term.term_name\<close>
ML_val \<open>Parse.position; Scan.succeed; Token.pos_of; Scan.ahead Parse.not_eof\<close>
ML_val \<open>Parse.position (Scan.succeed ()); Binding.set_pos\<close>
ML_val \<open>Variable.polymorphic @{context} [@{term Cons}]\<close>
ML_val \<open>@{typ _} |> dest_Type\<close>
ML_val \<open>@{typ "(_, _) state"}; dummyT; domain_type\<close>
ML_val \<open>val ctxt = @{context}\<close>
ML \<open>
fun mk_stateT tp = Type (@{type_name state}, [dummyT, tp])
fun is_ctr_sugar ctxt tp_name = is_some (Ctr_Sugar.ctr_sugar_of ctxt tp_name)
\<close>
ML \<open>
fun
  lift_type ctxt tp = mk_stateT (lift_type' ctxt tp)
and
  lift_type' ctxt (tp as Type (@{type_name fun}, _))
    = lift_type' ctxt (domain_type tp) --> lift_type ctxt (range_type tp)
| lift_type' ctxt (tp as Type (tp_name, tp_args))
    = if is_ctr_sugar ctxt tp_name then Type (tp_name, map (lift_type' ctxt) tp_args)
      else if null tp_args then tp (* int, nat, \<dots> *)
      else raise TYPE("not a ctr_sugar", [tp], [])
| lift_type' _ tp = tp
\<close>

ML_val \<open>is_some; BNF_Def.bnf_of; null\<close>
ML_val \<open>lift_type @{context} @{typ int}\<close>
ML_val \<open>lift_type @{context} @{typ "'a => 'b => 'c"}\<close>
ML_val \<open>lift_type @{context} (type_of @{term map})\<close>
ML_val \<open>lift_type @{context} (@{typ "('a \<Rightarrow> 'b) list"})\<close>
term return
ML_val \<open>betapply (@{term "op+"}, @{term "[]"}) |> fastype_of\<close>
term numeral
ML_file \<open>../transform/Transform_Misc.ML\<close>
ML_file \<open>../transform/Transform_Const.ML\<close>

ML \<open>
fun
  is_state (Type (@{type_name state}, _)) = true
| is_state _ = false
\<close>

context fixes x :: nat begin
ML_val \<open>binder_types (type_of @{term "op+"})\<close>
end
ML \<open>
(* ('a => 'a) list \<longrightarrow> false *)
fun is_1st_type ctxt tp =
  let val types = body_type tp :: binder_types tp
  in types = map (lift_type' ctxt) types
  end
\<close>


ML_val \<open>is_1st_type @{context} @{typ "nat => nat => nat"}\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term plus})\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term map})\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term fold})\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term Cons})\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term "id::(int\<Rightarrow>int)\<Rightarrow>int\<Rightarrow>int"})\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term comp})\<close>
ML_val \<open>is_1st_type @{context} (type_of @{term "[id]"})\<close>
ML_val \<open>@{term "[id]"}\<close>

ML \<open>
fun
  is_1st_const ctxt (Const (name, _)) =
    let
      val tm = Proof_Context.read_const {proper=true, strict=true} ctxt name
      val (_, tp) = dest_Const tm
    in
      is_1st_type ctxt tp
    end
| is_1st_const _ tm = raise TERM("not a const", [tm])
\<close>
ML_val \<open>Type_Infer.paramify_vars @{typ "'a=>'a"}\<close>

ML_val \<open>is_1st_const @{context} @{term "id::('a\<Rightarrow>'a) \<Rightarrow> ('a\<Rightarrow>'a)"}\<close>
ML_val \<open>@{const_name "id"}\<close>
ML_val \<open>strip_type; fastype_of; Term.strip_abs_eta\<close>
ML \<open>
fun
  lift_1st_const' ctxt tm =
    let
      val (name, tp) = dest_Const tm (* "id", (nat\<Rightarrow>int) \<Rightarrow> (nat\<Rightarrow>int)*)
      val orig_tm = Proof_Context.read_const {proper=true, strict=true} ctxt name
      val (arg_typs, body_typ) = strip_type tp (* [nat\<Rightarrow>int, nat], int *)
      val arg_typs' = map (lift_type' ctxt) arg_typs (* [nat => <int>, nat] *)
      val body_typ' = lift_type' ctxt body_typ (* int *)
      val tm' = Const (name, arg_typs' ---> body_typ') (* " *)
      val args' = map_range Bound (length arg_typs') |> rev
      val body' = list_comb (tm', args')
      fun wrap (tp, tm) = absdummy tp (Transform_Const.return tm)
      val result = Library.foldr wrap (arg_typs', body')
    in
      result
    end
\<close>

ML \<open>
fun type_nargs tp = tp |> strip_type |> fst |> length
fun term_nargs tm = type_nargs (type_of tm)
\<close>

ML \<open>
fun lift_1st_const' ctxt tm =
  let
    val (name, tp) = dest_Const tm
    val (arg_typs, body_typ) = strip_type tp

    val n_args = Proof_Context.read_const {proper=true, strict=true} ctxt name |> term_nargs

    val (arg_typs, body_arg_typs) = chop n_args arg_typs
    val arg_typs' = map (lift_type' ctxt) arg_typs
    val body_typ' = lift_type' ctxt (body_arg_typs ---> body_typ)

    val tm' = Const (name, arg_typs ---> body_typ') (* " *)
    val args' = map_range Bound n_args |> rev
    val body' = list_comb (tm', args')
    fun wrap (tp, tm) = absdummy tp (Transform_Const.return tm)
    val result = Library.foldr wrap (arg_typs', body')
  in
    result
  end
\<close>

context notes [[show_types]] begin
ML_command \<open>lift_1st_const' @{context} @{term id} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term plus} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "id::(nat\<Rightarrow>int)\<Rightarrow>nat\<Rightarrow>int"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "Cons"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "Cons::(nat\<Rightarrow>int)\<Rightarrow>_"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "Some"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>

end

ML_val \<open>Term.strip_comb\<close>

ML_val \<open>chop\<close>
ML_val \<open>map_range Bound 5 |> rev;Library.foldr\<close>
ML \<open>list_comb; op --->; absdummy\<close>
ML_val \<open>Option.map\<close>
thm list.rec
ML_val \<open>Function_Context_Tree.get_function_congs @{context} |> drop 30 |> take 10\<close>
ML \<open>
fun fixed_args head_n_args tm =
  let
    val (tm_head, tm_args) = strip_comb tm
    val n_tm_args = length tm_args
  in
    head_n_args tm_head
    |> Option.mapPartial (fn n_args =>
      if n_tm_args > n_args then NONE
      else if n_tm_args < n_args then raise TERM("need " ^ string_of_int n_args ^ " args", [tm])
      else SOME (tm_head, tm_args))
  end
\<close>

ML \<open>val case_terms = Ctr_Sugar.ctr_sugars_of @{context} |> map #casex\<close>
ML \<open>fun lookup_case_term tm = find_first (fn x => Term.aconv_untyped (x, tm)) case_terms\<close>
ML_val \<open>lookup_case_term @{term "case_prod ::_\<Rightarrow>(int\<times>nat) \<Rightarrow>_"}\<close>
ML_val \<open>lookup_case_term @{term case_nat}\<close>
ML_val \<open>Term.dest_abs; Term.strip_abs_eta; map_types; lift_type'; map2\<close>
ML_val \<open>Term.abs; lambda; Term.argument_type_of\<close>
ML \<open>
fun lift_abs' ctxt (name, typ) cont lift_dict body =
  let
    val free = Free (name, typ)
    val typ' = lift_type' ctxt typ
    val freeT' = Free (name, typ')
    val freeT = Transform_Const.return (freeT')
    val body' = (cont ((free,freeT)::lift_dict) body)
    val result = lambda freeT' body'
  in
    result
  end
\<close>

ML_val \<open>AList.lookup\<close>

ML_command \<open>lift_1st_const' @{context} @{term id} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term plus} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "id::(nat\<Rightarrow>int)\<Rightarrow>nat\<Rightarrow>int"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "Cons"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "Cons::(nat\<Rightarrow>int)\<Rightarrow>_"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_command \<open>lift_1st_const' @{context} @{term "Some"} |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>



ML_val \<open>
val ctxt = @{context};
(lift_abs' ctxt ("0", dummyT) o lift_abs' ctxt ("1", dummyT)) (K I);
Library.foldr (op o) (map (lift_abs' ctxt) [("0", dummyT)], I) (K I);
Library.foldr (fn (f, x) => f x) (map (lift_abs' ctxt) [("0", dummyT)], K I)
\<close>
ML_val \<open>nth_drop 3 [1,2,3,4,5]; List.last;\<close>
ML \<open>
fun check_case ctxt lift_dict check_cont tm =
  fixed_args (lookup_case_term #> Option.map (fn cs => term_nargs cs - 1)) tm
  |> Option.map (fn (head, args) =>
    let
      val (case_name, case_type) = lookup_case_term head |> the |> dest_Const
      val ((clause_typs, _), _) =
        strip_type case_type |>> split_last
      
      val clase_nparams = clause_typs |> map type_nargs
      (* ('a\<Rightarrow>'b) \<Rightarrow> ('a\<Rightarrow>'b) |> type_nargs = 1*)

      fun lift_clause n_param clause =
        let
          val (vars_name_typ, body) = Term.strip_abs_eta n_param clause
          val abs_lift_wraps = map (lift_abs' ctxt) vars_name_typ
          val lift_wrap = Library.foldr (op o) (abs_lift_wraps, I) check_cont        in
          lift_wrap lift_dict body
        end

      val head' = Const (case_name, dummyT) (* clauses are sufficient for type inference *)
      val clauses' = map2 lift_clause clase_nparams args
    in
      Transform_Const.return (list_comb (head', clauses'))
    end)
\<close>
declare [[ML_exception_trace]]
ML_command \<open>
val ctxt = @{context}
val (tmc $ tmx) = @{term "case xx of (a, b) \<Rightarrow> Suc a + b"};
tmc |> Syntax.pretty_term ctxt |> Pretty.writeln;

check_case ctxt [] (K (K @{term "(#\<lambda>a. (#\<lambda>b. (#a+b#)#)#) . (#a::nat#) . (#b::nat#)"})) tmc
|> the |> Syntax.pretty_term ctxt |> Pretty.writeln;
\<close>
ML_command \<open>
val ctxt = @{context}
val (tmc $ tmx) = @{term "case xx of (a::'a\<Rightarrow>'b, b::'a) \<Rightarrow> a b"};
tmc |> Syntax.pretty_term ctxt |> Pretty.writeln;
val tmr = @{term "(#a::'a\<Rightarrow>[_|'b]#) . (#b::'a#)"}|> subst_atomic_types [(@{typ 'c}, dummyT)];
check_case ctxt [] (K (K tmr)) tmc
|> the |> Syntax.pretty_term ctxt |> Pretty.writeln;
\<close>

ML_val \<open>op #>; Option.mapPartial; foldl1; split_list [(1,2),(3,4),(5,6)]\<close>
ML_val \<open>Free; list_comb; split_last; op --; op |>>\<close>
ML_val \<open>@{term 3}\<close>
term numeral
term "numeral (num.Bit0 (num.Bit1 (num.One)))"

ML_val \<open>
Proof_Context.read_const {proper=true, strict=true} @{context} @{const_name map}
\<close>

ML_val \<open>
@{apply 2};
@{map 2};
@{fold 2};
@{fold_map 2};
@{split_list 3};
\<close>


fun fd where
  "fd x = fold op+ (hd (map (map fd) [[0..<x]])) (Suc 0)"

value "fd 7"
