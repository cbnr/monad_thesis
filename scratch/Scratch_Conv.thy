theory Scratch_Conv
  imports Main
begin

lemma blah: "x \<equiv> id x" by simp
ML \<open>val blah_conv = Conv.rewr_conv @{thm blah}\<close>

ML \<open>
fun list_conv (fun_conv, arg_convs) =
  Library.foldl (uncurry Conv.combination_conv) (fun_conv, arg_convs)
\<close>


ML \<open>
fun conv_changed conv ctm =
  let val eq = conv ctm
  in if Thm.is_reflexive eq then Conv.no_conv ctm else eq end
\<close>

ML \<open>
fun repeat_unfold_conv thm lthy = Conv.repeat_conv (conv_changed (Conv.top_sweep_conv (K (Conv.rewr_conv thm)) lthy))
\<close>

ML \<open>
fun eta_conv1 ctxt =
  (Conv.abs_conv (K Conv.all_conv) ctxt)
  else_conv
  (Thm.eta_long_conversion then_conv Conv.abs_conv (K Thm.eta_conversion) ctxt)

fun eta_conv_n n =
  funpow n (fn conv => fn ctxt => eta_conv1 ctxt then_conv Conv.abs_conv (fn (_, ctxt) => conv ctxt) ctxt) (K Conv.all_conv)

\<close>

ML_val \<open>eta_conv_n 2 @{context} @{cterm "op+"} |> Thm.full_prop_of |> Logic.dest_equals |> snd\<close>

ML_val \<open>funpow\<close>
ML_val \<open>Conv.rewr_conv @{thm id_apply[symmetric]}\<close>
ML_val \<open>Conv.all_conv @{cterm "0"}\<close>
ML_val \<open>Conv.then_conv\<close>
ML_val \<open>Conv.else_conv\<close>
ML_val \<open>Thm.cterm_cache; Conv.cache_conv\<close>
ML_val \<open>Conv.abs_conv (fn (x,_) => (@{print} x; (fn tm => (@{print} tm; blah_conv tm)))) @{context} @{cterm "\<lambda>x f. f x"}\<close>
ML_val \<open>Conv.abs_conv (fn (x,_) => (@{print} x; (fn tm => (@{print} tm; Conv.rewr_conv @{thm blah} tm)))) @{context} @{cterm "id"}\<close>
ML_val \<open>Conv.combination_conv\<close>
ML_val \<open>Conv.fconv_rule;Thm.equal_elim\<close>
ML_val \<open>Thm.beta_conversion\<close>
ML_val \<open>Thm.eta_conversion @{cterm "\<lambda> x. f x"} |> Thm.full_prop_of\<close>
ML_val \<open>Thm.eta_conversion @{cterm "f"} |> Thm.full_prop_of\<close>
ML_val \<open>Thm.eta_long_conversion @{cterm "op+"} |> Thm.full_prop_of\<close>
ML_val \<open>Thm.eta_conversion @{cterm id} |> Thm.full_prop_of\<close>
ML_val \<open>@{term "\<lambda>f a b. f a b"}\<close>
ML_val \<open>Library.foldl; fold; replicate; split_list\<close>

ML_val \<open>list_conv (Conv.all_conv, [blah_conv]) @{cterm "id 3"}\<close>
ML_val \<open>list_conv (blah_conv, [blah_conv]) @{cterm "id 3"}\<close>
ML_val \<open>@{cterm "\<lambda>x. id id x"}\<close>
ML_val \<open>list_conv (Conv.all_conv, [blah_conv]) @{cterm "\<lambda>x. id id x"}\<close>
ML_val \<open>list_conv (Conv.all_conv, [blah_conv]) @{cterm "id id"}\<close>

ML \<open>
fun eta_conv1 ctxt =
  (Conv.abs_conv (K Conv.all_conv) ctxt)
  else_conv
  (Thm.eta_long_conversion then_conv Conv.abs_conv (K Thm.eta_conversion) ctxt)
\<close>
ML_val \<open>op |>>; op ||>; apply2; apfst\<close>

ML_val \<open>
val ctxt = @{context};
val ctm = @{cterm " id"};
eta_conv1 ctxt ctm |> Thm.full_prop_of;
blah_conv @{cterm "f a b c d"}
\<close>

ML_val \<open>blah_conv; val tconv = Conv.top_sweep_conv (K (Conv.binop_conv blah_conv)) @{context};
Conv.fconv_rule (Conv.implies_concl_conv tconv) @{thm arg_cong};
repeat_unfold_conv @{thm blah[symmetric]} @{context} @{cterm "(f (id (id 3)))"}
\<close>
ML_val \<open>
Thm.transitive ; Conv.top_sweep_conv; Conv.repeat_conv; Local_Defs.unfold
\<close>

term "\<lambda>a a a a a. a"
ML_val \<open>@{term "\<lambda>a a a a. a"} |> Syntax.check_term @{context}\<close>
ML_val \<open>Conv.abs_conv\<close>
end