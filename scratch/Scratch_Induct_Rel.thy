theory Scratch_Induct_Rel
  imports Main
begin

declare [[function_internals]]
fun (domintros) ff :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "ff [] [] = []"
| "ff (x#xs) [] = ff xs []"
| "ff [] (y#ys) = ff [] ys"
| "ff (x#xs) (y#ys) = ff xs ys @ y # concat (map (\<lambda>x. ff xs (x#ys)) xs)"
thm ff.induct[no_vars] list_all2_induct[no_vars] list.induct
lemma
  assumes "list_all2 P xs ys" "R [] []" "\<And>x xs y ys.
      \<lbrakk>P x y; list_all2 P xs ys; R xs ys\<rbrakk> \<Longrightarrow> R (x # xs) (y # ys)"
  shows "R xs ys" using list_all2_induct assms .
term ff_graph
thm ff_graph.intros
term ff_sumC
value "ff [Suc 0] [Suc 0]"
thm ff_sumC_def
thm ff.pinduct[rotated 1]
thm ff_def
thm ff_rel.induct
declare [[show_abbrevs]]
thm ff_rel.intros
thm ff.domintros[unfolded id_apply[of ff_rel, symmetric, THEN arg_cong, of Wellfounded.accp]]
find_theorems Wellfounded.acc Wellfounded.accp
thm accI accp.accI
ML_val \<open>SINGLE\<close>
term Wellfounded.accp
thm accp.induct
declare [[ML_print_depth=1000]]
ML_val \<open>
Inductive.the_inductive @{context} @{term ff_rel}
\<close>
thm ff.domintros[no_vars]
thm ff.pinduct[rotated 1, no_vars]

lemma
  fixes a :: "'a list \<times> 'a list"
  assumes
    "ff_dom a"
    "ff_dom ([]::'a list, []::'a list) \<Longrightarrow> P ([], [])"
     "\<And>(x::'a) (xs::'a list). ff_dom (x # xs, []) \<Longrightarrow> P (xs, []) \<Longrightarrow> P (x # xs, [])"
     "\<And>(y::'a) (ys::'a list). ff_dom ([], y # ys) \<Longrightarrow> P ([], ys) \<Longrightarrow> P ([], y # ys)"
     "\<And>(x::'a) xs (y::'a) ys. \<lbrakk>
        ff_dom (x # xs, y # ys);
        P (xs, ys);
        \<And>xa. xa \<in> set xs \<Longrightarrow> P (xs, xa # ys)
      \<rbrakk> \<Longrightarrow> P (x # xs, y # ys)"
  shows
    "P a"
  apply (rule accp_induct[OF assms(1), rule_format])
  subgoal premises prems for b  using prems ff_rel.cases
  (*
  using assms(1) apply (induction a rule: prod.induct)
  subgoal for a b
    apply (erule ff.pinduct[of a b])
    subgoal apply (rule assms(2)) .
    subgoal apply (rule assms(3)) .
    subgoal apply (rule assms(4)) .
    subgoal apply (rule assms(5)) .
    done
  done
*)
    value "(8::real) / 0"
    term inverse_divide
    oops
lemma
  assumes "P [] []" "\<And>x xs. P xs [] \<Longrightarrow> P (x # xs) []"
    "\<And>y ys. P [] ys \<Longrightarrow> P [] (y # ys)"
    "\<And>x xs y ys. P xs ys \<Longrightarrow> P (x # xs) (y # ys)"
  shows "P a0 a1"
  (*using ff.induct[OF assms] .*)
  apply (induction a1)
  subgoal
    apply (induction a0)
    subgoal using assms(1) .
    subgoal using assms(2) .
    done
  subgoal for x xs
    apply (induction a0 arbitrary: x xs)
    subgoal using assms(3) .
    subgoal  premises IH using IH assms(4)
      oops
lemma
  fixes xs ys us vs :: "'a list"
  assumes "list_all2 P xs ys" "list_all2 Q us vs"
    "R [] [] [] []"
    "\<And>x y xs ys. \<lbrakk>P x y; list_all2 P xs ys; R xs ys [] []\<rbrakk> \<Longrightarrow> R (x#xs) (y#ys) [] []"
    "\<And>u v us vs. \<lbrakk>Q u v; list_all2 Q us vs; R [] [] us vs\<rbrakk> \<Longrightarrow> R [] [] (u#us) (v#vs)"
    "\<And>x xs y ys u us v vs.
      \<lbrakk>P x y; Q u v; list_all2 P xs ys; list_all2 Q us vs;
      R xs ys us vs\<rbrakk> \<Longrightarrow> R (x#xs) (y#ys) (u#us) (v#vs)"
  shows "R xs ys us vs"
  using assms(1) apply (induction xs ys rule: list_all2_induct)
  subgoal
    using assms(2) apply (induction us vs rule: list_all2_induct)
    subgoal using assms(3) .
    subgoal using assms(5) .
    done
  subgoal for x xs y ys
    using assms(2) apply (induction us vs arbitrary: x xs y ys rule: list_all2_induct)
    subgoal using assms(4) .
    subgoal premises IH thm IH
      apply (rule assms(6))
      subgoal using IH(4) .
      subgoal using IH(1) .
      subgoal using IH(5) .
      subgoal using IH(2) .
      subgoal using IH
  
term 0 (*

schematic_goal "Transfer.Rel ?R map map\<^sub>T'"
  apply (rule Rel_abs)
  apply (rule Rel_abs)
  using list_all2_induct map\<^sub>T'.induct[where P="\<lambda>_ a. _ a"] list.induct
  using map\<^sub>T'.induct rel_optionI dp_consistency.map_transfer
  apply (induct rule: map\<^sub>T'.induct[where P="\<lambda>a b. Transfer.Rel _ (map a b) (map\<^sub>T' a b)"])
ML_val \<open>Binding.empty ; Variable.add_fixes; split_list; can\<close>
ML_val \<open>Binding.name "n" |> Binding.qualify true "q" |> Binding.suffix_name "_sf" |> Binding.name_of\<close>
ML_val \<open>op --;  op :::; @{keyword "("}; Library.get_first; Option.map; apply2\<close>
ML_val \<open>@{binding "\<Rightarrow>fs.e t3i%)*"}; type x = binding; Scan.optional\<close>
ML_val \<open>@{keyword "("}; Binding.make; Method.parse; Thm.make_def_binding; Thm.dest_comb; Term.dest_comb\<close>
ML_val \<open>Parse_Spec.name_facts; Parse.and_list1 Parse.thms1; Proof.using_cmd; Proof_Context.get_fact @{context}\<close>
ML_val \<open>Attrib.attribute_cmd; Parse.name;Print_Mode.with_modes;Attrib.eval_thms; Library.foldl\<close>
thm (a) TrueI
ML_val \<open>Thm.prop_of @{thm mp}; merge_options\<close>
ML_val \<open>Thm.full_prop_of @{thm mp}\<close>
ML_val \<open>Thm.full_prop_of @{thm map_rec}\<close>
ML_val \<open>Thm.full_prop_of @{thm List.fold_def}\<close>
ML_val \<open>Logic.dest_equals @{term "a\<equiv>b"}; Proof_Context.new_scope\<close>
ML_val \<open>op #->; op |->; Proof_Context.new_scope #-> Proof_Context.qualified_scope\<close>
definition "a==3"
thm         a_def
ML_val \<open>Binding.conglomerate; space_implode\<close>
ML_val \<open>Proof_Context.note_thmss; Local_Theory.note; Name_Space.private_scope\<close>

locale XX 
locale XXY = XX begin
local_setup \<open>
Local_Theory.note ((@{binding lm}, []), @{thms TrueI}) #> snd
\<close>
ML_val \<open>Named_Target.locale_of @{context}\<close>
end
thm XXY.lm
ML_val \<open>Expression.add_locale_cmd; Sign.full_name @{theory} @{binding sdf}\<close>
ML_val \<open>Named_Target.init; Locale.register_locale\<close>
ML_val \<open>Named_Target.init; Named_Target.exit\<close>
ML_val \<open>Named_Target.bottom_locale_of @{context}\<close>
ML_val \<open>Function.get_info @{context} @{term upto} |> #fs |> the_single\<close>
fun ff where
"ff (Suc P) = ff P"
thm ff.induct
thm upto.induct
find_theorems Cons list_all2
thm list.rel_intros
thm rel_sum.intros

thm option.rel_intros
ML_val \<open>\<close>

end