theory Scratch_Command
  imports Main
  keywords
    "cmd" :: thy_decl
begin

ML \<open>
fun cmd tm ctxt =
  let
    val _ = Syntax.read_term ctxt tm
  in
    ctxt
  end
\<close>

ML \<open>
  Outer_Syntax.local_theory @{command_keyword cmd} "read and print term"
    (Parse.term >> cmd);
Toplevel.local_theory_to_proof;
\<close>

cmd sfdsf
term asdads
lemma (in cc)
end