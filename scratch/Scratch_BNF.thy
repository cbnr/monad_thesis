theory Scratch_BNF
  imports Main
begin

ML_val \<open>@{type_name prod}\<close>

ML \<open>
val bnf_prod = BNF_Def.bnf_of @{context} @{type_name prod} |> the;
val bnf_option = BNF_Def.bnf_of @{context} @{type_name option} |> the;
val bnf_list = BNF_Def.bnf_of @{context} @{type_name list} |> the;
\<close>

ML_val \<open>
BNF_Def.name_of_bnf bnf_prod;
BNF_Def.T_of_bnf bnf_prod;
\<close>

definition "b==5"
definition "a x c==b" if "3=5" for  a c
ML_val \<open> Parse_Spec.specification\<close>
fun f where "f _= 0"
ML \<open>
val th = @{thm f.simps[no_vars]};
val tm = th |> Thm.full_prop_of;
Variable.import;
Term.add_frees tm [];
\<close>
ML_val \<open>bnf_list |> BNF_Def.inj_map_of_bnf\<close>
ML_val \<open>bnf_prod |> BNF_Def.set_transfer_of_bnf\<close>
ML \<open>val option_fp = BNF_FP_Def_Sugar.fp_sugar_of @{context} @{type_name option} |> the\<close>
ML_val \<open>option_fp |> #fp_ctr_sugar |> #ctr_sugar |> #casex\<close>
ML_val \<open>option_fp |> #fp_res\<close>
ML \<open>
fun case_term_of ctxt tp_name =
  Ctr_Sugar.ctr_sugar_of ctxt tp_name |> the |> #casex

fun case_terms ctxt =
  map (fn sg => (#casex sg |> Term.term_name, #kind sg)) (Ctr_Sugar.ctr_sugars_of ctxt)
\<close>

ML_val\<open>case_term_of @{context} @{type_name list}\<close>
ML_val\<open>case_term_of @{context} @{type_name option}\<close>
ML_val\<open>case_term_of @{context} @{type_name prod}\<close>
ML_val\<open>case_term_of @{context} @{type_name nat}\<close>
declare [[ML_print_depth=1000]]
ML_val \<open>case_terms @{context} \<close>
ML_val \<open>(Ctr_Sugar.ctr_sugar_of @{context} @{type_name option} |> the |> #kind) = Ctr_Sugar.Datatype\<close>
ML_val \<open>filter (fn sg:Ctr_Sugar.ctr_sugar => #kind sg = Ctr_Sugar.Datatype) (Ctr_Sugar.ctr_sugars_of @{context}) |> (map #T)\<close>
typ nat


end