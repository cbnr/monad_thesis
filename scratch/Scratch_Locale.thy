theory Scratch_Locale
  imports Main
begin
ML_val \<open>
val t = Locale.registrations_of (Context.Proof @{context})
@{locale dp_consistency_rbt} ;

val (name, phi) = t |> tl |> hd;

Locale.instance_of @{theory} name phi;
Morphism.term phi @{term mem_defs.checkmem}
;
val dp = Locale.params_of @{theory} @{locale dp_consistency_rbt} |> hd |> #1  |> (fn (a,_) => (a,dummyT)) |>Free;
Morphism.term phi dp
\<close>
ML_val \<open>
type x = Expression.expression_i;
Expression.Named
\<close>

ML_val \<open>
val ((tms,phis,phi),ctxt) =
Expression.cert_goal_expression
([(@{locale dp_consistency_rbt},(("zz",true),(Expression.Positional [SOME @{term "\<lambda>(i,j). bf i j"}])))],[])
@{context};
Interpretation.isar_interpretation;
\<close>
ML_val \<open>@{binding xy} |> Binding.name_of\<close>
term mem_defs.checkmem
term Test_Bellman_Ford1.bf
ML_val \<open>Free ("a.b.c  d", dummyT) |> Syntax.pretty_term @{context}\<close>
dp_monadify  bf\<^sub>T: bf (bf.simps) memoizes dp_consistency_rbt
thm bf\<^sub>T.crel_vs_checkmem_tupled
term bf\<^sub>T.checkmem
ML_val \<open>Syntax.read_term @{context} (Long_Name.qualify "bf\<^sub>T" "checkmem")\<close>
ML_val \<open>Locale.registrations_of (Context.Proof @{context}) @{locale dp_consistency_rbt}\<close>
ML_val \<open>Registrations.get\<close>
term 
thm bf\<^sub>T_def bf\<^sub>T'.simps
thm bf\<^sub>T'.simps[unfolded xx.checkmem_checkmem'[symmetric]]
ML_val \<open>Transform_Data.get_dp_info @{context} @{term bf}\<close>
ML_val \<open>@{locale dp_consistency_rbt}; Locale.intern @{theory} "dp_consistency_rbt";
\<close>
ML_val \<open>Name_Space.check; Name_Space.intern; type x = Position.T; Position.none;
Locale.check (Proof_Context.theory_of @{context}) ("dp_consistency_rbt", Position.none)
\<close>

local_setup \<open> fn lthy => 
let val lthy =
Transform_DP.dp_interpretation @{locale dp_consistency_rbt} "zz" @{term "\<lambda>(j, i). bf i j"} lthy
in
@{print} (Syntax.read_term lthy "zz.checkmem");
lthy
end

\<close>
ML_val \<open>Context.proof_map; Context.map_proof; Isar_Cmd.local_setup\<close>
ML_val \<open>Outer_Syntax.local_theory; Toplevel.local_theory; Context.mapping; Context.>>\<close>
ML_val \<open>Local_Theory.background_theory; Runtime.controlled_execution\<close>
ML_val \<open>type x = generic_theory\<close>
ML_val \<open>Named_Target.switch; Context.Theory; type x = theory; Local_Theory.exit_global\<close>
ML_val \<open>ML_Context.expression\<close>
find_theorems mem_defs.checkmem "\<lambda>(j, i). bf i j"
ML_val \<open>fold Locale.activate_declarations; op ||>; Local_Theory.open_target\<close>
ML_val \<open>Variable.export_morphism ; Morphism.thm\<close>
thm zz.cmem_empty
term zz.checkmem
ML_val \<open>Named_Target.is_theory; Local_Theory.theory_registration; Element.witness_proof_eqs\<close>
ML_val \<open>Name_Space.new_group;Local_Theory.reset\<close>
ML_val \<open>Parse.name; HOLogic.dest_eq\<close>
ML_val \<open>Term.dummy; lambda Term.dummy Term.dummy; HOLogic.tupled_lambda;
Name.invent Name.context "a" 3;
Name.variant_list [] ["a", "a", "a"];
val args = Name.invent_list [] "a" 3 |> map (fn x => Free (x, dummyT));
val args_tuple = HOLogic.mk_tuple args;
val tm = list_comb (Free ("f", @{typ "'a => 'b => 'b => 'c"}), args);
val tmt = HOLogic.tupled_lambda args_tuple tm;
Syntax.pretty_term @{context} tmt |> Pretty.writeln;
\<close>


term xx.crel_vs
thm xx.crel_vs_def
thm zz.crel_vs_def
term zz.crel_vs

term dp_consistency.crel_vs
term mem_defs.checkmem
term xx.checkmem
term xx.crel_vs
find_theorems mem_defs.checkmem "\<lambda>(j, i). bf i j"

ML_val \<open>Toplevel.local_theory_to_proof'\<close>
ML_val \<open>Local_Theory.reset_group\<close>
ML_val \<open>Toplevel.end_proof; type x = Proof.state\<close>
ML_val \<open>Toplevel.proof Proof.local_done_proof; Proof.theorem\<close>
ML_val \<open>Toplevel.proof; Toplevel.end_proof\<close>
ML_val \<open>Proof.local_immediate_proof; Proof.global_immediate_proof\<close>
  ML_val \<open>Element.witness_proof_eqs\<close>
  term 0 (*
context includes lifting_syntax begin
lemma "xx.crel_vs (op= ===> xx.crel_vs (op= ===> xx.crel_vs op=)) bf bf\<^sub>T"
  unfolding bf\<^sub>T_def
  apply (subst Rel_def[symmetric])
  apply (intro xx.crel_vs_return Rel_abs)?
  apply (unfold Rel_def)
  apply clarify?
  oops
  ML_val \<open>get_first; @{term "f a b c"} |> strip_comb |>
(fn (f,xs) => map (curry list_comb f) (prefixes xs))
\<close>

term 0 (*
term "mem_correct lookup update P"
term "dp_consistency.crel_vs lookup P bf R"
thm dp_consistency.crel_vs_return
thm dp_consistency.axioms
find_theorems dp_consistency_axioms
thm dp_consistency.intro
thm mem_correct.intro
thm mem_correct_def
thm dp_consistency.crel_vs_return[OF dp_consistency.intro]
thm dp_consistency.crel_vs_checkmem
end end end
term mem_correct_empty
thm mem_correct_empty.axioms
find_theorems mem_correct_empty
ML_val \<open>type x = Expression.expression_i;Proof.local_future_terminal_proof;
Toplevel.proof;
Method.report;
\<close>
  by
term 0 (*
ML_val \<open>Parse.term; \<close>
welcome
locale
dp_monadify bf: bf
ML_val \<open>type x = Markup.T; Pretty.string_of\<close>
ML_val \<open>Consts.retrieve_abbrevs\<close>
ML_val \<open>@{term bf}\<close>
term "Test_Bellman_Ford1.bf n W t"
print_abbrevs
ML_val \<open>Name_Space.markup_entries; is_Free\<close>
thm bf.simps
thm bf.induct
thm bf\<^sub>T'.simps
ML_val \<open>Transform_Data.get_dp_info @{term bf} @{context}\<close>
print_locale dp_consistency
print_locale dp_consistency_default
print_locale order

interpretation xx: dp_consistency
proof 

(*interpretation dp_consistency_default bf .*)

ML_val \<open>
Function.get_info @{context} @{term local.bf};
\<close>
ML_val \<open>@{term Test_Bellman_Ford1.bf}\<close>

local_setup \<open>Local_Theory.define ((@{binding xx}, NoSyn),((Binding.empty, []),@{term t})) #> snd\<close>
ML_val \<open>@{term xx}\<close>

ML_val \<open>
lambda @{term "a+b"} @{term "c+(a+b)+d\<equiv>e"};
\<close>
ML_val \<open>lambda @{term "a+b"} @{term "c+(a+b)+d=e"}\<close>
ML_val \<open>lambda (Const (@{const_name map}, dummyT)) @{term "map f [0]"}\<close>

ML_val \<open>Proof_Context.expand_abbrevs @{context} @{term bf}\<close>
ML_val \<open>Proof_Context.expand_abbrevs @{context} @{term xx}\<close>
ML_val \<open>Proof_Context.expand_abbrevs @{context} @{term 0}\<close>
ML_val \<open>Proof_Context.expand_abbrevs @{context} @{term 0}\<close>
ML_val \<open>Proof_Context.read_term_pattern @{context} "_ map _"\<close>
ML_val \<open>Proof_Context.read_term_schematic @{context} "map ?map"\<close>
ML_val \<open>Proof_Context.read_term_abbrev @{context} "map"\<close>
ML_val \<open>Proof_Context.read_term_abbrev @{context} "bf"\<close>
ML \<open>val Map = Proof_Context.read_const {proper=false, strict=false} @{context} @{const_name map}\<close>
ML_val \<open>lambda Map @{term "map f [0]"}\<close>
ML_val \<open>Conv.rewr_conv\<close>
ML_val \<open>Thm.match\<close>
ML_val \<open>Thm.rhs_of @{thm List.fold_def} |> Thm.term_of\<close>
ML_val \<open>@{thm List.fold_def} |> Thm.full_prop_of\<close>
ML_val \<open>Thm.instantiate\<close>
ML_val \<open>Thm.match (@{cterm map}, @{cterm "map::(int\<Rightarrow>nat) \<Rightarrow> _ \<Rightarrow> _"})\<close>
ML_val \<open>Thm.match (@{cterm map}, @{cterm "map::_"})\<close>
ML_val \<open>op aconvc\<close>
lemma blah: "id x \<equiv> x" unfolding id_def .
ML_val \<open>Conv.rewr_conv @{thm blah[symmetric]} @{cterm "Suc 0"}\<close>
ML_val \<open>Thm.incr_indexes (8+ 1) @{thm blah[symmetric]} |> Thm.lhs_of\<close>
ML_val \<open>Thm.rename_boundvars\<close>

ML_val \<open>
val rule = @{thm blah[symmetric]};
val ct = @{cterm "Suc 0"};
    val rule1 = Thm.incr_indexes (Thm.maxidx_of_cterm ct + 1) rule;
    val lhs = Thm.lhs_of rule1;
    val rule2 = Thm.rename_boundvars (Thm.term_of lhs) (Thm.term_of ct) rule1;
Thm.match (lhs, ct);
Pattern.first_order_match;
    val rule3 =
      Thm.instantiate (Thm.match (lhs, ct)) rule2
        handle Pattern.MATCH => raise CTERM ("rewr_conv", [lhs, ct]);
    val rule4 =
      if Thm.lhs_of rule3 aconvc ct then rule3
      else
        let val ceq = Thm.dest_fun2 (Thm.cprop_of rule3)
        in rule3 COMP Thm.trivial (Thm.mk_binop ceq ct (Thm.rhs_of rule3)) end;

\<close>

ML_val \<open>@{cterm bf} |> Thm.trim_context_cterm\<close>
thm bf.simps
ML_val \<open>@{thm bf.simps(2)} |> Thm.full_prop_of\<close>
end
end
end

ML_val \<open>@{term bf}\<close>
term bf
ML_val \<open>\<close>
ML_val \<open>
Function.get_info @{context} @{term bf};
\<close>
thm bf.simps
ML_val \<open>Morphism.term\<close>
ML_val \<open>Thm.trim_context\<close>
ML_val \<open>Thm.transfer\<close>
ML_val \<open>Syntax.parse_term @{context} "map" |> Syntax.check_term @{context}\<close>
ML_val \<open>Term.show_dummy_patterns (Free ("x", dummyT))\<close>
ML_val \<open>Variable.lookup_const @{context} @{const_name map}\<close>
ML_val \<open>Variable.is_const @{context} @{const_name map}\<close>
ML_val \<open>Local_Theory.abbrev; type x = Syntax.mode\<close>
context fixes d assumes [simp]: "d = (0::nat)" begin
fun f :: "nat \<Rightarrow> 'b \<Rightarrow> int" where
"f 0 t = 0"
| "f (Suc x) t = f (x+d) t"
fun g :: "nat \<Rightarrow> int" where
"g 0 = 0"
| "g (Suc x) = g x"
ML \<open>val x = @{thms f.simps}\<close>
ML_val \<open>@{thm f.simps(1)} |> Thm.full_prop_of |> HOLogic.dest_Trueprop
|> HOLogic.dest_eq |> fst |> head_of
\<close>


ML \<open>
fun behead head tm =
  let
    val head_nargs = strip_comb head |> snd |> length
    val (tm_head, tm_args) = strip_comb tm
    val (tm_args0, tm_args1) = chop head_nargs tm_args
    val tm_head' = list_comb (tm_head, tm_args0)
    val _ = if Term.aconv_untyped (head, tm_head')
      then () else raise TERM("head does not match", [head, tm_head'])
  in
    (tm_head', tm_args1)
  end
\<close>

ML_val \<open>@{term f}\<close>
ML_val \<open>behead @{term f} @{term "f 0 (Suc 0)"}\<close>
end
term f
thm f.simps
thm g.simps
ML_val \<open>strip_abs_body @{term "%f. %x. x f"}\<close>
ML_val \<open>
val Abs x = lambda @{term "map f"} @{term "map f xs"};
Term.dest_abs x;
val Abs y = Term.lambda_name ("MAP", @{term "map f"}) @{term "map f xs"};
Term.dest_abs y;
\<close>
end