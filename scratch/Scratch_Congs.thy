theory Scratch_Congs
  imports "../DP_CrelVS_Ext"
begin
notation fun_app_lifted (infixl "." 999)
notation Transfer.Rel ("Rel")

context dp_consistency begin
context includes lifting_syntax begin

thm map_cong
thm map1_cong2

lemma map1\<^sub>T_simps:
  "map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>[]\<rangle> = \<langle>[]\<rangle>"
  "map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>y#ys\<rangle> = \<langle>\<lambda>x. \<langle>\<lambda>xs. \<langle>x#xs\<rangle>\<rangle>\<rangle> . (f\<^sub>T' y) . (map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  unfolding map1\<^sub>T_def return_app_return by auto

lemma map1_cong2':
  assumes "list_all2 R xs ys" "crel_vs S (f x) (f\<^sub>T' y)"
  shows "crel_vs (list_all2 S) (map f xs) (map1\<^sub>T . (#f\<^sub>T'#) . (#ys#))"
  apply (rule list_all2_induct[OF assms(1)])
  subgoal
    unfolding list.map map1\<^sub>T_simps
    apply (rule crel_vs_return[unfolded Rel_def])
    apply simp
    done
  subgoal for x xs y ys
    unfolding list.map map1\<^sub>T_simps
    apply simp
    thm set_map set_upt atLeastLessThan_iff
    oops

lemma set_state_return: "set_state (return x) = {x}"
  unfolding return_def by simp

lemma "set_state (map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>) = {[]}"
  term "(map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  term "set_state (map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  term "\<Union>(set ` (set_state (map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)))"
  term "\<Union>x\<in>(set_state (map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)). set x"
  oops

lemma fun_app_set_state:
  "set_state (f\<^sub>T . x\<^sub>T) \<subseteq> (\<Union>f\<in>set_state f\<^sub>T. \<Union>x\<in>set_state x\<^sub>T. set_state (f x))"
  unfolding fun_app_lifted_def bind_def
  apply (auto split: prod.splits)
  subgoal for y M0 f M1 x M2
    apply (cases "f x")
    apply auto
    subgoal for fx
      apply (cases f\<^sub>T; cases x\<^sub>T)
      subgoal for ff xf
      apply (rule bexI[where x=f])
       apply (rule bexI[where x=x])
        apply auto
         apply (rule exI[where x=M1])
         apply auto
        apply (rule exI[where x=M0])
        apply auto
        done
      done
    done
  done

lemma "(\<Union>x\<in>(set_state (map1\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)). set x) \<subseteq> (\<Union>y\<in>set ys. set_state (f\<^sub>T' y))"
  apply (induction ys)
  unfolding map1\<^sub>T_simps set_state_return
  apply simp_all
  apply (auto simp: set_state_return
      dest!: in_mono[OF fun_app_set_state, rule_format])
  apply (unfold map1\<^sub>T_def)
  apply (auto simp: set_state_return
      dest!: in_mono[OF fun_app_set_state, rule_format])
  
  oops
  thm in_mono[OF fun_app_set_state, rule_format]

end (* context lifting_syntax *)
end (* context dp_consistency *)
end (* theory *)