theory Scratch_Curry
  imports "../DP_Lifting"
begin

locale normal begin
fun c :: "nat \<Rightarrow> nat \<Rightarrow> int" where
  "c n 0 = 1"
| "c 0 m = 0"
| "c (Suc n) (Suc m) = c n (Suc m) + c n m"
end

locale nocurry begin
fun c :: "(nat \<times> nat) \<Rightarrow> ('M, int) state" where
  "c (n, 0) = \<langle>1\<rangle>"
| "c (0, m) = \<langle>0\<rangle>"
| "c (Suc n, Suc m) = \<langle>\<lambda>a. \<langle>\<lambda>b. \<langle>a + b\<rangle>\<rangle>\<rangle> . (c (n, Suc m)) . (c (n, m))"
end

locale curry begin
fun c :: "nat \<Rightarrow> nat \<Rightarrow> ('M, int) state" where
  "c n 0 = \<langle>1\<rangle>"
| "c 0 m = \<langle>0\<rangle>"
| "c (Suc n) (Suc m) = \<langle>\<lambda>a. \<langle>\<lambda>b. \<langle>a + b\<rangle>\<rangle>\<rangle> . (c n (Suc m)) . (c n m)"
end

definition chmem :: "('M, int) state \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> ('M, int) state" where
  "chmem calc n m \<equiv> if lookup n m then it else calc and update n m it"

primrec f where
  "f None = Suc 0"
| "f (Some x) = Suc x"

thm f_def

find_theorems rec_option map_option
thm list.map_def
  
term ctor_fold_list
primrec g where
  "g (a, b) = a + b"


primrec h where
  "h [] = Suc 0"
| "h (x#xs) = x + (hd (map h [xs]))"

ML_val \<open>
type x = conv;
op then_conv;
\<close>
end