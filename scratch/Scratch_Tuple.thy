theory Scratch_Tuple
  imports Main
begin
ML_val \<open>HOLogic.tupled_lambda\<close>
ML_val \<open>lambda (Free ("x", dummyT)) (Free ("x", dummyT))\<close>
ML_val \<open>HOLogic.tupled_lambda (Free ("x", dummyT)) (Free ("x", dummyT))\<close>
ML_val \<open>lambda (Free ("x", dummyT)) (HOLogic.mk_prod (Free ("x", dummyT), Free ("y", dummyT)))\<close>
ML_val \<open>HOLogic.mk_prod (Free ("x", dummyT), Free ("y", dummyT))\<close>
ML_val \<open>HOLogic.tupled_lambda (HOLogic.mk_prod (Free ("x", dummyT), Free ("y", dummyT))) (Free ("x", dummyT))\<close>
ML_val \<open>HOLogic.tupled_lambda (HOLogic.unit) (Free ("x", HOLogic.unitT)) |> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_val \<open>@{term "\<lambda>(). (x::_)"}\<close>
ML_val \<open>HOLogic.mk_tuple [@{term a}, @{term b}, @{term "a+b"}]|> Syntax.pretty_term @{context} |> Pretty.writeln\<close>
ML_val \<open>HOLogic.mk_tuple [@{term a}, @{term "(c,b)"}, @{term "(a,b)"}]|> Term.type_of\<close>

end