theory Scratch_Fib_Tic
  imports
    State_Tic_Monad
    "../../state_monad/State_Main"
begin

fun fib :: "nat \<Rightarrow> nat" where
  "fib 0 = 1"
| "fib (Suc 0) = 1"
| "fib (Suc (Suc n)) = fib n + fib (Suc n)"


ML \<open>
fun note_thms ctxt binding thms =
  Local_Theory.note ((binding, []), thms) ctxt |> snd
\<close> -- \<open>auxiliary function for lemma binding\<close>

ML_val \<open>Inductive.the_inductive @{context} @{term fib_rel} |> snd\<close>

local_setup \<open>
fn ctxt => Inductive.the_inductive ctxt @{term fib_rel}
  |> snd |> #eqs
  |> note_thms ctxt @{binding fib_rel_def}
\<close>
thm fib_rel_def

local_setup \<open>
fn ctxt => Inductive.the_inductive ctxt @{term fib_rel}
  |> snd |> #intrs
  |> note_thms ctxt @{binding fib_rel_intro}
\<close>
thm fib_rel_intro

local_setup \<open>
fn ctxt => Inductive.the_inductive ctxt @{term fib_rel}
  |> snd |> #inducts
  |> note_thms ctxt @{binding fib_rel_induct}
\<close>
thm fib_rel_induct


locale mem_correct_strong =
  state_mem_inv where lookup=lookup and mem_inv=mem_inv
for lookup :: "'param \<Rightarrow> ('mem, 'result option) state" and mem_inv :: "'mem \<Rightarrow> bool" +
assumes lookup_correct_strong: "\<And>k v m m'.
    mem_inv m \<Longrightarrow> run_state (lookup k) m = (v, m') \<Longrightarrow> map_of m' = map_of m"
assumes update_correct_strong: "\<And>k v m m' u.
    mem_inv m \<Longrightarrow> run_state (update k v) m = (u, m') \<Longrightarrow> map_of m' = map_of m(k \<mapsto> v)"
begin

sublocale mem_correct
  by standard (auto simp: lookup_correct_strong update_correct_strong)

end

print_locale dp_consistency
locale dp_consistency_strong =
  mem_correct_strong where lookup=lookup for lookup :: "'param \<Rightarrow> ('mem, 'result option) state" +
fixes dp :: "'param \<Rightarrow> 'result"
begin

sublocale dp_consistency ..

end

locale dp_consistency_strong_empty =
  dp_consistency_strong + mem_correct_empty
begin

sublocale dp_consistency_empty ..

end
find_theorems dp_consistency_strong_empty dp_consistency_empty

locale dp_consistency_strong_default =
  fixes dp :: "'param \<Rightarrow> 'result"
begin

interpretation no_strong: dp_consistency_default .

thm no_strong.dp_consistency_empty_axioms
find_theorems dp_consistency_empty mem_correct_empty

sublocale dp_consistency_strong_empty
  where lookup=no_strong.lookup
    and update=no_strong.update
    and mem_inv=top
    and empty=Map.empty
  apply (rule dp_consistency_strong_empty.intro)
   apply (rule dp_consistency_strong.intro)
  subgoal
    apply standard
    unfolding no_strong.lookup_def no_strong.update_def state_mem_defs.map_of_def
    unfolding State_Monad.bind_def State_Monad.get_def State_Monad.set_def
    by auto
  apply (fact no_strong.mem_correct_empty_axioms)
  done

end

dp_fun fib\<^sub>T: fib memoizes dp_consistency_strong_default monadifies (state) fib.simps
dp_correctness by dp_match
thm fib\<^sub>T.rel_vs_checkmem_tupled


context dp_consistency_strong begin

context
  fixes y :: 'param
  fixes v :: "'result option"
begin

definition entry_fixval_inv :: "'mem \<Rightarrow> bool"  where
  "entry_fixval_inv mem \<longleftrightarrow> map_of mem y = v"

print_locale mem_subtype
interpretation entry_fixval: mem_subtype "inf mem_inv entry_fixval_inv" .
(*
definition cinv_vs where
  "cinv_vs mem_inv = cinv (\<lambda>mem. P mem \<and> mem_inv mem)"
*)
lemma cinv_vs_lookup:
  "entry_fixval.inv (lookup x)"
  using lookup_inv unfolding entry_fixval_inv_def
  by (auto intro!: mem_subtype.invI dest: mem.invD lookup_correct_strong)

lemma cinv_vs_update:
  "entry_fixval.inv (update x v')"
  if "x \<noteq> y"
  using that update_inv unfolding entry_fixval_inv_def
  by (auto intro!: mem_subtype.invI dest: mem.invD update_correct_strong)
  
lemma cinv_vs_checkmem:
  "entry_fixval.inv (checkmem x s)"
  if "entry_fixval.inv s" "x \<noteq> y"
  unfolding checkmem_def checkmem_lazy_def
  apply (rule entry_fixval.inv_bind)
  apply (fact cinv_vs_lookup)
  apply (split option.split)
  apply safe
   apply (rule entry_fixval.inv_bind)
    apply (fact that)
   apply (rule entry_fixval.inv_bind)
    apply (rule cinv_vs_update)
    apply (fact that)
   apply (fact entry_fixval.inv_return)
  apply (fact entry_fixval.inv_return)
  done
abbreviation "entry_fixval_invf \<equiv> entry_fixval.inv"
end
end

lemma fib_rel_trancl:
  "fib_rel\<^sup>*\<^sup>* y (Suc x) \<Longrightarrow> fib_rel\<^sup>*\<^sup>* y (Suc (Suc x))"
  "fib_rel\<^sup>*\<^sup>* y x \<Longrightarrow> fib_rel\<^sup>*\<^sup>* y (Suc (Suc x))"
  by (auto intro: rtranclp.intros fib_rel_intro)

term fib\<^sub>T.entry_fixval_inv
theorem fib_rel_complement_untouched:
  "\<not>fib_rel\<^sup>*\<^sup>* y x \<Longrightarrow> fib\<^sub>T.entry_fixval_invf y v (fib\<^sub>T' x)"
  apply (induction x rule: fib\<^sub>T'.induct)
  subgoal
    unfolding fib\<^sub>T'.simps
    apply (rule fib\<^sub>T.cinv_vs_checkmem)
     apply (rule mem_subtype.inv_return)
    apply safe
    done
  subgoal
    unfolding fib\<^sub>T'.simps
    apply (rule fib\<^sub>T.cinv_vs_checkmem)
     apply (rule mem_subtype.inv_return)
    apply safe
    done
  subgoal premises prems for x
    thm prems
    apply (subst fib\<^sub>T'.simps)
    apply (rule fib\<^sub>T.cinv_vs_checkmem)
    unfolding mem_subtype.inv_def
     apply (rule mem_subtype.pred_fun_app)
      apply (rule prems(2)[unfolded mem_subtype.inv_def])
      apply (rule contrapos_nn[rotated])
       apply (fact fib_rel_trancl(1))
      apply (fact prems(3))
     apply (rule mem_subtype.pred_fun_app)
      apply (rule prems(1)[unfolded mem_subtype.inv_def])
      apply (rule contrapos_nn[rotated])
       apply (fact fib_rel_trancl(2))
      apply (fact prems(3))
     apply (rule mem_subtype.pred_return)
     apply (rule pred_funI)
     apply (rule mem_subtype.pred_return)
     apply (rule pred_funI)
     apply (rule mem_subtype.pred_return)
    apply (rule TrueI)
    subgoal using prems(3) by safe
    done
  done

lemma fib_rel_rancl_contr:
  "\<not>fib_rel\<^sup>*\<^sup>* i j" if "\<not>i \<le> j"
  apply (rule contrapos_nn[OF that])
  unfolding fib_rel_def
  by (induction i j rule: rtranclp.induct) auto

corollary DPTime_fib_untouched:
  "i < j \<Longrightarrow> run_state (fib\<^sub>T' i) mem = (v, mem') \<Longrightarrow> fib\<^sub>T.map_of mem' j = fib\<^sub>T.map_of mem j"
  apply (fold linorder_not_le)
  apply (drule fib_rel_complement_untouched[OF fib_rel_rancl_contr])
  unfolding fib\<^sub>T.entry_fixval_inv_def
  by (auto dest: mem_subtype.invD)

end

  
