theory State_Writer_Monad
  imports "HOL-Library.State_Monad"
begin

datatype ('w, 's, 'a) state_writer = StateWriter (run_state_writer: "('s, 'a \<times> 'w) state")
welcome

context begin

qualified definition lift :: "('s, 'a) state \<Rightarrow> ('w::monoid_add, 's, 'a) state_writer" where
  "lift s = StateWriter (do {v \<leftarrow> s; State_Monad.return (v, 0)})"

qualified definition to_state :: "('w, 's, 'a) state_writer \<Rightarrow> ('s, 'a) state" where
  "to_state s = do {(a, v) \<leftarrow> run_state_writer s; State_Monad.return a}"

qualified definition return :: "'a \<Rightarrow> ('w::monoid_add, 's, 'a) state_writer" where
  "return = lift \<circ> State_Monad.return"

qualified definition bind :: "('w::monoid_add, 's, 'a) state_writer \<Rightarrow> ('a \<Rightarrow> ('w, 's, 'b) state_writer) \<Rightarrow> ('w, 's, 'b) state_writer" where
  "bind x f = StateWriter (do {
    (v0, w0) \<leftarrow> run_state_writer x;
    (v1, w1) \<leftarrow> run_state_writer (f v0);
    State_Monad.return (v1, w0+w1)
  })"

adhoc_overloading Monad_Syntax.bind bind

qualified lemma bind_left_identity:
  "bind (return a) f = f a"
  unfolding return_def lift_def bind_def by simp

qualified lemma bind_right_identity:
  "bind m return = m"
  unfolding return_def lift_def bind_def by simp

qualified lemma bind_assoc:
  "bind (bind m f) g = bind m (\<lambda>x. bind (f x) g)" for m :: "('w::monoid_add, _, _) state_writer"
  unfolding bind_def
  by (fastforce
      intro: arg_cong[where f="State_Monad.bind _"]
      simp: semigroup_add.add_assoc[OF semigroup_add_class.axioms])

lemma bind_to_state:
  "to_state (x \<bind> f) = to_state x \<bind> (\<lambda>v. to_state (f v))"
  unfolding to_state_def
  by (auto split: prod.split simp: State_Monad.bind_def State_Writer_Monad.bind_def)

lemma lift_to_state:
  "to_state (lift x) = x"
  unfolding to_state_def lift_def
  by simp

lemma run_state_writer_return:
  "run_state_writer (return v) = State_Monad.return (v, 0)"
  unfolding return_def lift_def by simp

lemmas [simp] =
  bind_left_identity
  bind_right_identity
  bind_assoc

lemmas [simp] = 
  run_state_writer_return

lemmas [simp] =
  bind_to_state
  lift_to_state
end (* context *)

end