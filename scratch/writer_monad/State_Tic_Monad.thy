theory State_Tic_Monad
  imports State_Writer_Monad
begin

type_synonym ('s, 'a) state_tic = "(nat, 's, 'a) state_writer"

end