theory Scratch_Attribute
  imports Main
begin

lemma True
  by transfer simp
ML_val \<open>Thm.declaration_attribute\<close>

ML \<open>
structure Data = Generic_Data (
  type T = int
  val empty = 1
  val extend = K 2
  val merge = op+
)
\<close>

ML_val \<open>Term.size_of_term (@{thm refl} |> Thm.full_prop_of)\<close>

ML \<open>
fun add thm ctxt =
  Data.map (curry op+ (Term.size_of_term (Thm.full_prop_of thm))) ctxt
\<close>

attribute_setup my_add = \<open>Scan.succeed (Thm.declaration_attribute add)\<close>

ML_val \<open>Context.Proof @{context} |> Data.get\<close>
declare TrueI[my_add]
ML_val \<open>Context.Proof @{context} |> Data.get\<close>
declare TrueI[my_add]
ML_val \<open>Context.Proof @{context} |> Data.get\<close>
declare map_concat[my_add]
ML_val \<open>Context.Proof @{context} |> Data.get\<close>

locale xx begin
declare fold_append[my_add]
ML_val \<open>Context.Proof @{context} |> Data.get\<close>
declaration (pervasive) \<open>
K (Data.map (curry op+ 5))
\<close>
ML_val \<open>Context.Proof @{context} |> Data.get\<close>
end

ML_val \<open>Context.Proof @{context} |> Data.get\<close>
ML_val \<open>Local_Theory.declaration; Morphism.term\<close>

declaration \<open>
K (Data.map (curry op+ 5))
\<close>

declaration \<open>fn _ => Local_Theory.note ((@{binding xx}, []),@{thms TrueI}) #> snd\<close>

end