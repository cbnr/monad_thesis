theory StateT_Heap_Pair_Memory
  imports
    StateT_Heap_Cache
    "IICF/IICF"
    "../Index"
begin

fun fill_heap where
  "fill_heap _ _ 0 = Heap_Monad.return ()"
| "fill_heap a v (Suc n) = do {
    Array.upd n v a;
    fill_heap a v n
  }"

lemma fill_heap_hoare':
  "n \<le> length l \<Longrightarrow> <a \<mapsto>\<^sub>a l> fill_heap a v n <\<lambda>_. a \<mapsto>\<^sub>a [if i<n then v else l!i. i \<leftarrow> [0..<length l]]>"
  apply (induction n arbitrary: l)
  subgoal by (sep_auto simp: map_nth)
  subgoal premises prems 
    using prems(2)
    apply (sep_auto heap: prems(1))
    apply (auto simp: less_Suc_eq intro!: back_subst[of "op \<Longrightarrow>\<^sub>A _", OF ent_refl] arg_cong[where f="op \<mapsto>\<^sub>a _"])
    done
  done

lemma fill_heap_hoare:
  "n = length l \<Longrightarrow> <a \<mapsto>\<^sub>a l> fill_heap a v n <\<lambda>_. a \<mapsto>\<^sub>a replicate n v>"
  by (sep_auto
      heap: fill_heap_hoare'
      intro!: back_subst[of "op \<Longrightarrow>\<^sub>A _", OF ent_refl] arg_cong[where f="op \<mapsto>\<^sub>a _"]
      simp: map_replicate_trivial[symmetric])

print_locale bounded_index
locale array = arrlim: bounded_index
begin

definition line_state_valid where
  "line_state_valid maep \<longleftrightarrow> (\<forall>k\<in>dom maep. arrlim.checked_idx k < arrlim.size)"

lemma line_state_valid_empty[intro]:
  "line_state_valid Map.empty"
  unfolding line_state_valid_def by simp

lemma line_state_valid_update:
  "arrlim.checked_idx k < arrlim.size \<Longrightarrow> line_state_valid (maep(k \<mapsto> v)) = line_state_valid maep"
  unfolding line_state_valid_def by auto

lemma line_state_validD:
  assumes "line_state_valid maep" "\<not>(arrlim.checked_idx k < arrlim.size)"
  shows "maep k = None"
  using assms unfolding line_state_valid_def by auto

definition
  "mem_of_line maep st =
    \<up>(line_state_valid maep) *
    (\<exists>\<^sub>A l. is_array l st *
           \<up>(length l = arrlim.size) *
           \<up>(\<forall>k. let i=arrlim.checked_idx k in i<arrlim.size \<longrightarrow> maep k = l ! i)
  )"

definition init_empty where
  "init_empty = (Array.new arrlim.size None)"

lemma init_empty_spec:
  "<emp> init_empty <mem_of_line Map.empty>"
  unfolding init_empty_def mem_of_line_def is_array_def
  by (sep_auto simp: Let_def)

sublocale init_empty
  where is_map=mem_of_line
    and init_empty_mem=init_empty
  by standard (rule init_empty_spec)

definition lookup where
  "lookup k = do {
    mem \<leftarrow> StateT_Heap_Monad.get;
    let i = arrlim.checked_idx k;
    if i < arrlim.size
      then StateT_Heap_Monad.lift (Array.nth mem i)
      else StateT_Heap_Monad.return None
  }"

definition lookup_pure where
  "lookup_pure k mem = (let i = arrlim.checked_idx k in
    if i<arrlim.size then mem k else None)"

lemma lookup_pure_empty[simp]:
  "lookup_pure k Map.empty = None"
  unfolding lookup_pure_def by simp

lemma lookup_spec:
  "<mem_of_line maep st>
    StateT_Heap_Monad.run (lookup k) st
  <\<lambda>(r, st'). mem_of_line maep st' * \<up>(st' = st) * \<up>(r = lookup_pure k maep)>"
  unfolding lookup_def lookup_pure_def mem_of_line_def is_array_def
  by (sep_auto simp add: StateT_Heap_Monad.run_simps Let_def line_state_validD)

lemma lookup_spec':
  "<mem_of_line maep st>
    StateT_Heap_Monad.run (lookup k) st
  <\<lambda>(r, st'). \<exists>\<^sub>A maep'. mem_of_line maep' st' * \<up>(maep' = maep) * \<up>(r = maep k)>"
  apply (sep_auto heap: lookup_spec)
  apply (auto simp: mem_of_line_def lookup_pure_def line_state_validD)
  done

sublocale forgettable_map_lookup_clean_nondec
  where is_map=mem_of_line
    and lookup=lookup
  by standard (rule lookup_spec')

definition update where
  "update k v = do {
    mem \<leftarrow> StateT_Heap_Monad.get;
    let i = arrlim.checked_idx k;
    if i < arrlim.size
      then do {
        StateT_Heap_Monad.lift (Array.upd i (Some v) mem);
        StateT_Heap_Monad.return ()
      }
      else StateT_Heap_Monad.return ()
  }"

definition update_pure where
  "update_pure k v mem = (if arrlim.checked_idx k < arrlim.size then mem(k\<mapsto>v) else mem)"

lemma update_pure_dom:
  "dom (update_pure k v mem) = (if arrlim.checked_idx k < arrlim.size then insert k (dom mem) else (dom mem))"
  unfolding update_pure_def by simp

lemma update_spec:
  "<mem_of_line maep st>
    StateT_Heap_Monad.run (update k v) st
  <\<lambda>(_, st'). mem_of_line (update_pure k v maep) st * \<up>(st' = st)>"
  unfolding update_def update_pure_def
  unfolding mem_of_line_def
  unfolding is_array_def
  using arrlim.checked_idx_injective
  by (sep_auto
      simp: StateT_Heap_Monad.run_simps Let_def injective_def line_state_valid_update
      intro: nth_list_update_neq[symmetric])

lemma update_pure_map_le:
  "line_state_valid mem \<Longrightarrow> update_pure k v mem \<subseteq>\<^sub>m mem(k\<mapsto>v)"
  unfolding update_pure_def
  unfolding line_state_valid_def
  by (auto intro!: map_leI)

lemma line_state_valid_update_pure:
  "line_state_valid (update_pure k v maep) = line_state_valid maep"
  unfolding line_state_valid_def update_pure_def by auto

lemma update_spec':
  "<mem_of_line maep st>
    StateT_Heap_Monad.run (update k v) st
  <\<lambda>(_, st'). \<exists>\<^sub>A maep'. mem_of_line maep' st' * \<up>(maep' \<subseteq>\<^sub>m maep(k\<mapsto>v))>"
  apply (sep_auto heap: update_spec)
  apply (auto simp: mem_of_line_def line_state_valid_update_pure update_pure_map_le)
  done

sublocale forgettable_map_update_clean
  where is_map=mem_of_line
    and update=update
  by standard (rule update_spec')

definition clear where
  "clear = do {
    mem \<leftarrow> StateT_Heap_Monad.get;
    StateT_Heap_Monad.lift (fill_heap mem None arrlim.size)
  }"

lemma clear_spec:
  "<mem_of_line maep st> StateT_Heap_Monad.run clear st <\<lambda>(_, st'). mem_of_line Map.empty st' * \<up>(st' = st)>"
  unfolding clear_def mem_of_line_def is_array_def
  by (sep_auto heap: fill_heap_hoare simp add: StateT_Heap_Monad.run_simps Let_def)

sublocale clearable_map
  where is_map=mem_of_line
    and clear=clear
  by standard (rule clear_spec)

end

datatype ('tag, 'value) state_line = Line (tag_of: 'tag) (arr_of: 'value)
instance state_line :: (heap, heap) heap by countable_datatype

abbreviation "map_tag f \<equiv> map_state_line f id"
abbreviation "map_arr f \<equiv> map_state_line id f"
abbreviation "put_tag x \<equiv> map_tag (\<lambda>_. x)"
abbreviation "put_arr x \<equiv> map_arr (\<lambda>_. x)"
term map_state_line

lemma state_line_put_self[simp]:
  "put_arr (arr_of l) l = l"
  "put_tag (tag_of l) l = l"
  by (cases l; simp)+

type_synonym 'a pair = "'a \<times>'a"
type_synonym ('tag, 'value) pair_state_h = "('tag, 'value option array) state_line pair"
type_synonym ('tag, 'key, 'value) pair_state = "('tag, 'key \<rightharpoonup> 'value) state_line pair"

lemma pair_state_h_cases[cases type: pair_state_h]:
  obtains tag1 tag2 arr1 arr2 where "st = (Line tag1 arr1, Line tag2 arr2)"
  by (metis prod.exhaust state_line.exhaust)

lemma pair_state_put_self[simp]:
  fixes psh :: "('tag, 'map) state_line pair"
  shows "apfst (put_arr (arr_of (fst psh))) psh = psh"
    and "apsnd (put_arr (arr_of (snd psh))) psh = psh"
  by (cases psh; simp)+

print_locale array
locale pair_mem =
  arr: array bound for bound :: "'col::index bound" +
fixes to_row :: "'k \<Rightarrow> 'row" and to_col :: "'k \<Rightarrow> 'col"
fixes init_row1 init_row2 :: 'row
assumes pair_injective: "\<forall> k k'. to_row k = to_row k' \<and> to_col k = to_col k' \<longrightarrow> k = k'"
assumes init_row_distinct: "init_row1 \<noteq> init_row2"
begin

definition rel_tag_map where
  "rel_tag_map tag maep = (\<forall>k\<in>dom maep. \<exists>k'. to_row k' = tag \<and> to_col k' = k)"

lemma rel_tag_map_empty[intro]:
  "rel_tag_map tag Map.empty"
  unfolding rel_tag_map_def by simp

lemma rel_tag_map_mono:
  assumes "rel_tag_map tag maep" "maep' \<subseteq>\<^sub>m maep"
  shows "rel_tag_map tag maep'"
  using assms unfolding rel_tag_map_def
  by (auto dest: map_leD)

lemma rel_tag_map_mono':
  assumes "rel_tag_map (to_row k) maep" "maep' \<subseteq>\<^sub>m maep(to_col k\<mapsto>v)"
  shows "rel_tag_map (to_row k) maep'"
  using assms unfolding rel_tag_map_def
  by (fastforce dest!: map_leD)

definition pair_line_valid where
  "pair_line_valid l = rel_tag_map (tag_of l) (arr_of l)"

definition pair_state_valid where
  "pair_state_valid st \<longleftrightarrow>
    tag_of (fst st) \<noteq> tag_of (snd st)
    \<and> pair_line_valid (fst st)
    \<and> pair_line_valid (snd st)
  "

lemma pair_line_valid_empty:
  "pair_line_valid (Line tag Map.empty)"
  unfolding pair_line_valid_def by auto

abbreviation is_pair_line where
  "is_pair_line lf lh \<equiv>
    \<up>(tag_of lf = tag_of lh) * arr.mem_of_line (arr_of lf) (arr_of lh)"

definition is_pair_state where
  "is_pair_state ps psh =
      \<up>(pair_state_valid ps)
    * is_pair_line (fst ps) (fst psh)
    * is_pair_line (snd ps) (snd psh)"

(*
lemma is_pair_state_def_alt:
  "is_pair_state ps psh =
      \<up>(pair_state_valid ps)
    * \<up>(tag_of (fst ps) = tag_of (fst psh))
    * \<up>(tag_of (snd ps) = tag_of (snd psh))
    * arr.mem_of_line (arr_of (fst ps)) (arr_of (fst psh))
    * arr.mem_of_line (arr_of (snd ps)) (arr_of (snd psh))"
  unfolding is_pair_state_def
(*  unfolding is_pair_line_def*)
  by simp
*)

definition lookup_pure where
  "lookup_pure st k = (
    let col'= to_col k in
    if to_row k = (tag_of (fst st)) then arr.lookup_pure col' (arr_of (fst st))
    else if to_row  k = (tag_of (snd st)) then arr.lookup_pure col'(arr_of (snd st))
    else None
  )"

lemma lookup_pure_miss:
  assumes "to_row k \<noteq> tag_of (fst ps)" "to_row k \<noteq> tag_of (snd ps)"
  shows "lookup_pure ps k = None"
  using assms unfolding lookup_pure_def by simp

definition
  "is_pair_mem maep psh = (\<exists>\<^sub>A ps.
      is_pair_state ps psh
    * \<up>(maep = lookup_pure ps))"

definition
  "init_state = do {
    m1 \<leftarrow> arr.init_empty;
    m2 \<leftarrow> arr.init_empty;
    Heap_Monad.return (Pair (Line init_row1 m1) (Line init_row2 m2))
  }"

definition empty_pair_state where
  "empty_pair_state = (Line init_row1 Map.empty, Line init_row2 Map.empty)"

lemma lookup_pure_empty[simp]:
  "lookup_pure empty_pair_state = Map.empty"
  unfolding empty_pair_state_def lookup_pure_def by (auto simp: Let_def)

lemma pair_state_valid_empty:
  "pair_state_valid empty_pair_state"
  unfolding pair_state_valid_def empty_pair_state_def
  using init_row_distinct by (auto intro: pair_line_valid_empty)

lemma is_pair_line_empty:
  "arr.mem_of_line Map.empty a \<Longrightarrow>\<^sub>A is_pair_line (Line tag Map.empty) (Line tag a)"
  (*unfolding is_pair_line_def*) by (sep_auto)

lemma init_state_spec':
  "<emp> init_state <\<lambda>psh. is_pair_state empty_pair_state psh>"
  unfolding init_state_def
  unfolding is_pair_state_def
  apply (sep_auto heap: arr.init_empty_spec intro: pair_state_valid_empty)[]
  unfolding empty_pair_state_def
  apply simp_all
  apply (subst mult.commute)
  apply (sep_auto eintros: ent_star_mono is_pair_line_empty)[]
  done

lemma init_state_spec:
  "<emp> init_state <is_pair_mem Map.empty>"
  unfolding is_pair_mem_def
  apply (sep_auto heap: init_state_spec' )
  done

sublocale init_empty
  where is_map=is_pair_mem
    and init_empty_mem=init_state
  by standard (rule init_state_spec)

definition
  "morph get_a put_a sh = do {
    st \<leftarrow> StateT_Heap_Monad.get;
    (v, arr') \<leftarrow> StateT_Heap_Monad.lift (StateT_Heap_Monad.run sh (get_a st));
    StateT_Heap_Monad.put (put_a arr' st);
    StateT_Heap_Monad.return v
  }"

definition "morph1 = morph (arr_of \<circ> fst) (apfst \<circ> put_arr)"
definition "morph2 = morph (arr_of \<circ> snd) (apsnd \<circ> put_arr)"

lemma morph_hoare1:
  assumes
    "pair_state_valid ps \<Longrightarrow> tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow> tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
       <arr.mem_of_line (arr_of (fst ps)) (arr_of (fst psh))>
         t.run x (arr_of (fst psh))
       <\<lambda>(r, ah). is_pair_line (map_arr f (fst ps)) (put_arr ah (fst psh)) * \<up>(pair_state_valid (apfst (map_arr f) ps)) * Q r (apfst (put_arr ah) psh)>"
  shows
    "<is_pair_state ps psh>
       t.run (morph1 x) psh
     <\<lambda>(r, psh'). is_pair_state (apfst (map_arr f) ps) psh' * Q r psh'>"
proof -
  have  assms1':
    "pair_state_valid ps \<Longrightarrow> tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow> tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
       <arr.mem_of_line (arr_of (fst ps)) (arr_of (fst psh))>
         t.run x (arr_of (fst psh))
       <\<lambda>(r, ah). is_pair_line (map_arr f (fst ps)) (put_arr ah (fst psh)) * \<up>(pair_state_valid (apfst (map_arr f) ps)) * \<up>(tag_of (snd ps) = tag_of (snd psh)) * Q r (apfst (put_arr ah) psh)>"
    by (sep_auto heap: assms(1))
  show ?thesis
    unfolding morph1_def morph_def
    unfolding is_pair_state_def
    by (sep_auto
      heap: assms1'
      simp: StateT_Heap_Monad.run_simps
      split: prod.splits)
qed

lemma morph_hoare2:
  assumes
    "pair_state_valid ps \<Longrightarrow> tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow> tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
       <arr.mem_of_line (arr_of (snd ps)) (arr_of (snd psh))>
         t.run x (arr_of (snd psh))
       <\<lambda>(r, ah). is_pair_line (map_arr f (snd ps)) (put_arr ah (snd psh)) * \<up>(pair_state_valid (apsnd (map_arr f) ps)) * Q r (apsnd (put_arr ah) psh)>"
  shows
    "<is_pair_state ps psh>
       t.run (morph2 x) psh
     <\<lambda>(r, psh'). is_pair_state (apsnd (map_arr f) ps) psh' * Q r psh'>"
proof -
  have  assms1':
    "pair_state_valid ps \<Longrightarrow> tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow> tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
       <arr.mem_of_line (arr_of (snd ps)) (arr_of (snd psh))>
         t.run x (arr_of (snd psh))
       <\<lambda>(r, ah). is_pair_line (map_arr f (snd ps)) (put_arr ah (snd psh)) * \<up>(pair_state_valid (apsnd (map_arr f) ps)) * \<up>(tag_of (fst ps) = tag_of (fst psh)) * Q r (apsnd (put_arr ah) psh)>"
    by (sep_auto heap: assms(1))
  show ?thesis
    unfolding morph2_def morph_def
    unfolding is_pair_state_def
    by (sep_auto
      heap: assms1'
      simp: StateT_Heap_Monad.run_simps
      split: prod.splits)
qed

definition
  "lookup k = do {
    let row' = to_row k;
    st \<leftarrow> StateT_Heap_Monad.get;
    let row1 = tag_of (fst st);
    let row2 = tag_of (snd st);
    if row' = row2 then morph2 (arr.lookup (to_col k))
    else if row' = row1 then morph1 (arr.lookup (to_col k))
    else StateT_Heap_Monad.return None
  }"

lemma map_of_pair_line_pair_state:
  assumes "pair_state_valid ps"
  shows "tag_of (fst ps) = to_row k \<Longrightarrow> lookup_pure ps k = arr.lookup_pure (to_col k) (arr_of (fst ps))"
    and "tag_of (snd ps) = to_row k \<Longrightarrow> lookup_pure ps k = arr.lookup_pure (to_col k) (arr_of (snd ps))"
  using assms
  unfolding pair_state_valid_def
  unfolding lookup_pure_def
  by auto

lemma lookup_morph1:
  assumes
    "tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow>
     tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
     tag_of (fst ps) = to_row k"
  shows "<is_pair_state ps psh>
          t.run (morph1 (arr.lookup (to_col k))) psh
         <\<lambda>(r, psh'). is_pair_state ps psh * \<up>(psh' = psh) * \<up>(r = lookup_pure ps k)>"
  by (sep_auto
      heap: morph_hoare1[where f=id and Q="\<lambda>a b. \<up>(a=lookup_pure ps k \<and> b=psh)"]
      heap: arr.lookup_spec
      dest!: assms
      simp: map_of_pair_line_pair_state
      simp: state_line.map_id0
      )

lemma lookup_morph2:
  assumes
    "tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow>
     tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
     tag_of (snd ps) = to_row k"
  shows "<is_pair_state ps psh>
          t.run (morph2 (arr.lookup (to_col k))) psh
         <\<lambda>(r, psh'). is_pair_state ps psh * \<up>(psh' = psh) * \<up>(r = lookup_pure ps k)>"
  (*unfolding is_pair_line_def*)
  by (sep_auto
      heap: morph_hoare2[where f=id and Q="\<lambda>a b. \<up>(a=lookup_pure ps k \<and> b=psh)"]
      heap: arr.lookup_spec
      dest!: assms
      simp: map_of_pair_line_pair_state
      simp: state_line.map_id0
      )

lemma lookup_pair_correct:
  "<is_pair_state ps psh>
    StateT_Heap_Monad.run (lookup k) psh
  <\<lambda>(r, psh'). is_pair_state ps psh * \<up>(psh' = psh) * \<up>(r = lookup_pure ps k)>"
  unfolding lookup_def
  apply (sep_auto
      simp: StateT_Heap_Monad.run_simps Let_def
      heap: lookup_morph1 lookup_morph2)
  apply (auto simp: is_pair_state_def lookup_pure_miss)
  done

lemma lookup_pair_correct':
  "<is_pair_mem maep st>
    StateT_Heap_Monad.run (lookup k) st
  <\<lambda>(r, st'). \<exists>\<^sub>A maep'. is_pair_mem maep' st' * \<up>(maep' \<subseteq>\<^sub>m maep) * \<up>(r = maep k)>\<^sub>t"
  unfolding is_pair_mem_def
  by (sep_auto heap: lookup_pair_correct)

sublocale forgettable_map_lookup
  where is_map=is_pair_mem
    and lookup=lookup
  by standard (rule lookup_pair_correct')

definition
  "update k v = do {
    let row' = to_row k;
    st \<leftarrow> StateT_Heap_Monad.get;
    let row1 = tag_of (fst st);
    let row2 = tag_of (snd st);
    if row1 = row' then morph1 (arr.update (to_col k) v)
    else if row2 = row' then morph2 (arr.update (to_col k) v)
    else do {
      arr' \<leftarrow> StateT_Heap_Monad.lift (arr.init_empty);
      let arr1 = arr_of (fst st);
      StateT_Heap_Monad.put (Pair (Line row' arr') (Line row1 arr1));
      morph1 (arr.update (to_col k) v)
    }
  }"

lemma update_pair_correct':
  "<is_pair_mem maep st>
    StateT_Heap_Monad.run (update k v) st
  <\<lambda>(r, st'). \<exists>\<^sub>A maep'. is_pair_mem maep' st' * \<up>(maep' \<subseteq>\<^sub>m maep(k\<mapsto>v))>\<^sub>t"
  oops
  term "apfst ( put_arr (arr_of (fst ps) (to_col\<mapsto>v))) ps"
  term "(arr_of (fst ps)) (to_col\<mapsto>v)"
  term apsnd
  thm update_def

abbreviation update_pure_line where
  "update_pure_line k v \<equiv> map_arr (arr.update_pure k v)"
(*
lemma arr_of_update_pure_line_commute[simp]:
  "arr_of (update_pure_line k v l) = arr.update_pure k v (arr_of l)"
  (*unfolding update_pure_line_def*)
  unfolding state_line.map_sel
  ..
*)
definition update_pure where
  "update_pure k v ps = (
    let row' = to_row k; col' = to_col k in
    if tag_of (fst ps) = row' then (apfst (update_pure_line col' v)) ps
    else if tag_of (snd ps) = row' then (apsnd (update_pure_line col' v)) ps
    else (Line row' (arr.update_pure col' v Map.empty), fst ps)
  )"

lemma state_line_of_put[simp]:
  "tag_of (map_arr f l) = tag_of l"
  "tag_of (map_tag p l) = p (tag_of l)"
  "arr_of (map_tag g l) = arr_of l"
  "arr_of (map_arr q l) = q (arr_of l)"
  by (cases l, simp)+

lemma update_pure_line_tag_untouched[simp]:
  "tag_of (update_pure_line col v l) = tag_of l"
  unfolding (*update_pure_line_def*) state_line_of_put ..

lemma rel_tag_map_update:
  "rel_tag_map (to_row k) (arr.update_pure (to_col k) v (arr_of l)) \<longleftrightarrow> rel_tag_map (to_row k) (arr_of l)"
  unfolding (*update_pure_line_def*) rel_tag_map_def
  by (cases l) (auto simp: arr.update_pure_dom)

lemma pair_line_valid_update_pure_line:
  "tag_of l = to_row k \<Longrightarrow> pair_line_valid (update_pure_line (to_col k) v l) = pair_line_valid l"
  unfolding pair_line_valid_def by (auto simp: rel_tag_map_update)

lemma pair_state_valid_update_pure_line:
  "tag_of (fst ps) = to_row k \<Longrightarrow> pair_state_valid (apfst (update_pure_line (to_col k) v) ps) = pair_state_valid ps"
  "tag_of (snd ps) = to_row k \<Longrightarrow> pair_state_valid (apsnd (update_pure_line (to_col k) v) ps) = pair_state_valid ps"
  unfolding pair_state_valid_def
  by (auto simp: pair_line_valid_update_pure_line)

lemma update_morph1:
  fixes psh :: "('row, 'a::heap) pair_state_h"
  assumes
    "tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow>
     tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
     tag_of (fst ps) = to_row k"
  shows
    "<is_pair_state ps psh>
      t.run (morph1 (arr.update (to_col k) v)) psh
     <\<lambda>(_, psh'). is_pair_state (apfst (update_pure_line (to_col k) v) ps) psh'>"
  by (sep_auto
      heap: morph_hoare1[where Q="\<lambda>_ _. 1"]
      heap: arr.update_spec
      dest: assms
      dest:pair_state_valid_update_pure_line[THEN iffD2]
      )

lemma update_morph2:
  fixes psh :: "('row, 'a::heap) pair_state_h"
  assumes
    "tag_of (fst ps) = tag_of (fst psh) \<Longrightarrow>
     tag_of (snd ps) = tag_of (snd psh) \<Longrightarrow>
     tag_of (snd ps) = to_row k"
  shows
    "<is_pair_state ps psh>
      t.run (morph2 (arr.update (to_col k) v)) psh
     <\<lambda>(_, psh'). is_pair_state (apsnd (update_pure_line (to_col k) v) ps) psh'>"
  by (sep_auto
      heap: morph_hoare2[where Q="\<lambda>_ _. 1"]
      heap: arr.update_spec
      dest: assms
      dest:pair_state_valid_update_pure_line[THEN iffD2]
      )


lemma lookup_pair_correct:
  "<is_pair_state ps psh>
    StateT_Heap_Monad.run (update k v) psh
  <\<lambda>(_, psh'). is_pair_state (update_pure k v ps) psh'>\<^sub>t"
  unfolding update_def
  unfolding update_pure_def
  apply (sep_auto
      simp: StateT_Heap_Monad.run_simps Let_def
      )
  subgoal by (sep_auto heap: update_morph1 update_morph2)[]
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal by (sep_auto heap: update_morph1 update_morph2)[]
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal by (sep_auto heap: update_morph1 update_morph2)[]
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal by (sep_auto heap: update_morph1 update_morph2)[]
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto
  subgoal unfolding is_pair_state_def by sep_auto

  apply (sep_auto
      heap: arr.init_empty_spec)
  
  welcome (*
  oops

sublocale row_new: forgettable_map_update
  where is_map=is_pair_mem
    and update=update
  by standard (rule update_pair_correct)

definition
  "update2 k v = do {
    let row' = to_row k;
    st \<leftarrow> StateT_Heap_Monad.get;
    let row1 = tag_of (fst st);
    let row2 = tag_of (snd st);
    if row' = row1 then morph1 (arr.update (to_col k) v)
    else if row' = row2 then morph2 (arr.update (to_col k) v)
    else do {
      let arr1 = arr_of (fst st);
      let arr2 = arr_of (snd st);
      StateT_Heap_Monad.put (Pair (Line row' arr2) (Line row1 arr1));
      morph1 arr.clear;
      morph1 (arr.update (to_col k) v)
    }
  }"

lemma update2_pair_correct:
  "<is_pair_mem maep st>
    StateT_Heap_Monad.run (update2 k v) st
  <\<lambda>(r, st'). \<exists>\<^sub>A maep'. is_pair_mem maep' st' * \<up>(maep' \<subseteq>\<^sub>m maep(k\<mapsto>v))>"
  unfolding update2_def morph1_def morph2_def morph_def
  unfolding is_pair_mem_def
  apply (cases st)
  apply (sep_auto
      simp: StateT_Heap_Monad.run_simps Let_def
      split!: prod.splits
      dest: rel_tag_map_mono'
      intro!: lookup_map_mono'
      heap: arr.update_spec
      )
   apply (rule frame_rule_left[OF arr.clear_spec])
  apply (sep_auto simp: StateT_Heap_Monad.run_simps)
   apply (rule frame_rule_left[OF arr.update_spec])
  apply (sep_auto
      simp: StateT_Heap_Monad.run_simps
      dest: rel_tag_map_mono'[OF rel_tag_map_empty]
      intro: lookup_map_mono'
      )[]
  using pair_injective
  apply (auto
      intro!: map_leI
      simp: lookup_map_simps
      split: if_splits
      dest!: map_leD
      )
  done

sublocale row_clear: forgettable_map_update_clean
  where is_map=is_pair_mem
    and update=update2
  by standard (rule update2_pair_correct)

sublocale forgettable_inited_map
  where is_map=is_pair_mem
    and lookup=lookup
    and update=update2
    and init_empty_mem=init_state
  ..

end (* locale *)

end