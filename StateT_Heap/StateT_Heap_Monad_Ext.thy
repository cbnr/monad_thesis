theory StateT_Heap_Monad_Ext
  imports
    "./StateT_Heap_Monad"
    (*"../state_monad/State_Monad_Ext"*)
    Separation_Logic_Imperative_HOL.Sep_Main
    StateT_Heap_Misc
begin
                             
type_synonym ('s, 'a) t = "('s, 'a) StateT_Heap_Monad.t"

definition fun_app_lifted :: "('M,'a \<Rightarrow> ('M, 'b) t) t \<Rightarrow> ('M,'a) t \<Rightarrow> ('M,'b) t" where
  "fun_app_lifted f\<^sub>T x\<^sub>T \<equiv> do { f \<leftarrow> f\<^sub>T; x \<leftarrow> x\<^sub>T; f x }"

bundle syn begin

notation fun_app_lifted (infixl "." 999)
type_synonym ('a,'M,'b) fun_lifted = "'a \<Rightarrow> ('M,'b) t" ("_ ==_\<Longrightarrow> _" [3,1000,2] 2)
type_synonym ('a,'b) dpfun = "'a ==('a\<rightharpoonup>'b)\<Longrightarrow> 'b" (infixr "\<Rightarrow>\<^sub>T" 2)
type_notation t ("[_| _]\<^sub>r")

notation StateT_Heap_Monad.return ("\<langle>_\<rangle>")
notation (ASCII) StateT_Heap_Monad.return ("(#_#)")
notation Transfer.Rel ("Rel")

end

context includes syn begin

qualified lemma return_app_return:
  "\<langle>f\<rangle> . \<langle>x\<rangle> = f x"
  unfolding fun_app_lifted_def bind_left_identity ..

qualified lemma return_app_return_meta:
  "\<langle>f\<rangle> . \<langle>x\<rangle> \<equiv> f x"
  unfolding return_app_return .

qualified definition if\<^sub>T :: "('s, bool) t \<Rightarrow> ('s, 'a) t \<Rightarrow> ('s, 'a) t \<Rightarrow> ('s, 'a) t" where
  "if\<^sub>T b\<^sub>T x\<^sub>T y\<^sub>T \<equiv> do {b \<leftarrow> b\<^sub>T; if b then x\<^sub>T else y\<^sub>T}"
end


hide_type t

(*
locale mem_subtype =
  fixes mem_inv :: "('mem \<times> heap) \<Rightarrow> bool"
begin

definition pred :: "('a \<Rightarrow> bool) \<Rightarrow> ('mem, 'a) StateT_Heap_Monad.t \<Rightarrow> bool" where
  "pred val_pred sh \<longleftrightarrow> (\<forall>mem heap.
    mem_inv (mem, heap) \<longrightarrow> (case Heap_Monad.execute (StateT_Heap_Monad.run sh mem) heap of
      None \<Rightarrow> False
    | Some ((val, mem'), heap') \<Rightarrow> val_pred val \<and> mem_inv (mem', heap')))"

definition rel :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> ('mem, 'b) StateT_Heap_Monad.t \<Rightarrow> bool" where
  "rel val_rel v = pred (val_rel v)"

definition inv :: "('mem, 'a) StateT_Heap_Monad.t \<Rightarrow> bool" where
  "inv = pred (\<lambda>_. True)"

lemma predI:
  assumes "\<And>m h. mem_inv (m, h) \<Longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h"
  assumes "\<And>m h v m' h'. mem_inv (m, h) \<Longrightarrow> Heap_Monad.execute (StateT_Heap_Monad.run sh m) h = Some ((v, m'), h') \<Longrightarrow>
    P v \<and> mem_inv (m', h')"
  shows "pred P sh"
  unfolding pred_def
  by (auto split: option.split dest: assms[unfolded Heap_Monad.success_def])

lemma predE:
  assumes "pred P sh" "mem_inv (m, h)"
  obtains v m' h' where "Heap_Monad.execute (StateT_Heap_Monad.run sh m) h = Some ((v, m'), h')" "P v" "mem_inv (m', h')"
  using assms(1)[unfolded pred_def, rule_format, OF assms(2)]
  by (auto split: option.split_asm)

lemma predD:
  assumes "pred P sh" "mem_inv (m, h)" "Heap_Monad.execute (StateT_Heap_Monad.run sh m) h = Some ((v, m'), h')"
  shows "P v \<and> mem_inv (m', h')"
  using assms by (auto elim: predE)

lemma invI:
  assumes "\<And>m h. mem_inv (m, h) \<Longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h"
  assumes "\<And>m h v m' h'. mem_inv (m, h) \<Longrightarrow> Heap_Monad.execute (StateT_Heap_Monad.run sh m) h = Some ((v, m'), h') \<Longrightarrow> mem_inv (m', h')"
  shows "inv sh"
  unfolding inv_def by (auto intro: predI dest: assms)

lemma invE:
  assumes "inv sh" "mem_inv (m, h)"
  obtains v m' h' where "Heap_Monad.execute (StateT_Heap_Monad.run sh m) h = Some ((v, m'), h')" "mem_inv (m', h')"
  using assms unfolding inv_def by (rule predE)

sublocale as_state: State_Monad_Ext.mem_subtype .

lemma pred_pred_as_state:
  assumes "pred P sh"
  shows "as_state.pred P (StateT_Heap_Monad.to_state sh)" "mem_inv (m, h) \<Longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h"
  using assms unfolding StateT_Heap_Monad.to_state_def
  by (auto intro!: as_state.predI successI elim: predE)

lemma pred_as_state_pred:
  assumes "\<And>m h. mem_inv (m, h) \<Longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h"
  assumes "as_state.pred P (StateT_Heap_Monad.to_state sh)"
  shows "pred P sh"
  using assms unfolding StateT_Heap_Monad.to_state_def
  by (auto intro!: predI elim: as_state.predE)

lemma pred_as_state:
  "pred P sh \<longleftrightarrow> (\<forall>m h. mem_inv (m, h) \<longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h) \<and> as_state.pred P (StateT_Heap_Monad.to_state sh)"
  by (auto intro: pred_as_state_pred pred_pred_as_state)

lemma inv_inv_as_state:
  assumes "inv sh"
  shows "as_state.inv (StateT_Heap_Monad.to_state sh)" "mem_inv (m, h) \<Longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h"
  using pred_pred_as_state[OF assms[unfolded inv_def]] unfolding as_state.inv_def .

lemma inv_as_state_inv:
  assumes "\<And>m h. mem_inv (m, h) \<Longrightarrow> Heap_Monad.success (StateT_Heap_Monad.run sh m) h"
  assumes "as_state.inv (StateT_Heap_Monad.to_state sh)"
  shows "inv sh"
  using pred_as_state_pred[OF assms[unfolded as_state.inv_def]] unfolding inv_def .

end (* locale mem_subtype *)
*)

locale mem_subtype =
  fixes mem_inv :: "'state \<Rightarrow> assn"
begin

term hoare_triple
definition pred :: "('a \<Rightarrow> bool) \<Rightarrow> ('state, 'a) StateT_Heap_Monad.t \<Rightarrow> bool" where
  "pred val_pred sh \<longleftrightarrow> (\<forall>s. <mem_inv s> StateT_Heap_Monad.run sh s <\<lambda>(v, s'). mem_inv s' * \<up>(val_pred v)>\<^sub>t)"

definition rel :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> ('state, 'b) StateT_Heap_Monad.t \<Rightarrow> bool" where
  "rel val_rel v = pred (val_rel v)"

definition inv :: "('state, 'a) StateT_Heap_Monad.t \<Rightarrow> bool" where
  "inv = pred (\<lambda>_. True)"

lemma pred_mono:
  assumes "f \<le> g" "pred f sh"
  shows "pred g sh"
  unfolding pred_def
  using assms(1)
  by (sep_auto heap: Hoare_Triple.cons_post_rule[OF assms(2)[unfolded pred_def, rule_format]])

corollary pred_to_inv:
  "inv sh" if "pred f sh"
  using that unfolding inv_def by (auto intro: pred_mono)

corollary rel_to_inv:
  "inv sh" if "rel R v sh"
  using that unfolding rel_def by (fact pred_to_inv)

lemma pred_return:
  "P v \<Longrightarrow> pred P (StateT_Heap_Monad.return v)"
  unfolding pred_def
  unfolding StateT_Heap_Monad.return_def_alt
  by sep_auto

lemma pred_bind:
  "pred Q (s \<bind> f)"
  if "pred P s" "\<And>v. P v \<Longrightarrow> pred Q (f v)"
  unfolding pred_def
  unfolding StateT_Heap_Monad.bind_def
  by (sep_auto heap: that[unfolded pred_def, rule_format])

lemma rel_bind:
  "rel S (f v) (s \<bind> g)"
  if "rel R v s" "\<And>v'. R v v' \<Longrightarrow> rel S (f v) (g v')"
  using that unfolding rel_def by (fact pred_bind)

lemma rel_bind_transfer[transfer_rule]:
  includes lifting_syntax
  shows "(rel R ===> (R ===> rel S) ===> rel S) (\<lambda>v f. f v) (\<bind>)"
  unfolding rel_fun_def by (auto elim: rel_bind)

lemma rel_fun_app_transfer[transfer_rule]:
  includes lifting_syntax syn
  shows "(rel (R ===> rel S) ===> rel R ===> rel S) (\<lambda>f v. f v) (.)"
  unfolding fun_app_lifted_def by transfer_prover

lemma rel_eq_execute:
  assumes "rel (=) v sh" "(h, as) \<Turnstile> mem_inv st"
  shows "fst (fst (the (execute (StateT_Heap_Monad.run sh st) h))) = v"
proof -
  note hd = hoare_tripleD[OF assms(1)[unfolded rel_def pred_def, rule_format] assms(2)]
  let ?rsh = "StateT_Heap_Monad.run sh st"
  have "success ?rsh h"
    using hd(1)[of None] unfolding run_fail by auto
  then obtain h' r where r: "run ?rsh (Some h) (Some h') r"
    by (auto intro: success_run)
  from r have "effect ?rsh h h' r"
    unfolding run_execute_some .
  moreover
  have "fst r = v"
    using hd(2)[OF r] by (auto split: prod.splits)
  ultimately show ?thesis
    by (simp add: effect_def)
qed

lemma inv_return:
  "inv (StateT_Heap_Monad.return v)"
  unfolding inv_def
  by (rule pred_return) (rule TrueI)

lemma inv_bind:
  "inv (s \<bind> f)"
  if "inv s" "\<And>v. inv (f v)"
  using that unfolding inv_def
  by (auto intro: pred_bind)

end

end (* theory *)