theory StateT_Heap_Monad_Loop
  imports
    "./StateT_Heap_Monad"
begin


fun sequence :: "('m, 'a) StateT_Heap_Monad.t list \<Rightarrow> ('m, 'a list) StateT_Heap_Monad.t" where
  "sequence [] = StateT_Heap_Monad.return []"
| "sequence (h#hs) = do {
    v \<leftarrow> h;
    vs \<leftarrow> sequence hs;
    StateT_Heap_Monad.return (v#vs)
  }"

lemma sequence_append:
  "sequence (hs1 @ hs2) = do {
    vs1 \<leftarrow> sequence hs1;
    vs2 \<leftarrow> sequence hs2;
    StateT_Heap_Monad.return (vs1@vs2)
  }"
  by (induction hs1) auto

definition sequence' where
  "sequence' hs = do {sequence hs; StateT_Heap_Monad.return ()}"

lemma sequence'_simps:
  "sequence' [] = StateT_Heap_Monad.return ()"
  "sequence' (h#hs) = do {h; sequence' hs}"
  unfolding sequence'_def by auto

lemma sequence'_append:
  "sequence' (hs1 @ hs2) = do {sequence' hs1; sequence' hs2}"
  unfolding sequence'_def sequence_append by simp

definition for_loop where
  "for_loop l r f = sequence [f i. i \<leftarrow> [l..<r]]"

definition for_loop' where
  "for_loop' l r f= sequence' [f i. i \<leftarrow> [l..<r]]"

lemma for_loop'_def_alt:
  "for_loop' l r f = (
    if l\<ge>r
      then StateT_Heap_Monad.return ()
      else do {f l; for_loop' (l+1) r f}
  )"
  unfolding for_loop'_def
  by (subst upt_rec) (auto simp: sequence'_simps)

lemma for_loop'_empty:
  "l \<ge> r \<Longrightarrow> for_loop' l r f = StateT_Heap_Monad.return ()"
  unfolding for_loop'_def by (auto simp: sequence'_simps)

lemma for_loop'_rear':
  "l \<le> r \<Longrightarrow> for_loop' l (Suc r) f = do {for_loop' l r f; f r; StateT_Heap_Monad.return ()}"
  unfolding for_loop'_def
  by (auto simp: sequence'_append sequence'_simps)

end