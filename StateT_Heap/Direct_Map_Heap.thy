theory Direct_Map_Heap
  imports
    "./Direct_Map_Pure"
    Refine_Imperative_HOL.IICF
    "./StateT_Heap_Monad_Ext"
    "./MyArray"
    "./StateT_Heap_Cache"
    "./StateT_Heap_Sep_Logic_Aux"
    "./StateT_Heap_Monad_Loop_Lazy"
begin

definition statet_morph :: "('lm, 'sm) StateT_Heap_Monad.t \<Rightarrow> ('sm \<Rightarrow> ('lm, unit) StateT_Heap_Monad.t) \<Rightarrow> ('sm, 'v) StateT_Heap_Monad.t \<Rightarrow> ('lm, 'v) StateT_Heap_Monad.t" where
  "statet_morph gets puts s = do {
    sm \<leftarrow> gets;
    (v, sm') \<leftarrow> StateT_Heap_Monad.lift (StateT_Heap_Monad.run s sm);
    puts sm';
    StateT_Heap_Monad.return v
  }"

definition statet_morph_heap :: "('lm \<Rightarrow> 'sm Heap) \<Rightarrow> ('sm \<Rightarrow> 'lm \<Rightarrow> unit Heap) \<Rightarrow> ('sm, 'v) StateT_Heap_Monad.t \<Rightarrow> ('lm, 'v) StateT_Heap_Monad.t" where
  "statet_morph_heap gets puts = statet_morph
    (do {dmc \<leftarrow> StateT_Heap_Monad.get; StateT_Heap_Monad.lift (gets dmc)})
    (\<lambda>sm. do {dmc \<leftarrow> StateT_Heap_Monad.get; StateT_Heap_Monad.lift (puts sm dmc)})"

definition statet_morph_fun :: "('lm \<Rightarrow> 'sm) \<Rightarrow> ('sm \<Rightarrow> 'lm \<Rightarrow> 'lm) \<Rightarrow> ('sm, 'v) StateT_Heap_Monad.t \<Rightarrow> ('lm, 'v) StateT_Heap_Monad.t" where
  "statet_morph_fun gets puts = statet_morph
    (do {dmc \<leftarrow> StateT_Heap_Monad.get; StateT_Heap_Monad.return (gets dmc)})
    (\<lambda>sm. do {dmc \<leftarrow> StateT_Heap_Monad.get; StateT_Heap_Monad.put (puts sm dmc)})"

type_synonym ('tag, 'v) dmc_heap = "('tag \<times> 'v option array) option array"

locale direct_map_cache_heap_def =
  direct_map_cache_def where split_key=split_key
for split_key :: "'key \<Rightarrow> 'tag::heap \<times>'row::index \<times> 'col::index"
begin

sublocale arr_row: array_heap row_bound .
sublocale arr_col: array_heap col_bound .

definition init_mem :: "('tag, 'v) dmc_heap Heap" where
  "init_mem = arr_row.init_mem"

definition morph_entry where
  "morph_entry i = statet_morph_heap
    (\<lambda>a. Array.nth a i)
    (\<lambda>v a. do {Array.upd i v a; Heap_Monad.return ()})"

definition morph_row where
  "morph_row = morph_entry \<circ> row_bound.checked_idx"

definition morph_tag where
  "morph_tag = statet_morph_fun fst (\<lambda>t. apfst (\<lambda>_. t))"

definition morph_data where
  "morph_data = statet_morph_fun snd (\<lambda>d. apsnd (\<lambda>_. d))"

definition morph_the where
  "morph_the = statet_morph_fun the (\<lambda>t _. Some t)"

definition lookup :: "'key \<Rightarrow> (('tag, 'v::heap) dmc_heap, 'v option) StateT_Heap_Monad.t" where
  "lookup k = do {
    let (tag, row, col) = split_key k;
    dmc \<leftarrow> StateT_Heap_Monad.get;
    morph_row row (do {
      line \<leftarrow> StateT_Heap_Monad.get;
      if line = None \<or> fst (the line) \<noteq> tag
        then StateT_Heap_Monad.return None
        else morph_the (morph_data (arr_col.lookup col))
    })
  }"

definition update :: "'key \<Rightarrow> 'val::heap \<Rightarrow> (('tag, 'val) dmc_heap, unit) StateT_Heap_Monad.t" where
  "update k v = do {
    let (tag, row, col) = split_key k;
    dmc \<leftarrow> StateT_Heap_Monad.get;
    morph_row row (do {
      line \<leftarrow> StateT_Heap_Monad.get;
      (if line = None
        then do {
          a \<leftarrow> StateT_Heap_Monad.lift (arr_col.init_mem);
          StateT_Heap_Monad.put (Some (tag, a))
        }
        else if fst (the line) \<noteq> tag
          then do {
            morph_the (morph_data (arr_col.clear));
            morph_the (morph_tag (StateT_Heap_Monad.put tag))
          }
          else StateT_Heap_Monad.return ());
      morph_the (morph_data (arr_col.update col v))
    })
  }"

end

locale direct_map_cache_heap =
  direct_map_cache where split_key=split_key
for split_key :: "'key \<Rightarrow> 'tag::heap \<times>'row::index \<times> 'col::index"
begin

print_locale direct_map_cache_pure
interpretation ap: direct_map_cache_pure ..

sublocale direct_map_cache_heap_def .

term list_assn
term option_assn
term prod_assn

definition "dmc_line_assn = option_assn (prod_assn id_assn is_array)"
definition "dmc_assn = arrayx_assn dmc_line_assn"

theorem init_mem_refine:
  "<emp>
    init_mem
   <dmc_assn ap.init_mem>"
  unfolding dmc_assn_def dmc_line_assn_def
  unfolding init_mem_def ap.init_mem_def
  unfolding ap.arr_row.init_mem_def
  unfolding arr_row.init_mem_def
  by (sep_auto
      heap: arr_row.init_mem_refine
      simp: arrayx_assn_None
      simp: is_array_def
      )

lemma
  assumes
    "i < length dmcp"
    "dmcp ! i = linep"
  shows
    "<arrayx_assn (option_assn (id_assn \<times>\<^sub>a is_array)) dmcp dmch>
      Array.nth dmch i
     <option_assn (id_assn \<times>\<^sub>a is_array) linep>"
  using assms
  unfolding arrayx_assn_def comp_assn_def
  apply (subst list_assn_aux_len)
  unfolding is_array_def
  apply sep_auto
  using Hoare_Triple.nth_rule
  
  oops

lemma
  assumes
    "i < length dmcp"
  shows
    "<arrayx_assn A dmcp dmch>
      Array.nth dmch i
     <\<lambda>lh. arrayx_assn A dmcp dmch>"
  unfolding arrayx_assn_def
  unfolding comp_assn_def
  unfolding is_array_def
  using assms
  apply (subst list_assn_aux_len)
  apply sep_auto
  done

lemma "(A \<and>\<^sub>A B) * C = (A * C) \<and>\<^sub>A (B * C)"
  unfolding inf_assn_def
  unfolding times_assn_def
  apply (rule arg_cong[where f=Abs_assn])
  apply (rule ext)
  
  apply (clarsimp simp: Abs_assn_inverse)
    apply auto
  subgoal
    
    oops

lemma "(A \<and>\<^sub>A B) * C \<Longrightarrow>\<^sub>A (A * C) \<and>\<^sub>A (B * C)"
  unfolding entails_def
  unfolding inf_assn_def
  unfolding times_assn_def
  apply clarsimp
  apply (auto simp: Abs_assn_inverse)
  done



lemma list_assn_take_nth_drop:
  assumes
    "i < length xs"
    "length xs = length ys"
  shows
    "list_assn A xs ys =
        list_assn A (take i xs) (take i ys)
      * A (xs!i) (ys!i)
      * list_assn A (drop (Suc i) xs) (drop (Suc i) ys)"
  using assms
  unfolding list_assn_def
  apply simp
  apply (subst id_take_nth_drop[OF assms(1)])
  apply (subst id_take_nth_drop[OF assms(1)[unfolded assms(2)]])
  apply simp
  apply (subst mult.assoc)
  ..

lemma list_assn_remove_nth:
  assumes
    "i < length xs"
  shows
    "list_assn A xs ys =
        \<up>(length xs = length ys)
      * A (xs!i) (ys!i)
      * list_assn A (remove_nth i xs) (remove_nth i ys)"
  apply (rule ent_iffI)
  using assms
   apply (auto simp: length_remove_nth)
  subgoal unfolding list_assn_def by simp
  subgoal
    apply (subst list_assn_aux_len)
    apply clarsimp
    apply (subst list_assn_take_nth_drop[of i xs ys A])
      apply simp_all
    unfolding remove_nth_take_drop
    apply simp
    apply (subst mult.assoc[symmetric])
    apply (subst (3) mult.commute)
    apply simp
    done
  subgoal
    apply (subst list_assn_take_nth_drop[of i xs ys A])
    apply simp_all
    unfolding remove_nth_take_drop
    apply simp
    apply (subst mult.assoc[symmetric])
    apply (subst (3) mult.commute)
    apply simp
    done
  done


lemma arrayx_assn_nth_hoare:
  assumes
    "i < length dmcp"
  shows
    "<arrayx_assn A dmcp dmch>
      Array.nth dmch i
     <\<lambda>lh. A (dmcp!i) lh * (\<exists>\<^sub>A l. is_array l dmch * \<up>(length l = length dmcp) * list_assn A (remove_nth i dmcp) (remove_nth i l))>"
  unfolding arrayx_assn_def
  unfolding comp_assn_def
  unfolding is_array_def
  using assms
  apply (subst list_assn_aux_len)
  apply (subst list_assn_remove_nth[of i dmcp A])
  subgoal .
  apply sep_auto
  done

lemma list_assn_remove_nth_update:
  assumes
    "i < length xs"
    "length xs = length ys"
  shows
    "A x y * list_assn A (remove_nth i xs) (remove_nth i ys)
      = list_assn A (xs[i:=x]) (ys[i:=y])"
  
  using assms
  apply (subst (2) list_assn_remove_nth[where i=i])
  subgoal by simp
  apply (rule ent_iffI)
   apply auto
  unfolding remove_nth_take_drop
   apply auto
  done

lemma morph_entry_hoare:
  assumes "i < length dmcp"
  assumes
    "\<And>lh. i < length dmcp \<Longrightarrow>
      <A (dmcp!i) lh>
       StateT_Heap_Monad.run h lh
      <prod_assn R A (run_state p (dmcp!i))>"
  shows
    "<arrayx_assn A dmcp dmch>
      StateT_Heap_Monad.run (morph_entry i h) dmch
     <(prod_assn R (arrayx_assn A)) (run_state (ap.morph_entry i p) dmcp)>"
  unfolding morph_entry_def ap.morph_entry_def
  unfolding statet_morph_heap_def state_morph_fun_def
  unfolding statet_morph_def state_morph_def
  apply (clarsimp
      simp: StateT_Heap_Monad.run_simps state_run_simps
      simp: prod.case_distrib prod.case_distrib[where h="\<lambda>f. f _"]
      split: prod.splits
      )
  using assms(1)
  apply (sep_auto heap: arrayx_assn_nth_hoare assms(2))
   apply (unfold is_array_def)
  apply (subst list_assn_aux_len)
   apply (sep_auto simp: length_remove_nth)
  apply sep_auto
  unfolding arrayx_assn_def comp_assn_def
  unfolding is_array_def
  apply sep_auto
  apply (subst (2) mult.assoc)
  apply (subst list_assn_remove_nth_update)
  subgoal by simp
  subgoal by simp
  apply sep_auto
  done

  
theorem lookup_refine:
  assumes
    "ap.mem_inv dmcp"
  shows
    "<dmc_assn dmcp dmch>
      StateT_Heap_Monad.run (lookup k) dmch
     <prod_assn id_assn dmc_assn (run_state (ap.lookup k) dmcp)>"
  unfolding lookup_def ap.lookup_def
  apply (clarsimp simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
  unfolding morph_row_def ap.morph_row_def
  apply clarsimp
  unfolding dmc_assn_def
  apply (sep_auto heap: morph_entry_hoare)
  subgoal
    unfolding row_bound.checked_idx_def
    using assms[unfolded ap.mem_inv_def ap.arr_row.mem_inv_def]
    by (auto split: prod.splits dest: in_bound row_bound.idx.idx_valid)
  unfolding dmc_line_assn_def
  apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
  subgoal by sep_auto
  subgoal by sep_auto
  subgoal by sep_auto
  subgoal by sep_auto
  subgoal
    unfolding pure_def
    apply (subst option_assn_aux_Some1)
    by auto
  subgoal
    
    apply (subst option_assn_aux_Some2)
    unfolding ap.morph_the_def ap.morph_data_def
    unfolding state_morph_fun_def state_morph_def
    unfolding pure_def
    by (auto simp: state_run_simps split: prod.split)

  subgoal
    unfolding ap.morph_the_def ap.morph_data_def
    unfolding state_morph_fun_def state_morph_def
    unfolding morph_the_def morph_data_def
    unfolding statet_morph_fun_def statet_morph_def
    apply  (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply (sep_auto heap: arr_col.lookup_refine)
    subgoal
      using assms[unfolded ap.mem_inv_def list_all_length]
      by auto
    apply  (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply sep_auto
    done
  done

lemma morph_the_hoare:
  assumes
    "lp \<noteq> None"
    "lp \<noteq> None \<Longrightarrow> lh \<noteq> None \<Longrightarrow>
      <P (the lp) (the lh)> StateT_Heap_Monad.run h (the lh) <prod_assn R Q (run_state s (the lp))>"
  shows
    "<option_assn P lp lh> StateT_Heap_Monad.run (morph_the h) lh <prod_assn R (option_assn Q) (run_state (ap.morph_the s) lp)>"
  apply (cases lh)
  unfolding ap.morph_the_def ap.morph_data_def
  unfolding state_morph_fun_def state_morph_def
  unfolding morph_the_def morph_data_def
  unfolding statet_morph_fun_def statet_morph_def
   apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
  subgoal using assms(1) by auto
  apply (clarsimp
      simp: StateT_Heap_Monad.run_simps state_run_simps
      simp: prod.case_distrib prod.case_distrib[where h="\<lambda>f. f _"]
      split: prod.splits
      )
  using assms
  apply sep_auto
  done

lemma morph_data_hoare:
  assumes
    "<Q (snd dp) (snd dh)>
      StateT_Heap_Monad.run h (snd dh)
     <prod_assn A R (run_state s (snd dp))>"
  shows
    "<prod_assn P Q dp dh>
      StateT_Heap_Monad.run (morph_data h) dh
    <prod_assn A (prod_assn P R) (run_state (ap.morph_data s) dp)>"
  unfolding ap.morph_the_def ap.morph_data_def
  unfolding state_morph_fun_def state_morph_def
  unfolding morph_the_def morph_data_def
  unfolding statet_morph_fun_def statet_morph_def
  apply (clarsimp
      simp: StateT_Heap_Monad.run_simps state_run_simps
      simp: prod.case_distrib prod.case_distrib[where h="\<lambda>f. f _"]
      split: prod.splits
      )
  using assms
  apply (cases dp; cases dh)
  apply sep_auto
   apply (rule frame_rule_left)
   apply assumption
  apply sep_auto
  done

lemma morph_tag_hoare:
  assumes
    "<P (fst dp) (fst dh)>
      StateT_Heap_Monad.run h (fst dh)
     <prod_assn A R (run_state s (fst dp))>"
  shows
    "<prod_assn P Q dp dh>
      StateT_Heap_Monad.run (morph_tag h) dh
    <prod_assn A (prod_assn R Q) (run_state (ap.morph_tag s) dp)>"
  unfolding ap.morph_the_def ap.morph_tag_def
  unfolding state_morph_fun_def state_morph_def
  unfolding morph_the_def morph_tag_def
  unfolding statet_morph_fun_def statet_morph_def
  apply (clarsimp
      simp: StateT_Heap_Monad.run_simps state_run_simps
      simp: prod.case_distrib prod.case_distrib[where h="\<lambda>f. f _"]
      split: prod.splits
      )
  using assms
  apply (cases dp; cases dh)
  apply sep_auto
   apply (rule frame_rule)
   apply assumption
  apply sep_auto
  done

lemma prod_unit_assn:
  "prod_assn unit_assn A ab xy = A (snd ab) (snd xy)"
  by (cases ab; cases xy) auto

theorem update_refine:
  assumes
    "ap.mem_inv dmcp"
  shows
    "<dmc_assn dmcp dmch>
      StateT_Heap_Monad.run (update k v) dmch
     <prod_assn id_assn dmc_assn (run_state (ap.update k v) dmcp)>"
  unfolding update_def ap.update_def
  apply  (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
  unfolding morph_row_def ap.morph_row_def
  apply clarsimp
  unfolding dmc_assn_def
  apply (sep_auto heap: morph_entry_hoare)
  subgoal
    unfolding row_bound.checked_idx_def
    using assms[unfolded ap.mem_inv_def ap.arr_row.mem_inv_def]
    by (auto split: prod.splits dest: in_bound row_bound.idx.idx_valid)
  unfolding dmc_line_assn_def
  unfolding StateT_Heap_Monad.run_simps state_run_simps
  unfolding return_bind
  unfolding prod.case
  subgoal for x1 a b lh
    apply (cases lh)
  apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
  subgoal
    apply (sep_auto heap: arr_col.init_mem_refine)
    unfolding ap.morph_the_def ap.morph_data_def
    unfolding state_morph_fun_def state_morph_def
    unfolding morph_the_def morph_data_def
    unfolding statet_morph_fun_def statet_morph_def
    apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply (sep_auto heap: arr_col.update_refine)
    subgoal using ap.arr_col.mem_inv_init_mem .
    apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply sep_auto
    done
  subgoal
    apply (rule morph_the_hoare[of "Some _" "Some _", simplified])
    apply (rule Hoare_Triple.cons_pre_rule) defer
     apply (rule morph_data_hoare[where dp="(_,_)" and dh="(_,_)" and P="id_assn", simplified])
     apply (rule arr_col.update_refine)
    subgoal using assms[unfolded ap.mem_inv_def list_all_length] by auto
    subgoal by auto
    done
  subgoal
    unfolding pure_def
    by auto
  subgoal
    unfolding pure_def
    by auto
  subgoal
    apply sep_auto
    apply (rule morph_the_hoare[of "Some (_,_)" "Some (_,_)", where P="prod_assn id_assn is_array", simplified])
     apply (rule morph_data_hoare[where dp="(_,_)" and dh="(_,_)" and P=id_assn and Q=is_array, simplified])
     apply (rule arr_col.clear_refine)
    subgoal using assms[unfolded ap.mem_inv_def list_all_length] by auto
    apply clarify
    
    apply (subst ap.morph_the_def)
    apply (subst ap.morph_data_def)
    apply (subst (1 2) state_morph_fun_def)
    apply (subst (1 2) state_morph_def)
    unfolding ap.arr_col.clear_mem_def
    apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply (subst ap.morph_the_def)
    apply (subst ap.morph_data_def)
    apply (subst (1 2) state_morph_fun_def)
    apply (subst (1 2) state_morph_def)
    apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply sep_auto
    apply (rule morph_the_hoare[where R=unit_assn and Q="id_assn \<times>\<^sub>a is_array"
        and s="ap.morph_tag (State_Monad.set _)"])
      apply auto
     apply (rule morph_tag_hoare[where dp="(_,_)" and dh="(_,_)" and P=id_assn and Q=is_array, simplified])
    apply (unfold pure_def)[]
    apply auto
     apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply sep_auto
    apply (subst (1 2) ap.morph_the_def)
    apply (subst (1 2) ap.morph_tag_def)
    apply (subst (1 2 3 4) state_morph_fun_def)
    apply (subst (1 2 3 4) state_morph_def)
     apply (auto simp: StateT_Heap_Monad.run_simps state_run_simps split: prod.split)
    apply (rule morph_the_hoare)
    apply auto
    apply (rule morph_data_hoare[where dp="(_,_)" and dh="(_,_)" and P="id_assn", simplified])
    apply (sep_auto heap: arr_col.update_refine simp: ap.arr_col.mem_inv_init_mem)
    unfolding prod_unit_assn prod_assn_def by auto
  done
  done

definition is_map where
  "is_map = comp_assn (\<lambda>m p. \<up>(m = ap.to_map p \<and> ap.mem_inv p)) dmc_assn"

lemma lookup_spec:
  "<is_map maep dmch>
    t.run (lookup k) dmch
   <\<lambda>(v, s'). is_map maep s' * \<up> (v = maep k)>"
  unfolding is_map_def comp_assn_def
  by (sep_auto heap: lookup_refine simp: ap.lookup_to_map pure_def)
  
print_locale forgettable_map_lookup_clean
sublocale forgettable_map_lookup_clean
  where lookup=lookup
   and is_map=is_map
  thm lookup_refine
  by standard (sep_auto heap: lookup_spec)

lemma update_spec:
  assumes
    "maep = ap.to_map dmcp"
    "ap.mem_inv dmcp"
  shows
    "<dmc_assn dmcp dmch>
    t.run (update k v) dmch
   <\<lambda>(_, s'). \<up>(ap.mem_inv (snd (run_state (ap.update k v) dmcp))) * dmc_assn (snd (run_state (ap.update k v) dmcp)) s' * \<up> (ap.to_map (snd (run_state (ap.update k v) dmcp)) \<subseteq>\<^sub>m maep(k \<mapsto> v))>"
  using assms
  unfolding is_map_def comp_assn_def
  apply auto
  apply (sep_auto heap: update_refine
      simp: ap.update_spec'
      )
  subgoal using ap.update_inv .
  subgoal unfolding pure_def prod_assn_def by (auto split: prod.splits)
  done
  
sublocale forgettable_map_update_clean
  where update=update
    and is_map=is_map
  unfolding is_map_def comp_assn_def
  by standard (sep_auto heap: update_spec)

lemma init_mem_spec:
  "<emp> init_mem <is_map Map.empty>"
  unfolding is_map_def comp_assn_def
  by (sep_auto
      heap: init_mem_refine
      intro: ap.mem_inv_init_mem
      simp: ap.init_mem_to_map
      )

sublocale init_empty
  where is_map=is_map
    and init_empty_mem=init_mem
  by standard (sep_auto heap: init_mem_spec)


end

locale dp_consistency_dmc =
  direct_map_cache_heap where split_key=split_key
for split_key :: "'key \<Rightarrow> 'tag::heap \<times> 'row::index \<times> 'col::index" +
fixes dp :: "'key \<Rightarrow> 'value::heap"
begin

sublocale dp_consistency_forgettable_inited_map
  where lookup=lookup
    and update=update
    and init_empty_mem=init_mem
    and is_map=is_map
  ..

thm memoized_execute
end

locale dp_consistency_dmc_filled =
  dp_consistency_dmc where split_key=split_key and dp=dp +
  filled_mem_def fill
for split_key :: "'key \<Rightarrow> 'tag::heap \<times> 'row::index \<times> 'col::index"
  and dp :: "'key \<Rightarrow> 'val::heap"
  and fill :: "('key \<Rightarrow> (('tag, 'val) dmc_heap, 'val) StateT_Heap_Monad.t) \<Rightarrow> _" +
assumes fill_correct: "consistentDP dp\<^sub>T \<Longrightarrow> dp_mem.inv (fill dp\<^sub>T)"
begin

print_locale init_filled_mem_def
sublocale init_filled_mem_def
  where init_empty_mem=init_mem
  .

thm init_consistentDP
lemma fill_consistentDP:
  assumes "consistentDP dp\<^sub>T"
  shows "consistentDP (filled dp\<^sub>T)"
  unfolding filled_def
  apply (rule init_consistentDP)
   apply (rule fill_correct)
   apply (fact assms)
  apply (fact assms)
  done

lemmas filled_memoized =
  memoized[OF fill_consistentDP, folded inited_filled_def, rule_format]

end

locale dp_consistency_dmc_indicies =
  dp_consistency_dmc where split_key=split_key and dp=dp +
  filled_indicies_mem_def indicies
for split_key :: "'key \<Rightarrow> 'tag::heap \<times> 'row::index \<times> 'col::index"
  and dp :: "'key \<Rightarrow> 'val::heap"
  and indicies :: "'key list"
begin


print_locale dp_consistency_dmc_filled
print_locale filled_mem_def

sublocale dp_consistency_dmc_filled
  where fill=fill
  apply standard
  unfolding fill_def
  apply (rule dp_mem.sequence'_inv)
  apply clarsimp
  apply (rule consistentDP_inv)
  .

end

lemmas [code] =
  direct_map_cache_heap_def.lookup_def
  direct_map_cache_heap_def.update_def
  direct_map_cache_heap_def.morph_data_def
  direct_map_cache_heap_def.morph_tag_def
  direct_map_cache_heap_def.morph_the_def
  direct_map_cache_heap_def.morph_entry_def
  direct_map_cache_heap_def.morph_row_def
  array_heap.init_mem_def
  array_heap.update_def
  array_heap.lookup_def
  array_heap.clear_def

end