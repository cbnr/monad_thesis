theory StateT_Heap_Sep_Logic_Aux
  imports
    Refine_Imperative_HOL.IICF
begin

lemma option_assn_aux_Some1:
  "option_assn A x (Some y) = option_assn A x (Some y) * \<up>(x \<noteq> None)"
  by (cases x) auto

lemma option_assn_aux_Some2:
  "option_assn A (Some y) x = option_assn A (Some y) x * \<up>(x \<noteq> None)"
  by (cases x) auto

lemma option_assn_aux_Some_iff:
  "option_assn A x y = option_assn A x y * \<up>(x=None \<longleftrightarrow> y=None)"
  by (cases x; cases y) auto

lemma list_assn_def:
  shows
    "list_assn A xs ys =
        \<up>(length xs = length ys)
      * (\<Prod>xy \<leftarrow> zip xs ys. case_prod A xy)"
  by (induction A xs ys rule: list_assn.induct) auto

lemma replicate_eq_Cons:
  assumes "replicate n x = y#ys"
  obtains m where "n = Suc m" "y = x" "ys = replicate m x"
  using assms
  by (cases n) auto

definition comp_assn where
  "comp_assn R1 R2 a c = (\<exists>\<^sub>A b. R1 a b * R2 b c)"

definition arrayx_assn where
  "arrayx_assn A = comp_assn (list_assn A) is_array"

lemma
  "hr_comp A S = comp_assn (\<up> \<circ>\<circ> in_rel (S\<inverse>)) A"
  unfolding hr_comp_def
  unfolding comp_assn_def
  unfolding ex_assn_def
  by auto

lemma
  "is_pure A \<Longrightarrow> array_assn A = arrayx_assn A"
  unfolding is_pure_conv
  apply clarsimp
  unfolding array_assn_def
  unfolding arrayx_assn_def
  unfolding comp_assn_def
  unfolding hr_comp_def
  unfolding list_assn_pure_conv
  apply (intro ext)
  apply clarsimp
  unfolding pure_def
  apply clarsimp
  done

lemma
  "array_assn id_assn = is_array"
  unfolding array_assn_def
  unfolding is_array_def
  by auto

lemma list_assn_None:
  "(replicate n None) = xs \<Longrightarrow> list_assn (option_assn A) xs ys = \<up>(ys = replicate n None)"
  by (induction "option_assn A" _ _ arbitrary: n rule: list_assn.induct) (auto elim: replicate_eq_Cons)

lemma arrayx_assn_None:
  "(replicate n None) = xs \<Longrightarrow> arrayx_assn (option_assn A) xs ys = is_array (replicate n None) ys"
  unfolding arrayx_assn_def comp_assn_def
  by (auto simp add: pure_def ex_assn_def list_assn_None[unfolded constraint_simps, simplified])

lemma arrayx_assn_None':
  "xs = replicate n None \<Longrightarrow> arrayx_assn (option_assn A) xs = is_array xs"
  by (auto intro: arrayx_assn_None)
term " arrayx_assn (option_assn A)"

end