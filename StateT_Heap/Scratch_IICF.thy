theory Scratch_IICF
  imports
    "IICF/IICF"
    "../Index"
begin

locale array =
  arrlim: bounded_index bound
  for bound :: "'key::index bound"
begin

definition lookup_spec where
  "lookup_spec k l = do {
    ASSERT (length l = arrlim.size);
    let i = arrlim.checked_idx k;
    RETURN (if i<arrlim.size then l!i else None)
  }"

sepref_register arrlim.size arrlim.checked_idx

lemma arrlim_checked_idx[sepref_import_param]:
  "(arrlim.checked_idx,arrlim.checked_idx)\<in>Id \<rightarrow> nat_rel"
  by auto

lemma arrlim_size[sepref_import_param]:
  "(arrlim.size,arrlim.size)\<in> nat_rel"
  by auto

sepref_definition lookup_heap is "uncurry lookup_spec" ::
  "(id_assn\<^sup>d *\<^sub>a (array_assn (option_assn id_assn))\<^sup>k)
    \<rightarrow>\<^sub>a (option_assn id_assn)"
  unfolding lookup_spec_def
  apply sepref
  done

definition update_spec where
  "update_spec k v l = do {
    ASSERT (length l = arrlim.size);
    let i = arrlim.checked_idx k;
    RETURN (if i<arrlim.size then l[i:=Some v] else l)
  }"

sepref_definition update_heap is "uncurry2 update_spec" ::
  "(id_assn\<^sup>d *\<^sub>a id_assn\<^sup>d *\<^sub>a (array_assn (option_assn id_assn))\<^sup>d)
    \<rightarrow>\<^sub>a (array_assn (option_assn id_assn))"
  unfolding update_spec_def
  apply sepref
  done

definition init_mem_spec where
  "init_mem_spec = RETURN (replicate arrlim.size None)"

sepref_definition init_mem_heap is "uncurry0 init_mem_spec" ::
  "unit_assn\<^sup>k \<rightarrow>\<^sub>a (array_assn (option_assn id_assn))"
  unfolding init_mem_spec_def
  unfolding array_fold_custom_replicate
  apply sepref
  done

definition clear_mem_spec where
  "clear_mem_spec l = nfoldli
    [0..<length l]
    (\<lambda>_. True)
    (\<lambda>i l. ASSERT (i<length l) \<then> RETURN (l[i:=None]))
    l"

sepref_definition clear_mem_heap is clear_mem_spec :: "(array_assn (option_assn id_assn))\<^sup>d \<rightarrow>\<^sub>a (array_assn (option_assn id_assn))"
  unfolding clear_mem_spec_def
  by sepref

end (* locale array *)

locale dmc =
