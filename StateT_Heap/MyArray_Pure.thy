theory MyArray_Pure
  imports
    "../state_monad/State_Monad_Ext"
    "../Index1"
    "../state_monad/State_Monad_Lens"
begin

named_theorems state_run_simps
lemma [state_run_simps]:
  "run_state (State_Monad.get \<bind> f) st = run_state (f st) st"
  unfolding State_Monad.get_def State_Monad.bind_def
  by simp

lemma [state_run_simps]:
  "run_state (State_Monad.set st' \<bind> f) st = run_state (f ()) st'"
  unfolding State_Monad.set_def State_Monad.bind_def
  by simp

lemma [state_run_simps]:
  "run_state (State_Monad.set st') st = ((), st')"
  unfolding State_Monad.set_def
  by simp

lemma [state_run_simps]:
  "run_state State_Monad.get st = (st, st)"
  unfolding State_Monad.get_def
  by simp

lemma state_run_bind:
  "run_state (x \<bind> f) s = Let (run_state x s) (\<lambda>(a,y). run_state (f a) y)"
  unfolding State_Monad.bind_def by auto

locale array_pure =
  arrlim: bounded_index bound +
  elem: mem_subtype elem_inv
  for bound :: "'key::index bound"
    and elem_inv :: "'v \<Rightarrow> bool"
begin

definition mem_inv :: "'v option list \<Rightarrow> bool" where
  "mem_inv mem \<longleftrightarrow> length mem = arrlim.the_sz \<and> (\<forall>elem. Some elem \<in> set mem \<longrightarrow> elem_inv elem)"

sublocale elem_opt: mem_subtype \<open>pred_option elem_inv\<close> .
sublocale mem_subtype mem_inv .

definition lookup :: "'key \<Rightarrow> ('v option list, 'v option) state" where
  "lookup k = do {
    let i = arrlim.checked_idx k;
    xs \<leftarrow> State_Monad.get;
    if i < arrlim.the_sz
      then State_Monad.return (xs!i)
      else State_Monad.return None
  }"

definition update :: "'key \<Rightarrow> 'v \<Rightarrow> ('v option list, unit) state" where
  "update k v = do {
    let i = arrlim.checked_idx k;
    xs \<leftarrow> State_Monad.get;
    if i < arrlim.the_sz
      then State_Monad.set (xs[i:=Some v])
      else State_Monad.return ()
  }"

definition init_mem :: "'v option list" where
  "init_mem = replicate arrlim.the_sz None"

lemma mem_inv_init_mem:
  "mem_inv init_mem"
  unfolding mem_inv_def
  unfolding init_mem_def
  by simp

definition clear_mem :: "('v option list, unit) state" where
  "clear_mem = State_Monad.set init_mem"

definition to_map where
  "to_map m k = fst (run_state (lookup k) m)"

term hoare_triple

lemma lookup_hoare:
  \<open>hoare_triple (\<lambda>m'. m'=m) (lookup k) (\<lambda>(v,m'). v=to_map m k \<and> m'=m)\<close>
  unfolding lookup_def Let_def
  supply hoare_intros = hoare_bind[OF hoare_get] hoare_if hoare_return
  by (auto intro!: hoare_intros simp: to_map_def lookup_def)

corollary lookup_result:
  "mem_inv m \<Longrightarrow> run_state (lookup k) m = (to_map m k, m)"
  apply (drule lookup_hoare[THEN hoare_tripleD, OF refl])
   apply (rule surjective_pairing)
  apply auto
  done

lemma mem_inv_upd:
  assumes \<open>mem_inv m'\<close> \<open>arrlim.checked_idx k < arrlim.the_sz\<close> \<open>elem_inv v\<close>
  shows \<open>mem_inv (m'[arrlim.checked_idx k := Some v])\<close>
  using assms unfolding mem_inv_def
  by (auto dest: set_update_subset_insert[THEN set_mp])

lemma update_hoare:
  \<open>hoare_triple (\<lambda>_. elem_inv v) (update k v) (\<lambda>_. True)\<close>
  unfolding update_def
  supply hoare_intros = hoare_bind[OF hoare_get] hoare_relax_post[OF hoare_set] hoare_let hoare_if hoare_return
  by (auto intro!: hoare_intros arrlim.in_bound_less_sz mem_inv_upd)
  
corollary update_inv:
  "mem_inv m \<Longrightarrow> elem_inv v \<Longrightarrow> mem_inv (snd (run_state (update k v) m))"
  apply (drule update_hoare[THEN hoare_tripleD])
    apply assumption
   apply (rule surjective_pairing)
  apply auto
  done

print_locale list_lens
interpretation lens: list_lens
  where list_inv=mem_inv
    and elem_inv=\<open>pred_option elem_inv\<close>
  apply standard
  subgoal premises prems for i l
    apply (cases \<open>l!i\<close>)
    subgoal by simp
    subgoal
      apply simp
      apply (rule prems(2)[unfolded mem_inv_def, THEN conjunct2, rule_format])
      using nth_mem[OF prems(1)]
      apply simp
      done
    done
  subgoal premises prems for i v l
    unfolding mem_inv_def
    apply safe
    subgoal using prems(3) unfolding mem_inv_def by simp
    subgoal 
      apply (drule set_update_subset_insert[THEN subsetD])
      using prems unfolding mem_inv_def by auto
    done
  done

definition focus where \<open>focus=list_lens_def.focus \<circ> arrlim.checked_idx\<close>

theorem hoare:
  fixes idx
  assumes "arrlim.in_the_bound idx"
    and "elem_opt.hoare_triple Psmall s Qsmall"
    and "\<And>lm. P lm \<Longrightarrow> mem_inv lm \<Longrightarrow> Psmall (list_lens_def.getf (arrlim.checked_idx idx) lm)"
    and "\<And>v sm lm.\<lbrakk>P lm; mem_inv lm; Qsmall (v, sm); pred_option elem_inv sm\<rbrakk> \<Longrightarrow>
          Q (v, list_lens_def.putf (arrlim.checked_idx idx) sm lm)"
  shows
    "hoare_triple P (focus idx s) Q"
  unfolding focus_def comp_def list_lens_def.focus_def
  apply (rule lens.hoare)
  subgoal unfolding mem_inv_def using assms(1) arrlim.in_bound_less_sz by simp
    apply fact+
  done

end

end