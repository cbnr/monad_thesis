theory Heap_Monad_Loop
  imports
    Separation_Logic_Imperative_HOL.Sep_Main
begin

fun sequence :: "'a Heap list \<Rightarrow> 'a list Heap" where
  "sequence [] = Heap_Monad.return []"
| "sequence (h#hs) = do {
    v \<leftarrow> h;
    vs \<leftarrow> sequence hs;
    Heap_Monad.return (v#vs)
  }"

lemma sequence_append:
  "sequence (hs1 @ hs2) = do {
    vs1 \<leftarrow> sequence hs1;
    vs2 \<leftarrow> sequence hs2;
    Heap_Monad.return (vs1@vs2)
  }"
  by (induction hs1) auto
  
definition sequence' where
  "sequence' hs = do {sequence hs; Heap_Monad.return ()}"

lemma sequence'_simps:
  "sequence' [] = Heap_Monad.return ()"
  "sequence' (h#hs) = do {h; sequence' hs}"
  unfolding sequence'_def by auto

lemma sequence'_append:
  "sequence' (hs1 @ hs2) = do {sequence' hs1; sequence' hs2}"
  unfolding sequence'_def sequence_append by simp

definition for_loop where
  "for_loop l r f = sequence [f i. i \<leftarrow> [l..<r]]"

definition for_loop' where
  "for_loop' l r f= sequence' [f i. i \<leftarrow> [l..<r]]"

definition fill where
  "fill l r v a = for_loop' l r (\<lambda>i. Array.upd i v a)"

lemma for_loop'_def_alt:
  "for_loop' l r f = (
    if l\<ge>r
      then Heap_Monad.return ()
      else do {f l; for_loop' (l+1) r f}
  )"
  unfolding for_loop'_def
  by (subst upt_rec) (auto simp: sequence'_simps)

lemma for_loop'_empty:
  "l \<ge> r \<Longrightarrow> for_loop' l r f = Heap_Monad.return ()"
  unfolding for_loop'_def by (auto simp: sequence'_simps)

lemma for_loop'_rear':
  "l \<le> r \<Longrightarrow> for_loop' l (Suc r) f = do {for_loop' l r f; f r; Heap_Monad.return ()}"
  unfolding for_loop'_def
  by (auto simp: sequence'_append sequence'_simps)

definition fill_list :: "nat \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "fill_list l r v xs = take l xs @ replicate ((min r (length xs))-l) v @ drop (max l r) xs"

lemma fill_list_empty:
  "l \<ge> r \<Longrightarrow> fill_list l r v xs = xs"
  unfolding fill_list_def by (auto simp: max_def)

lemma fill_list_length:
  "length (fill_list l r v xs) = length xs"
  unfolding fill_list_def by simp

lemma fill_list_extend:
  "l \<le> r \<Longrightarrow> r < length xs \<Longrightarrow> (fill_list l r v xs)[r := v] = fill_list l (Suc r) v xs"
  unfolding fill_list_def
  by (auto simp: list_update_append Suc_diff_le upd_conv_take_nth_drop replicate_app_Cons_same max_def)

lemma fill_list_full:
  "fill_list 0 (length xs) v xs = replicate (length xs) v"
  unfolding fill_list_def by auto

lemma fill_hoare':
  assumes "r \<le> length xs"
  shows "<a \<mapsto>\<^sub>a xs> fill l r v a <\<lambda>_. a \<mapsto>\<^sub>a fill_list l r v xs>"
  apply (cases "l\<le>r")
  subgoal premises prems
    using prems assms apply (induction arbitrary: xs rule: dec_induct)
    unfolding fill_def
    subgoal by (sep_auto simp: for_loop'_empty fill_list_empty)
    subgoal premises prems
      using prems by (sep_auto heap: prems(3) simp: for_loop'_rear' fill_list_length fill_list_extend)
    done
  subgoal 
    unfolding fill_def
    by (sep_auto simp: for_loop'_empty fill_list_empty)
  done

lemma fill_hoare:
  assumes "n = length xs"
  shows "<a \<mapsto>\<^sub>a xs> fill 0 n v a <\<lambda>_. a \<mapsto>\<^sub>a replicate (length xs) v>"
  unfolding assms by (sep_auto heap: fill_hoare' simp: fill_list_full)

lemma sequence_hoare_aux':
  assumes pre:  "P \<Longrightarrow>\<^sub>A I xs0"
  assumes post: "\<And>xs. \<up>(length xs = length hs) * I (xs0@xs) \<Longrightarrow>\<^sub>A Q xs"
  assumes inv:  "\<And>i xs. i < length hs \<Longrightarrow> <I xs * \<up>(length xs = i + length xs0)> hs!i <\<lambda>x. I (xs@[x]) * \<up>(length xs = i + length xs0)>"
  shows "<P> sequence hs <Q>"
  apply (rule Hoare_Triple.cons_rule[OF pre post])
  using inv apply (induction hs arbitrary: xs0)
  subgoal by sep_auto
  subgoal premises prems
    supply ps = prems(1) prems(2)[where i=0] prems(2)[where i="Suc _"] thm ps
    by (sep_auto heap: ps[simplified, rule_format])
  done

lemma sequence_hoare_aux:
  assumes pre:  "P \<Longrightarrow>\<^sub>A I xs0"
  assumes post: "\<And>xs. length xs = length hs \<Longrightarrow> I (xs0@xs) \<Longrightarrow>\<^sub>A Q xs"
  assumes inv:  "\<And>i xs. i < length hs \<Longrightarrow> length xs = i + length xs0 \<Longrightarrow> <I xs> hs!i <\<lambda>x. I (xs@[x])>"
  shows "<P> sequence hs <\<lambda>xs. Q xs * \<up>(length xs = length hs)>"
  by (sep_auto heap: sequence_hoare_aux'[where I=I, OF pre] inv eintros: post)

lemma for_loop_hoare_aux:
  assumes pre:  "P \<Longrightarrow>\<^sub>A I xs"
  assumes post: "\<And>ys. length ys = r-l \<Longrightarrow> I (xs@ys) \<Longrightarrow>\<^sub>A Q ys"
  assumes inv:  "\<And>i ys. l\<le>i \<Longrightarrow> i<r \<Longrightarrow> length ys = i-l+length xs \<Longrightarrow> <I ys> f i <\<lambda>y. I (ys@[y])>"
  shows "<P> for_loop l r f <\<lambda>xs. Q xs * \<up>(length xs = r-l)>"
  unfolding for_loop_def
  by (auto intro!: sequence_hoare_aux' assms)

lemma for_loop_hoare:
(*
  assumes pre:  "P \<Longrightarrow>\<^sub>A I []"
  assumes post: "\<And>xs. length xs = r-l \<Longrightarrow> I xs \<Longrightarrow>\<^sub>A Q xs"
*)
  assumes inv:  "\<And>i xs. l\<le>i \<Longrightarrow> i<r \<Longrightarrow> length xs = i-l \<Longrightarrow> <I xs> f i <\<lambda>x. I (xs@[x])>"
  shows "<I []> for_loop l r f <\<lambda>xs. I xs * \<up>(length xs = r-l)>"
  by (sep_auto heap: for_loop_hoare_aux inv)

end