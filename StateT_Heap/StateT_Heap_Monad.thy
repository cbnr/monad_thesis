theory StateT_Heap_Monad
  imports "HOL-Imperative_HOL.Imperative_HOL"
begin

datatype ('s, 'a) t = T (run: "'s \<Rightarrow> ('a \<times> 's) Heap")

context begin

qualified definition lift :: "'a Heap \<Rightarrow> ('s, 'a) t" where
  "lift h = T (\<lambda>mem. do {
    v \<leftarrow> h;
    Heap_Monad.return (v, mem)
  })"

qualified definition return :: "'a \<Rightarrow> ('s, 'a) t" where
  "return a = lift (Heap_Monad.return a)"

qualified definition bind :: "('s, 'a) t \<Rightarrow> ('a \<Rightarrow> ('s, 'b) t) \<Rightarrow> ('s, 'b) t" where
  "bind x f = T (\<lambda>mem. Heap_Monad.bind (run x mem) (\<lambda>(a, mem'). run (f a) mem'))"

qualified definition get :: "('s, 's) t" where
  "get = T (\<lambda>s. Heap_Monad.return (s, s))"

qualified definition put :: "'s \<Rightarrow> ('s, unit) t" where
  "put s' = T (\<lambda>_. Heap_Monad.return ((), s'))"

adhoc_overloading Monad_Syntax.bind bind

lemma return_def_alt:
  "return a = T (\<lambda>mem. Heap_Monad.return (a, mem))"
  unfolding return_def lift_def by auto

lemma bind_left_identity[simp]:
  "bind (return a) f = f a"
  unfolding return_def_alt bind_def by simp

lemma bind_right_identity[simp]:
  "bind m return = m"
  unfolding return_def_alt bind_def by simp

lemma bind_assoc[simp]:
  "bind (bind m f) g = bind m (\<lambda>x. bind (f x) g)"
  unfolding bind_def by (fastforce intro: arg_cong[where f="Heap_Monad.bind _"])

lemma bind_lift_lift:
  "lift (m \<bind> f) = lift m \<bind> (\<lambda>x. lift (f x))"
  unfolding lift_def bind_def
  by auto

lemma bind_lift_return:
  "lift (m \<bind> (\<lambda>x. Heap_Monad.return (f x))) = lift m \<bind> (\<lambda>x. return (f x))"
  unfolding return_def
  unfolding bind_lift_lift
  ..

qualified named_theorems run_simps

qualified lemma run_get[run_simps]:
  "run get s = Heap_Monad.return (s, s)"
  unfolding get_def by simp

qualified lemma run_put[run_simps]:
  "run (put s) s' = Heap_Monad.return ((), s)"
  unfolding put_def by simp

qualified lemma run_lift[run_simps]:
  "run (lift h) s = h \<bind> (\<lambda>v. Heap_Monad.return (v, s))"
  unfolding lift_def by auto

qualified lemma run_return[run_simps]:
  "run (return a) s = Heap_Monad.return (a, s)"
  unfolding return_def_alt by auto

qualified lemma run_bind[run_simps]:
  "run (bind x f) s = run x s \<bind> (\<lambda>(a, y). run (f a) y)"
  unfolding bind_def by auto

end

hide_type (open) t
hide_const (open) run T success

end