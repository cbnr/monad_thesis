theory StateT_Heap_Cache
  imports
    "../Pure_Monad"
    StateT_Heap_Monad_Ext
    StateT_Heap_Monad_Loop
begin

locale abs_mem_def =
  fixes is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"

locale forgettable_map_lookup_def = abs_mem_def is_map for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  fixes lookup :: "'key \<Rightarrow> ('state, 'value option) StateT_Heap_Monad.t"

locale forgettable_map_update_def = abs_mem_def is_map for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  fixes update :: "'key \<Rightarrow> 'value \<Rightarrow> ('state, unit) StateT_Heap_Monad.t"

locale forgettable_map_def =
  forgettable_map_lookup_def where is_map=is_map +
  forgettable_map_update_def where is_map=is_map
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"
begin

definition checkmem' :: "'key \<Rightarrow> (unit \<Rightarrow> ('state, 'value) StateT_Heap_Monad.t) \<Rightarrow> ('state, 'value) StateT_Heap_Monad.t" where
  "checkmem' k calc = do {
    v \<leftarrow> lookup k;
    case v of
      Some x \<Rightarrow> StateT_Heap_Monad.return x
    | None \<Rightarrow> do {
        v' \<leftarrow> calc ();
        update k v';
        StateT_Heap_Monad.return v'
      }
  }"

definition checkmem where
  "checkmem k calc = checkmem' k (\<lambda>_. calc)"

end

locale forgettable_map_lookup = 
  forgettable_map_lookup_def where is_map=is_map
  for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  assumes lookup_correct: "\<And>m s k.
    <is_map m s>
      StateT_Heap_Monad.run (lookup k) s
    <\<lambda>(v, s'). \<exists>\<^sub>A m'. is_map m' s' * \<up>(m' \<subseteq>\<^sub>m m) * \<up>(v = m k)>\<^sub>t"

locale forgettable_map_lookup_clean = 
  forgettable_map_lookup_def where is_map=is_map
  for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  assumes lookup_correct: "\<And>m s k.
    <is_map m s>
      StateT_Heap_Monad.run (lookup k) s
    <\<lambda>(v, s'). \<exists>\<^sub>A m'. is_map m' s' * \<up>(m' \<subseteq>\<^sub>m m) * \<up>(v = m k)>"
begin
sublocale lookup_sub: forgettable_map_lookup
  by standard (sep_auto heap: lookup_correct)
end

locale forgettable_map_lookup_clean_nondec = 
  forgettable_map_lookup_def where is_map=is_map
  for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  assumes lookup_correct: "\<And>m s k.
    <is_map m s>
      StateT_Heap_Monad.run (lookup k) s
    <\<lambda>(v, s'). \<exists>\<^sub>A m'. is_map m' s' * \<up>(m' = m) * \<up>(v = m k)>"
begin
sublocale forgettable_map_lookup_clean
  by standard (sep_auto heap: lookup_correct)
end

locale forgettable_map_update =
  forgettable_map_update_def where is_map=is_map
  for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  assumes update_correct: "\<And>m s k v.
    <is_map m s>
      StateT_Heap_Monad.run (update k v) s
    <\<lambda>(_, s'). \<exists>\<^sub>A m'. is_map m' s' * \<up>(m' \<subseteq>\<^sub>m m(k\<mapsto>v))>\<^sub>t"

locale forgettable_map_update_clean =
  forgettable_map_update_def where is_map=is_map
  for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  assumes update_correct: "\<And>m s k v.
    <is_map m s>
      StateT_Heap_Monad.run (update k v) s
    <\<lambda>(_, s'). \<exists>\<^sub>A m'. is_map m' s' * \<up>(m' \<subseteq>\<^sub>m m(k\<mapsto>v))>"
begin
sublocale update_sub: forgettable_map_update
  by standard (sep_auto heap: update_correct)
end

locale forgettable_map_update_clean_nondec =
  forgettable_map_update_def where is_map=is_map
  for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  assumes update_correct: "\<And>m s k v.
    <is_map m s>
      StateT_Heap_Monad.run (update k v) s
    <\<lambda>(_, s'). \<exists>\<^sub>A m'. is_map m' s' * \<up>(m' = m(k := None) \<or> m' = m(k\<mapsto>v))>"
begin
sublocale update_sub: forgettable_map_update_clean
  by standard (sep_auto heap: update_correct intro!: map_leI)
end

locale forgettable_map =
  forgettable_map_lookup where is_map=is_map +
  forgettable_map_update where is_map=is_map
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"
begin
sublocale forgettable_map_def .
end

locale dp_consistency = abs_mem_def is_map for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  fixes dp :: "'key \<Rightarrow> 'value"
begin

definition cmem :: "('key \<rightharpoonup> 'value) \<Rightarrow> bool" where
  "cmem m = (\<forall>k\<in>dom m. m k = Some (dp k))"

definition cmem_assn :: "'state \<Rightarrow> assn" where
  "cmem_assn s = (\<exists>\<^sub>A m. is_map m s * \<up>(cmem m))"

print_locale mem_subtype
sublocale dp_mem: mem_subtype "cmem_assn" .
thm dp_mem.rel_def[unfolded dp_mem.pred_def, THEN fun_cong, rule_format]

lemma
  assumes "dp_mem.rel (=) v sh" "(h, as) \<Turnstile> cmem_assn st"
  shows "fst (fst (the (execute (StateT_Heap_Monad.run sh st) h))) = v"
proof -
  note hd = hoare_tripleD[OF assms(1)[unfolded dp_mem.rel_def dp_mem.pred_def, rule_format] assms(2)]
  let ?rsh = "StateT_Heap_Monad.run sh st"
  have "success ?rsh h"
    using hd(1)[of None] unfolding run_fail by auto
  then obtain h' r where r: "run ?rsh (Some h) (Some h') r"
    by (auto intro: success_run)
  from r have "effect ?rsh h h' r"
    unfolding run_execute_some .
  moreover
  have "fst r = v"
    using hd(2)[OF r] by (auto split: prod.splits)
  ultimately show ?thesis
    by (simp add: effect_def)
qed


context
  includes lifting_syntax StateT_Heap_Monad_Ext.syn
begin

abbreviation rel_fun_lifted ::
  "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c \<Rightarrow> ('state, 'd) StateT_Heap_Monad.t) \<Rightarrow> bool"
  (infixr "===>\<^sub>T" 55) where
  "rel_fun_lifted R R' \<equiv> R ===> dp_mem.rel R'"

definition consistentDP :: "('key \<Rightarrow> ('state, 'value) StateT_Heap_Monad.t) \<Rightarrow> bool" where
  "consistentDP \<equiv> ((=) ===> dp_mem.rel (=)) dp"

lemma consistentDP_intro:
  assumes "\<And>param. Rel (dp_mem.rel (=)) (dp param) (dp\<^sub>T param)"
  shows "consistentDP dp\<^sub>T"
  using assms unfolding consistentDP_def Rel_def by blast

lemma consistentDP_D:
  assumes "consistentDP dp\<^sub>T"
  shows "<cmem_assn s> StateT_Heap_Monad.run (dp\<^sub>T k) s <\<lambda>(v, s'). cmem_assn s' * \<up>(dp k = v)>\<^sub>t"
  using assms
  by (auto simp: consistentDP_def dp_mem.rel_def dp_mem.pred_def dest!: rel_funD)

lemma consistentDP_inv:
  assumes "consistentDP dp\<^sub>T"
  shows "dp_mem.inv (dp\<^sub>T k)"
  using assms unfolding consistentDP_def
  by (auto dest!: rel_funD dp_mem.rel_to_inv)

lemma init_consistentDP:
  assumes
    "dp_mem.inv init"
    "consistentDP dp\<^sub>T"
  shows
    "consistentDP (\<lambda>k. init \<then> dp\<^sub>T k)"
  unfolding consistentDP_def
  apply (rule rel_funI)
  apply (rule dp_mem.rel_bind)
   apply (fastforce simp: dp_mem.rel_def intro: assms(1)[unfolded dp_mem.inv_def])
  apply (use assms(2) in \<open>fastforce simp: consistentDP_def dest: rel_funD\<close>)
  done


lemma rel_vs_return:
  "\<lbrakk>Rel R x y\<rbrakk> \<Longrightarrow> Rel (dp_mem.rel R) (Wrap x) (StateT_Heap_Monad.return y)"
  unfolding Wrap_def Rel_def
  unfolding StateT_Heap_Monad.return_def_alt
  unfolding dp_mem.rel_def dp_mem.pred_def
  by sep_auto

lemma rel_vs_return_ext:
  "\<lbrakk>Rel R x y\<rbrakk> \<Longrightarrow> Rel (dp_mem.rel R) x (StateT_Heap_Monad.return y)"
  by (fact rel_vs_return[unfolded Wrap_def])

(** Transfer rules **)
lemma return_transfer[transfer_rule]:
  "(R ===>\<^sub>T R) Wrap StateT_Heap_Monad.return"
  unfolding rel_fun_def by (metis rel_vs_return Rel_def)

lemma fun_app_lifted_transfer[transfer_rule]:
  "(dp_mem.rel (R0 ===>\<^sub>T R1) ===> dp_mem.rel R0 ===> dp_mem.rel R1) App (.)"
  unfolding App_def fun_app_lifted_def by transfer_prover

lemma rel_vs_fun_app:
  "\<lbrakk>Rel (dp_mem.rel R0) x x\<^sub>T; Rel (dp_mem.rel (R0 ===>\<^sub>T R1)) f f\<^sub>T\<rbrakk> \<Longrightarrow> Rel (dp_mem.rel R1) (App f x) (f\<^sub>T . x\<^sub>T)"
  unfolding Rel_def using fun_app_lifted_transfer[THEN rel_funD, THEN rel_funD] .

(* HOL *)
lemma if\<^sub>T_transfer[transfer_rule]:
  "(dp_mem.rel (=) ===> dp_mem.rel R ===> dp_mem.rel R ===> dp_mem.rel R) If StateT_Heap_Monad_Ext.if\<^sub>T"
  unfolding StateT_Heap_Monad_Ext.if\<^sub>T_def by transfer_prover

end (* syntax *)
end (* locale dp_consistency *)

print_locale dp_consistency
locale dp_consistency_forgettable_map =
  forgettable_map where is_map=is_map +
  dp_consistency where is_map=is_map
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"
begin

context includes StateT_Heap_Monad_Ext.syn begin
private lemma pred_lookup:
  "dp_mem.pred (\<lambda> v. v\<in>{Some (dp k), None}) (lookup k)"
  unfolding dp_mem.pred_def
  unfolding cmem_assn_def cmem_def
  apply (sep_auto heap: lookup_correct)
  by (auto dest!: map_leD simp: dom_def)

private lemma inv_update:
  "dp_mem.inv (update k (dp k))"
  unfolding dp_mem.inv_def dp_mem.pred_def
  unfolding cmem_assn_def cmem_def
  apply (sep_auto heap: update_correct)
  unfolding map_le_def Ball_def by (fastforce split: if_split_asm)

private lemma rel_vs_checkmem:
  "\<lbrakk>is_equality R; Rel (dp_mem.rel R) (dp param) s\<rbrakk>
  \<Longrightarrow> Rel (dp_mem.rel R) (dp param) (checkmem param s)"
  unfolding checkmem_def checkmem'_def Rel_def is_equality_def
  unfolding dp_mem.rel_def
  supply intros = dp_mem.pred_return dp_mem.pred_bind pred_lookup inv_update[unfolded dp_mem.inv_def]
  thm intros
  by (auto intro!: intros)

lemma rel_vs_checkmem_tupled:
  assumes "v = dp param"
  shows "\<lbrakk>is_equality R; Rel (dp_mem.rel R) v s\<rbrakk>
        \<Longrightarrow> Rel (dp_mem.rel R) v (checkmem param s)"
  unfolding assms by (fact rel_vs_checkmem)

end (* syntax *)
end (* locale dp_consistency *)

locale init_empty_def =
  fixes init_empty_mem :: "'state Heap"
begin

definition inited :: "('state, 'a) StateT_Heap_Monad.t \<Rightarrow> ('a \<times> 'state) Heap" where
  "inited sh = init_empty_mem \<bind> StateT_Heap_Monad.run sh"

end

locale init_empty =
  abs_mem_def where is_map=is_map + init_empty_def init_empty_mem
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"
  and init_empty_mem :: "'state Heap" +
assumes empty_correct: "<emp> init_empty_mem <is_map Map.empty>"

locale dp_consistency_init_empty =
  init_empty where is_map=is_map +
  dp_consistency where is_map=is_map
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"
begin

lemma cmem_empty[simp]:
  "cmem Map.empty"
  unfolding cmem_def by simp

lemma memoized:
  assumes "consistentDP dp\<^sub>T"
  shows "<emp> inited (dp\<^sub>T k) <\<lambda>(v, mem). \<up>(v = dp k) * cmem_assn mem>\<^sub>t"
  unfolding inited_def
  by (sep_auto
      heap: empty_correct
      heap: Hoare_Triple.cons_pre_rule[OF _ assms[THEN consistentDP_D]]
      simp: cmem_assn_def)

lemma memoized_execute:
  assumes "consistentDP dp\<^sub>T"
  shows "success (inited (dp\<^sub>T k)) Heap.empty"
    and "fst (fst (the (execute (inited (dp\<^sub>T k)) Heap.empty))) = dp k"
proof -
  note hd = hoare_tripleD[OF memoized[OF assms] mod_emp_simp]
  show "success (inited (dp\<^sub>T k)) Heap.empty"
    using hd(1)[of _ _ "None", unfolded run_fail] by auto
  thus "fst (fst (the (execute (inited (dp\<^sub>T k)) Heap.empty))) = dp k"
    using hd(2)[of k _ "Some _", unfolded run_execute_some]
    by (cases "execute (inited (dp\<^sub>T k)) Heap.empty") (auto simp: success_def effect_def)
qed

end (* locale *)

locale forgettable_inited_map =
  forgettable_map where is_map=is_map +
  init_empty where is_map=is_map
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"

locale dp_consistency_forgettable_inited_map =
  forgettable_inited_map where is_map=is_map +
  dp_consistency where is_map=is_map
for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn"
begin
sublocale dp_consistency_forgettable_map ..
sublocale dp_consistency_init_empty ..
end

locale clearable_map = abs_mem_def is_map for is_map :: "('key \<rightharpoonup> 'value) \<Rightarrow> 'state \<Rightarrow> assn" +
  fixes clear :: "('state, unit) StateT_Heap_Monad.t"
  assumes clear_correct: "\<And>st. <is_map Map.empty st> StateT_Heap_Monad.run clear st <\<lambda>(_, st'). is_map Map.empty st' * \<up>(st'= st)>"

locale filled_mem_def =
  fixes fill :: "('key \<Rightarrow> ('state, 'val) StateT_Heap_Monad.t) \<Rightarrow> ('state, unit) StateT_Heap_Monad.t"
begin

definition filled where
  "filled dp\<^sub>T k = fill dp\<^sub>T \<then> dp\<^sub>T k"

end

locale filled_indicies_mem_def =
  fixes indicies :: "'key list"
begin

abbreviation "fill \<equiv> (\<lambda>f. sequence' (map f indicies))"

sublocale filled_mem_def fill .

end

locale init_filled_mem_def =
  init_empty_def init_empty_mem +
  filled_mem_def fill
  for init_empty_mem :: "'state Heap" and fill :: "('key \<Rightarrow> ('state, 'val) StateT_Heap_Monad.t) \<Rightarrow> _"
begin

definition inited_filled where
  "inited_filled dp\<^sub>T k = inited (filled dp\<^sub>T k)"

end

locale init_indicies_mem_def =
  init_empty_def init_empty_mem +
  filled_indicies_mem_def indicies
  for init_empty_mem :: "'state Heap" and indicies :: "'key list"
begin

definition inited_filled where
  "inited_filled dp\<^sub>T k = inited (filled dp\<^sub>T k)"

end

end (* theory *)
