theory StateT_Heap_Direct_Map
  imports
    "./StateT_Heap_Cache"
    "./MyArray"
    "./StateT_Heap_Monad_Loop"
    "./Heap_Monad_Loop"
begin

lemma StateT_heap_Monad_MyArray_sequence':
  "StateT_Heap_Monad.lift (Heap_Monad_Loop.sequence' hs)
 = StateT_Heap_Monad_Loop.sequence' (map StateT_Heap_Monad.lift hs)"
  by (induction hs)
    (auto
      simp: StateT_Heap_Monad.return_def StateT_Heap_Monad.bind_lift_lift
      simp: Heap_Monad_Loop.sequence'_simps
      simp: StateT_Heap_Monad_Loop.sequence'_simps)


print_locale array
print_locale forgettable_map_lookup_clean
print_locale forgettable_map_update_clean
print_locale init_empty
print_locale clearable_map

datatype ('tag, 'map) cache_line = Line (tag_of: "'tag option") (data_of: 'map)

abbreviation "put_tag t c \<equiv>  case c of Line _ d \<Rightarrow> Line t d"
abbreviation "put_data d c \<equiv> case c of Line t _ \<Rightarrow> Line t d"


type_synonym ('tag, 'v) line_heap = "('tag, 'v option array) cache_line"
type_synonym ('tag, 'v) line_map  = "('tag, 'v option list) cache_line"

type_synonym ('tag, 'v) dmc_heap = "('tag, 'v) line_heap array"
type_synonym ('tag, 'v) dmc_map  = "('tag, 'v) line_map list"

instance cache_line :: (heap, heap) heap by countable_datatype

print_locale array
locale direct_map_cache =
  arr_row: array row_bound +
  arr_col: array col_bound
  for row_bound :: "'row::index bound"
    and col_bound :: "'col::index bound" +
  fixes split_key :: "'key \<Rightarrow> ('tag::heap) \<times>'row \<times> 'col"
  (*assumes split_key_inj: "inj split_key"*)
begin

abbreviation "to_tag key \<equiv> case split_key key of (tag, row, col) \<Rightarrow> tag"
abbreviation "to_row key \<equiv> case split_key key of (tag, row, col) \<Rightarrow> row"
abbreviation "to_col key \<equiv> case split_key key of (tag, row, col) \<Rightarrow> col"

definition rel_pure_heap_line :: "('tag, 'v::heap) line_map \<Rightarrow> ('tag, 'v) line_heap \<Rightarrow> assn" where
  "rel_pure_heap_line lm lh =
    \<up>(tag_of lm = tag_of lh) * data_of lh \<mapsto>\<^sub>a data_of lm"

term list_assn
definition rel_pure_heap_dmc :: "('tag, 'v::heap) dmc_map \<Rightarrow> ('tag, 'v) dmc_heap \<Rightarrow> assn" where
  "rel_pure_heap_dmc dmcm dmch = (\<exists>\<^sub>A lhs.
    dmch \<mapsto>\<^sub>a lhs * list_assn rel_pure_heap_line dmcm lhs)"

definition init_mem_line :: "('tag, 'v::heap) line_heap Heap" where
  "init_mem_line = do {
    arr \<leftarrow> Array.new arr_col.arrlim.size None;
    Heap_Monad.return (Line None arr)
  }"

definition init_mem_line_m :: "('tag, 'v) line_map" where
  "init_mem_line_m = Line None (replicate arr_col.arrlim.size None)"

definition init_mem :: "('tag::heap, 'v::heap) dmc_heap Heap" where
  "init_mem = do {
    init_lines_ls \<leftarrow> Heap_Monad_Loop.for_loop 0 arr_row.arrlim.size (\<lambda>_. init_mem_line);
    Array.of_list init_lines_ls
  }"

definition init_mem_pure :: "('tag, 'v) dmc_map" where
  "init_mem_pure = replicate arr_row.arrlim.size init_mem_line_m"

lemma init_mem_line_spec:
  "<emp> init_mem_line <rel_pure_heap_line init_mem_line_m>"
  unfolding init_mem_line_def init_mem_line_m_def
  unfolding rel_pure_heap_line_def
  by sep_auto

definition list_assn1 where
  "list_assn1 A xs = list_assn (\<lambda>x y. \<up>(x=y) * A x) xs xs"

lemma list_assn1_simps[simp]:
  "list_assn1 A [] = emp"
  "list_assn1 A (x#xs) = A x * list_assn1 A xs"
  unfolding list_assn1_def by auto

lemma list_assn1_curry:
  "length ys = n \<Longrightarrow> list_assn1 (R x) ys = list_assn R (replicate n x) ys"
  by (induction ys arbitrary: n) auto

lemma list_assn1_append:
  "list_assn1 A (xs@ys) = list_assn1 A xs * list_assn1 A ys"
  unfolding list_assn1_def by auto

lemma init_mem_spec:
  "<emp> init_mem <rel_pure_heap_dmc init_mem_pure>"
  unfolding init_mem_def init_mem_pure_def
  unfolding rel_pure_heap_dmc_def
  supply inv_hoare = for_loop_hoare[where I="\<lambda>xs. list_assn1 (rel_pure_heap_line init_mem_line_m) xs"]
  by (sep_auto
      heap: Hoare_Triple.cons_pre_rule[OF _ inv_hoare] init_mem_line_spec
      simp: list_assn1_append list_assn1_curry)

definition lookup_pure :: "'key \<Rightarrow> ('tag, 'v) dmc_map \<Rightarrow> 'v option" where
  "lookup_pure k dmcm = (
    let
      (tag, row, col) = split_key k;
      line = dmcm ! (arr_row.arrlim.checked_idx row)
    in
      if tag_of line = Some tag
        then data_of line ! (arr_col.arrlim.checked_idx col)
        else None
  )"

definition morph where
  "morph gets puts sh = do {
    dmc \<leftarrow> StateT_Heap_Monad.get;
    sm \<leftarrow> gets dmc;
    (v, sm') \<leftarrow> StateT_Heap_Monad.lift (StateT_Heap_Monad.run sh sm);
    puts sm' dmc;
    StateT_Heap_Monad.return v
  }"

definition morph_entry :: "nat \<Rightarrow> (('tag, 'v) line_heap, 'a) StateT_Heap_Monad.t \<Rightarrow> (('tag, 'v) dmc_heap, 'a) StateT_Heap_Monad.t" where
  "morph_entry r = morph
    (\<lambda>dmc. StateT_Heap_Monad.lift (Array.nth dmc r))
    (\<lambda>line dmc. StateT_Heap_Monad.lift (Array.upd r line dmc))"

lemma morph_entry_def_alt:
  "morph_entry r sh = do {
    dmc \<leftarrow> StateT_Heap_Monad.get;
    line \<leftarrow> StateT_Heap_Monad.lift (Array.nth dmc r);
    (v, line') \<leftarrow> StateT_Heap_Monad.lift (StateT_Heap_Monad.run sh line);
    StateT_Heap_Monad.lift (Array.upd r line' dmc);
    StateT_Heap_Monad.return v
  }"
  unfolding morph_entry_def
  unfolding morph_def
  by auto

definition morph_row where
  "morph_row row = morph_entry (arr_row.arrlim.checked_idx row)"

definition morph_data :: "('v option array, 'a) StateT_Heap_Monad.t \<Rightarrow> (('tag, 'v) line_heap, 'a) StateT_Heap_Monad.t" where
  "morph_data = morph
    (StateT_Heap_Monad.return \<circ> data_of)
    (StateT_Heap_Monad.put \<circ>\<circ> put_data)"

definition morph_tag :: "('tag option, 'a) StateT_Heap_Monad.t \<Rightarrow> (('tag, 'v) line_heap, 'a) StateT_Heap_Monad.t" where
  "morph_tag = morph
    (StateT_Heap_Monad.return \<circ> tag_of)
    (StateT_Heap_Monad.put \<circ>\<circ> put_tag)"

lemma morph_data_def_alt:
  "morph_data sh = do {
    line \<leftarrow> StateT_Heap_Monad.get;
    let data = data_of line;
    (v, data') \<leftarrow> StateT_Heap_Monad.lift (StateT_Heap_Monad.run sh data);
    StateT_Heap_Monad.put (put_data data' line);
    StateT_Heap_Monad.return v
  }"
  unfolding morph_data_def
  unfolding morph_def
  by auto

definition lookup :: "'key \<Rightarrow> (('tag, 'v::heap) dmc_heap, 'v option) StateT_Heap_Monad.t" where
  "lookup k = do {
    let (tag, row, col) = split_key k;
    morph_row row (do {
      line \<leftarrow> StateT_Heap_Monad.get;
      if tag_of line = Some tag
        then morph_data (arr_col.lookup col)
        else StateT_Heap_Monad.return None
    })
  }"

thm morph_def
definition morph_pure where
  "morph_pure gets puts f dmc =
    let
      sm = gets dmc;
      sm' = f sm;
      dmc' = puts sm' dmc"

definition lookup_pure :: "'key \<Rightarrow> ('tag, 'v) dmc_map \<Rightarrow> ('tag, 'v) dmc_map" where
  "lookup_pure k dmc = (
    let
      (tag, row, col) = split_key k in
    
  )"
welcome (*

definition update :: "'key \<Rightarrow> 'v::heap \<Rightarrow> (('tag, 'v) dmc_heap, unit) StateT_Heap_Monad.t" where
  "update k v = do {
    let (tag, row, col) = split_key k;
    morph_row row (do {
      line \<leftarrow> StateT_Heap_Monad.get;
      if tag_of line = Some tag
        then morph_data (arr_col.update col v)
        else do {
          morph_data arr_col.clear;
          morph_tag (StateT_Heap_Monad.put (Some tag))
        }
    })
  }"

definition clear :: "(('tag, 'v::heap) dmc_heap, unit) StateT_Heap_Monad.t" where
  "clear = for_loop' 0 arr_row.arrlim.size (\<lambda>r. morph_entry r (morph_tag (StateT_Heap_Monad.put None)))"

lemma clear_def_alt:
  "clear = do {
    dmh \<leftarrow> StateT_Heap_Monad.get;
    StateT_Heap_Monad.lift (
      Heap_Monad_Loop.for_loop' 0 arr_row.arrlim.size (\<lambda>i. do {
        line \<leftarrow> Array.nth dmh i;
        Array.upd i (put_tag None line) dmh
      })
    )
  }"
  oops


end


end