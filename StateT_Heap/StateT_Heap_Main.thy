theory StateT_Heap_Main
  imports
    "../transform/Transform_Cmd"
    "./Direct_Map_Heap"
    "./StateT_Heap_Monad_Loop_Lazy"
begin

local_setup \<open>
let
  val monad_name = "StateT_Heap"

  val memT = TFree ("MemoryType", @{sort type})
  val memT = dummyT

  fun mk_stateT tp =
    Type (@{type_name StateT_Heap_Monad.t}, [memT, tp])

  val returnN = @{const_name StateT_Heap_Monad.return}
  fun return tm = Const (returnN, dummyT --> mk_stateT dummyT) $ tm

  val appN = @{const_name StateT_Heap_Monad_Ext.fun_app_lifted}
  fun app (tm0, tm1) = Const (appN, dummyT) $ tm0 $ tm1

  fun checkmem'C ctxt = Transform_Misc.get_const_pat ctxt "checkmem'"
  fun checkmem' ctxt param body = checkmem'C ctxt $ param $ body

  val checkmemVN = "checkmem"
  val checkmemC = @{const_name "forgettable_map_def.checkmem"}

  fun rewrite_app_beta_conv ctm =
    case Thm.term_of ctm of
      Const (@{const_name StateT_Heap_Monad_Ext.fun_app_lifted}, _)
        $ (Const (@{const_name StateT_Heap_Monad.return}, _) $ Abs _)
        $ (Const (@{const_name StateT_Heap_Monad.return}, _) $ _)
        => Conv.rewr_conv @{thm StateT_Heap_Monad_Ext.return_app_return_meta} ctm
    | _ => Conv.no_conv ctm
in
  Transform_Const.add_monad_const {
    monad_name = monad_name,
    mk_stateT = mk_stateT,
    return = return,
    app = app,
    if_termN = @{const_name StateT_Heap_Monad_Ext.if\<^sub>T},
    checkmemVN = checkmemVN,
    rewrite_app_beta_conv = rewrite_app_beta_conv
  }
end
\<close>

context includes StateT_Heap_Monad_Ext.syn begin

thm if_cong
lemma ifT_cong:
  assumes "b = c" "c \<Longrightarrow> x = u" "\<not>c \<Longrightarrow> y = v"
  shows "StateT_Heap_Monad_Ext.if\<^sub>T \<langle>b\<rangle> x y = StateT_Heap_Monad_Ext.if\<^sub>T \<langle>c\<rangle> u v"
  unfolding StateT_Heap_Monad_Ext.if\<^sub>T_def
  unfolding StateT_Heap_Monad.bind_left_identity
  using if_cong[OF assms] .

lemma return_app_return_cong:
  assumes "f x = g y"
  shows "\<langle>f\<rangle> . \<langle>x\<rangle> = \<langle>g\<rangle> . \<langle>y\<rangle>"
  unfolding StateT_Heap_Monad_Ext.return_app_return_meta assms ..

lemmas [fundef_cong] =
  return_app_return_cong
  ifT_cong
end

dp_fun comp\<^sub>T: comp monadifies (StateT_Heap) comp_def
lemma (in dp_consistency) comp\<^sub>T_transfer[transfer_rule]:
  "dp_mem.rel ((R1 ===>\<^sub>T R2) ===>\<^sub>T (R0 ===>\<^sub>T R1) ===>\<^sub>T (R0 ===>\<^sub>T R2)) comp comp\<^sub>T"
  apply dp_combinator_init
  subgoal premises IH [transfer_rule] thm IH by dp_unfold_defs transfer_prover
  done

dp_fun map\<^sub>T: map monadifies (StateT_Heap) list.map
lemma (in dp_consistency) map\<^sub>T_transfer[transfer_rule]:
  "dp_mem.rel ((R0 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T list_all2 R1) map map\<^sub>T"
  apply dp_combinator_init
  apply (erule list_all2_induct)
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  done

dp_fun fold\<^sub>T: fold monadifies (StateT_Heap) fold.simps
lemma (in dp_consistency) fold\<^sub>T_transfer[transfer_rule]:
  "dp_mem.rel ((R0 ===>\<^sub>T R1 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T R1 ===>\<^sub>T R1) fold fold\<^sub>T"
  apply dp_combinator_init
  apply (erule list_all2_induct)
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  done

context includes StateT_Heap_Monad_Ext.syn begin

thm map_cong
lemma mapT_cong:
  assumes "xs = ys" "\<And>x. x\<in>set ys \<Longrightarrow> f x = g x"
  shows "map\<^sub>T . \<langle>f\<rangle> . \<langle>xs\<rangle> = map\<^sub>T . \<langle>g\<rangle> . \<langle>ys\<rangle>"
  unfolding map\<^sub>T_def 
  unfolding assms(1)
  using assms(2) by (induction ys) (auto simp: StateT_Heap_Monad_Ext.return_app_return_meta)

thm fold_cong
lemma foldT_cong:
  assumes "xs = ys" "\<And>x. x\<in>set ys \<Longrightarrow> f x = g x"
  shows "fold\<^sub>T . \<langle>f\<rangle> . \<langle>xs\<rangle> = fold\<^sub>T . \<langle>g\<rangle> . \<langle>ys\<rangle>"
  unfolding fold\<^sub>T_def
  unfolding assms(1)
  using assms(2) by (induction ys) (auto simp: StateT_Heap_Monad_Ext.return_app_return_meta)

lemma abs_unit_cong:
  (* for lazy checkmem *)
  assumes "x = y"
  shows "(\<lambda>_::unit. x) = (\<lambda>_. y)"
  using assms ..

lemmas [fundef_cong] =
  return_app_return_cong
  ifT_cong
  mapT_cong
  foldT_cong
  abs_unit_cong
end

context dp_consistency begin
context includes lifting_syntax StateT_Heap_Monad_Ext.syn begin

named_theorems dp_match_rule

thm if_cong
lemma if\<^sub>T_cong2:
  assumes "Rel (=) b c" "c \<Longrightarrow> Rel (dp_mem.rel R) x x\<^sub>T" "\<not>c \<Longrightarrow> Rel (dp_mem.rel R) y y\<^sub>T"
  shows "Rel (dp_mem.rel R) (if (Wrap b) then x else y) (StateT_Heap_Monad_Ext.if\<^sub>T \<langle>c\<rangle> x\<^sub>T y\<^sub>T)"
  using assms unfolding StateT_Heap_Monad_Ext.if\<^sub>T_def bind_left_identity Rel_def Wrap_def
  by (auto split: if_split)

lemma map\<^sub>T_cong2:
  assumes
    "is_equality R"
    "Rel R xs ys"
    "\<And>x. x\<in>set ys \<Longrightarrow> Rel (dp_mem.rel S) (f x) (f\<^sub>T' x)"
  shows "Rel (dp_mem.rel (list_all2 S)) (App (App map (Wrap f)) (Wrap xs)) (map\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  unfolding map\<^sub>T_def
  unfolding StateT_Heap_Monad_Ext.return_app_return_meta
  unfolding assms(2)[unfolded Rel_def assms(1)[unfolded is_equality_def]]
  using assms(3)
  unfolding Rel_def Wrap_def App_def
  apply (induction ys)
  subgoal premises by (dp_unfold_defs (StateT_Heap) map) transfer_prover
  subgoal premises prems for a ys
  apply (dp_unfold_defs (StateT_Heap) map)
    apply (unfold StateT_Heap_Monad_Ext.return_app_return_meta Wrap_App_Wrap)
    supply [transfer_rule] =
      prems(2)[OF list.set_intros(1)]
      prems(1)[OF prems(2)[OF list.set_intros(2)], simplified]
    by transfer_prover
  done

lemma fold\<^sub>T_cong2:
  assumes
    "is_equality R"
    "Rel R xs ys"
    "\<And>x. x\<in>set ys \<Longrightarrow> Rel (dp_mem.rel (S ===> dp_mem.rel S)) (f x) (f\<^sub>T' x)"
  shows
    "Rel (dp_mem.rel (S ===> dp_mem.rel S)) (fold f xs) (fold\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  unfolding fold\<^sub>T_def
  unfolding StateT_Heap_Monad_Ext.return_app_return_meta
  unfolding assms(2)[unfolded Rel_def assms(1)[unfolded is_equality_def]]
  using assms(3)
  unfolding Rel_def
  apply (induction ys)
  subgoal premises by (dp_unfold_defs (StateT_Heap) fold) transfer_prover
  subgoal premises prems for a ys
    apply (dp_unfold_defs (StateT_Heap) fold)
    apply (unfold StateT_Heap_Monad_Ext.return_app_return_meta Wrap_App_Wrap)
    supply [transfer_rule] =
      prems(2)[OF list.set_intros(1)]
      prems(1)[OF prems(2)[OF list.set_intros(2)], simplified]
    by transfer_prover
  done

lemma refl2:
  "is_equality R \<Longrightarrow> Rel R x x"
  unfolding is_equality_def Rel_def by simp

lemma rel_fun2:
  assumes "is_equality R0" "\<And>x. Rel R1 (f x) (g x)"
  shows "Rel (rel_fun R0 R1) f g"
  using assms unfolding is_equality_def Rel_def by auto

lemma crel_vs_return_app_return:
  assumes "Rel R (f x) (g x)"
  shows "Rel R (App (Wrap f) (Wrap x)) (\<langle>g\<rangle> . \<langle>x\<rangle>)"
  using assms unfolding StateT_Heap_Monad_Ext.return_app_return_meta Wrap_App_Wrap .

thm option.case_cong[no_vars]
lemma option_case_cong':
"Rel (=) option' option \<Longrightarrow>
(option = None \<Longrightarrow> Rel R f1 g1) \<Longrightarrow>
(\<And>x2. option = Some x2 \<Longrightarrow> Rel R (f2 x2) (g2 x2)) \<Longrightarrow>
Rel R (case option' of None \<Rightarrow> f1 | Some x2 \<Rightarrow> f2 x2)
(case option of None \<Rightarrow> g1 | Some x2 \<Rightarrow> g2 x2)"
  unfolding Rel_def by (auto split: option.split)

thm prod.case_cong[no_vars]
lemma prod_case_cong': fixes prod prod' shows
"Rel (=) prod prod' \<Longrightarrow>
(\<And>x1 x2. prod' = (x1, x2) \<Longrightarrow> Rel R (f x1 x2) (g x1 x2)) \<Longrightarrow>
Rel R (case prod of (x1, x2) \<Rightarrow> f x1 x2)
(case prod' of (x1, x2) \<Rightarrow> g x1 x2)"
  unfolding Rel_def by (auto split: prod.splits)

thm nat.case_cong[no_vars]
lemma nat_case_cong': fixes nat nat' shows
"Rel (=) nat nat' \<Longrightarrow>
(nat' = 0 \<Longrightarrow> Rel R f1 g1) \<Longrightarrow>
(\<And>x2. nat' = Suc x2 \<Longrightarrow> Rel R (f2 x2) (g2 x2)) \<Longrightarrow>
Rel R (case nat of 0 \<Rightarrow> f1 | Suc x2 \<Rightarrow> f2 x2) (case nat' of 0 \<Rightarrow> g1 | Suc x2 \<Rightarrow> g2 x2)"
  unfolding Rel_def by (auto split: nat.splits)
  

lemmas [dp_match_rule] =
  prod_case_cong'
  option_case_cong'
  nat_case_cong'


lemmas [dp_match_rule] =
  crel_vs_return_app_return

lemmas [dp_match_rule] =
  map\<^sub>T_cong2
  fold\<^sub>T_cong2
  if\<^sub>T_cong2

lemmas [dp_match_rule] =
  rel_vs_return
  rel_vs_fun_app
  refl2
  rel_fun2

(*
lemmas [dp_match_rule] =
  crel_vs_checkmem_tupled
*)

end (* context lifting_syntax *)
end (* context dp_consistency *)

lemmas [code_unfold] =
  forgettable_map_def.checkmem_def
lemmas [code] =
  forgettable_map_def.checkmem'_def

lemmas [code_unfold] =
  map\<^sub>T_def
end (* theory *)