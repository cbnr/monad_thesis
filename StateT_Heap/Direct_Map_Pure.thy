theory Direct_Map_Pure
  imports
    "HOL-Library.State_Monad"
    "./MyArray_Pure"
    "../state_monad/State_Monad_Lens"
begin
chapter \<open>Direct Mapped Cache, Pure\<close>

text \<open>
 [1] Wimmer, S., Hu, S., & Nipkow, T. (2018, July). Verified memoization and dynamic programming. ITP2018
 [2] https://en.wikipedia.org/wiki/Cache_placement_policies#Direct-mapped_cache

 This theory is a generalization of [1], \<section>2.9 “Memory Implementations”.

 In the context of system programming, a “Direct-mapped cache”[2] is an array of records with three fields:
 boolean “isValid”, address “tag”, value array of fixed length “line”:

   cache = [
    {isValid1, tag1, line1[...]},
    {isValid2, tag2, line2[...]},
    ...
    {isValidN, tagN, lineN[...]}
   ]

 When accessing the 32-bit memory at, say, 0xABBBCCCC, and suppose the system divides all the 32-bits address into
 1*4 bits of tag (0xA), 3*4 bits of index (0xBBB), and 4*4 bits of offset(0xCCCC), the data is then retrieved
 from the cache in the following procedure

   if \<not>cache[0xBBB].isValid \<or> cache[0xBBB].tag \<noteq> 0xA
     cache[0xBBB] \<leftarrow> {
       isValid = true,
       tag = 0xA,
       line = loadFromMemory(0xABBB0000 \<dots> 0xABBBFFFF)
     }
   return cache[0xBBB].line[0xCCCC]

 Initially, all isValid is false an all tags and lines are random garbage.

 [1] \<section>2.9 is effectively a special case of a direct-mapped cache with 2 cache lines.

 In the following code, we use a less alien terminology: we call the offset into cache-line “column”
 (0xCCCC in the above example), and we call the index into the cache “row” (0xBBB).
\<close>

text \<open>
 This theory is an abstract of and the basis for further refinement into the StateT Heap implementation.

 The “morph”s are helpers for encoding the separation properties into the structure of the codes, which
 are convenient or arguably necessary for the proofs based on separation logic.

 The motivating use case is the “morph_entry”. Without it, we would write something like:

  entry \<leftarrow> cache[index]
  if entry.tag valid:
    v \<leftarrow> entry.line[offset]
  otherwise:
    v \<leftarrow> call dp(...); entry.line[offset] \<leftarrow> v
  return v

 The code looks fine, but the carefully crafted separation predicates are often too fragile for this
 kind of randomly scattered memory access. To address this issue, we introduce helpers that handle
 the read/write of "small memory" within "large memory" and take as argument the detailed procedure
 applied on the small memory. The code above can then be separated into the helper part:

   entry \<leftarrow> cache[index]
   (value, new_entry) \<leftarrow> do_something_about_the_entry_and_return_something
   cache[index] \<leftarrow> new_entry
   return value

 and

  do_something_about_the_entry_and_return_something:
    if entry.tag valid:
      return entry.line[offset]
    otherwise:
      v \<leftarrow> call dp(...); entry.line[offset] \<leftarrow> v; return v

 then we can prove hoare triples separately for the array read/write helper and for the operation
 on a specific array entry, so that the proof becomes a bit less than a nightmare.
\<close>
(*
definition state_morph_fun :: "('lm \<Rightarrow> 'sm) \<Rightarrow> ('sm \<Rightarrow> 'lm \<Rightarrow> 'lm) \<Rightarrow> ('sm, 'v) state \<Rightarrow> ('lm, 'v) state" where
  "state_morph_fun gets puts = state_morph
    (do {dmc \<leftarrow> State_Monad.get; State_Monad.return (gets dmc)})
    (\<lambda>sm. do {dmc \<leftarrow> State_Monad.get; State_Monad.set (puts sm dmc)})"
*)
type_synonym ('tag, 'v) dmc_pure = "('tag \<times> 'v option list) option list"

locale direct_map_cache_def =
  row_bound: bounded_index row_bound +
  col_bound: bounded_index col_bound
  for row_bound :: "'row::index bound"
    and col_bound :: "'col::index bound" +
  fixes split_key :: "'key \<Rightarrow> 'tag \<times>'row \<times> 'col"

locale direct_map_cache =
  direct_map_cache_def where split_key=split_key
for split_key :: "'key \<Rightarrow> 'tag \<times>'row::index \<times> 'col::index" +
assumes split_key_inj: "inj split_key"
assumes in_bound: "\<And>k tag row col. split_key k = (tag, row, col) \<Longrightarrow>
      row_bound.in_the_bound row"

locale direct_map_cache_pure =
  direct_map_cache where split_key=split_key
for split_key :: "'key \<Rightarrow> 'tag \<times>'row::index \<times> 'col::index"
begin

print_locale array_pure
sublocale arr_col: array_pure where bound=col_bound and elem_inv=\<open>\<lambda>_. True\<close> .
sublocale arr_row: array_pure where bound=row_bound and elem_inv=\<open>\<lambda>(_,v). arr_col.mem_inv v\<close> .

definition init_mem :: "('tag, 'v) dmc_pure" where
  "init_mem = arr_row.init_mem"

definition mem_inv :: "('tag, 'v) dmc_pure \<Rightarrow> bool" where
  \<open>mem_inv = arr_row.mem_inv\<close>

(*
definition mem_inv :: "('tag, 'v) dmc_pure \<Rightarrow> bool" where
  "mem_inv dmc \<longleftrightarrow>
      arr_row.mem_inv dmc
    \<and> (\<forall>tag line. Some (tag, line) \<in> set dmc \<longrightarrow> arr_col.mem_inv line)"

sublocale mem_subtype mem_inv .

lemma mem_inv_def_alt:
  "mem_inv dmc \<longleftrightarrow>
      arr_row.mem_inv dmc
    \<and> list_all (pred_option (pred_prod (\<lambda>_.True) arr_col.mem_inv)) dmc"
  unfolding mem_inv_def list_all_iff pred_option_def pred_prod_beta
  apply safe
  subgoal for entry by (cases \<open>entry\<close>) auto
  subgoal by auto
  done
*)
(*
definition morph_entry where
  "morph_entry i = state_morph_fun (\<lambda>dmc. dmc!i) (\<lambda>l dmc. dmc[i:=l])"

definition morph_row where
  "morph_row = morph_entry \<circ> row_bound.checked_idx"

definition morph_tag where
  "morph_tag = state_morph_fun fst (\<lambda>t. apfst (\<lambda>_. t))"

definition morph_data where
  "morph_data = state_morph_fun snd (\<lambda>d. apsnd (\<lambda>_. d))"

definition morph_the where
  "morph_the = state_morph_fun the (\<lambda>t _. Some t)"
*)
(*
context
  fixes row::'row
  assumes assms: \<open>row_bound.in_the_bound row\<close>
begin

print_locale state_fun_lens
interpretation row_lens: state_fun_lens
  where getf=\<open>\<lambda>dmc. dmc!row_bound.checked_idx row\<close>
    and putf=\<open>\<lambda>r dmc. dmc[row_bound.checked_idx row := r]\<close>
    and lm_inv=mem_inv
    and sm_inv=\<open>pred_option (pred_prod (\<lambda>_.True) arr_col.mem_inv)\<close>
  apply standard
  subgoal
    unfolding mem_inv_def_alt arr_row.mem_inv_def list_all_length
    using row_bound.in_bound_less_sz[OF assms] by auto
  subgoal
    unfolding mem_inv_def
using row_bound.in_bound_less_sz[OF assms]    apply auto
   

interpretation row_lens: state_fun_lens
  where i=\<open>row_bound.checked_idx row\<close>
    and sm_inv=arr_row.mem_inv
    and sz=row_bound.the_sz
  for row
  apply unfold_locales
  apply (rule row_bound.in_bound_less_sz)
  apply (fact assms)
  done
end

term row_lens.focus
*)
definition lookup :: "'key \<Rightarrow> (('tag, 'v) dmc_pure, 'v option) state" where
  "lookup k = do {
    let (tag, row, col) = split_key k;
    dmc \<leftarrow> State_Monad.get;
    arr_row.focus row (do {
      line \<leftarrow> State_Monad.get;
      if line = None \<or> fst (the line) \<noteq> tag
        then State_Monad.return None
        else the_lens_focus (snd_lens_focus (arr_col.lookup col))
    })
  }"

definition update :: "'key \<Rightarrow> 'val \<Rightarrow> (('tag, 'val) dmc_pure, unit) state" where
  "update k v = do {
    let (tag, row, col) = split_key k;
    dmc \<leftarrow> State_Monad.get;
    arr_row.focus row (do {
      line \<leftarrow> State_Monad.get;
      (if line = None
        then State_Monad.set (Some (tag, arr_col.init_mem))
        else if fst (the line) \<noteq> tag
          then the_lens_focus (do {
            snd_lens_focus arr_col.clear_mem;
            fst_lens_focus (State_Monad.set tag)
          })
          else State_Monad.return ());
      the_lens_focus (snd_lens_focus (arr_col.update col v))
    })
  }"

definition to_map where
  "to_map dmc = (\<lambda>k. fst (run_state (lookup k) dmc))"

lemma lookup_to_map:
  \<open>arr_row.hoare_triple (\<lambda>dmc'. dmc'=dmc) (lookup k) (\<lambda>(v,dmc'). v=to_map dmc k \<and> dmc'=dmc)\<close>
proof -
  obtain tag row col where split_k: \<open>split_key k = (tag, row, col)\<close>
    by (fact prod_cases3)
  let ?line_row = \<open>dmc!row_bound.checked_idx row\<close>
  let ?Psmall=\<open>\<lambda>line. line=?line_row\<close>
  let ?Qsmall=\<open>\<lambda>(v,line'). v=Option.bind ?line_row (\<lambda>r. if tag\<noteq>fst r then None else arr_col.to_map (snd r) col) \<and> line'=?line_row\<close>

  show ?thesis
    unfolding lookup_def split_k
    apply (rule arr_row.hoare_let)
    unfolding prod.case
    apply (rule arr_row.hoare_bind_get)
    apply (rule arr_row.hoare[where Psmall=\<open>?Psmall\<close> and Qsmall=\<open>?Qsmall\<close>])
    subgoal using in_bound[OF split_k] .
    subgoal
      apply (rule arr_row.elem_opt.hoare_bind_get)
      apply (rule arr_row.elem_opt.hoare_if)
      subgoal for line
        apply (rule arr_row.elem_opt.hoare_return)
        apply (cases \<open>?line_row\<close>)
         apply (auto)
        done
      subgoal for line
        using the_lens_hoare

  welcome
(*
lemma lookup_to_map:
  assumes
    "mem_inv dmc"
  shows
    "run_state (lookup k) dmc = (to_map dmc k, dmc)"

  unfolding to_map_def
  unfolding lookup_def
  unfolding morph_row_def morph_entry_def state_morph_fun_def state_morph_def morph_the_def morph_data_def
  apply (auto simp: state_run_simps split: prod.splits)
  apply (auto simp: arr_col.lookup_result)
  apply (drule in_bound)
  apply (drule row_bound.idx.idx_valid)
  unfolding row_bound.checked_idx_def
  apply auto
  subgoal by (metis list_update_id)
  subgoal using row_bound.idx.idx_valid assms[unfolded mem_inv_def arr_row.mem_inv_def]
    by auto
  done

lemma lookup_to_map:
  assumes
    "mem_inv dmc"
  shows
    "run_state (lookup k) dmc = (to_map dmc k, dmc)"
proof -
  have "snd (run_state (lookup k) dmc) = dmc"
    unfolding lookup_def
    apply (clarsimp split: prod.split)
    apply (rule morph_row_hoare)
    subgoal using assms unfolding mem_inv_def by auto
    subgoal using in_bound .
    
lemma mem_inv_rowD:
  "mem_inv dmc \<Longrightarrow> split_key k = (tag, row, col) \<Longrightarrow>
    row_bound.checked_idx row < length dmc"
  unfolding mem_inv_def arr_row.mem_inv_def row_bound.checked_idx_def
  by (auto dest: row_bound.idx.idx_valid in_bound)

lemma mem_inv_colD:
  assumes "mem_inv dmc"
  shows " split_key k = (tag, row, col) \<Longrightarrow> dmc!row_bound.checked_idx row = Some (tag', data)
    \<Longrightarrow> arr_col.mem_inv data"
  unfolding mem_inv_def arr_row.mem_inv_def row_bound.checked_idx_def
  apply (frule assms[THEN mem_inv_rowD])
  apply (frule in_bound)
  apply simp
  apply (frule assms[unfolded mem_inv_def_alt, THEN conjunct2, unfolded list_all_length, rule_format, OF assms[THEN mem_inv_rowD]])
  unfolding mem_inv_def arr_row.mem_inv_def row_bound.checked_idx_def
  apply auto
  done



term morph_entry
lemma update_spec':
  assumes
    "mem_inv dmc"
  shows
    "to_map (snd (run_state (update k v) dmc)) \<subseteq>\<^sub>m to_map dmc(k\<mapsto>v)"
proof -
  obtain tag row col where \<open>split_key k = (tag, row, col)\<close>
    using prod_cases3 .


  welcome (*
  unfolding map_le_def apply auto
  subgoal
    unfolding to_map_def lookup_def update_def
    unfolding morph_row_def morph_entry_def state_morph_fun_def state_morph_def morph_the_def morph_data_def morph_tag_def
    apply (auto 
        simp: state_run_simps
        split: prod.splits if_splits)
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def arr_col.clear_mem_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      apply (frule assms[THEN mem_inv_colD])
       apply assumption
      apply (frule arr_col.mem_inv_def[THEN iffD1, symmetric])
      apply auto
      done
    done
  subgoal for k1 v1
    apply (cases "split_key k" rule: prod_cases3; cases "split_key k1" rule: prod_cases3)
    subgoal for tag row col tag1 row1 col1
      apply (cases "dmc!row_bound.checked_idx row"; cases "dmc!row_bound.checked_idx row1")
      unfolding to_map_def lookup_def update_def
      unfolding morph_row_def morph_entry_def state_morph_fun_def state_morph_def morph_the_def morph_data_def morph_tag_def
     apply (auto 
      simp: state_run_simps
      split: prod.splits if_splits)
(*    subgoal
      unfolding arr_col.update_def arr_col.lookup_def col_bound.checked_idx_def
      apply (auto
          simp: state_run_simps
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: Let_def
          split: if_splits)
      done
*)    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
        apply (frule  mem_inv_rowD[OF assms, unfolded assms[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def]])
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: split_key_inj[THEN injD]
          split: if_splits)
      subgoal unfolding arr_col.init_mem_def by auto
      subgoal unfolding arr_col.init_mem_def by auto
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def arr_col.clear_mem_def arr_col.init_mem_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
        apply (frule  mem_inv_rowD[OF assms, unfolded assms[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def]])
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: split_key_inj[THEN injD]
          split: if_splits)
      using split_key_inj[THEN injD, of k k1]
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def], of "col_bound.checked_idx col", unfolded arr_col.init_mem_def]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: split_key_inj[THEN injD]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      done
    subgoal
      unfolding arr_col.update_def arr_col.lookup_def arr_col.clear_mem_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits;
          frule  mem_inv_rowD[OF assms, unfolded assms[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def]])
      done
    subgoal for data data1 data' data1' data2 v2 data3
      unfolding arr_col.update_def arr_col.lookup_def arr_col.clear_mem_def
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp: nth_list_update[where xs=arr_col.init_mem, unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def]]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          split: if_splits)
      apply (frule  mem_inv_rowD[OF assms, unfolded assms[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def]])
      apply (simp only: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format])
      using split_key_inj[THEN injD, of k k1]
      apply auto
      apply (frule mem_inv_colD[OF assms])
      apply assumption
      apply (drule arr_col.mem_inv_def[THEN iffD1, symmetric])
      apply simp
      using nth_list_update[unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def], of "col_bound.checked_idx col" data3]
      apply (auto
          simp: state_run_simps
          simp: Let_def
          simp: mem_inv_rowD[OF assms, THEN nth_list_update]
          simp:nth_list_update[unfolded arr_col.mem_inv_init_mem[unfolded arr_col.mem_inv_def], of "col_bound.checked_idx col" data3]
          simp: col_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: row_bound.checked_idx_injective[unfolded injective_def_alt, rule_format]
          simp: split_key_inj[THEN injD]
          split: if_splits)
      done
    done
  done
  done

lemma update_spec:
  assumes
    "mem_inv dmc"
    "run_state (update k v) dmc = (u, dmc')"
  shows
    "to_map dmc' \<subseteq>\<^sub>m to_map dmc(k\<mapsto>v)"
  using update_spec'[OF assms(1), of k v, unfolded assms(2), simplified]
  .

lemma morph_row_inv:
  assumes
    "split_key k = (tag, row, col)"
    "mem_inv dmc"
    "pred_option (pred_prod top arr_col.mem_inv) (snd (run_state h (dmc!row_bound.checked_idx row)))"
  shows
    "mem_inv (snd (run_state (morph_row row h) dmc))"
  
  unfolding mem_inv_def list_all_length
  unfolding morph_row_def morph_entry_def
  unfolding state_morph_fun_def state_morph_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits
      simp: arr_row.mem_inv_def
      simp: assms(2)[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def]
      simp: nth_list_update
      )
  apply (subst nth_list_update)
  subgoal using in_bound[OF assms(1), THEN row_bound.idx.idx_valid]
    apply (auto simp: assms(2)[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def] row_bound.checked_idx_def in_bound[OF assms(1)])
    done
  subgoal
    apply (auto)
    subgoal using assms(3) by auto
    subgoal
      apply (rule assms(2)[unfolded mem_inv_def, THEN conjunct2, unfolded list_all_length, rule_format])
      apply (simp add: assms(2)[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def])
      done
    done
  done

lemma morph_the_inv:
  assumes
    "A (snd (run_state h s))"
  shows
    "pred_option A (snd (run_state (morph_the h) (Some s)))"
  using assms
  unfolding morph_the_def state_morph_fun_def state_morph_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits)
  done

lemma morph_data_inv:
  assumes
    "A t"
    "B (snd (run_state h d))"
  shows
    "pred_prod A B (snd (run_state (morph_data h) (t, d)))"
  using assms
  unfolding morph_data_def state_morph_fun_def state_morph_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits)
  done

lemma morph_the_bind:
  "morph_the h \<bind> (\<lambda>v. morph_the (g v)) = morph_the (h \<bind> g)"
  apply (rule state_ext)
  unfolding morph_the_def state_morph_fun_def state_morph_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits)
   apply (auto simp: State_Monad.bind_def)
  done

lemma update_inv:
  assumes
    "mem_inv dmc"
  shows
    "mem_inv (snd (run_state (update k v) dmc))"
  unfolding update_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits
      )
  apply (rule morph_row_inv)
    apply assumption
  subgoal using assms .
  apply (auto
      simp: state_run_simps
      simp: Let_def
      simp: morph_the_bind
      split: prod.splits
      simp: arr_col.mem_inv_init_mem
      intro!: morph_the_inv morph_data_inv
      intro!: arr_col.update_inv
      )
  defer
  subgoal
    apply (frule in_bound)
    apply (drule  in_bound[THEN row_bound.idx.idx_valid])
    apply (frule assms[unfolded mem_inv_def, THEN conjunct2, unfolded list_all_length, rule_format,
        unfolded assms[unfolded mem_inv_def, THEN conjunct1, unfolded arr_row.mem_inv_def]])
    unfolding row_bound.checked_idx_def     
    apply auto
    done
  unfolding morph_data_def morph_tag_def state_morph_fun_def state_morph_def
  unfolding arr_col.clear_mem_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits
      )
  using arr_col.update_inv[OF arr_col.mem_inv_init_mem]
  by (smt snd_conv)

lemma mem_inv_init_mem:
  "mem_inv init_mem"
  unfolding mem_inv_def
  unfolding init_mem_def
  apply (auto simp: arr_row.mem_inv_init_mem list_all_length)
  apply (auto simp: arr_row.init_mem_def)
  done

lemma init_mem_to_map:
  "to_map init_mem = Map.empty"
  apply (rule ext)
  unfolding to_map_def
  unfolding lookup_def
  unfolding morph_row_def morph_entry_def state_morph_fun_def state_morph_def morph_tag_def morph_data_def morph_the_def init_mem_def arr_row.init_mem_def
  apply (auto
      simp: state_run_simps
      simp: Let_def
      split: prod.splits
      )
  apply (frule in_bound)
  apply (frule in_bound[THEN row_bound.idx.idx_valid])
  unfolding row_bound.checked_idx_def
  apply auto
  done

end
end