theory StateT_Heap_Misc
  imports Separation_Logic_Imperative_HOL.Sep_Main
begin

lemma run_execute_some:
  "run H (Some h) (Some h') r \<longleftrightarrow> effect H h h' r"
  by (auto intro: run_effectI effect_run)

lemma is_exn_def':
  "is_exn \<sigma> \<longleftrightarrow> \<sigma> = None"
  by (cases \<sigma>) auto

lemma run_fail:
  "run H (Some h) None r \<longleftrightarrow> \<not>success H h"
  by (auto intro: run.intros elim: run.cases simp: success_def)

lemma run_def_alt:
  "run H \<sigma> \<sigma>' r = (case (\<sigma>, \<sigma>') of
    (None, None) \<Rightarrow> True
  | (None, Some _) \<Rightarrow> False
  | (Some h, None) \<Rightarrow> \<not>success H h
  | (Some h, Some h') \<Rightarrow> effect H h h' r)"
  by (cases \<sigma>; cases \<sigma>') (auto simp: run_execute_some run_fail intro: run.intros elim: run.cases)

end