theory StateT_Heap_Monad_Loop_Lazy
  imports
    "./StateT_Heap_Monad_Ext"
    "../utils/LazyList3"
    "./StateT_Heap_Monad_Loop"
    "HOL-Library.Code_Target_Numeral"
begin

definition lsequence' where
  "lsequence' = sequence' \<circ> to_list"

thm sequence'_simps[no_vars]
lemma lsequence'_simps:
  "lsequence' LNil = StateT_Heap_Monad.return ()"
  "lsequence' (LCons f) = (case f() of
    (x, xs) \<Rightarrow> do {x; lsequence' xs}
  )"
  unfolding lsequence'_def
  by (auto simp: sequence'_simps split: prod.split)

lemmas [code del] = lsequence'_def
lemmas [code] = lsequence'_simps

named_theorems lazy_list_code

lemmas [lazy_list_code] =
  lupt.abs_eq[symmetric]
  lmap.abs_eq[symmetric]

lemma from_list_sequence'[lazy_list_code]:
  "sequence' xs = lsequence' (from_list xs)"
  unfolding lsequence'_def
  by (auto simp: to_list_from_list)

lemma bi_total_bi_unique_eq:
  "left_total R \<Longrightarrow> left_unique R \<Longrightarrow> R OO R\<inverse>\<inverse> = (=)"
  unfolding left_total_def left_unique_def
  by fastforce

thm lconcat.abs_eq[symmetric, no_vars]
lemma [lazy_list_code]:
  "from_list (concat x) = lconcat (from_list (map from_list x))"
  apply (subst lconcat.abs_eq)
   apply (fold llist.pcr_cr_eq)
   apply (subst eq_OO)
   apply (subst bi_total_bi_unique_eq)
  subgoal by (intro list.left_total_rel left_total_eq llist.left_total)
  subgoal by (intro list.left_unique_rel left_unique_eq llist.left_unique)
  subgoal by (rule refl)
  subgoal by (rule refl)
  done

lemma (in mem_subtype) sequence'_inv:
  "inv (sequence' xs)" if "\<And>x. x\<in>set xs \<Longrightarrow> inv x"
  using that
  by (induction xs) (auto simp: sequence'_simps intro: inv_return inv_bind)

lemmas [intro!] = mem_subtype.sequence'_inv
end