theory StateT_Heap_Memory
  imports "./StateT_Heap_Monad_Ext"
begin

locale array_map =
  fixes size :: nat
begin

definition empty :: "'v::heap option array Heap" where
  "empty = (Array.new size (None :: ('v :: heap) option))"

definition lookup :: "'v::heap option array \<Rightarrow> nat \<Rightarrow> 'v option Heap" where
  "lookup mem i = (if i < size
    then Array.nth mem i
    else Heap_Monad.return None)"

definition update :: "'v::heap option array \<Rightarrow> nat \<Rightarrow> 'v \<Rightarrow> unit Heap" where
  "update mem i v = (if i < size
    then do {Array.upd i (Some v) mem; Heap_Monad.return ()}
    else Heap_Monad.return ())"

end

type_synonym ('row, 'v) pair_mem = "'row \<times> 'row \<times> 'v option array \<times> 'v option array"

locale pair_mem_stateT = row: array_map +
  fixes to_row :: "'k \<Rightarrow> 'row"
    and to_col :: "'k \<Rightarrow> 'col"
    and col_index :: "'col \<Rightarrow> nat"
begin

definition lookup :: "'k \<Rightarrow> (('row, 'v::heap) pair_mem, 'v option) StateT_Heap_Monad.t" where
  "lookup k = do {
    (row1, row2, arr1, arr2) \<leftarrow> StateT_Heap_Monad.get;
    let row' = to_row k;
    if row' = row1
      then StateT_Heap_Monad.lift (row.lookup arr1 (col_index (to_col k)))
      else if row' = row2
        then StateT_Heap_Monad.lift (row.lookup arr2 (col_index (to_col k)))
        else StateT_Heap_Monad.return None
  }"

definition update :: "'k \<Rightarrow> 'v::heap \<Rightarrow> (('row, 'v) pair_mem, unit) StateT_Heap_Monad.t" where
  "update k v = do {
    (row1, row2, arr1, arr2) \<leftarrow> StateT_Heap_Monad.get;
    let row' = to_row k;
    if row' = row1
      then StateT_Heap_Monad.lift (row.update arr1 (col_index (to_col k)) v)
      else if row' = row2
        then StateT_Heap_Monad.lift (row.update arr2 (col_index (to_col k)) v)
        else do {
          arr' \<leftarrow> StateT_Heap_Monad.lift row.empty;
          StateT_Heap_Monad.put (row', row1, arr', arr1);
          StateT_Heap_Monad.lift (row.update arr' (col_index (to_col k)) v)
        }
  }"

end

experiment begin

print_locale pair_mem
thm pair_mem_defs.lookup_pair_def

end