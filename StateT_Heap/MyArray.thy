theory MyArray
  imports
    Refine_Imperative_HOL.IICF
    "../Index1"
    "./StateT_Heap_Cache"
    "Heap_Monad_Loop"
    "./MyArray_Pure"
begin

lemma id_assn_refl[simp]:
  "id_assn x x = emp"
  unfolding pure_def
  by auto
  

locale array_heap =
  arrlim: bounded_index bound for bound :: "'key::index bound"
begin

print_locale array_pure
interpretation ap: array_pure bound .
term array_assn thm array_assn_def
term hr_comp
(*
definition mem_of_line :: "('k \<rightharpoonup> 'v::heap) \<Rightarrow> ('v option array) \<Rightarrow> assn" where
  "mem_of_line maep st = (\<exists>\<^sub>A l.
      st \<mapsto>\<^sub>a l
    * \<up>(lines_valid l)
    * \<up>(maep = lookup_pure l))"
*)
definition init_mem :: "'v::heap option array Heap" where
  "init_mem = (Array.new arrlim.sz None)"

lemma init_mem_refine:
  "<emp> init_mem <is_array ap.init_mem>"
  unfolding init_mem_def
  unfolding ap.init_mem_def
  unfolding is_array_def
  by sep_auto

definition lookup :: "'key \<Rightarrow> ('v::heap option array, 'v option) StateT_Heap_Monad.t" where
  "lookup k = do {
    let i = arrlim.checked_idx k;
    mem \<leftarrow> StateT_Heap_Monad.get;
    if i < arrlim.sz
      then StateT_Heap_Monad.lift (Array.nth mem i)
      else StateT_Heap_Monad.return None
  }"

lemma lookup_refine:
  assumes
    "ap.mem_inv xs"
  shows
    "<is_array xs mem>
      StateT_Heap_Monad.run (lookup k) mem
     <prod_assn id_assn is_array (run_state (ap.lookup k) xs)>"
  unfolding lookup_def
  unfolding ap.lookup_def
  apply (sep_auto
      simp: StateT_Heap_Monad.run_simps
      simp: Let_def
      simp: state_run_simps
      simp: assms[unfolded ap.mem_inv_def]
      simp: is_array_def
      )
  done

definition update where
  "update k v = do {
    let i = arrlim.checked_idx k;
    mem \<leftarrow> StateT_Heap_Monad.get;
    if i < arrlim.sz
      then do {
        StateT_Heap_Monad.lift (Array.upd i (Some v) mem);
        StateT_Heap_Monad.return ()
      }
      else StateT_Heap_Monad.return ()
  }"

thm ap.update_def

lemma update_refine:
  assumes
    "ap.mem_inv xs"
  shows
    "<is_array xs mem>
      StateT_Heap_Monad.run (update k v) mem
     <prod_assn unit_assn is_array (run_state (ap.update k v) xs)>"
  unfolding update_def
  unfolding ap.update_def
  by (sep_auto
    simp: StateT_Heap_Monad.run_simps
    simp: Let_def
    simp: is_array_def
    simp: assms[unfolded ap.mem_inv_def]
    simp: state_run_simps
    )

definition clear where
  "clear = do {
    mem \<leftarrow> StateT_Heap_Monad.get;
    StateT_Heap_Monad.lift (fill 0 arrlim.sz None mem)
  }"

lemma clear_refine:
  assumes
    "ap.mem_inv xs"
  shows
    "<is_array xs mem>
      StateT_Heap_Monad.run clear mem
     <prod_assn unit_assn is_array (run_state ap.clear_mem xs)>"
  unfolding clear_def ap.clear_mem_def
  by (sep_auto
      simp: StateT_Heap_Monad.run_simps
      simp: state_run_simps
      simp: is_array_def
      simp: assms[unfolded ap.mem_inv_def]
      simp: ap.init_mem_def
      heap: fill_hoare
      )
end

end