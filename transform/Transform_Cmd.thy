theory Transform_Cmd
  imports
    "../Pure_Monad"
  keywords
    "dp_fun" :: thy_decl
    and "monadifies" :: thy_decl
    and "dp_correctness" :: thy_goal
    and "memoizes" :: thy_goal
begin

ML_file \<open>../transform/Transform_Misc.ML\<close>
ML_file \<open>../transform/Transform_Data.ML\<close>
ML_file \<open>../transform/Transform_Const.ML\<close>
ML_file \<open>../transform/Transform_Tactic.ML\<close>
ML_file \<open>../transform/Transform_Term.ML\<close>
ML_file \<open>../transform/Transform.ML\<close>
ML_file \<open>../transform/Transform_Parser.ML\<close>

ML \<open>
val _ =
  Outer_Syntax.local_theory @{command_keyword dp_fun} "whatever"
    (Transform_Parser.dp_fun_part0_parser >> Transform_DP.dp_fun_part0_cmd)

val _ =
  Outer_Syntax.local_theory_to_proof @{command_keyword memoizes} "whatever"
    (Transform_Parser.dp_fun_part1_parser >> Transform_DP.dp_fun_part1_cmd)

val _ =
  Outer_Syntax.local_theory @{command_keyword monadifies} "whatever"
    (Transform_Parser.dp_fun_part2_parser >> Transform_DP.dp_fun_part2_cmd)
\<close>

ML \<open>
val _ =
  Outer_Syntax.local_theory_to_proof @{command_keyword dp_correctness} "whatever"
    (Scan.succeed Transform_DP.dp_correct_cmd)
\<close>

method_setup dp_match = \<open>
Scan.succeed (fn ctxt => (SIMPLE_METHOD' (
  Transform_Data.get_last_cmd_info ctxt
  |> Transform_Tactic.solve_consistentDP_tac ctxt)))
\<close>

method_setup dp_match_init = \<open>
Scan.succeed (fn ctxt => (SIMPLE_METHOD' (
  Transform_Data.get_last_cmd_info ctxt
  |> Transform_Tactic.prepare_consistentDP_tac ctxt)))
\<close>

method_setup dp_match_case_init = \<open>
Scan.succeed (fn ctxt => (SIMPLE_METHOD' (
  Transform_Data.get_last_cmd_info ctxt
  |> Transform_Tactic.prepare_case_tac ctxt)))
\<close>

method_setup dp_match_step = \<open>
Scan.succeed (fn ctxt => (SIMPLE_METHOD' (
  Transform_Data.get_last_cmd_info ctxt
  |> Transform_Tactic.step_tac ctxt)))
\<close>

method_setup dp_unfold_defs  = \<open>
Scan.option (Scan.lift (Args.parens Args.name) -- Args.term) >> (fn tm_opt => fn ctxt => (SIMPLE_METHOD' (
  Transform_Data.get_or_last_cmd_info ctxt tm_opt
  |> Transform_Tactic.dp_unfold_defs_tac ctxt)))
\<close>

method_setup dp_combinator_init  = \<open>
Scan.option (Scan.lift (Args.parens Args.name) -- Args.term) >> (fn tm_opt => fn ctxt => (SIMPLE_METHOD' (
  Transform_Data.get_or_last_cmd_info ctxt tm_opt
  |> Transform_Tactic.prepare_combinator_tac ctxt)))
\<close>

syntax
  "_uncurry" :: "'a \<Rightarrow> 'b"  ("(UNCURRY _)" 10)

parse_translation \<open>
  let
    fun f ctxt [x] =
      let
        val x = Term_Position.strip_positions x
        val (cn, ct) = dest_Free x
        val c = Proof_Context.read_const {proper=false, strict=false} ctxt cn
        val a = fastype_of c |> binder_types |> map (fn _ => dummyT)
      in Transform_Misc.uncurry (Free (cn, a ---> dummyT)) end
  in
    [(@{syntax_const "_uncurry"}, f)]
  end
\<close>

end (* theory *)