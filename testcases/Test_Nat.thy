theory Test_Nat
  imports "../state_monad/State_Main"
begin

fun f :: "nat \<Rightarrow> nat" where
  "f x = (case x of 0 \<Rightarrow> 0 | Suc n \<Rightarrow> Suc (f n))"

context begin

dp_fun f\<^sub>T: f memoizes dp_consistency_rbt monadifies (state) f.simps

dp_correctness
  by dp_match

end

end