theory Test_Grid_Path
  imports
    "../transform/Transform_Cmd2"
    "../scratch/Memory"
begin

definition "min_opt a b \<equiv>
  case (a, b) of
    (None, None) \<Rightarrow> None
  | (Some a0, None) \<Rightarrow> Some a0
  | (None, Some b0) \<Rightarrow> Some b0
  | (Some a0, Some b0) \<Rightarrow> Some (min a0 b0)"
term 0 (**)

context
  fixes W :: "nat \<Rightarrow> nat \<Rightarrow> nat option"
begin

fun ed :: "nat \<Rightarrow>nat \<Rightarrow> nat option" where
  "ed 0 0 = W 0 0"
| "ed 0 (Suc j) = case_option None (\<lambda>prev.
                     case_option None (\<lambda>here.
                       Some (plus prev here)
                     ) (W 0 (Suc j))
                   ) (ed 0 j)"
| "ed (Suc i) 0 = case_option None (\<lambda>prev.
                     case_option None (\<lambda>here.
                       Some (plus prev here)
                     ) (W (Suc i) 0)
                   ) (ed i 0)"
| "ed (Suc i) (Suc j) = case_option None (\<lambda>prev.
                         case_option None (\<lambda>here.
                           Some (plus prev here)
                         ) (W (Suc i) (Suc j))
                       ) (min_opt (ed i j) (min_opt (ed (Suc i) j) (ed i (Suc j))))"
thm ed.simps
thm ed.induct

dp_fun ed\<^sub>T: ed memoizes dp_consistency_rbt monadifies ed.simps
dp_correctness by dp_match


end
end