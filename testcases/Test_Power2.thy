theory Test_Power2
  imports
    "../transform/Transform_Cmd2"
    "../scratch/Memory"
begin

context begin
fun fd where
  "fd x = fold op+ (hd (map (map fd) [[0..<x]])) (Suc 0)"

dp_fun fd\<^sub>T: fd memoizes dp_consistency_rbt monadifies fd.simps
term 0 (*
interpretation dp_consistency_default fd .

transform_dp_def_eqs
thm fd\<^sub>T.psimps
end

context begin
fun fe where
  "fe x = fold op+ (hd (map (\<lambda>xs. map fe (id xs)) [[0..<x]])) (Suc 0)"

interpretation dp_consistency_default fe .

transform_dp_def
thm fe\<^sub>T.psimps
thm fe.simps
ML_val \<open>@{thm fe.simps} |> Thm.full_prop_of\<close>