theory Test_Min_Ed_Dist0
  imports
    "../transform/Transform_Cmd2"
    "../scratch/Memory"
begin

fun min_list :: "'a::ord list \<Rightarrow> 'a" where
"min_list (x # xs) = (case xs of [] \<Rightarrow> x | _ \<Rightarrow> min x (min_list xs))"

context
fixes xs ys :: "nat \<Rightarrow> 'a"
fixes m n :: nat
begin

function (sequential)
  min_ed_ix :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"min_ed_ix i j =
  (if i \<ge> m then
     if j \<ge> n then 0 else n-j else
   if j \<ge> n then m-i
   else
   min_list [1 + min_ed_ix i (j+1), 1 + min_ed_ix (i+1) j,
       (if xs i = ys j then 0 else 1) + min_ed_ix (i+1) (j+1)])"
by pat_completeness auto
termination by(relation "measure(\<lambda>(i,j). (m - i) + (n - j))") auto
thm min_ed_ix.simps
thm min_ed_ix.induct

dp_fun min_ed_ix\<^sub>T: min_ed_ix memoizes dp_consistency_rbt monadifies min_ed_ix.simps
dp_correctness by dp_match

end
end