theory Test_Longest_Path
  imports
    "../transform/Transform_Cmd2"
    "../scratch/Memory"
begin

context (* Longest path *)
  fixes v :: "nat \<Rightarrow> nat"
    and p :: "nat \<Rightarrow> nat"
  assumes p_lt: "p (Suc j) < Suc j"
begin


text \<open>Dimensionality given by i\<close>
function wis :: "nat \<Rightarrow> nat" where
  "wis 0 = 0" |
  "wis (Suc i) = max (wis (p (Suc i)) + v i) (wis i)"
  by pat_completeness auto
termination
  by (relation "(\<lambda>p. size p) <*mlex*> {}") (auto intro: wf_mlex mlex_less simp: p_lt)

thm wis.simps
thm wis.induct

dp_fun wis\<^sub>T: wis memoizes dp_consistency_rbt monadifies wis.simps
dp_correctness by dp_match

end
end