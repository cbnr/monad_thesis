theory Test_Fibonacci
  imports 
    "HOL-Library.Extended"
    "HOL-Library.Code_Target_Numeral"
    "../heap_monad/Reader_Main1"
begin

fun fib :: "nat \<Rightarrow> int option" where
  "fib 0 = Some 0"
| "fib (Suc 0) = Some 1"
| "fib (Suc (Suc n)) = case_prod
      (\<lambda>of1 of0. case_option
        None
        (\<lambda>f1. case_option
          None
          (\<lambda>f0. Some (f1 + f0))
          of0)
        of1)
      (Pair (fib (Suc n)) (fib n))"
thm fib.simps
thm fib.induct
(*
context
  fixes mem :: "nat ref \<times> nat ref \<times> int option option array ref \<times> int option option array ref"
  assumes mem_is_init: "mem = result_of (init_state 1 0 1) Heap.empty"
begin

(* Should be an assumption,
   just wanted to make sure that the error is not due to the assumption in the context *)

lemma [intro]:
  "dp_consistency_heap_array_pair' 1 id id id 0 1 mem"
  by standard (auto simp: injective_def mem_is_init)

dp_fun fib\<^sub>5: fib
  memoizes (default_proof) dp_consistency_heap_array_pair'
  where size = "1 :: nat"
    and key1="id :: nat \<Rightarrow> nat" and key2="id :: nat \<Rightarrow> nat" and k1="0 :: nat" and k2="1 :: nat"
    and to_index = "id :: nat \<Rightarrow> nat"
    and mem = mem
monadifies (heap) fib.simps

dp_correctness
  by dp_match

lemmas memoized_empty5 = fib\<^sub>5.memoized_empty[of "\<lambda> _. fib\<^sub>5'", OF fib\<^sub>5.crel]

end

print_statement fib\<^sub>5'.simps

fun fib\<^sub>5_impl where
  "fib\<^sub>5_impl mem 0 =
         heap_mem_defs.checkmem
          (case mem of
           (k_ref1, k_ref2, m_ref1, m_ref2) \<Rightarrow> lookup_pair 1 id id id m_ref1 m_ref2 k_ref1 k_ref2)
          (case mem of
           (k_ref1, k_ref2, m_ref1, m_ref2) \<Rightarrow> update_pair 1 id id id m_ref1 m_ref2 k_ref1 k_ref2)
          0 (return (Some 0))" |
  "fib\<^sub>5_impl mem (Suc 0) =
         heap_mem_defs.checkmem
          (case mem of
           (k_ref1, k_ref2, m_ref1, m_ref2) \<Rightarrow> lookup_pair 1 id id id m_ref1 m_ref2 k_ref1 k_ref2)
          (case mem of
           (k_ref1, k_ref2, m_ref1, m_ref2) \<Rightarrow> update_pair 1 id id id m_ref1 m_ref2 k_ref1 k_ref2)
          (Suc 0) (return (Some 1))" |
  "fib\<^sub>5_impl mem (Suc (Suc na)) =
         heap_mem_defs.checkmem
          (case mem of
           (k_ref1, k_ref2, m_ref1, m_ref2) \<Rightarrow> lookup_pair 1 id id id m_ref1 m_ref2 k_ref1 k_ref2)
          (case mem of
           (k_ref1, k_ref2, m_ref1, m_ref2) \<Rightarrow> update_pair 1 id id id m_ref1 m_ref2 k_ref1 k_ref2)
          (Suc (Suc na))
          (Heap_Monad_Ext.fun_app_lifted
            (return
              (\<lambda>a. return
                    (case a of (None, of0) \<Rightarrow> None | (Some f1, None) \<Rightarrow> None
                     | (Some f1, Some f0) \<Rightarrow> Some (f1 + f0))))
            (Heap_Monad_Ext.fun_app_lifted
              (Heap_Monad_Ext.fun_app_lifted (return (\<lambda>a. return (\<lambda>b. return (a, b))))
                (fib\<^sub>5_impl mem (Suc na)))
              (fib\<^sub>5_impl mem na)))"

context
  fixes mem :: "nat ref \<times> nat ref \<times> int option option array ref \<times> int option option array ref"
  assumes mem_is_init: "mem = result_of (init_state 1 0 1) Heap.empty"
begin

thm fib\<^sub>5'.simps[OF mem_is_init]

lemma fib\<^sub>5_impl_def:
  "fib\<^sub>5_impl mem m = fib\<^sub>5' mem m"
  by (induction rule: fib\<^sub>5'.induct[OF mem_is_init]; simp add: fib\<^sub>5'.simps[OF mem_is_init])

end

thm memoized_empty5 fib\<^sub>5'.simps fib\<^sub>5_impl_def

definition
  "fib5 n = (init_state n (0::nat) (1::nat) ::
    (nat ref \<times> nat ref \<times> int option option array ref \<times> int option option array ref) Heap)
  \<bind> (\<lambda>mem. fib\<^sub>5_impl mem n)"

lemma fib5_correct:
  "fib n = result_of (fib5 n) Heap.empty"
  using memoized_empty5 fib\<^sub>5'.simps fib\<^sub>5_impl_def
  sorry

(*
context
  fixes n :: nat
  fixes mem :: "nat ref \<times> nat ref \<times> int option option array ref \<times> int option option array ref"
begin

lemma [intro]:
  "dp_consistency_heap_array_pair n id id id 0 1"
  by standard (auto simp: injective_def mem_is_init)

dp_fun fib\<^sub>5: fib
  memoizes (default_proof) dp_consistency_heap_array_pair
  where size = n
    and key1="id :: nat \<Rightarrow> nat" and key2="id :: nat \<Rightarrow> nat" and k1="0 :: nat" and k2="1 :: nat"
    and to_index = "id :: nat \<Rightarrow> nat"
monadifies (heap) fib.simps

dp_correctness
  by dp_match

lemmas memoized_empty5 = fib\<^sub>5.memoized_empty[of "\<lambda> _. fib\<^sub>5'", OF fib\<^sub>5.crel, of n]

thm fib\<^sub>5'.simps

end

thm memoized_empty5 fib\<^sub>5'.simps



definition
  "fib5 n = (init_state n (0::nat) (1::nat) ::
    (nat ref \<times> nat ref \<times> int option option array ref \<times> int option option array ref) Heap)
  \<bind> (\<lambda>mem. fib\<^sub>5' n n)"

lemma fib5_correct:
  "fib n = result_of (fib5 n) Heap.empty"
  by (simp add: memoized_empty5 fib5_def)
*)

context
  fixes n :: nat
  fixes mem :: "int option option array"
begin

lemma [intro]:
  "dp_consistency_heap_array_new' n id mem"
  by standard (simp add: injective_def)

dp_fun fib\<^sub>4: fib
  memoizes (default_proof) dp_consistency_heap_array_new'
  where size = n
    and to_index = "id :: nat \<Rightarrow> nat"
monadifies (heap) fib.simps

dp_correctness
  by dp_match

lemmas memoized_empty4 = fib\<^sub>4.memoized_empty[of "\<lambda> _. fib\<^sub>4'", OF fib\<^sub>4.crel]

end

thm fib\<^sub>4'.simps
thm memoized_empty4

context
  fixes n :: nat
  fixes mem :: "int option option array"
begin

lemma [intro]:
  "dp_consistency_heap_array_new n id"
  by standard (simp add: injective_def)

dp_fun fib\<^sub>4: fib
  memoizes (default_proof) dp_consistency_heap_array_new
  where size = n
    and to_index = "id :: nat \<Rightarrow> nat"
monadifies (heap) fib.simps

dp_correctness
  by dp_match

lemmas memoized_empty4 = fib\<^sub>4.memoized_empty[of "\<lambda> _. fib\<^sub>4'", OF fib\<^sub>4.crel]

end

thm fib\<^sub>4'.simps
thm memoized_empty4


context
  fixes n :: nat
  fixes mem :: "int option option array"
begin

lemma [intro]:
  "dp_consistency_heap_array n id"
  by standard (simp add: injective_def)

dp_fun fib\<^sub>3: fib
  memoizes (default_proof) dp_consistency_heap_array
  where size = n
    and to_index = "id :: nat \<Rightarrow> nat"
monadifies (heap) fib.simps

dp_correctness
  by dp_match

lemmas memoized_empty3 = fib\<^sub>3.memoized_empty[of "\<lambda> _. fib\<^sub>3'", OF fib\<^sub>3.crel]

end

thm memoized_empty3 fib\<^sub>3'.simps

locale dp_consistency_heap_array =
  fixes size :: nat
    and to_index :: "('k :: heap) \<Rightarrow> nat"
    and mem :: "('v :: heap) option array"
    and dp :: "'k \<Rightarrow> 'v"
  assumes injective: "injective size to_index"
begin

sublocale dp_consistency_heap
  where P="\<lambda>heap. Array.length heap mem = size"
    and lookup="mem_lookup size to_index mem"
    and update="mem_update size to_index mem"
  by (intro dp_consistency_heap.intro mem_heap_correct injective)

end

locale dp_consistency_heap_array_id =
  fixes size :: nat
    and mem :: "('v :: heap) option array"
    and dp :: "nat \<Rightarrow> 'v"
begin 

sublocale dp_consistency_heap
  where P="\<lambda>heap. Array.length heap mem = size"
    and lookup="mem_lookup size id mem"
    and update="mem_update size id mem"
  by (intro dp_consistency_heap.intro mem_heap_correct; simp add: injective_def)

end

context
  fixes n :: nat
  fixes mem :: "int option option array"
begin

dp_fun fib\<^sub>1: fib
  memoizes dp_consistency_heap_array_id
  where mem  = mem
    and size = n
monadifies (heap) fib.simps

dp_correctness
  by dp_match

end

context
  fixes n :: nat
  fixes mem :: "int option option array"
begin

lemma [intro]:
  "dp_consistency_heap_array n id"
  by standard (simp add: injective_def)

dp_fun fib\<^sub>2: fib
  memoizes (default_proof) dp_consistency_heap_array
  where mem  = mem
    and size = n
    and to_index = "id :: nat \<Rightarrow> nat"
monadifies (heap) fib.simps

dp_correctness
  by dp_match

end

thm fib\<^sub>2'.simps

paragraph \<open>Functional Memoization\<close>

context begin
dp_fun fib\<^sub>T: fib memoizes dp_consistency_rbt monadifies (state) fib.simps
thm fib\<^sub>T'.simps

dp_correctness
  by dp_match
end
thm fib\<^sub>T.memoized_correct

*)
paragraph \<open>Imperative Memoization\<close>

context
  fixes n :: nat
  fixes mem :: "int option option array"
begin

dp_fun fib\<^sub>h: fib
  memoizes dp_consistency_reader_default
    where bound = "Bound 0 n"
  monadifies (reader) fib.simps

dp_correctness
  by dp_match

definition
  "fib' = fib\<^sub>h.inited fib\<^sub>h'"
end

thm fib\<^sub>h'.simps
term fib'

(*
definition
  "fib' n =
    Heap_Monad.bind (mem_empty (bounded_index.size (Bound 0 n))) (\<lambda>mem. fib\<^sub>h' n mem n)"

lemma fib_heap:
  "fib n = result_of (fib' n) Heap.empty"
  using memoized_empty by (simp add: execute_bind_success success_empty fib'_def)


paragraph \<open>Code Setup for Functional Version\<close>

lemmas [code] =
  state_mem_defs.checkmem'_def
  state_mem_defs.lookup_trace_def
  state_mem_defs.update_trace_def
  fib\<^sub>T'.simps[unfolded state_mem_defs.checkmem_eq_alt]


paragraph \<open>Code Setup for Imperative Version\<close>


code_reflect Fib5 functions fib5 nat_of_integer

ML_val \<open>timeit (Fib5.fib5 (Fib5.nat_of_integer 30))\<close>

*)
lemmas [code] =
  fib'_def
  dp_consistency_reader_default.inited_def

export_code open fib' in SML_imp module_name fib_test file "fib_test.ML"

paragraph \<open>Regression Tests\<close>
(*
text \<open>Functional\<close>

value "fst (run_state (fib\<^sub>T' 40) (Mapping.empty))"
value "fib 40"
value "fst (run_state (fib\<^sub>T' 40) (Mapping.empty))"
value "snd (run_state (fib\<^sub>T' 40) (Mapping.empty))"
value "fst (run_state (fib\<^sub>T' 500) (Mapping.empty))"
value "fst (run_state (fib\<^sub>T' 10000) (Mapping.empty))"
*)
text \<open>Imperative\<close>

ML_file "fib_test.ML"

ML \<open>
open fib_test;;
\<close>

ML_val "timeit (fib (Nat 10000) (Nat 10000))"
(*
export_code fib\<^sub>5_impl nat_of_integer checking SML

(* export_code fib\<^sub>5_impl in SML module_name Fib5 *)

code_reflect Fib5 functions fib\<^sub>5_impl nat_of_integer


                       
ML_val \<open>timeit (Fib5.fib_5 (Fib5.nat_of_integer 10000) (Fib5.nat_of_integer 10000))\<close>
*)
end