theory Test_CYK
  imports "../transform/Transform_Cmd2"
    "~~/src/HOL/Library/Product_Lexorder"
    "../scratch/Memory"
begin


datatype ('n, 't) rhs = NN 'n 'n | T 't 

type_synonym ('n, 't) prods = "('n \<times> ('n, 't) rhs) list"

(* 1. local context for unqualified interpretation *)
context
fixes P :: "('n, 't) prods"
begin

fun cyk :: "'t list \<Rightarrow> 'n list" where
"cyk [] = []" |
"cyk [a] = [A . (A, T a') <- P, a'= a]" |
"cyk w =
  [A. k <- [1..<length w], B <- cyk (take k w), C <- cyk (drop k w), (A, NN B' C') <- P, B' = B, C' = C]"

thm cyk.simps
thm cyk.induct

dp_fun cyk\<^sub>T: cyk memoizes dp_consistency_rbt monadifies cyk.simps
thm cyk\<^sub>T'.simps
dp_correctness
  by dp_match

end

context
fixes P :: "('n, 't) prods"
begin

context
fixes w :: "nat \<Rightarrow> 't"
begin

fun cyk_ix :: "nat * nat \<Rightarrow> 'n list" where
"cyk_ix (i,0) = []" |
"cyk_ix (i,Suc 0) = [A . (A, T a) <- P, a = w i]" |
"cyk_ix (i,n) =
  [A. k <- [1..<n], B <- cyk_ix (i,k), C <- cyk_ix (i+k,n-k), (A, NN B' C') <- P, B' = B, C' = C]"

dp_fun cyk_ix\<^sub>T: cyk_ix memoizes dp_consistency_rbt monadifies cyk_ix.simps

dp_correctness
  by dp_match

end

definition "cyk_list w = cyk_ix\<^sub>T' (\<lambda>i. w ! i) (0,length w)"

end

context
begin

fun test where
  "test 0 = [(0::nat)]" |
  "test (Suc n) = concat (map test [0..<n])"

dp_fun test\<^sub>T: test memoizes dp_consistency_rbt monadifies test.simps
dp_correctness by dp_match

export_code "map\<^sub>T'" checking SML

definition foo where
  "foo \<equiv> \<langle>\<lambda> x. \<langle>x\<rangle>\<rangle>"

definition bar where
  "bar \<equiv> \<lambda> x. \<langle>x\<rangle>"

term foo

export_code foo checking SML

definition foo where
  "foo \<equiv> \<lambda>\<^sub>T g x. (\<langle>g\<rangle> . \<langle>x\<rangle>)"

export_code foo checking SML

definition comp\<^sub>T' :: "('b =='M\<Longrightarrow> 'c) =='M\<Longrightarrow> ('a =='M\<Longrightarrow> 'b) =='M\<Longrightarrow> ('a =='M\<Longrightarrow> 'c)" where
  "comp\<^sub>T' f \<equiv> \<lambda>\<^sub>T g x. \<langle>f\<rangle> . (\<langle>g\<rangle> . \<langle>x\<rangle>)"

export_code "comp\<^sub>T'" checking SML

export_code "comp\<^sub>T" checking SML

export_code "map\<^sub>T" in SML module_name Test
  file "Test.ML"

SML_file "Test.ML"






export_code "map\<^sub>T'" checking SML

export_code "map\<^sub>T" in SML module_name Test
  file "Test.ML"

SML_file "Test.ML"

export_code "map\<^sub>T" in SML module_name Test

end

lemmas [code] = mem_defs.checkmem_def

value
  "(let P = [(0::int, NN 1 2), (0, NN 2 3),
            (1, NN 2 1), (1, T (CHR ''a'')),
            (2, NN 3 3), (2, T (CHR ''b'')),
            (3, NN 1 2), (3, T (CHR ''a''))]
  in map (cyk P) [''baaba'', ''baba''])"

term map\<^sub>T

export_code "map\<^sub>T'" checking SML
export_code "map\<^sub>T" checking SML

thm test\<^sub>T.simps

value "runState (test\<^sub>T (3 :: nat)) Mapping.empty"

term cyk_ix\<^sub>T

value
  "(let P = [(0::int, NN 1 2), (0, NN 2 3),
            (1, NN 2 1), (1, T (CHR ''a'')),
            (2, NN 3 3), (2, T (CHR ''b'')),
            (3, NN 1 2), (3, T (CHR ''a''))]
  in map (\<lambda> xs. snd (runState (cyk_ix\<^sub>T P (\<lambda>i. xs ! i) (0,length xs)) Mapping.empty)) [''baaba'', ''baba''])"

value
  "(let P = [(0::int, NN 1 2), (0, NN 2 3),
            (1, NN 2 1), (1, T (CHR ''a'')),
            (2, NN 3 3), (2, T (CHR ''b'')),
            (3, NN 1 2), (3, T (CHR ''a''))]
  in map (\<lambda> xs. snd (runState (cyk_list P xs) Mapping.empty)) [''baaba'', ''baba''])"

end
