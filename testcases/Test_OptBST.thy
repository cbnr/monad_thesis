theory Test_OptBST
  imports
    "../transform/Transform_Cmd2"
    "../scratch/Memory"
    "~~/src/HOL/Library/Tree"
begin

fun min_list :: "'a::ord list \<Rightarrow> 'a" where
"min_list (x # xs) = (case xs of [] \<Rightarrow> x | _ \<Rightarrow> min x (min_list xs))"

fun arg_min_list :: "('a \<Rightarrow> ('b::linorder)) \<Rightarrow> 'a list \<Rightarrow> 'a" where
"arg_min_list f [x] = x" |
"arg_min_list f (x#y#zs) = (let m = arg_min_list f (y#zs) in if f x \<le> f m then x else m)"

context
fixes W :: "int \<Rightarrow> int \<Rightarrow> nat"
begin

fun wpl :: "int \<Rightarrow> int \<Rightarrow> int tree \<Rightarrow> nat" where
   "wpl i j Leaf = 0"
 | "wpl i j (Node l k r) = wpl i (k-1) l + wpl (k+1) j r + W i j"

function min_wpl :: "int \<Rightarrow> int \<Rightarrow> nat" where
"min_wpl i j =
  (if i > j then 0
   else min_list (map (\<lambda>k. min_wpl i (k-1) + min_wpl (k+1) j + W i j) [i..j]))"
by auto
termination by (relation "measure (\<lambda>(i,j) . nat(j-i+1))") auto
thm min_wpl.simps
thm min_wpl.induct

dp_fun min_wpl\<^sub>T: min_wpl memoizes dp_consistency_rbt monadifies min_wpl.simps
dp_correctness by dp_match

function opt_bst :: "int \<Rightarrow> int \<Rightarrow> int tree" where
"opt_bst i j =
  (if i > j then Leaf else arg_min_list (wpl i j) [\<langle>opt_bst i (k-1), k, opt_bst (k+1) j\<rangle>. k \<leftarrow> [i..j]])"
by auto
termination by (relation "measure (\<lambda>(i,j) . nat(j-i+1))") auto
thm opt_bst.simps
thm opt_bst.induct

dp_fun opt_bst\<^sub>T: opt_bst memoizes dp_consistency_rbt monadifies opt_bst.simps
dp_correctness by dp_match

end (* fixes W *)

end