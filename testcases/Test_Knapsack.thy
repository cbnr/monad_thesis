theory Test_Knapsack
  imports
    "../transform/Transform_Cmd2"
    "../scratch/Memory"
begin

context (* Subset Sum *)
  fixes w :: "nat \<Rightarrow> nat"
begin

context (* Knapsack *)
  fixes v :: "nat \<Rightarrow> nat"
begin

fun knapsack :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "knapsack 0 W = 0" |
  "knapsack (Suc i) W = (if W < w (Suc i)
    then knapsack i W
    else max (knapsack i W) (v (Suc i) + knapsack i (W - w (Suc i))))"

thm knapsack.simps
thm knapsack.induct

dp_fun knapsack\<^sub>T: knapsack memoizes dp_consistency_rbt monadifies knapsack.simps
dp_correctness by dp_match
end
end