theory Misc_Attrib
  imports Main
begin

lemma abs_unit_rule:
  "x \<equiv> y \<Longrightarrow> (\<lambda>_. x) \<equiv> (\<lambda>_. y)"
  by simp

lemma abs_case_prod_rule:
  "f \<equiv> g \<Longrightarrow> case_prod f \<equiv> case_prod g"
  by simp

lemma reform_pair: "f \<equiv> g \<Longrightarrow> f (fst (x ()), snd (x ())) = g (x ())"
  by simp

ML_file "./Misc_Attrib.ML"

end