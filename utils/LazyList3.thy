theory LazyList3
  imports
    Main
    "./Misc_Attrib"
begin

lemma abs_unit_cases:
  obtains x where "f = (\<lambda>_::unit. x)"
  by fastforce

datatype 'a llist = LNil | LCons "unit \<Rightarrow> 'a \<times> 'a llist"

lemma [simp]: "range (\<lambda>_::unit. x) = {x}"
  by auto

definition "LCons_rel = {(xs, LCons ((\<lambda>_. (x, xs)))) | x xs. True}"
lemma wf_LCons_rel:
  "wf LCons_rel"
  unfolding LCons_rel_def
  apply (rule wfUNIVI,simp)
  subgoal premises prems for P x
    supply prems = prems[rule_format]
    apply (induction x)
    subgoal by (rule prems) auto
    subgoal for x  apply (auto)
      apply (rule prems)
      apply auto
      done
    done
  done
  
function lsize :: "'a llist \<Rightarrow> nat" where
  "lsize LNil = 0"
| "lsize (LCons (f)) = (case f () of (x, xs) \<Rightarrow> Suc (lsize xs))"
  by pat_completeness auto
termination
  apply (rule )
   apply (rule wf_LCons_rel)
  unfolding LCons_rel_def
  apply auto
  subgoal for f x y by (rule exI[where x=x]) auto
  done

value "lsize (LCons (\<lambda>_. (0::nat, (LCons (\<lambda>_. (0, (LCons (\<lambda>_. (0, LNil)))))))))"

lemma [measure_function]: "is_measure lsize" ..

lemma LCons_lsize:
  "f () = (x, xs) \<Longrightarrow> lsize xs < lsize (LCons f)"
  by simp

lemmas [simp] = LCons_lsize[OF sym, unfolded lsize.simps]

fun to_list :: "'a llist \<Rightarrow> 'a list" where
  "to_list LNil = []"
| "to_list (LCons f) = (case f () of (x,xs) \<Rightarrow> x # to_list xs)"

fun from_list :: "'a list \<Rightarrow> 'a llist" where
  "from_list [] = LNil"
| "from_list (x#xs) = LCons (\<lambda>_. (x, from_list xs))"

lemmas [code del] =
  to_list.simps
  from_list.simps

lemma to_list_from_list:
  "to_list (from_list x) = x"
  by (induction x) auto

lemma from_list_to_list:
  "from_list (to_list x) = x"
  apply (induction x)
  subgoal by simp
  subgoal for x by (cases x rule: abs_unit_cases) auto
  done

interpretation l: type_definition
  where Rep=to_list
    and Abs=from_list
    and A=UNIV
  by standard (auto simp: from_list_to_list to_list_from_list)

setup_lifting l.type_definition_axioms

lemma [transfer_rule]:
  includes lifting_syntax
  shows "(R ===> pcr_llist R ===> pcr_llist R) Cons (\<lambda>x xs. LCons (\<lambda>_. (x, xs)))"
    and "pcr_llist R Nil LNil"
  unfolding pcr_llist_def cr_llist_def
  by fastforce+

lift_definition lupt :: "nat \<Rightarrow> nat \<Rightarrow> nat llist" is upt .
lemmas [code] = upt_rec[Transfer.transferred]

lift_definition lupto :: "int \<Rightarrow> int \<Rightarrow> int llist" is upto .
lemmas [code] = upto.simps[Transfer.transferred]

lift_definition lreplicate :: "nat \<Rightarrow> 'a \<Rightarrow> 'a llist" is replicate .
lemmas [code] = replicate.simps[Transfer.transferred]

lift_definition lmap :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a llist \<Rightarrow> 'b llist" is map .
lemmas [code] = list.map[Transfer.transferred, abs_pairs]
thm list.map[Transfer.transferred]

lift_definition lappend :: "'a llist \<Rightarrow> ('a llist) \<Rightarrow> 'a llist" is append .
lemmas [code] = append.simps[Transfer.transferred, abs_pairs]
thm list.map[Transfer.transferred]
thm append.simps[Transfer.transferred]

lift_definition lconcat :: "'a llist llist \<Rightarrow> 'a llist" is concat .
lemmas [code] = concat.simps[Transfer.transferred, abs_pairs]
thm concat.simps[Transfer.transferred]

lift_definition lfold :: "('a \<Rightarrow> 'b \<Rightarrow> 'b) \<Rightarrow> 'a llist \<Rightarrow> 'b \<Rightarrow> 'b" is fold .

lemmas [code] = fold_simps[Transfer.transferred, abs_pairs]

lemma [transfer_rule]:
  (*generalize @{thm lfold.transfer} to lift rev_conv_fold*)
  includes lifting_syntax
  shows "((A ===> B ===> B) ===> pcr_llist A ===> B ===> B) fold lfold"
  print_statement lfold.transfer
  apply (subst fun_cong[of "\<lambda>x. x" "id ---> (\<lambda>x. x) ---> id" fold])
  subgoal by (fold id_def, unfold map_fun_id, rule)
  unfolding lfold_def
  by transfer_prover

lift_definition lrev :: "'a llist \<Rightarrow> 'a llist" is rev .
lemmas [code] = rev_conv_fold[Transfer.transferred]
lemma nat_prod: "
case_nat
  (case_prod (\<lambda>x xs. f2 x xs) l)
  (\<lambda>m. case_prod (\<lambda>x xs. g2 m x xs) l)
  n
=
case_prod
  (\<lambda>x xs. case_nat (f2 x xs) (\<lambda>m. g2 m x xs) n)
  l
"
  apply (auto split: nat.split list.split)
  done

lemma if_prod: "
If b
  (case_prod (\<lambda>x xs. f2 x xs) l)
  (\<lambda>m. case_prod (\<lambda>x xs. g2 m x xs) l)
=
case_prod
  (\<lambda>x xs. If b (f2 x xs) (\<lambda>m. g2 m x xs))
  l
"
  by (cases b; cases l; simp)

lift_definition lfilter :: "('a \<Rightarrow> bool) \<Rightarrow> 'a llist \<Rightarrow> 'a llist" is filter .
lemmas [code] = filter.simps[Transfer.transferred, abs_pairs
, folded if_prod]

lift_definition ltake :: "nat \<Rightarrow> 'a llist \<Rightarrow> 'a llist" is take .
lemmas [code] = take.simps[Transfer.transferred, abs_pairs
, folded nat_prod]

lift_definition ldrop :: "nat \<Rightarrow> 'a llist \<Rightarrow> 'a llist" is drop .
lemmas [code] = drop.simps[Transfer.transferred, abs_pairs
, folded nat_prod
]

lift_definition lnth :: "'a llist \<Rightarrow> nat \<Rightarrow> 'a" is nth .
lemmas [code] = nth.simps[Transfer.transferred, abs_pairs
, folded nat_prod
]

code_datatype LNil LCons

lemma "(\<lambda>x. Let x (case_prod (\<lambda>x21 x22.
     LCons
         (\<lambda>_. (f x21, lmap f x22))))) =
(case_prod (\<lambda>x21 x22.
     LCons
         (\<lambda>_. (f x21, lmap f x22))))"
  apply auto
  done

  

ML_val \<open>@{term "(let (x21, x22) = x ()
     in LCons
         (\<lambda>_. (f x21, lmap f x22)))"}\<close>

lemma "(case x () of
     (x, xs) \<Rightarrow>
       LCons (\<lambda>_. (x, lappend xs ys)))  =
 LCons (\<lambda>_. (case x () of
     (x, xs) \<Rightarrow>
      (x, lappend xs ys))) "
  apply (fold prod.case_distrib[where h="\<lambda>t. LCons (\<lambda>_. t)"])
  apply (auto split: prod.splits)
  done
lemma "case_prod (\<lambda>x xs.
       LCons (\<lambda>_. (x, lappend xs ys))) (x ())  =
 LCons (case_prod (\<lambda>x xs.
      \<lambda>_. ((x, lappend xs ys))) (x())) "

  using prod.case_distrib[where h="\<lambda>t. LCons (\<lambda>_. t)" and f="\<lambda>x xs. (x, lappend xs ys)"]
  using prod.case_distrib
  apply (auto split: prod.splits)
  done

lemma "
 case_prod (\<lambda>x xs. 
     case_nat (case_prod (\<lambda>x xs. x) X) (\<lambda>y. (case_prod (\<lambda>x xs. lnth xs y) X)) n)
X
=
case_nat
 (case_prod (\<lambda>x xs. x) X)
(\<lambda>y. case_prod (\<lambda>x xs. lnth xs y) X) n
"
  apply (auto split: nat.splits)
  done

lemma case_prod_self:
  "case_prod (f (case_prod g X)) X = case_prod (\<lambda>x xs. f (g x xs) x xs) X"
  apply (auto split: prod.splits)
  done
thm nat.case_distrib[of "\<lambda>f. case_prod f _"
,unfolded nat.case_distrib[where h="\<lambda>x. x _ _"]
]

lemma "(\<lambda>n. case_prod (\<lambda>a b. case_nat (f1 a b) (\<lambda>m. f2 a b m) n))
= (\<lambda>n. case_nat (case_prod (\<lambda>a b. f1 a b)) (\<lambda>m. case_prod (\<lambda>a b. f2 a b m)) n)"
  apply (auto split: nat.splits)
  done

lemma "
case_nat
  (case_list (f1) (\<lambda>x xs. f2 x xs) l)
  (\<lambda>m. case_list (g1 m) (\<lambda>x xs. g2 m x xs) l)
  n
=
case_list
  (case_nat (f1) (\<lambda>m. g1 m) n)
  (\<lambda>x xs. case_nat (f2 x xs) (\<lambda>m. g2 m x xs) n)
  l
"
  apply (auto split: nat.split list.split)
  done
end
