theory LazyList
  imports Main
begin
ML_file "./Misc_Attrib.ML"

datatype 'a llist = LNil | LCons (lhd: 'a) (ltl: "unit \<Rightarrow> 'a llist")

abbreviation (input) "LConsd x xs \<equiv> LCons x (\<lambda>_. xs)"

primrec to_list :: "'a llist \<Rightarrow> 'a list" where
  "to_list LNil = []"
| "to_list (LCons x xs) = x # to_list (xs ())"
print_theorems

primrec from_list :: "'a list \<Rightarrow> 'a llist" where
  "from_list [] = LNil"
| "from_list (x#xs) = LConsd x (from_list xs)"

lemma to_list_from_list:
  "to_list (from_list x) = x"
  by (induction x) auto

lemma from_list_to_list:
  "from_list (to_list x) = x"
  by (induction x) auto

interpretation l: type_definition
  where Rep=to_list
    and Abs=from_list
    and A=UNIV
  by standard (auto simp: from_list_to_list to_list_from_list)

setup_lifting l.type_definition_axioms

lemma [transfer_rule]:
  includes lifting_syntax
  shows "(R ===> pcr_llist R ===> pcr_llist R) Cons LConsd"
    and "pcr_llist R Nil LNil"
  unfolding pcr_llist_def cr_llist_def
  by fastforce+

lemma [transfer_rule]:
  "pcr_llist R Nil LNil"
  unfolding pcr_llist_def cr_llist_def
  by fastforce

lift_definition lupt :: "nat \<Rightarrow> nat \<Rightarrow> nat llist" is upt .
lemmas [code] = upt_rec[Transfer.transferred]

lift_definition lupto :: "int \<Rightarrow> int \<Rightarrow> int llist" is upto .
lemmas [code] = upto.simps[Transfer.transferred]

lift_definition lreplicate :: "nat \<Rightarrow> 'a \<Rightarrow> 'a llist" is replicate .
lemmas [code] = replicate.simps[Transfer.transferred]

lift_definition lmap :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a llist \<Rightarrow> 'b llist" is map .
lemmas [code] = list.map[Transfer.transferred, elim_abs]

definition "lazy_append xs ys = append xs (ys ())"
lemmas lazy_append_simps = append.simps[elim_abs ys, folded lazy_append_def]

lift_definition lappend :: "'a llist \<Rightarrow> (unit \<Rightarrow> 'a llist) \<Rightarrow> 'a llist" is lazy_append .
lemmas [code] = lazy_append_simps[Transfer.transferred, elim_abs]

lift_definition lconcat :: "'a llist llist \<Rightarrow> 'a llist" is concat .
lemmas [code] = concat.simps[folded lazy_append_def[add_abs ys], Transfer.transferred, elim_abs]

code_datatype LNil LCons

value "hd (upto 0 30000000)"
value "lhd (lupto 0 30000000)"

end