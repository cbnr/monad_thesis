theory LazyList2
  imports
    Main
    "HOL-Library.FSet"
    "HOL-Library.Dlist"
begin
ML_file "./Misc_Attrib.ML"

typedef 'a lazy = "UNIV::(unit \<Rightarrow> 'a) set" ..

setup_lifting type_definition_lazy

lemma abs_unit_cases:
  obtains x where "f = (\<lambda>_::unit. x)"
  by fastforce

lemma lazy_cases:
  obtains x where "f = Abs_lazy (\<lambda>_::unit. x)"
  apply (cases f)
  subgoal for y
    by (cases y rule: abs_unit_cases) auto
  done

definition "Lazy x = Abs_lazy (\<lambda>_. x)"

free_constructors case_lazy for Lazy
  unfolding Lazy_def
   apply auto
  subgoal for P y
    apply (cases y)
    by (cases y rule: lazy_cases)
  subgoal for x y
    apply (auto simp: Abs_lazy_inject)
    using fun_cong[of "\<lambda>_. x" "\<lambda>_. y"] .
  done

bnf "'a lazy"
  map: "\<lambda>f. case_lazy (\<lambda>x. Lazy (f x))"
  sets: "case_lazy (\<lambda>x. {x})"
  bd: natLeq
         apply (auto split: lazy.splits
      simp add: natLeq_card_order natLeq_cinfinite
      simp add: Card_order_singl_ordLeq Field_natLeq)
  subgoal for R S x y b z za
    apply (rule exI[where x="case (z, za) of (Lazy (a,_), Lazy (_,c)) \<Rightarrow> Lazy (a,c)"])
    apply (auto split: prod.splits lazy.splits)
    done
  done
instantiation lazy :: (size) size begin
definition size_lazy where
  "size_lazy = case_lazy size"
instance ..
end

datatype 'a llist = LNil | LCons "('a \<times> 'a llist) lazy"
thm llist.size
value "size (LCons (Lazy (0::nat, LNil)))"
value "size [1,2,3::nat]"
value "size (LCons (Lazy (x, xs)))"
thm list.size
thm prod.size
term Nil
term "{}"
definition "R = {(xs, LCons (Lazy (x, xs))) | x xs. True}"
lemma wf_R:
  "wf R"
  unfolding R_def
  apply (rule wfUNIVI,simp)
  subgoal premises prems for P x
    supply prems = prems[rule_format]
    apply (induction x)
    subgoal by (rule prems) auto
    subgoal for x apply (cases x) apply (auto split: lazy.splits)
      apply (rule prems)
      apply auto
      done
    done
  done

function lsize :: "'a llist \<Rightarrow> nat" where
  "lsize LNil = 0"
| "lsize (LCons (Lazy (x, xs))) = Suc (lsize xs)"
  by pat_completeness auto
termination
  apply (rule )
   apply (rule wf_R)
  unfolding R_def
  apply auto
  done

lemma [measure_function]: "is_measure lsize" ..

fun to_list :: "'a llist \<Rightarrow> 'a list" where
  "to_list LNil = []"
| "to_list (LCons (Lazy (x, xs))) = x # to_list xs"

fun from_list :: "'a list \<Rightarrow> 'a llist" where
  "from_list [] = LNil"
| "from_list (x#xs) = LCons (Lazy (x, from_list xs))"

lemma to_list_from_list:
  "to_list (from_list x) = x"
  by (induction x) auto

lemma from_list_to_list:
  "from_list (to_list x) = x"
  apply (induction x)
  subgoal by simp
  subgoal for x by (cases x) auto
  done

interpretation l: type_definition
  where Rep=to_list
    and Abs=from_list
    and A=UNIV
  by standard (auto simp: from_list_to_list to_list_from_list)

definition "x = LCons (Lazy (3::nat, LNil))"
value x
export_code x in SML
setup_lifting l.type_definition_axioms

lemma [transfer_rule]:
  includes lifting_syntax
  shows "(R ===> pcr_llist R ===> pcr_llist R) Cons LConsd"
    and "pcr_llist R Nil LNil"
  unfolding pcr_llist_def cr_llist_def
  by fastforce+

lemma [transfer_rule]:
  "pcr_llist R Nil LNil"
  unfolding pcr_llist_def cr_llist_def
  by fastforce

lift_definition lupt :: "nat \<Rightarrow> nat \<Rightarrow> nat llist" is upt .
lemmas [code] = upt_rec[Transfer.transferred]

lift_definition lupto :: "int \<Rightarrow> int \<Rightarrow> int llist" is upto .
lemmas [code] = upto.simps[Transfer.transferred]

lift_definition lreplicate :: "nat \<Rightarrow> 'a \<Rightarrow> 'a llist" is replicate .
lemmas [code] = replicate.simps[Transfer.transferred]

lift_definition lmap :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a llist \<Rightarrow> 'b llist" is map .
lemmas [code] = list.map[Transfer.transferred, elim_abs]

definition "lazy_append xs ys = append xs (ys ())"
lemmas lazy_append_simps = append.simps[elim_abs ys, folded lazy_append_def]

lift_definition lappend :: "'a llist \<Rightarrow> (unit \<Rightarrow> 'a llist) \<Rightarrow> 'a llist" is lazy_append .
lemmas [code] = lazy_append_simps[Transfer.transferred, elim_abs]

lift_definition lconcat :: "'a llist llist \<Rightarrow> 'a llist" is concat .
lemmas [code] = concat.simps[folded lazy_append_def[add_abs ys], Transfer.transferred, elim_abs]

code_datatype LNil LCons

value "hd (upto 0 30000000)"
value "lhd (lupto 0 30000000)"

end