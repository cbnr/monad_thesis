theory State_Monad_Ext
  imports "HOL-Library.State_Monad"
begin

definition fun_app_lifted :: "('M,'a \<Rightarrow> ('M, 'b) state) state \<Rightarrow> ('M,'a) state \<Rightarrow> ('M,'b) state" where
  "fun_app_lifted f\<^sub>T x\<^sub>T \<equiv> do { f \<leftarrow> f\<^sub>T; x \<leftarrow> x\<^sub>T; f x }"

bundle state_monad_syntax begin

notation fun_app_lifted (infixl "." 999)
type_synonym ('a,'M,'b) fun_lifted = "'a \<Rightarrow> ('M,'b) state" ("_ ==_\<Longrightarrow> _" [3,1000,2] 2)
type_synonym ('a,'b) dpfun = "'a ==('a\<rightharpoonup>'b)\<Longrightarrow> 'b" (infixr "\<Rightarrow>\<^sub>T" 2)
type_notation state ("[_| _]\<^sub>m")

notation State_Monad.return ("\<langle>_\<rangle>")
notation (ASCII) State_Monad.return ("(#_#)")
notation Transfer.Rel ("Rel")

end

context includes state_monad_syntax begin

qualified lemma return_app_return:
  "\<langle>f\<rangle> . \<langle>x\<rangle> = f x"
  unfolding fun_app_lifted_def bind_left_identity ..

qualified lemma return_app_return_meta:
  "\<langle>f\<rangle> . \<langle>x\<rangle> \<equiv> f x"
  unfolding return_app_return .

qualified definition if\<^sub>T :: "('M, bool) state \<Rightarrow> ('M, 'a) state \<Rightarrow> ('M, 'a) state \<Rightarrow> ('M, 'a) state" where
  "if\<^sub>T b\<^sub>T x\<^sub>T y\<^sub>T \<equiv> do {b \<leftarrow> b\<^sub>T; if b then x\<^sub>T else y\<^sub>T}"

end

locale mem_subtype =
  fixes mem_inv :: "'mem \<Rightarrow> bool"
begin

context includes state_monad_syntax begin

definition pred :: "('a \<Rightarrow> bool) \<Rightarrow> ('mem, 'a) state \<Rightarrow> bool" where
  "pred val_pred s \<longleftrightarrow> (\<forall>mem.
    mem_inv mem \<longrightarrow> (case run_state s mem of
      (val, mem') \<Rightarrow> val_pred val \<and> mem_inv mem'))"

definition rel :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> ('mem, 'b) state \<Rightarrow> bool" where
  "rel val_rel v = pred (val_rel v)"

definition inv :: "('mem, 'a) state \<Rightarrow> bool" where
  "inv = pred (\<lambda>_. True)"

(* pred *)

lemma predI:
  "pred P s"
  if "\<And>mem v mem'. mem_inv mem \<Longrightarrow> run_state s mem = (v, mem') \<Longrightarrow> P v \<and> mem_inv mem'"
  using that unfolding pred_def by auto

lemma predE:
  assumes "pred P s" "mem_inv mem"
  obtains v mem' where "run_state s mem = (v, mem')" "P v" "mem_inv mem'"
  using assms unfolding pred_def by auto

lemma predD:
  assumes "pred P s" "mem_inv mem" "run_state s mem = (v, mem')"
  shows "P v \<and> mem_inv mem'"
  using assms by (auto elim: predE)

lemma pred_mono:
  "pred P s \<Longrightarrow> P \<le> Q \<Longrightarrow> pred Q s"
  by (auto intro: predI dest: predD)

notation pred_fun (infixr "~~~~>" 55)

lemma pred_bind:
  "pred Q (s \<bind> f)"
  if "pred P s" "\<And>v. P v \<Longrightarrow> pred Q (f v)"
  unfolding State_Monad.bind_def
  using that(1) by (auto intro!: predI elim!: predE dest: that(2) split: prod.splits)

lemma pred_bind_transfer:
  "(pred P ~~~~> (P ~~~~> pred Q) ~~~~> pred Q) (\<bind>)"
  unfolding pred_fun_def using pred_bind by auto

lemma pred_funD:
  "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by simp

lemma pred_fun_app:
  assumes "pred P x" "pred (P ~~~~> pred Q) f"
  shows "pred Q (f . x)"
  unfolding fun_app_lifted_def
  apply (rule pred_bind)
   apply (rule assms(2))
  apply (rule pred_bind)
   apply (rule assms(1))
  apply (fact pred_funD)
  done


(* rel *)

lemma relI:
  "rel R v s"
  if "\<And>mem mem' v'. mem_inv mem \<Longrightarrow> run_state s mem = (v', mem') \<Longrightarrow> R v v' \<and> mem_inv mem'"
  using that unfolding rel_def by (rule predI)
  

lemma relE:
  assumes "rel R v s" "mem_inv mem"
  obtains v' mem' where "run_state s mem = (v', mem')" "R v v'" "mem_inv mem'"
  using assms unfolding rel_def by (rule predE)

lemma relD:
  assumes "rel R v s" "mem_inv mem" "run_state s mem = (v', mem')"
  shows "R v v' \<and> mem_inv mem'"
  using assms unfolding rel_def by (rule predD)


(* inv *)

lemma invI[intro]:
  "inv s"
  if "\<And>mem v mem'. mem_inv mem \<Longrightarrow> run_state s mem = (v, mem') \<Longrightarrow> mem_inv mem'"
  using that unfolding inv_def by (auto intro: predI)

lemma invE[elim]:
  assumes "inv s" "mem_inv mem"
  obtains v mem' where "run_state s mem = (v, mem')" "mem_inv mem'"
  using assms unfolding inv_def by (rule predE)

lemma invD[dest]:
  assumes "inv s" "mem_inv mem" "run_state s mem = (v, mem')"
  shows "mem_inv mem'"
  using assms unfolding inv_def by (auto dest: predD)

lemma pred_return:
  "P v \<Longrightarrow> pred P (State_Monad.return v)"
  by (rule predI) simp

lemma pred_then:
  "pred Q (s \<then> t)"
  if "inv s" "pred Q t"
  using pred_bind[OF that[unfolded inv_def]] .

lemma pred_get:
  \<open>pred mem_inv State_Monad.get\<close>
  unfolding State_Monad.get_def
  by (rule predI) auto

lemma pred_get':
  "pred P (State_Monad.get \<bind> f)"
  if "\<And>mem. mem_inv mem \<Longrightarrow> pred P (f mem)"
  apply (rule pred_bind)
   apply (rule pred_get)
  apply (fact that)
  done

lemma inv_set:
  \<open>inv (State_Monad.set m)\<close>
  if \<open>mem_inv m\<close>
  unfolding set_def
  apply (rule invI)
  using that by (auto)

lemma pred_set':
  "pred P (State_Monad.set mem \<then> s)"
  if "mem_inv mem" "pred P s"
  apply (rule pred_then)
   apply (rule inv_set)
   apply (fact that)
  apply (fact that)
  done

lemma rel_return:
  "R v v' \<Longrightarrow> rel R v (State_Monad.return v')"
  unfolding rel_def by (fact pred_return)

lemma rel_bind:
  "rel S (f v) (s \<bind> g)"
  if "rel R v s" "\<And>v'. R v v' \<Longrightarrow> rel S (f v) (g v')"
  using that unfolding rel_def by (fact pred_bind)

lemma rel_get:
  "rel R (f v) (State_Monad.get \<bind> g)"
  if "\<And>mem. mem_inv mem \<Longrightarrow> rel R (f v) (g mem)"
  using that unfolding rel_def by (fact pred_get')

lemma rel_set:
  "rel R v (State_Monad.set mem \<then> s)"
  if "mem_inv mem" "rel R v s"
  using that unfolding rel_def by (fact pred_set')


lemma rel_bind_transfer[transfer_rule]:
  includes lifting_syntax
  shows "(rel R ===> (R ===> rel S) ===> rel S) (\<lambda>v f. f v) (\<bind>)"
  unfolding rel_fun_def by (auto elim: rel_bind)

lemma rel_fun_app_transfer[transfer_rule]:
  includes lifting_syntax
  shows "(rel (R ===> rel S) ===> rel R ===> rel S) (\<lambda>f v. f v) (.)"
  unfolding fun_app_lifted_def by transfer_prover

lemma inv_return:
  "inv (State_Monad.return v)"
  by (rule invI) simp

lemma inv_bind:
  "inv (s \<bind> f)"
  if "pred P s" "\<And>v. P v \<Longrightarrow> inv (f v)"
  using that unfolding inv_def by (rule pred_bind)

lemma inv_get:
  "inv State_Monad.get"
  unfolding State_Monad.get_def
  by (rule invI) auto

lemma inv_get':
  "inv (State_Monad.get \<bind> f)"
  if "\<And>mem. mem_inv mem \<Longrightarrow> inv (f mem)"
  apply (rule inv_bind)
   apply (rule pred_get)
  apply (fact that)
  done

lemma inv_set':
  "inv (State_Monad.set mem \<then> s)"
  if "mem_inv mem" "inv s"
  apply (rule inv_bind)
   apply (rule inv_set[unfolded inv_def])
   apply (fact that)
  apply (rule that)
  done

definition hoare_triple where
  \<open>hoare_triple P s Q = (\<forall>mem. P mem \<and> mem_inv mem \<longrightarrow>
    (let (v,mem') = run_state s mem in Q (v,mem') \<and> mem_inv mem'))\<close>

lemma hoare_tripleI[intro]:
  assumes \<open>\<And>mem v mem'. \<lbrakk>P mem; mem_inv mem; run_state s mem = (v,mem')\<rbrakk> \<Longrightarrow> Q (v,mem') \<and> mem_inv mem'\<close>
  shows \<open>hoare_triple P s Q\<close>
  unfolding hoare_triple_def
  by (auto dest: assms)

lemma hoare_tripleD[dest]:
  assumes \<open>hoare_triple P s Q\<close>
  assumes \<open>P mem\<close> \<open>mem_inv mem\<close> \<open>run_state s mem = (v,mem')\<close>
  shows \<open>Q (v,mem') \<and> mem_inv mem'\<close>
  using assms[unfolded hoare_triple_def]
  by auto

lemma hoare_let:
  assumes \<open>hoare_triple P (f v) Q\<close>
  shows \<open>hoare_triple P (Let v f) Q\<close>
  using assms unfolding Let_def .

lemma hoare_pure_conj:
  \<open>hoare_triple (\<lambda>m. b \<and> P m) m Q \<longleftrightarrow> b \<longrightarrow> hoare_triple P m Q\<close>
  by (auto)

lemma hoare_if:
  assumes \<open>b \<Longrightarrow> hoare_triple P x Q\<close>
  assumes \<open>\<not>b \<Longrightarrow> hoare_triple P y Q\<close>
  shows \<open>hoare_triple P (if b then x else y) Q\<close>
  using assms by (cases b) auto

lemma hoare_case_prod:
  assumes \<open>\<And>a b. x = (a,b) \<Longrightarrow> hoare_triple P (f a b) Q\<close>
  shows \<open>hoare_triple P (case x of (a,b) \<Rightarrow> f a b) Q\<close>
  using assms by (auto split: prod.split)

lemma hoare_bind:
  assumes \<open>hoare_triple P s P'\<close>
  assumes \<open>\<And>v. hoare_triple (\<lambda>mem. P' (v, mem)) (f v) Q\<close>
  shows \<open>hoare_triple P (s \<bind> f) Q\<close>
  unfolding State_Monad.bind_def
  by (rule hoare_tripleI) (auto split: prod.splits dest: assms[THEN hoare_tripleD])

lemma hoare_relax_pre:
  assumes \<open>hoare_triple P s Q\<close> \<open>\<And>mem. P' mem \<Longrightarrow> mem_inv mem \<Longrightarrow> P mem\<close>
  shows \<open>hoare_triple P' s Q\<close>
  by (rule hoare_tripleI) (auto dest: assms(2) assms(1)[THEN hoare_tripleD])

lemma hoare_relax_post:
  assumes \<open>hoare_triple P s Q\<close>
  assumes \<open>\<And>v mem mem'. \<lbrakk>run_state s mem = (v, mem'); P mem; mem_inv mem; Q (v, mem'); mem_inv mem'\<rbrakk> \<Longrightarrow> Q' (v, mem')\<close>
  shows \<open>hoare_triple P s Q'\<close>
  by (rule hoare_tripleI) (auto dest: assms(2) assms(1)[THEN hoare_tripleD])

lemma hoare_relax:
  assumes \<open>hoare_triple P s Q\<close>
  assumes \<open>\<And>mem. P' mem \<Longrightarrow> mem_inv mem \<Longrightarrow> P mem\<close>
  assumes \<open>\<And>v mem mem'. \<lbrakk>run_state s mem = (v, mem'); P' mem; mem_inv mem; Q (v, mem'); mem_inv mem'\<rbrakk> \<Longrightarrow> Q' (v, mem')\<close>
  shows \<open>hoare_triple P' s Q'\<close>
  apply (rule hoare_relax_post)
  apply (rule hoare_relax_pre)
    apply fact+
  done

lemma hoare_return':
  \<open>hoare_triple P (State_Monad.return v) (\<lambda>(v',mem). v'=v \<and> P mem)\<close>
  unfolding State_Monad.return_def
  by (auto)

lemma hoare_return:
  assumes \<open>\<And>mem. P mem \<Longrightarrow> mem_inv mem \<Longrightarrow> Q (v, mem)\<close>
  shows \<open>hoare_triple P (State_Monad.return v) Q\<close>
  apply (rule hoare_relax_post)
  apply (rule hoare_return')
  using assms by (auto)

lemma inv_hoare_eq:
  shows \<open>inv s \<longleftrightarrow> hoare_triple (\<lambda>_. True) s (\<lambda>_. True)\<close>
  by auto

lemma hoare_get:
  shows \<open>hoare_triple P State_Monad.get (\<lambda>(m,m'). P m \<and> m=m')\<close>
  unfolding State_Monad.get_def
  by auto

lemma hoare_set:
  assumes \<open>\<And>m'. P m' \<Longrightarrow> mem_inv m' \<Longrightarrow> Q m \<and> mem_inv m\<close>
  shows \<open>hoare_triple P (State_Monad.set m) (\<lambda>(_,m'). Q m' \<and> m'=m)\<close>
  unfolding State_Monad.set_def
  using assms by auto

lemmas hoare_bind_get = hoare_bind[OF hoare_get]
end

end

end
