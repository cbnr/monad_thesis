theory DP_CRelVS
  imports "./State_Monad_Ext" "../Pure_Monad"
begin

locale state_mem_defs =
fixes lookup :: "'param \<Rightarrow> ('mem, 'result option) state"
  and update :: "'param \<Rightarrow> 'result \<Rightarrow> ('mem, unit) state"
begin

definition map_of where
  "map_of heap k = fst (run_state (lookup k) heap)"

definition checkmem_lazy :: "'param \<Rightarrow> (unit \<Rightarrow> ('mem, 'result) state) \<Rightarrow> ('mem, 'result) state" where
  "checkmem_lazy param calc = do {
    x \<leftarrow> lookup param;
    case x of
      Some x \<Rightarrow> State_Monad.return x
    | None \<Rightarrow> do {
        x \<leftarrow> calc ();
        update param x;
        State_Monad.return x
      }
  }"

definition checkmem :: "'param \<Rightarrow> ('mem, 'result) state \<Rightarrow> ('mem, 'result) state" where
  "checkmem param calc = checkmem_lazy param (\<lambda>_. calc)"

end (* Mem Defs *)

locale state_mem_inv =
  state_mem_defs where lookup=lookup +
  mem: mem_subtype mem_inv
for lookup :: "'param \<Rightarrow> ('mem, 'result option) state" and mem_inv :: "'mem \<Rightarrow> bool" +
assumes lookup_inv: "\<And>k. mem.inv (lookup k)"
assumes update_inv: "\<And>k v. mem.inv (update k v)"



locale mem_correct =
  state_mem_inv where lookup=lookup and mem_inv=mem_inv
for lookup :: "'param \<Rightarrow> ('mem, 'result option) state" and mem_inv :: "'mem \<Rightarrow> bool" +
assumes lookup_correct: "\<And>k v m m'.
    mem_inv m \<Longrightarrow> run_state (lookup k) m = (v, m') \<Longrightarrow> map_of m' \<subseteq>\<^sub>m map_of m"
assumes update_correct: "\<And>k v m m' u.
    mem_inv m \<Longrightarrow> run_state (update k v) m = (u, m') \<Longrightarrow> map_of m' \<subseteq>\<^sub>m map_of m(k \<mapsto> v)"

locale dp_consistency =
  mem_correct where lookup=lookup for lookup :: "'param \<Rightarrow> ('mem, 'result option) state" +
fixes dp :: "'param \<Rightarrow> 'result"
begin


definition cmem :: "'mem \<Rightarrow> bool" where
  "cmem M \<equiv> \<forall>param\<in>dom (map_of M). map_of M param = Some (dp param)"

sublocale dp_mem: mem_subtype "inf mem_inv cmem" .

context
  includes lifting_syntax state_monad_syntax
begin

abbreviation rel_fun_lifted :: "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c ==_\<Longrightarrow> 'd) \<Rightarrow> bool" (infixr "===>\<^sub>T" 55) where
  "rel_fun_lifted R R' \<equiv> R ===> dp_mem.rel R'"

definition consistentDP :: "('param == 'mem \<Longrightarrow> 'result) \<Rightarrow> bool" where
  "consistentDP = (op = ===> dp_mem.rel op =) dp"

private lemma cmemI:
  assumes "\<And>param v M'. State_Monad.run_state (lookup param) M = (Some v, M') \<Longrightarrow> v = dp param"
  shows "cmem M"
  unfolding cmem_def map_of_def fst_def  
  by (auto split: prod.splits intro: assms)

private lemma cmemD:
  assumes "cmem M" "State_Monad.run_state (lookup param) M = (Some v, M')"
  shows "dp param = v"
  using assms unfolding cmem_def dom_def map_of_def fst_def
  by (auto split: prod.splits)

lemma consistentDP_intro:
  assumes "\<And>param. Rel (dp_mem.rel op=) (dp param) (dp\<^sub>T param)"
  shows "consistentDP dp\<^sub>T"
  using assms unfolding consistentDP_def Rel_def by blast

lemma rel_vs_return:
  "\<lbrakk>Rel R x y\<rbrakk> \<Longrightarrow> Rel (dp_mem.rel R) (Wrap x) (State_Monad.return y)"
  unfolding Wrap_def Rel_def by (fact dp_mem.rel_return)

lemma rel_vs_return_ext:
  "\<lbrakk>Rel R x y\<rbrakk> \<Longrightarrow> Rel (dp_mem.rel R) x (State_Monad.return y)"
  by (fact rel_vs_return[unfolded Wrap_def])

lemma cmem_map_of_le:
  "map_of mem' \<subseteq>\<^sub>m map_of mem \<Longrightarrow> cmem mem \<Longrightarrow> cmem mem'"
  unfolding cmem_def map_le_def Ball_def by fastforce

lemma cmem_map_of_le_upd:
  "map_of mem' \<subseteq>\<^sub>m map_of mem(k\<mapsto>dp k) \<Longrightarrow> cmem mem \<Longrightarrow> cmem mem'"
  unfolding cmem_def map_le_def Ball_def by (fastforce split: if_splits)

lemma inv_lookup: "dp_mem.inv (lookup k)"
  by (auto intro: dp_mem.invI dest: lookup_inv[THEN mem.invD] lookup_correct[THEN cmem_map_of_le])

lemma inv_update: "dp_mem.inv (update k (dp k))"
  by (auto intro: dp_mem.invI dest: update_inv[THEN mem.invD] update_correct[THEN cmem_map_of_le_upd])

lemma rel_vs_lookup':
  "dp_mem.pred (\<lambda> v. v\<in>{Some (dp k), None}) (lookup k)"
  using inv_lookup by (auto intro!: dp_mem.predI split: option.split dest: dp_mem.invD cmemD)

lemma rel_vs_update':
  "dp_mem.pred (\<lambda> _. True) (update k (dp k))"
  using inv_update unfolding dp_mem.inv_def .

private lemma rel_vs_checkmem:
  "\<lbrakk>is_equality R; Rel (dp_mem.rel R) (dp param) s\<rbrakk>
  \<Longrightarrow> Rel (dp_mem.rel R) (dp param) (checkmem param s)"
  unfolding checkmem_def checkmem_lazy_def Rel_def is_equality_def
  unfolding dp_mem.rel_def
  by (auto split: option.split_asm intro!:
      dp_mem.pred_return
      mem_subtype.pred_bind
      rel_vs_lookup' rel_vs_update')

lemma rel_vs_checkmem_tupled:
  assumes "v = dp param"
  shows "\<lbrakk>is_equality R; Rel (dp_mem.rel R) v s\<rbrakk>
        \<Longrightarrow> Rel (dp_mem.rel R) v (checkmem param s)"
  unfolding assms by (fact rel_vs_checkmem)

(** Transfer rules **)
lemma return_transfer[transfer_rule]:
  "(R ===>\<^sub>T R) Wrap State_Monad.return"
  unfolding rel_fun_def by (metis rel_vs_return Rel_def)

lemma fun_app_lifted_transfer[transfer_rule]:
  "(dp_mem.rel (R0 ===>\<^sub>T R1) ===> dp_mem.rel R0 ===> dp_mem.rel R1) App (op .)"
  unfolding App_def fun_app_lifted_def by transfer_prover

lemma rel_vs_fun_app:
  "\<lbrakk>Rel (dp_mem.rel R0) x x\<^sub>T; Rel (dp_mem.rel (R0 ===>\<^sub>T R1)) f f\<^sub>T\<rbrakk> \<Longrightarrow> Rel (dp_mem.rel R1) (App f x) (f\<^sub>T . x\<^sub>T)"
  unfolding Rel_def using fun_app_lifted_transfer[THEN rel_funD, THEN rel_funD] .

(* HOL *)
lemma if\<^sub>T_transfer[transfer_rule]:
  "(dp_mem.rel op = ===> dp_mem.rel R ===> dp_mem.rel R ===> dp_mem.rel R) If State_Monad_Ext.if\<^sub>T"
  unfolding State_Monad_Ext.if\<^sub>T_def by transfer_prover

end (* Lifting Syntax *)
end (* Consistency *)
end (* Theory *)