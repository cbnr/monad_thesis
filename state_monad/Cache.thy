theory Cache
  imports
    "HOL-Library.State_Monad"
    "HOL-Imperative_HOL.Imperative_HOL"
    "./State_Monad_Ext"
    "HOL-Library.RBT_Mapping"
    "../StateT_Heap/StateT_Heap_Monad_Ext"
begin

locale abs_mem_def =
  State_Monad_Ext.mem_subtype mem_inv
  for mem_inv :: "'mem \<Rightarrow> bool" +
  fixes to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value"

locale forgettable_map =
  mem: abs_mem_def where to_map=to_map for to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value" +
  fixes lookup :: "'key \<Rightarrow> ('mem, 'value option) State_Monad.state"
  fixes update :: "'key \<Rightarrow> 'value \<Rightarrow> ('mem, unit) State_Monad.state"
  assumes lookup_inv: "\<And>k. mem.inv (lookup k)"
  assumes update_inv: "\<And>k v. mem.inv (update k v)"
  assumes lookup_correct: "\<And>mem k v mem'.
    mem_inv mem \<Longrightarrow> State_Monad.run_state (lookup k) mem = (v, mem') \<Longrightarrow>
      v = to_map mem k \<and> to_map mem' \<subseteq>\<^sub>m to_map mem"
  assumes update_correct: "\<And>mem k v mem'.
    mem_inv mem \<Longrightarrow> State_Monad.run_state (update k v) mem = ((), mem') \<Longrightarrow>
      to_map mem' \<subseteq>\<^sub>m to_map mem(k\<mapsto>v)"

locale forgettable_map_clear =
  mem: abs_mem_def
  where to_map=to_map for to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value" +
  fixes clear :: "('mem, unit) State_Monad.state"
  assumes clear_inv: "mem.inv clear"
  assumes clear_correct:
    "mem_inv mem \<Longrightarrow> State_Monad.run_state clear mem = ((), mem') \<Longrightarrow>
      to_map mem' = Map.empty"

locale forgettable_map_clearable =
  forgettable_map where to_map=to_map +
  forgettable_map_clear where to_map=to_map
for to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value"

locale forgettable_map_empty =
  mem: abs_mem_def
  where to_map=to_map for to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value" +
  fixes empty_map :: 'mem
  assumes empty_inv: "mem_inv empty_map"
  assumes empty_correct: "to_map empty_map = Map.empty"

locale forgettable_map_pure =
  mem: abs_mem_def
  where mem_inv=mem_inv
    and to_map=to_map
  for mem_inv and to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value" +
  fixes lookup_fun :: "'mem \<Rightarrow> 'key \<Rightarrow> 'value option"
  fixes update_fun :: "'mem \<Rightarrow> 'key \<Rightarrow> 'value \<Rightarrow> 'mem"
  assumes lookup_correct: "\<And>k. lookup_fun mem k = to_map mem k"
  assumes update_inv: "\<And>mem k v. mem_inv mem \<Longrightarrow> mem_inv (update_fun mem k v)"
  assumes update_correct: "\<And>mem k v. to_map (update_fun mem k v) \<subseteq>\<^sub>m to_map mem(k\<mapsto>v)"
begin

definition "lookup k = do {m \<leftarrow> State_Monad.get; State_Monad.return (lookup_fun m k)}"
definition "update k v = do {m \<leftarrow> State_Monad.get; State_Monad.set (update_fun m k v); State_Monad.return ()}"

sublocale forgettable_map
  where lookup=lookup
    and update=update
  supply defs = lookup_def update_def State_Monad.get_def State_Monad.set_def State_Monad.bind_def
  supply assms = lookup_correct update_inv update_correct
  by standard (auto intro!: mem.invI simp: defs assms)

end

locale forgettable_map_clear_pure =
  forgettable_map_empty where to_map=to_map for to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value"
begin

definition "clear = State_Monad.set empty_map"

sublocale forgettable_map_clear
  where mem_inv=mem_inv
    and to_map=to_map
    and clear=clear
   supply defs = clear_def State_Monad.set_def
   supply assms = empty_inv empty_correct
  by standard (auto intro!: mem.invI simp: defs assms)

end

locale forgettable_map_clearable_pure =
  forgettable_map_pure where to_map=to_map +
  forgettable_map_clear_pure where to_map=to_map
for to_map :: "'mem \<Rightarrow> 'key \<rightharpoonup> 'value"
begin

sublocale forgettable_map_clearable
  where lookup=lookup
    and update=update
    and clear=clear
  ..
end

locale forgettable_map_rbt
begin

sublocale forgettable_map_clearable_pure
  where mem_inv=top
    and to_map=Mapping.mapping.rep
    and lookup_fun=Mapping.lookup
    and update_fun="\<lambda>mem k v. Mapping.update k v mem"
    and empty_map=Mapping.empty
  supply reps = Mapping.lookup.rep_eq Mapping.update.rep_eq Mapping.empty.rep_eq
  by standard (auto simp: reps)

thm forgettable_map_clear_pure.axioms[OF forgettable_map_clear_pure_axioms]

end

locale forgettable_map_map
begin

sublocale forgettable_map_clearable_pure
  where mem_inv=top
    and to_map=id
    and lookup_fun="\<lambda>mem k. mem k"
    and update_fun="\<lambda>mem k v. mem(k\<mapsto>v)"
    and empty_map=Map.empty
  by standard (auto simp del: fun_upd_apply)

end

print_locale forgettable_map
locale direct_mapped_cache =
  line: forgettable_map_clearable
  where mem_inv=line_inv
    and lookup=lookup_line
    and update=update_line
    and clear=clear_line
    and to_map=line_to_map
  for line_to_map :: "'cache_line \<Rightarrow> 'block_offset \<rightharpoonup> 'value"
    and lookup_line  update_line clear_line line_inv +
  fixes split_key :: "'key \<Rightarrow> ('tag \<times> 'set_index \<times> 'block_offset)"
  fixes get_cache_line :: "'set_index \<Rightarrow> ('tag \<times> 'cache_line)"
  fixes get_tag :: "'cache_line \<Rightarrow> 'tag"
begin

definition lookup where
  "lookup k = do {
    let (tag, set_index, block_offset) = split_key k;
    let (tag', cache_line) = get_cache_line set_index;
    if tag = tag'
      then do {
        State_Monad.return None
      }
      else State_Monad.return None
  }"

definition update where
  "update k v = do {
    let (tag, set_index, block_offset) = split_key k;
    let (tag', cache_line) = get_cache_line set_index;
    if tag = tag'
      then do {
        State_Monad.return ()
      }
      else do {
        State_Monad.return ()
      }
  }"

sublocale forgettable_map
  where lookup=lookup
    and update=update
  apply standard
  oops
end

locale sh_abs_mem_def =
  StateT_Heap_Monad_Ext.mem_subtype mem_inv
  for mem_inv :: "('mem \<times> heap) \<Rightarrow> bool" +
  fixes to_map :: "('mem \<times> heap) \<Rightarrow> 'key \<rightharpoonup> 'value"
begin

sublocale as_state: abs_mem_def .

end (* locale sh_abs_mem_def *)

locale sh_forgettable_map =
  mem: sh_abs_mem_def where to_map=to_map for to_map :: "('mem \<times> heap) \<Rightarrow> 'key \<rightharpoonup> 'value" +
  fixes lookup :: "'key \<Rightarrow> ('mem, 'value option) StateT_Heap_Monad.t"
  fixes update :: "'key \<Rightarrow> 'value \<Rightarrow> ('mem, unit) StateT_Heap_Monad.t"
  assumes lookup_inv: "\<And>k. mem.inv (lookup k)"
  assumes update_inv: "\<And>k v. mem.inv (update k v)"
  assumes lookup_success: "\<And>m h k. mem_inv (m, h) \<Longrightarrow>
    Heap_Monad.success (StateT_Heap_Monad.run (lookup k) m) h"
  assumes update_success: "\<And>m h k v. mem_inv (m, h) \<Longrightarrow>
    Heap_Monad.success (StateT_Heap_Monad.run (update k v) m) h"
  assumes lookup_correct: "\<And>m h k v m' h'. mem_inv (m, h) \<Longrightarrow>
    \<lbrakk>Heap_Monad.execute (StateT_Heap_Monad.run (lookup k) m) h = Some ((v, m'), h')\<rbrakk> \<Longrightarrow>
      v = to_map (m, h) k \<and> to_map (m', h') \<subseteq>\<^sub>m to_map (m, h)"
  assumes update_correct: "\<And>m h k v m' h'. mem_inv (m, h) \<Longrightarrow>
    \<lbrakk>Heap_Monad.execute (StateT_Heap_Monad.run (update k v) m) h = Some (((), m'), h')\<rbrakk> \<Longrightarrow>
      to_map (m', h') \<subseteq>\<^sub>m to_map (m, h)(k\<mapsto>v)"
begin

definition "lookup_as_state k = StateT_Heap_Monad.to_state (lookup k)"
definition "update_as_state k v = StateT_Heap_Monad.to_state (update k v)"
sublocale as_state: forgettable_map
  where lookup=lookup_as_state
    and update=update_as_state
  apply standard
  subgoal
    unfolding lookup_as_state_def
    apply (rule mem.inv_inv_as_state(1))
    apply (rule lookup_inv)
    done
  subgoal
    unfolding update_as_state_def
    apply (rule mem.inv_inv_as_state(1))
    apply (rule update_inv)
    done
  subgoal
    unfolding lookup_as_state_def
    unfolding StateT_Heap_Monad.to_state_def
    apply (auto split: option.split_asm prod.split_asm)
       apply (auto dest: lookup_success[unfolded success_def] lookup_correct)
    done
  subgoal
    unfolding update_as_state_def
    unfolding StateT_Heap_Monad.to_state_def
    apply (auto split: option.split_asm prod.split_asm)
       apply (auto dest: update_success[unfolded success_def] update_correct)
    done
  done

end (* locale sh_forgettable_map *)


end