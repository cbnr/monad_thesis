theory State_Monad_Lens
  imports State_Monad_Ext "../Index1"
begin

locale state_lens_def =
  fixes gets :: \<open>('lm, 'sm) state\<close>
  fixes puts :: \<open>'sm \<Rightarrow> ('lm, unit) state\<close>
begin

definition focus :: \<open>('sm, 'v) state \<Rightarrow> ('lm, 'v) state\<close> where
  "focus s = do {
    sm \<leftarrow> gets;
    let (v, sm') = run_state s sm;
    puts sm';
    State_Monad.return v
  }"

end

locale state_lens =
  state_lens_def where gets=\<open>gets::('lm, 'sm) state\<close>
  + large_mem: mem_subtype \<open>lm_inv::'lm\<Rightarrow>bool\<close> + small_mem: mem_subtype \<open>sm_inv::'sm\<Rightarrow>bool\<close>
for gets lm_inv sm_inv +
assumes gets_hoare: \<open>\<And>P. large_mem.hoare_triple P gets (\<lambda>(sm,lm). sm_inv sm \<and> P lm)\<close>
assumes puts_inv: \<open>\<And>sm. sm_inv sm \<Longrightarrow> large_mem.inv (puts sm)\<close>
begin

lemma hoare:
  assumes \<open>small_mem.hoare_triple Psmall s Qsmall\<close>
  assumes pre: \<open>\<And>lm. P lm \<Longrightarrow> lm_inv lm \<Longrightarrow> Psmall (fst (run_state gets lm))\<close>
  assumes post: \<open>\<And>v sm lm. \<lbrakk>P lm; lm_inv lm; Qsmall (v, sm); sm_inv sm\<rbrakk> \<Longrightarrow> Q (v, snd (run_state (puts sm) lm))\<close>
  shows \<open>large_mem.hoare_triple P (focus s) Q\<close>
proof -
  let ?gets_post = \<open>(\<lambda>(sm,lm). Psmall sm \<and> sm_inv sm \<and> P lm)\<close>
  have gets_hoare': \<open>large_mem.hoare_triple P gets ?gets_post\<close>
    apply (rule large_mem.hoare_relax_post)
     apply (rule gets_hoare)
    apply (auto dest: pre)
    done

  let ?puts_pre = \<open>(\<lambda>v sm. \<lambda>lm. Qsmall (v, sm) \<and> sm_inv sm \<and> P lm)\<close>
  let ?puts_post = \<open>\<lambda>v. \<lambda>(_,lm). Q (v, lm)\<close>
  have puts_hoare: \<open>\<And>sm v. large_mem.hoare_triple (?puts_pre v sm) (puts sm) (?puts_post v)\<close>
    apply (rule large_mem.hoare_tripleI)
    using puts_inv by (auto dest: post)

  show ?thesis
    unfolding focus_def
    apply (rule large_mem.hoare_bind[where P'=\<open>?gets_post\<close>])
    subgoal by (rule gets_hoare')
    apply (clarsimp split: prod.split)
    subgoal for sm v sm'
      apply (rule large_mem.hoare_bind[where P'=\<open>?puts_post v\<close>])
      subgoal
        apply (rule large_mem.hoare_relax_pre[where P=\<open>?puts_pre v sm'\<close>])
        subgoal by (rule puts_hoare)
        subgoal using assms(1) by (auto)
        done
      subgoal by (rule large_mem.hoare_relax_post[OF large_mem.hoare_return]) auto
      done
    done
qed

end

locale state_fun_lens_def =
  fixes getf :: \<open>'lm \<Rightarrow> 'sm\<close>
  fixes putf :: \<open>'sm \<Rightarrow> 'lm \<Rightarrow> 'lm\<close>
begin

definition \<open>gets = do {lm \<leftarrow> State_Monad.get; State_Monad.return (getf lm)}\<close>
definition \<open>puts sm = do {lm \<leftarrow> State_Monad.get; State_Monad.set (putf sm lm)}\<close>
sublocale state_lens_def
  where gets=gets
    and puts=puts
  .

end

locale state_fun_lens =
  state_fun_lens_def where getf=\<open>getf::'lm \<Rightarrow> 'sm\<close>
  + large_mem: mem_subtype \<open>lm_inv::'lm\<Rightarrow>bool\<close> + small_mem: mem_subtype \<open>sm_inv::'sm\<Rightarrow>bool\<close>
for getf lm_inv sm_inv +
assumes getf_inv: \<open>\<And>lm. lm_inv lm \<Longrightarrow> sm_inv (getf lm)\<close>
assumes putf_inv: \<open>\<And>sm lm. sm_inv sm \<Longrightarrow> lm_inv lm \<Longrightarrow> lm_inv (putf sm lm)\<close>
begin

interpretation state_lens
  where gets=gets
    and puts=puts
  apply unfold_locales
  subgoal
    unfolding gets_def
    apply (rule large_mem.hoare_bind[OF large_mem.hoare_get])
    apply (rule large_mem.hoare_return)
    apply (auto simp: getf_inv)
    done
  subgoal
    unfolding puts_def
    apply (rule large_mem.inv_bind)
     apply (rule large_mem.pred_get)
    apply (rule large_mem.inv_set)
    apply (fact putf_inv)
    done
  done

lemma hoare:
  assumes \<open>small_mem.hoare_triple Psmall s Qsmall\<close>
  assumes pre: \<open>\<And>lm. P lm \<Longrightarrow> lm_inv lm \<Longrightarrow> Psmall (getf lm)\<close>
  assumes post: \<open>\<And>v sm lm. \<lbrakk>P lm; lm_inv lm; Qsmall (v, sm); sm_inv sm\<rbrakk> \<Longrightarrow> Q (v, putf sm lm)\<close>
  shows \<open>large_mem.hoare_triple P (focus s) Q\<close>
  apply (rule hoare[OF assms(1)])
  subgoal unfolding gets_def by simp (fact pre)
  subgoal unfolding puts_def by simp (fact post)
  done

end

context begin
interpretation the_lens: state_fun_lens
  where getf=the
    and putf=\<open>\<lambda>t _. Some t\<close>
    and lm_inv=\<open>\<lambda>m. m\<noteq>None \<and> sm_inv (the m)\<close>
  for sm_inv
  apply standard
  subgoal by simp
  subgoal by simp
  done

definition \<open>the_lens_focus = the_lens.focus\<close>
lemmas the_lens_hoare = the_lens.hoare[folded the_lens_focus_def]

interpretation fst_lens: state_fun_lens
  where getf=fst
    and putf=\<open>\<lambda>d. apfst (\<lambda>_. d)\<close>
    and lm_inv=\<open>pred_prod fst_inv snd_inv\<close>
    and sm_inv=fst_inv
  for fst_inv snd_inv
  apply standard
  subgoal for lm by (cases lm) simp
  subgoal for sm lm by (cases lm) simp
  done

definition \<open>fst_lens_focus = fst_lens.focus\<close>
lemmas fst_lens_hoare = fst_lens.hoare[folded fst_lens_focus_def]

interpretation snd_lens: state_fun_lens
  where getf=snd
    and putf=\<open>\<lambda>d. apsnd (\<lambda>_. d)\<close>
    and lm_inv=\<open>pred_prod fst_inv snd_inv\<close>
    and sm_inv=snd_inv
  for fst_inv snd_inv
  apply standard
  subgoal for lm by (cases lm) simp
  subgoal for sm lm by (cases lm) simp
  done

definition \<open>snd_lens_focus = snd_lens.focus\<close>
lemmas snd_lens_hoare = snd_lens.hoare[folded snd_lens_focus_def]

end

locale list_lens_def
begin
context
  fixes i :: nat
begin

definition \<open>getf l = l!i\<close>
definition \<open>putf x l = l[i:=x]\<close>
interpretation lens: state_fun_lens_def
  where getf=getf
    and putf=putf
  .
definition \<open>focus=lens.focus\<close>
end

end

locale list_lens =
  large_mem: mem_subtype list_inv + small_mem: mem_subtype elem_inv
  for list_inv::\<open>'elem list \<Rightarrow> bool\<close> and elem_inv::\<open>'elem \<Rightarrow> bool\<close> +
  assumes get_inv: \<open>\<And>i l. i < length l \<Longrightarrow> list_inv l \<Longrightarrow> elem_inv (l!i)\<close>
  assumes put_inv: \<open>\<And>i v l. i < length l \<Longrightarrow> elem_inv v \<Longrightarrow> list_inv l \<Longrightarrow> list_inv (l[i:=v])\<close>
begin

context
  fixes i :: nat
  assumes i: \<open>\<And>l. list_inv l \<Longrightarrow> i < length l\<close>
begin

interpretation def: list_lens_def .
print_locale state_fun_lens
interpretation state_fun_lens
  where getf=\<open>def.getf i\<close>
    and putf=\<open>def.putf i\<close>
    and lm_inv=list_inv
    and sm_inv=elem_inv
  apply standard
  subgoal unfolding def.getf_def list_all_iff using get_inv[OF i] by (auto)
  subgoal unfolding def.putf_def list_all_iff using put_inv[OF i]
    by (auto dest: set_update_subset_insert[THEN subsetD])
  done

lemmas hoare=hoare
end
end


end