theory Imperative_HOL_Ext
  imports
    "HOL-Imperative_HOL.Imperative_HOL"
begin

definition array_valid :: "Heap.heap \<Rightarrow> 'a::heap array \<Rightarrow> bool" where
  "array_valid h a \<longleftrightarrow> (\<exists>xs::'a list.
      arrays h TYPEREP('a) (addr_of_array a) = map to_nat xs
    )"

find_theorems Array.present 

thm Array.not_present_alloc

thm Array.present_alloc[no_vars]
lemma array_valid_alloc:
  "array_valid (snd (Array.alloc xs h)) (fst (Array.alloc xs h))"
  unfolding array_valid_def
  unfolding Array.alloc_def Array.set_def Let_def
  by auto

lemma from_nat_to_nat[simp]: "from_nat \<circ> to_nat = id"
  by auto

thm Array.present_update[no_vars]
lemma array_present_update:
  fixes a b :: "'a::heap array"
  shows "array_valid h a \<Longrightarrow> array_valid (Array.update b i v h) a"
  unfolding array_valid_def
  unfolding Array.update_def
  unfolding Array.get_def Array.set_def
  by (auto simp: fun_eq_iff)

lemma array_set_get:
  fixes a :: "'a::heap array"
  assumes "array_valid h a"
  shows "Array.set a (Array.get h a) h = h"
  using assms
  unfolding array_valid_def
  unfolding Array.set_def Array.get_def
  by (auto simp add: fun_upd_idem)

end