theory Memory_Tracing
  imports Memory
begin

paragraph \<open>Tracing Memory\<close>
context state_mem_defs
begin

definition
  "lookup_trace k =
  State (\<lambda> (log, m). case State_Monad.run_state (lookup k) m of
    (None, m) \<Rightarrow> (None, ((''Missed'', k) # log, m)) |
    (Some v, m) \<Rightarrow> (Some v, ((''Found'', k) # log, m))
  )"

definition
  "update_trace k v =
  State (\<lambda> (log, m). case State_Monad.run_state (update k v) m of
    (_, m) \<Rightarrow> ((), ((''Stored'', k) # log, m))
  )"

end

context mem_correct
begin

lemma map_of_simp:
  "state_mem_defs.map_of lookup_trace = map_of o snd"
  unfolding state_mem_defs.map_of_def lookup_trace_def
  by (rule ext) (auto split: prod.split option.split)

lemma mem_correct_tracing: "mem_correct update_trace (P o snd) lookup_trace"
  by standard
    (auto
      intro!: mem_subtype.invI
      elim: mem_subtype.invE
      simp: lookup_trace_def update_trace_def state_mem_defs.map_of_def map_of_simp
      simp: lookup_correct update_correct
      split: prod.splits option.splits
      dest: lookup_inv[THEN mem_subtype.invD] update_inv[THEN mem_subtype.invD])

end

context mem_correct_empty
begin

lemma mem_correct_tracing_empty:
  "mem_correct_empty update_trace (P o snd) lookup_trace ([], empty)"
  by (intro mem_correct_empty.intro mem_correct_tracing mem_correct_empty_axioms.intro)
     (simp add: map_of_simp empty_correct P_empty)+

end

locale dp_consistency_rbt_tracing =
  fixes dp :: "'param \<Rightarrow> 'result"
begin

interpretation rbt: dp_consistency_rbt .

sublocale dp_consistency_empty
  rbt.update_trace "top o snd" rbt.lookup_trace dp "([], Mapping.empty)"
  by (rule dp_consistency_empty.intro dp_consistency.intro
      rbt.mem_correct_tracing_empty mem_correct_empty.axioms(1)
     )+

end (* DP Consistency RBT *)
