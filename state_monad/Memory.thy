theory Memory
  imports "DP_CRelVS" "HOL-Library.RBT_Mapping"
begin

locale mem_correct_empty = mem_correct where lookup=lookup for lookup :: "'param \<Rightarrow> ('mem, 'result option) state"+
  fixes empty
  assumes empty_correct: "map_of empty = Map.empty" and P_empty: "mem_inv empty"

lemma (in mem_correct_empty) dom_empty[simp]:
  "dom (map_of empty) = {}"
  using empty_correct by (auto dest: map_le_implies_dom_le)

locale dp_consistency_empty =
  dp_consistency + mem_correct_empty
begin

lemma cmem_empty:
  "cmem empty"
  using empty_correct unfolding cmem_def by auto

corollary memoization_correct:
  "dp x = v" "cmem m" if "consistentDP dp\<^sub>T" "State_Monad.run_state (dp\<^sub>T x) empty = (v, m)"
  using that unfolding consistentDP_def
  by (auto dest!: rel_funD[where x = x] elim!: dp_mem.relE intro: P_empty cmem_empty)

lemma memoized:
  "dp x = fst (State_Monad.run_state (dp\<^sub>T x) empty)" if "consistentDP dp\<^sub>T"
  using surjective_pairing memoization_correct(1)[OF that] by blast

lemma cmem_result:
  "cmem (snd (State_Monad.run_state (dp\<^sub>T x) empty))" if "consistentDP dp\<^sub>T"
  using surjective_pairing memoization_correct(2)[OF that] by blast

end (* DP Consistency Empty *)

locale dp_consistency_default =
  fixes dp :: "'param \<Rightarrow> 'result"
begin

definition "lookup k = do {m \<leftarrow> State_Monad.get; State_Monad.return (m k)}"
definition "update k v = do {m \<leftarrow> State_Monad.get; State_Monad.set (m(k\<mapsto>v))}"

sublocale dp_consistency_empty
  where lookup=lookup
    and update=update
    and mem_inv=top
    and empty=Map.empty
  unfolding lookup_def update_def
  by standard (auto
      intro: mem_subtype.invI
      simp: map_le_def state_mem_defs.map_of_def State_Monad.bind_def State_Monad.get_def State_Monad.set_def)

end (* DP Consistency Default *)

locale dp_consistency_rbt =
  fixes dp :: "'param \<Rightarrow> 'result"
begin

definition "lookup k = do {m \<leftarrow> State_Monad.get; State_Monad.return (Mapping.lookup m k)}"
definition "update k v = do {m \<leftarrow> State_Monad.get; State_Monad.set (Mapping.update k v m)}"

sublocale dp_consistency_empty
  where lookup=lookup
    and update=update
    and mem_inv=top
    and empty=Mapping.empty
  unfolding lookup_def update_def
  by standard
    (auto intro: mem_subtype.invI simp:map_le_def state_mem_defs.map_of_def State_Monad.bind_def State_Monad.get_def State_Monad.return_def State_Monad.set_def lookup_update' lookup_empty)

end (* DP Consistency RBT *)

end (* Theory *)