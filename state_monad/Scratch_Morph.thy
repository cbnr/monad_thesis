theory Scratch_Morph
  imports
    "HOL-Library.State_Monad"
    "./Imperative_HOL_Ext"
    "../StateT_Heap/StateT_Heap_Monad_Ext"
begin

definition morph_st :: "nat \<Rightarrow> ('mem, 'value option) state \<Rightarrow> (nat \<Rightarrow> 'mem option, 'value option) state" where
  "morph_st k s = do {
    ml \<leftarrow> State_Monad.get;
    case ml k of
      None \<Rightarrow> State_Monad.return None
    | Some ms \<Rightarrow> (case run_state s ms of
        (vo, ms') \<Rightarrow> do {
          State_Monad.set (ml(k\<mapsto>ms'));
          State_Monad.return vo
        }
      )
  }"

text "morph i (m \<bind> f) = (morph i m) \<bind> (morph i \<circ> f)"
text "morph i (State_Monad.return x) = State_Monad.return x"
find_consts " ('a \<Rightarrow> 'b) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('a \<Rightarrow> bool ) \<Rightarrow>bool"

term Array.new
term arrays
term Array.get
term Array.nth
term Array.len
term Array.length
term Array.update
term Ref.get
term StateT_Heap_Monad.get
term StateT_Heap_Monad.lift
term StateT_Heap_Monad_Ext.mem_subtype.inv
term "op mod"
value "(3::nat) mod 2"
value "(3::nat) mod 0"
thm pos_mod_bound
term pred_state
ML_val \<open>@{thm pos_mod_bound} |> Thm.full_prop_of\<close>
print_locale dvd
definition morph :: "nat \<Rightarrow> ('memS::heap, 'value) StateT_Heap_Monad.t \<Rightarrow> ('memS array, 'value) StateT_Heap_Monad.t"where
  "morph i sh = do {
    ml \<leftarrow> StateT_Heap_Monad.get;
    mi \<leftarrow> StateT_Heap_Monad.lift (Array.nth ml i);
    (v, mi') \<leftarrow> StateT_Heap_Monad.lift (StateT_Heap_Monad.run sh mi);
    StateT_Heap_Monad.lift (Array.upd i mi' ml);
    StateT_Heap_Monad.return v
  }"

lemma
  assumes idx_valid: "i < Array.length heap ml"
  assumes mem_array_valid: "array_valid heap ml"
  shows "Heap_Monad.execute (StateT_Heap_Monad.run (morph i (StateT_Heap_Monad.return x)) ml) heap
       = Heap_Monad.execute (StateT_Heap_Monad.run (StateT_Heap_Monad.return x) ml) heap"
  unfolding morph_def
  unfolding StateT_Heap_Monad.return_def
  unfolding StateT_Heap_Monad.lift_def
  unfolding StateT_Heap_Monad.bind_def
  unfolding StateT_Heap_Monad.get_def
  using idx_valid
  using mem_array_valid[THEN array_set_get]
  apply (auto simp: execute_simps Array.update_def)
  done

lemma
  assumes idx_valid: "i < Array.length heap ml"
  assumes mem_array_valid: "array_valid heap ml"
  shows "Heap_Monad.execute (StateT_Heap_Monad.run (morph i (x \<bind> f)) ml) heap
       = Heap_Monad.execute (StateT_Heap_Monad.run (morph i x \<bind> morph i \<circ> f) ml) heap"
  unfolding morph_def
  unfolding StateT_Heap_Monad.bind_def
  unfolding StateT_Heap_Monad.return_def
  unfolding StateT_Heap_Monad.lift_def
  unfolding StateT_Heap_Monad.bind_def
  unfolding StateT_Heap_Monad.get_def
  using idx_valid
  apply (auto split: prod.splits simp: execute_simps )