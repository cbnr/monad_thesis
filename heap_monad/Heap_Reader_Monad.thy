theory Heap_Reader_Monad
  imports
    "HOL-Imperative_HOL.Imperative_HOL"
    Monad_Syntax
begin

datatype ('s, 'a) readerT = ReaderT (run_readerT: "'s \<Rightarrow> 'a Heap")

context begin
qualified definition lift :: "'a Heap \<Rightarrow> ('s, 'a) readerT" where
  "lift h = ReaderT (\<lambda>_. h)"

qualified definition return :: "'a \<Rightarrow> ('s, 'a) readerT" where
  "return = lift \<circ> Heap_Monad.return"

qualified definition bind :: "('s, 'a) readerT \<Rightarrow> ('a \<Rightarrow> ('s, 'b) readerT) \<Rightarrow> ('s, 'b) readerT" where
  "bind x f = ReaderT (\<lambda>s. Heap_Monad.bind (run_readerT x s) (\<lambda>a. run_readerT (f a) s))"

adhoc_overloading Monad_Syntax.bind bind

lemma return_def_alt:
  "return a = ReaderT (\<lambda>_. Heap_Monad.return a)"
  unfolding return_def lift_def by auto

lemma bind_left_identity[simp]: "bind (return a) f = f a"
unfolding return_def_alt bind_def by simp

lemma bind_right_identity[simp]: "bind m return = m"
unfolding return_def_alt bind_def by simp

lemma bind_assoc[simp]: "bind (bind m f) g = bind m (\<lambda>x. bind (f x) g)"
unfolding bind_def by (auto split: prod.splits)

end (* context*)
end (* theory *)
