theory Heap_Reader_Locales
  imports
    Heap_Locales
    Heap_Reader_Monad_Ext
begin

locale reader_mem_defs =
  fixes P_mem :: "'mem \<Rightarrow> heap \<Rightarrow> bool"
    and lookup_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v option Heap"
    and update_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v \<Rightarrow> unit Heap"
begin

context fixes mem :: 'mem begin
interpretation h: heap_mem_defs .
abbreviation "h_checkmem' \<equiv> h.checkmem'"
end

thm heap_mem_defs.checkmem'_def
definition "checkmem' param calc' \<equiv> ReaderT (\<lambda>mem. h_checkmem' mem param (\<lambda>_. run_readerT (calc' ()) mem))"

definition "checkmem param calc \<equiv> checkmem' param (\<lambda>_. calc)"

lemma checkmem_checkmem':
  "checkmem' param (\<lambda>_. calc) = checkmem param calc"
  unfolding checkmem'_def checkmem_def ..
end

locale dp_consistency_reader = reader_mem_defs where lookup_mem=lookup_mem for lookup_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v option Heap"+
  fixes dp :: "'k \<Rightarrow> 'v"
    and inv_mem :: "'mem \<Rightarrow> bool"
  assumes reader_heap: "\<And>mem. inv_mem mem \<Longrightarrow> dp_consistency_heap mem P_mem update_mem lookup_mem"
begin

context
  fixes mem :: 'mem
  assumes inv_mem: "inv_mem mem"
begin

interpretation h: dp_consistency_heap
  apply (rule reader_heap)
  apply (rule inv_mem)
  done

abbreviation "h_crel_vs \<equiv> h.crel_vs"
(*lemmas h_crel_vs_def = h.crel_vs_def*)
lemmas h_crel_vs_checkmem_tupled = h.crel_vs_checkmem_tupled
lemmas h_checkmem_checkmem' = h.checkmem_checkmem'

abbreviation "h_consistentDP \<equiv> h.consistentDP"
lemmas h_consistentDP_intro =  h.consistentDP_intro
lemmas h_crel_vs_return_ext = h.crel_vs_return_ext
lemmas h_crel_vs_return = h.crel_vs_return
lemmas h_transfer'_return = h.transfer'_return
lemmas h_bind_transfer = h.bind_transfer
lemmas h_fun_app_lifted_transfer = h.fun_app_lifted_transfer
lemmas h_crel_vs_fun_app = h.crel_vs_fun_app

end


context includes lifting_syntax heap_reader_monad_syntax begin

definition crel_vs where
  "crel_vs R v r \<equiv> \<forall>mem. inv_mem mem \<longrightarrow> h_crel_vs mem R v (run_readerT r mem)"

abbreviation rel_fun_lifted :: "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c =='mem\<Longrightarrow> 'd) \<Rightarrow> bool" (infixr "===>\<^sub>T" 55) where
  "rel_fun_lifted R R' \<equiv> R ===> crel_vs R'"

lemma rel_fun_lifted_heap:
  "(R0 ===>\<^sub>T R1) f g \<longleftrightarrow> (\<forall>mem. inv_mem mem \<longrightarrow> (R0 ===> h_crel_vs mem R1) f (\<lambda>x. run_readerT (g x) mem))"
  unfolding crel_vs_def by (fastforce dest: rel_funD)

thm h_crel_vs_checkmem_tupled
lemma crel_vs_checkmem_tupled:
  "\<lbrakk>v = dp param; is_equality R; Transfer.Rel (crel_vs R) v s\<rbrakk> \<Longrightarrow>
    Transfer.Rel (crel_vs R) v (checkmem param s)"
  using h_crel_vs_checkmem_tupled h_checkmem_checkmem'
  unfolding crel_vs_def checkmem_def checkmem'_def Rel_def
  by auto

term h_consistentDP
definition "consistentDP dp\<^sub>T \<equiv> \<forall>mem. inv_mem mem \<longrightarrow> h_consistentDP mem (\<lambda>x. run_readerT (dp\<^sub>T x) mem)"

thm h_consistentDP_intro
lemma consistentDP_intro:
  "consistentDP dp\<^sub>T"
  if "(\<And>param. Transfer.Rel (crel_vs op =) (dp param) (dp\<^sub>T param))"
  using that h_consistentDP_intro 
  unfolding consistentDP_def crel_vs_def Rel_def
  by auto

thm h_crel_vs_return_ext
lemma crel_vs_return_ext:
  "Transfer.Rel R x y \<Longrightarrow> Transfer.Rel (crel_vs R) x \<langle>y\<rangle>"
  using h_crel_vs_return_ext
  unfolding crel_vs_def Rel_def return_def_alt
  by fastforce

thm h_crel_vs_return
lemma crel_vs_return:
  "Transfer.Rel R x y \<Longrightarrow> Transfer.Rel (crel_vs R) (Wrap x) \<langle>y\<rangle>"
  unfolding Wrap_def by (fact crel_vs_return_ext)

thm h_transfer'_return
lemma return_transfer[transfer_rule]:
  "(R ===>\<^sub>T R) Wrap Heap_Reader_Monad.return"
  using h_transfer'_return 
  unfolding crel_vs_def return_def_alt
  by (fastforce dest: rel_funD)

thm h_bind_transfer
lemma bind_transfer[transfer_rule]:
  "(crel_vs R0 ===> (R0 ===>\<^sub>T R1) ===> crel_vs R1) (\<lambda>v f. f v) (op \<bind>)"
  unfolding rel_fun_lifted_heap
  unfolding crel_vs_def Heap_Reader_Monad.bind_def
  using h_bind_transfer  
  by (fastforce dest: rel_funD)

thm h_fun_app_lifted_transfer
lemma fun_app_lifted_transfer[transfer_rule]:
  "(crel_vs (R0 ===>\<^sub>T R1) ===> crel_vs R0 ===> crel_vs R1) App (op .)"
  unfolding App_def Heap_Reader_Monad_Ext.fun_app_lifted_def
  by transfer_prover

thm h_crel_vs_fun_app
lemma crel_vs_fun_app:
  "\<lbrakk>Transfer.Rel (crel_vs R0) x x\<^sub>T; Transfer.Rel (crel_vs (R0 ===>\<^sub>T R1)) f f\<^sub>T\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R1) (App f x) (f\<^sub>T . x\<^sub>T)"
  unfolding Rel_def using fun_app_lifted_transfer[THEN rel_funD, THEN rel_funD] .

end

context includes heap_reader_monad_syntax begin

thm if_cong
lemma ifT_cong:
  assumes "b = c" "c \<Longrightarrow> x = u" "\<not>c \<Longrightarrow> y = v"
  shows "Heap_Reader_Monad_Ext.if\<^sub>T \<langle>b\<rangle> x y = Heap_Reader_Monad_Ext.if\<^sub>T \<langle>c\<rangle> u v"
  unfolding Heap_Reader_Monad_Ext.if\<^sub>T_def
  unfolding Heap_Reader_Monad.bind_left_identity
  using if_cong[OF assms] .

lemma return_app_return_cong:
  assumes "f x = g y"
  shows "\<langle>f\<rangle> . \<langle>x\<rangle> = \<langle>g\<rangle> . \<langle>y\<rangle>"
  unfolding Heap_Reader_Monad_Ext.return_app_return_meta assms ..

lemmas [fundef_cong] =
  return_app_return_cong
  ifT_cong
end (* syntaxes *)
end (* locale dp_consistency_reader *)


locale dp_consistency_reader_default =
  fixes bound :: "'k :: {index, heap} bound" 
    and dp :: "'k \<Rightarrow> 'v :: heap" 
begin

interpretation idx: bounded_index bound .

print_locale dp_consistency_reader
sublocale dp_consistency_reader
  where P_mem="\<lambda> mem heap. Array.length heap mem = idx.size"
    and lookup_mem="mem_lookup idx.size idx.checked_idx"
    and update_mem="mem_update idx.size idx.checked_idx"
    and dp=dp
    and inv_mem="\<lambda>_. True"
  apply (rule dp_consistency_reader.intro)
  apply (rule dp_consistency_heap_default.dp_consistency_heap)
  done

abbreviation "inited dp\<^sub>T x \<equiv> (mem_empty idx.size :: 'v option array Heap) \<bind> run_readerT (dp\<^sub>T x)"

lemma memoized:
  "dp x = result_of (inited dp\<^sub>T x) Heap.empty"
  if "consistentDP dp\<^sub>T"
  (*unfolding inited_def*)
  apply (rule dp_consistency_heap_default.memoized_empty)
  subgoal using that unfolding consistentDP_def by auto
  apply (rule refl)
  done

end (* locale dp_consistency_reader_default *)

locale dp_consistency_reader_array_pair'_def =
  fixes bound :: "'k2 :: {index, heap} bound"
    and dp :: "'k \<Rightarrow> 'v::heap"
  fixes key1 :: "'k \<Rightarrow> ('k1 :: heap)" and key2 :: "'k \<Rightarrow> 'k2"
    and k1 k2 :: "'k1"
begin

sublocale idx: bounded_index bound .

abbreviation "inited dp\<^sub>T x \<equiv> (init_state idx.size k1 k2 \<bind> run_readerT (dp\<^sub>T x))"

end

locale dp_consistency_reader_array_pair' =
  dp_consistency_reader_array_pair'_def
  where bound=bound and dp=dp and k1=k1
  for bound::"'k2::{heap,index} bound" and dp::"'k \<Rightarrow> ('v::heap)" and k1::"'k1::heap"+
  assumes keys_injective: "\<forall>k k'. key1 k = key1 k' \<and> key2 k = key2 k' \<longrightarrow> k = k'"
      and keys_neq: "k1 \<noteq> k2"
begin

print_locale dp_consistency_reader
sublocale dp_consistency_reader
  where inv_mem="curry_mem inv_distinct"
    and P_mem="pair_inv_pair idx.size idx.checked_idx key1 key2"
    and lookup_mem="curry_mem (lookup_pair idx.size idx.checked_idx key1 key2)"
    and update_mem="curry_mem (update_pair idx.size idx.checked_idx key1 key2)"
    and dp=dp
  apply (rule dp_consistency_reader.intro)
  apply (rule dp_consistency_heap.intro)
  apply safe
  apply (rule heap_correct_pairI)
  subgoal unfolding curry_mem_beta .
  subgoal using idx.checked_idx_injective .
  subgoal using keys_injective .
  done

context
  fixes mem :: " 'k1 ref \<times> 'k1 ref \<times> 'v option array ref \<times> 'v option array ref"
  assumes mem_is_init: "mem = result_of (init_state idx.size k1 k2) Heap.empty"
begin
print_locale dp_consistency_heap_array_pair'
interpretation h: dp_consistency_heap_array_pair'
  where size=idx.size and to_index=idx.checked_idx
  apply standard
     apply (fact mem_is_init)
    apply (fact idx.checked_idx_injective)
   apply (fact keys_injective)
  apply (fact keys_neq)
  done
lemmas h_dp_consistency_heap_array_pair' = h.dp_consistency_heap_array_pair'_axioms
end

lemma memoized:
  "dp x = result_of (inited dp\<^sub>T x) Heap.empty"
  if "consistentDP dp\<^sub>T"
    (*unfolding inited_def*)
  apply (rule dp_consistency_new'.memoized_empty[OF dp_consistency_heap_array_pair'.dp_consistency_new'[OF h_dp_consistency_heap_array_pair']])
   apply (rule refl)
  apply (unfold dp_consistency_heap_array_pair'.inv_pair'_def[OF h_dp_consistency_heap_array_pair', OF refl])
  apply (fold curry_mem_def)
  apply (fold pair_inv_pair_def)
  apply (rule that[unfolded consistentDP_def, rule_format])
  apply (rule result_of_init_state_distinct)
  done

end
end