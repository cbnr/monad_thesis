theory Heap_Locales
  imports
    "../heap_monad/Memory_Heap"
begin

paragraph \<open>More Heap\<close> -- \<open>TODO: Move\<close>

lemma execute_heap_ofD:
  "heap_of c h = h'" if "execute c h = Some (v, h')"
  using that by auto

lemma execute_result_ofD:
  "result_of c h = v" if "execute c h = Some (v, h')"
  using that by auto

locale dp_consistency_heap_default =
  fixes bound :: "'k :: {index, heap} bound"
    and mem :: "'v::heap option array"
    and dp :: "'k \<Rightarrow> 'v"
begin

interpretation idx: bounded_index bound .

print_locale dp_consistency_heap

sublocale dp_consistency_heap
  where mem = mem
    and P_mem="\<lambda> mem heap. Array.length heap mem = idx.size"
    and lookup_mem="mem_lookup idx.size idx.checked_idx"
    and update_mem="mem_update idx.size idx.checked_idx"
  apply (rule dp_consistency_heap.intro)
  apply (rule mem_heap_correct)
  apply (rule idx.checked_idx_injective)
  done

lemmas dp_consistency_heap = dp_consistency_heap_axioms

context
  fixes empty
  assumes empty: "map_of_heap empty \<subseteq>\<^sub>m Map.empty"
      and len: "Array.length empty mem = idx.size"
begin

print_locale dp_consistency_heap_empty
interpretation consistent: dp_consistency_heap_empty
  where mem = mem
    and P_mem="\<lambda> mem heap. Array.length heap mem = idx.size"
    and lookup_mem="mem_lookup idx.size idx.checked_idx"
    and update_mem="mem_update idx.size idx.checked_idx"
    and empty=empty
  by (standard; rule len empty)

lemmas memoizedI = consistent.memoized

end

lemma memoized_empty:
  "dp x = result_of ((mem_empty idx.size :: 'v option array Heap) \<bind> (\<lambda>mem. dp\<^sub>T mem x)) Heap.empty"
  if "consistentDP (dp\<^sub>T mem)" "mem = result_of (mem_empty idx.size) Heap.empty"
  apply (subst execute_bind_success)
   defer
   apply (subst memoizedI[OF _ _ that(1)])
  by (auto intro: map_emptyI simp:
      that(2) length_mem_empty Let_def nth_mem_empty mem_lookup_def heap_mem_defs.map_of_heap_def
      )

end

locale heap_correct_init_defs =
  fixes P :: "'m \<Rightarrow> heap \<Rightarrow> bool"
    and lookup :: "'m \<Rightarrow> 'k \<Rightarrow> 'v option Heap"
    and update :: "'m \<Rightarrow> 'k \<Rightarrow> 'v \<Rightarrow> unit Heap"
begin

definition map_of_heap' where
  "map_of_heap' m heap k = fst (the (execute (lookup m k) heap))"

end

locale heap_correct_init_inv = heap_correct_init_defs +
  assumes lookup_inv: "\<And> m. lift_p (P m) (lookup m k)"
  assumes update_inv: "\<And> m. lift_p (P m) (update m k v)"

locale heap_correct_init =
  heap_correct_init_inv +
  assumes lookup_correct:
      "\<And> a. P a m \<Longrightarrow> map_of_heap' a (snd (the (execute (lookup a k) m))) \<subseteq>\<^sub>m (map_of_heap' a m)"
  and update_correct:
      "\<And> a. P a m \<Longrightarrow>
        map_of_heap' a (snd (the (execute (update a k v) m))) \<subseteq>\<^sub>m (map_of_heap' a m)(k \<mapsto> v)"
begin

end

locale dp_consistency_heap_init = heap_correct_init _ lookup for lookup :: "'m \<Rightarrow> 'k \<Rightarrow> 'v option Heap"  +
  fixes dp :: "'k \<Rightarrow> 'v"
  fixes init :: "'m Heap"
  assumes success: "success init Heap.empty"
  assumes empty_correct:
    "\<And> empty heap. execute init Heap.empty = Some (empty, heap) \<Longrightarrow> map_of_heap' empty heap \<subseteq>\<^sub>m Map.empty"
    and P_empty: "\<And> empty heap. execute init Heap.empty = Some (empty, heap) \<Longrightarrow> P empty heap"
begin

definition "init_mem = result_of init Heap.empty"

print_locale dp_consistency_heap
sublocale dp_consistency_heap
  where mem=init_mem
    and P_mem=P
    and lookup_mem=lookup
    and update_mem=update
  apply standard
       apply (rule lookup_inv[of init_mem])
      apply (rule update_inv[of init_mem])
  subgoal
    unfolding heap_mem_defs.map_of_heap_def
    by (rule lookup_correct[of init_mem, unfolded map_of_heap'_def])
  subgoal
    unfolding heap_mem_defs.map_of_heap_def
    by (rule update_correct[of init_mem, unfolded map_of_heap'_def])
  done

print_locale dp_consistency_heap_empty
interpretation consistent: dp_consistency_heap_empty
  where mem=init_mem
    and P_mem=P
    and lookup_mem=lookup
    and update_mem=update
  apply standard
  subgoal
    apply (rule successE[OF success])
    apply (frule empty_correct)
    unfolding heap_mem_defs.map_of_heap_def init_mem_def map_of_heap'_def
    apply simp
    oops
(*
  subgoal
    apply (rule successE[OF success])
    apply (frule P_empty)
    unfolding init_mem_def
    by simp
  done
*)

lemma memoized_empty:
  "dp x = result_of (init \<bind> (\<lambda>mem. dp\<^sub>T mem x)) Heap.empty"
  oops
(*
  if "consistentDP (dp\<^sub>T (result_of init Heap.empty))"
  by (simp add: execute_bind_success consistent.memoized[OF that(1)] success)
*)
end

locale dp_consistency_heap_init' = heap_correct_init _ lookup for lookup :: "'m \<Rightarrow> 'k \<Rightarrow> 'v option Heap"  +
  fixes dp :: "'k \<Rightarrow> 'v"
  fixes init :: "'m Heap"
  assumes success: "success init Heap.empty"
  assumes empty_correct:
    "\<And> empty heap. execute init Heap.empty = Some (empty, heap) \<Longrightarrow> map_of_heap' empty heap \<subseteq>\<^sub>m Map.empty"
    and P_empty: "\<And> empty heap. execute init Heap.empty = Some (empty, heap) \<Longrightarrow> P empty heap"
begin

sublocale dp_consistency_heap
  where mem=init_mem
    and P_mem=P
    and lookup_mem=lookup
    and update_mem=update
  apply standard
       apply (rule lookup_inv[of init_mem])
      apply (rule update_inv[of init_mem])
  subgoal
    unfolding heap_mem_defs.map_of_heap_def
    by (rule lookup_correct[of init_mem, unfolded map_of_heap'_def])
  subgoal
    unfolding heap_mem_defs.map_of_heap_def
    by (rule update_correct[of init_mem, unfolded map_of_heap'_def])
  done

definition "init_mem = result_of init Heap.empty"

print_locale dp_consistency_heap_empty
interpretation consistent: dp_consistency_heap_empty
  where mem=init_mem
    and P_mem=P
    and lookup_mem=lookup
    and update_mem=update
    and empty= "heap_of init Heap.empty"
  apply standard
  subgoal
    apply (rule successE[OF success])
    apply (frule empty_correct)
    unfolding heap_mem_defs.map_of_heap_def init_mem_def map_of_heap'_def
    by simp
  subgoal
    apply (rule successE[OF success])
    apply (frule P_empty)
    unfolding init_mem_def
    by simp
  done

lemma memoized_empty:
  "dp x = result_of (init \<bind> (\<lambda>mem. dp\<^sub>T mem x)) Heap.empty"
  if "consistentDP init_mem (dp\<^sub>T (result_of init Heap.empty))"
  by (simp add: execute_bind_success consistent.memoized[OF that(1)] success)

end

locale dp_consistency_new =
  fixes dp :: "'k \<Rightarrow> 'v"
  fixes P :: "'m \<Rightarrow> heap \<Rightarrow> bool"
    and lookup :: "'m \<Rightarrow> 'k \<Rightarrow> 'v option Heap"
    and update :: "'m \<Rightarrow> 'k \<Rightarrow> 'v \<Rightarrow> unit Heap"
    and init
  assumes
    success: "success init Heap.empty"
  assumes
    inv_init: "\<And> empty heap. execute init Heap.empty = Some (empty, heap) \<Longrightarrow> P empty heap"
  assumes consistent:
    "\<And> empty heap. execute init Heap.empty = Some (empty, heap)
    \<Longrightarrow> dp_consistency_heap_empty empty P update lookup heap"
begin

sublocale dp_consistency_heap_empty
  where mem="result_of init Heap.empty"
    and P_mem=P
    and lookup_mem=lookup
    and update_mem=update
    and empty= "heap_of init Heap.empty"
  using success by (auto 4 3 intro: consistent successE) (* Extract Theorem *)

lemma memoized_empty:
  "dp x = result_of (init \<bind> (\<lambda>mem. dp\<^sub>T mem x)) Heap.empty"
  if "consistentDP (dp\<^sub>T (result_of init Heap.empty))"
  by (simp add: execute_bind_success memoized[OF that(1)] success)

end

locale dp_consistency_new' =
  fixes dp :: "'k \<Rightarrow> 'v"
  fixes P :: "'m \<Rightarrow> heap \<Rightarrow> bool"
    and lookup :: "'m \<Rightarrow> 'k \<Rightarrow> 'v option Heap"
    and update :: "'m \<Rightarrow> 'k \<Rightarrow> 'v \<Rightarrow> unit Heap"
    and init
    and mem :: 'm
  assumes mem_is_init: "mem = result_of init Heap.empty"
  assumes
    success: "success init Heap.empty"
  assumes
    inv_init: "\<And> empty heap. execute init Heap.empty = Some (empty, heap) \<Longrightarrow> P empty heap"
  assumes consistent:
    "\<And> empty heap. execute init Heap.empty = Some (empty, heap)
    \<Longrightarrow> dp_consistency_heap_empty empty P update lookup heap"
begin

sublocale dp_consistency_heap_empty
  where mem=mem
    and P_mem=P
    and lookup_mem=lookup
    and update_mem=update
    and empty= "heap_of init Heap.empty"
  unfolding mem_is_init
  using success by (auto 4 3 intro: consistent successE) (* Extract Theorem *)

lemma memoized_empty:
  "dp x = result_of (init \<bind> (\<lambda>mem. dp\<^sub>T mem x)) Heap.empty"
  if "consistentDP (dp\<^sub>T (result_of init Heap.empty))"
  by (simp add: execute_bind_success memoized[OF that(1)] success)

end

locale dp_consistency_heap_array_new' =
  fixes size :: nat
    and to_index :: "('k :: heap) \<Rightarrow> nat"
    and mem :: "('v::heap) option array"
    and dp :: "'k \<Rightarrow> 'v::heap"
  assumes mem_is_init: "mem = result_of (mem_empty size) Heap.empty"
  assumes injective: "injective size to_index"
begin

sublocale dp_consistency_new'
  where P      = "\<lambda> mem heap. Array.length heap mem = size"
    and lookup = "\<lambda> mem. mem_lookup size to_index mem"
    and update = "\<lambda> mem. mem_update size to_index mem"
    and init   = "mem_empty size"
    and mem    = mem
  apply (rule dp_consistency_new'.intro)
  subgoal
    by (rule mem_is_init)
  subgoal
    by (rule success_empty)
  subgoal for empty heap
    using length_mem_empty by (metis fst_conv option.sel snd_conv)
  subgoal
    apply (frule execute_heap_ofD[symmetric])
    apply (frule execute_result_ofD[symmetric])
    apply simp
    apply (rule array_consistentI[OF injective HOL.refl])
    done
  done

thm memoized_empty

end

locale dp_consistency_heap_array_new =
  fixes size :: nat
    and to_index :: "('k :: heap) \<Rightarrow> nat"
    and dp :: "'k \<Rightarrow> 'v::heap"
  assumes injective: "injective size to_index"
begin

sublocale dp_consistency_new
  where P      = "\<lambda> mem heap. Array.length heap mem = size"
    and lookup = "\<lambda> mem. mem_lookup size to_index mem"
    and update = "\<lambda> mem. mem_update size to_index mem"
    and init   = "mem_empty size"
  apply (rule dp_consistency_new.intro)
  subgoal
    by (rule success_empty)
  subgoal for empty heap
    using length_mem_empty by (metis fst_conv option.sel snd_conv)
  subgoal
    apply (frule execute_heap_ofD[symmetric])
    apply (frule execute_result_ofD[symmetric])
    apply simp
    apply (rule array_consistentI[OF injective HOL.refl])
    done
  done

thm memoized_empty

end

locale dp_consistency_heap_array =
  fixes size :: nat
    and to_index :: "('k :: heap) \<Rightarrow> nat"
    and dp :: "'k \<Rightarrow> 'v::heap"
  assumes injective: "injective size to_index"
begin

sublocale dp_consistency_heap_init
  where P="\<lambda>mem heap. Array.length heap mem = size"
    and lookup="\<lambda> mem. mem_lookup size to_index mem"
    and update="\<lambda> mem. mem_update size to_index mem"
    and init="mem_empty size"
  apply standard
  subgoal lookup_inv
    unfolding lift_p_def mem_lookup_def by (simp add: Let_def execute_simps)
  subgoal update_inv
    unfolding State_Heap.lift_p_def mem_update_def by (simp add: Let_def execute_simps)
  subgoal for k heap
    unfolding heap_correct_init_defs.map_of_heap'_def map_le_def mem_lookup_def
    by (auto simp: execute_simps Let_def split: if_split_asm)
  subgoal for heap k
    unfolding heap_correct_init_defs.map_of_heap'_def map_le_def mem_lookup_def mem_update_def
    apply (auto simp: execute_simps Let_def length_def split: if_split_asm)
    apply (subst (asm) nth_list_update_neq)
    using injective[unfolded injective_def] apply auto
    done
  subgoal
    by (rule success_empty)
  subgoal for empty' heap
    unfolding heap_correct_init_defs.map_of_heap'_def mem_lookup_def
    by (auto intro!: map_emptyI simp: Let_def ) (metis fst_conv option.sel snd_conv nth_mem_empty)
  subgoal for empty' heap
    unfolding heap_correct_init_defs.map_of_heap'_def mem_lookup_def map_le_def
    using length_mem_empty by (metis fst_conv option.sel snd_conv)
  done

end


locale dp_consistency_heap_array_pair' =
  fixes size :: nat
  fixes key1 :: "'k \<Rightarrow> ('k1 :: heap)" and key2 :: "'k \<Rightarrow> 'k2 :: heap"
    and to_index :: "'k2 \<Rightarrow> nat"
    and dp :: "'k \<Rightarrow> 'v::heap"
    and k1 k2 :: "'k1"
    and mem :: "('k1 ref \<times>
             'k1 ref \<times>
             'v option array ref \<times>
             'v option array ref)"
  assumes mem_is_init: "mem = result_of (init_state size k1 k2) Heap.empty"
  assumes injective: "injective size to_index"
      and keys_injective: "\<forall>k k'. key1 k = key1 k' \<and> key2 k = key2 k' \<longrightarrow> k = k'"
      and keys_neq: "k1 \<noteq> k2"
begin

definition
  "inv_pair' = (\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
      pair_mem_defs.inv_pair (lookup1 size to_index m_ref1)
        (lookup2 size to_index m_ref2) (get_k1 k_ref1)
        (get_k2 k_ref2)
        (inv_pair_weak size k_ref1 k_ref2 m_ref1 m_ref2) key1 key2)"

print_locale dp_consistency_new'
sublocale dp_consistency_new'
  where P=inv_pair'
    and lookup="\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
      lookup_pair size to_index key1 key2 k_ref1 k_ref2 m_ref1 m_ref2"
    and update="\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
      update_pair size to_index key1 key2 k_ref1 k_ref2 m_ref1 m_ref2"
    and init="init_state size k1 k2"
  apply (rule dp_consistency_new'.intro)
  subgoal
    by (rule mem_is_init)
  subgoal
    by (rule succes_init_state)
  subgoal for empty heap
    unfolding inv_pair'_def
    apply safe
    apply (rule init_state_inv'[unfolded curry_mem_beta])
        apply (erule init_state_distinct)
       apply (rule injective)
      apply (rule keys_injective; assumption)
     apply assumption
    apply (rule keys_neq)
    done
  apply safe
  unfolding inv_pair'_def
  apply (fold curry_mem_def)
  apply (fold pair_inv_pair_def)
  apply (rule consistent_empty_pairI)
      apply (erule init_state_distinct)
     apply (rule injective)
    apply (rule keys_injective)
   apply assumption
  apply (rule keys_neq)
  done

lemmas dp_consistency_new' = dp_consistency_new'_axioms

thm memoized_empty
end


locale dp_consistency_heap_array_pair =
  fixes size :: nat
  fixes key1 :: "'k \<Rightarrow> ('k1 :: heap)" and key2 :: "'k \<Rightarrow> 'k2 :: heap"
    and to_index :: "'k2 \<Rightarrow> nat"
    and dp :: "'k \<Rightarrow> 'v::heap"
    and k1 k2 :: "'k1"
  assumes injective: "injective size to_index"
      and keys_injective: "\<forall>k k'. key1 k = key1 k' \<and> key2 k = key2 k' \<longrightarrow> k = k'"
      and keys_neq: "k1 \<noteq> k2"
begin

definition
  "inv_pair' = (\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
      pair_mem_defs.inv_pair (lookup1 size to_index m_ref1)
        (lookup2 size to_index m_ref2) (get_k1 k_ref1)
        (get_k2 k_ref2)
        (inv_pair_weak size k_ref1 k_ref2 m_ref1 m_ref2) key1 key2)"

print_locale dp_consistency_new
sublocale dp_consistency_new
  where P=inv_pair'
    and lookup="\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
      lookup_pair size to_index key1 key2 k_ref1 k_ref2 m_ref1 m_ref2"
    and update="\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
      update_pair size to_index key1 key2 k_ref1 k_ref2 m_ref1 m_ref2"
    and init="init_state size k1 k2"
  apply (rule dp_consistency_new.intro)
  subgoal
    by (rule succes_init_state)
  subgoal for empty heap
    unfolding inv_pair'_def
    apply safe
    apply (rule init_state_inv')
        apply (erule init_state_distinct)
       apply (rule injective)
      apply (rule keys_injective)
     apply assumption
    apply (rule keys_neq)
    done
  apply safe
  unfolding inv_pair'_def
  apply (fold curry_mem_def)
  apply (fold pair_inv_pair_def)
  apply (rule consistent_empty_pairI)
      apply (erule init_state_distinct)
     apply (rule injective)
    apply (rule keys_injective)
   apply assumption
  apply (rule keys_neq)
  done

thm memoized_empty
end
end