theory DP_CRelVH
  imports State_Heap
begin

print_locale dp_consistency
print_locale heap_mem_defs
locale dp_heap =
  s: dp_consistency lookup_st update_st P_st dp
  + h: heap_mem_defs mem Q_heap_mem lookup_mem update_mem
  for mem and P_st :: "heap \<Rightarrow> bool" and Q_heap_mem :: "'mem \<Rightarrow> heap \<Rightarrow> bool" and dp :: "'k \<Rightarrow> 'v" and lookup_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v option Heap"
  and lookup_st update_mem update_st +
  assumes
    rel_state_lookup: "rel_fun op = (h.rel_state op =) lookup_st h.lookup"
      and
    rel_state_update: "rel_fun op = (rel_fun op = (h.rel_state op =)) update_st h.update"
begin

context
  includes lifting_syntax heap_monad_syntax
begin

definition "crel_vs R v f \<equiv>
  \<forall>heap. P_st heap \<and> h.P heap \<and> s.cmem heap \<longrightarrow>
    (case execute f heap of
      None \<Rightarrow> False |
      Some (v', heap') \<Rightarrow> P_st heap' \<and> h.P heap' \<and> R v v' \<and> s.cmem heap'
    )
"

abbreviation rel_fun_lifted :: "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c ==H\<Longrightarrow> 'd) \<Rightarrow> bool" (infixr "===>\<^sub>T" 55) where
  "rel_fun_lifted R R' \<equiv> R ===> crel_vs R'"


definition consistentDP :: "('k \<Rightarrow> 'v Heap) \<Rightarrow> bool" where
  "consistentDP \<equiv> (op = ===> crel_vs op =) dp"

lemma consistentDP_intro:
  assumes "\<And>param. Transfer.Rel (crel_vs op=) (dp param) (dp\<^sub>T param)"
  shows "consistentDP dp\<^sub>T"
  using assms unfolding consistentDP_def Rel_def by blast

lemma crel_vs_execute_None:
  False if "crel_vs R a b" "execute b heap = None" "P_st heap" "h.P heap" "s.cmem heap"
  using that unfolding crel_vs_def by auto

lemma crel_vs_execute_Some:
  assumes "crel_vs R a b" "P_st heap" "h.P heap" "s.cmem heap"
  obtains x heap' where "execute b heap = Some (x, heap')" "P_st heap'" "h.P heap'"
  using assms unfolding crel_vs_def by (cases "execute b heap") auto

lemma crel_vs_executeD:
  assumes "crel_vs R a b" "P_st heap" "h.P heap" "s.cmem heap"
  obtains x heap' where
    "execute b heap = Some (x, heap')" "P_st heap'" "h.P heap'" "s.cmem heap'" "R a x"
  using assms unfolding crel_vs_def by (cases "execute b heap") auto

lemma crel_vs_success:
  assumes "crel_vs R a b" "P_st heap" "h.P heap" "s.cmem heap"
  shows "success b heap"
  using assms unfolding success_def by (auto elim: crel_vs_executeD)

lemma crel_vsI: "crel_vs R a b" if "(s.crel_vs R OO h.rel_state (op =)) a b"
  using that by (auto 4 3 simp: s.crel_vs_def elim: crelE h.rel_state_elim simp: crel_vs_def)

lemma transfer'_return[transfer_rule]:
  "(R ===> crel_vs R) Wrap return"
proof -
  have "(R ===> (s.crel_vs R OO h.rel_state (op =))) Wrap return"
    by (rule rel_fun_comp1 s.return_transfer h.transfer_return)+ auto
  then show ?thesis
    by (blast intro: rel_fun_mono crel_vsI)
qed

lemma crel_vs_return:
  "Transfer.Rel (crel_vs R) (Wrap x) (return y)" if "Transfer.Rel R x y"
  using that unfolding Rel_def by (rule transfer'_return[unfolded rel_fun_def, rule_format])

lemma crel_vs_return_ext:
  "\<lbrakk>Transfer.Rel R x y\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R) x (Heap_Monad.return y)"
  by (fact crel_vs_return[unfolded Wrap_def])
term 0 (**)

lemma bind_transfer[transfer_rule]:
  "(crel_vs R0 ===> (R0 ===> crel_vs R1) ===> crel_vs R1) (\<lambda>v f. f v) (op \<bind>)"
  unfolding rel_fun_def bind_def
  by safe (subst crel_vs_def, auto 4 4 elim: crel_vs_execute_Some elim!: crel_vs_executeD)

lemma crel_vs_update:
  "crel_vs op = () (h.update param (dp param))"
  by (rule
      crel_vsI relcomppI s.crel_vs_update
      rel_state_update[unfolded rel_fun_def, rule_format] HOL.refl
     )+

lemma crel_vs_lookup:
  "crel_vs
    (\<lambda> v v'. case v' of None \<Rightarrow> True | Some v' \<Rightarrow> v = v' \<and> v = dp param) (dp param) (h.lookup param)"
  by (rule
      crel_vsI relcomppI s.crel_vs_lookup
      rel_state_lookup[unfolded rel_fun_def, rule_format] HOL.refl
     )+

lemma crel_vs_eq_eq_onp:
  "crel_vs (eq_onp (\<lambda> x. x = v)) v s" if "crel_vs op = v s"
  using that unfolding crel_vs_def by (auto split: option.split simp: eq_onp_def)

lemma crel_vs_bind_eq:
  "\<lbrakk>crel_vs op = v s; crel_vs R (f v) (sf v)\<rbrakk> \<Longrightarrow> crel_vs R (f v) (s \<bind> sf)"
  by (erule bind_transfer[unfolded rel_fun_def, rule_format, OF crel_vs_eq_eq_onp])
     (auto simp: eq_onp_def)

lemma crel_vs_checkmem:
  "Transfer.Rel (crel_vs R) (dp param) (h.checkmem param s)" if "is_equality R" "Transfer.Rel (crel_vs R) (dp param) s"
  unfolding h.checkmem_def Rel_def that(1)[unfolded is_equality_def]
  by (rule bind_transfer[unfolded rel_fun_def, rule_format, OF crel_vs_lookup])
     (auto 4 3 split: option.split_asm intro: crel_vs_bind_eq crel_vs_update crel_vs_return[unfolded Wrap_def Rel_def] that(2)[unfolded Rel_def that(1)[unfolded is_equality_def]])

lemma crel_vs_checkmem_tupled:
  assumes "v = dp param"
  shows "\<lbrakk>is_equality R; Transfer.Rel (crel_vs R) v s\<rbrakk>
        \<Longrightarrow> Transfer.Rel (crel_vs R) v (h.checkmem param s)"
  unfolding assms by (fact crel_vs_checkmem)

lemma transfer_fun_app_lifted[transfer_rule]:
  "(crel_vs (R0 ===> crel_vs R1) ===> crel_vs R0 ===> crel_vs R1)
    App Heap_Monad_Ext.fun_app_lifted"
  unfolding Heap_Monad_Ext.fun_app_lifted_def App_def by transfer_prover

lemma crel_vs_fun_app:
  "\<lbrakk>Transfer.Rel (crel_vs R0) x x\<^sub>T; Transfer.Rel (crel_vs (R0 ===>\<^sub>T R1)) f f\<^sub>T\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R1) (App f x) (f\<^sub>T . x\<^sub>T)"
  unfolding Rel_def using transfer_fun_app_lifted[THEN rel_funD, THEN rel_funD] .

end (* Lifting Syntax *)

end (* Dynamic Programming Problem *)
thm heap_inv.transfer_lookup
print_locale heap_correct
locale dp_consistency_heap = heap_correct +
  fixes dp :: "'b \<Rightarrow> 'c"
begin

interpretation state_mem_correct: mem_correct lookup' update' P
  by (rule mem_correct_heap)

interpretation s: dp_consistency lookup' update' P dp ..

print_locale dp_heap
term "dp_heap mem P P_mem lookup_mem lookup' update_mem update'"
term mem
term P
term P_mem
lemma dp_heap: "dp_heap mem P P_mem lookup_mem lookup' update_mem update'"
  by (standard; rule transfer_lookup transfer_update)

sublocale dp_heap mem P P_mem dp lookup_mem lookup' update_mem update'
  by (rule dp_heap)

notation rel_fun_lifted (infixr "===>\<^sub>T" 55)
end

locale heap_correct_empty = heap_correct +
  fixes empty
  assumes empty_correct: "map_of_heap empty \<subseteq>\<^sub>m Map.empty" and P_empty: "P empty"

locale dp_consistency_heap_empty =
  dp_consistency_heap + heap_correct_empty
begin

lemma cmem_empty:
  "s.cmem empty"
  using empty_correct
  unfolding s.cmem_def
  unfolding map_of_heap_def
  unfolding s.map_of_def
  unfolding lookup'_def
  unfolding map_le_def
  by auto

corollary memoization_correct:
  "dp x = v" "s.cmem m" if
  "consistentDP dp\<^sub>T" "Heap_Monad.execute (dp\<^sub>T x) empty = Some (v, m)"
  using that unfolding consistentDP_def
  by (auto dest!: rel_funD[where x = x] elim!: crel_vs_executeD intro: P_empty cmem_empty)

lemma memoized_success:
  "success (dp\<^sub>T x) empty" if "consistentDP dp\<^sub>T"
  using that cmem_empty P_empty
  by (auto dest!: rel_funD intro: crel_vs_success simp: consistentDP_def)

lemma memoized:
  "dp x = fst (the (Heap_Monad.execute (dp\<^sub>T x) empty))" if "consistentDP dp\<^sub>T"
  using surjective_pairing memoization_correct(1)[OF that]
    memoized_success[OF that, unfolded success_def]
  by (cases "execute (dp\<^sub>T x) empty"; auto)

lemma cmem_result:
  "s.cmem (snd (the (Heap_Monad.execute (dp\<^sub>T x) empty)))" if "consistentDP dp\<^sub>T"
  using surjective_pairing memoization_correct(2)[OF that(1)]
    memoized_success[OF that, unfolded success_def]
  by (cases "execute (dp\<^sub>T x) empty"; auto)

end

end (* Theory *)