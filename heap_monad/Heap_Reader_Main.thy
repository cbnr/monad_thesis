theory Heap_Reader_Main
  imports
    "../transform/Transform_Cmd"
    Heap_Reader_Locales
begin

local_setup \<open>
let
  val memT = TFree ("MemoryType", @{sort type})
  val memT = dummyT

  fun mk_stateT tp =
    Type (@{type_name Heap_Reader_Monad.readerT}, [memT, tp])

  val returnN = @{const_name Heap_Reader_Monad.return}
  fun return tm = Const (returnN, dummyT --> mk_stateT dummyT) $ tm

  val appN = @{const_name Heap_Reader_Monad_Ext.fun_app_lifted}
  fun app (tm0, tm1) = Const (appN, dummyT) $ tm0 $ tm1

  fun checkmem'C ctxt = Transform_Misc.get_const_pat ctxt "checkmem'"
  fun checkmem' ctxt param body = checkmem'C ctxt $ param $ body

  val checkmemVN = "checkmem"
  (* val checkmemC = @{const_name "reader_mem_defs.checkmem"} *)

  fun rewrite_app_beta_conv ctm =
    case Thm.term_of ctm of
      Const (@{const_name Heap_Reader_Monad_Ext.fun_app_lifted}, _)
        $ (Const (@{const_name Heap_Reader_Monad.return}, _) $ Abs _)
        $ (Const (@{const_name Heap_Reader_Monad.return}, _) $ _)
        => Conv.rewr_conv @{thm Heap_Reader_Monad_Ext.return_app_return_meta} ctm
    | _ => Conv.no_conv ctm
in
  Transform_Const.add_monad_const {
    monad_name = "heap_reader",
    mk_stateT = mk_stateT,
    return = return,
    app = app,
    if_termN = @{const_name Heap_Reader_Monad_Ext.if\<^sub>T},
    checkmemVN = checkmemVN,
    rewrite_app_beta_conv = rewrite_app_beta_conv
  }
end
\<close>

locale reader_mem_defs =
  fixes P_mem :: "'mem \<Rightarrow> heap \<Rightarrow> bool"
    and lookup_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v option Heap"
    and update_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v \<Rightarrow> unit Heap"
begin

context fixes mem :: 'mem begin
interpretation h: heap_mem_defs .
abbreviation "h_checkmem' \<equiv> h.checkmem'"
end

thm heap_mem_defs.checkmem'_def
definition "checkmem' param calc' \<equiv> ReaderT (\<lambda>mem. h_checkmem' mem param (\<lambda>_. run_readerT (calc' ()) mem))"

definition "checkmem param calc \<equiv> checkmem' param (\<lambda>_. calc)"

lemma checkmem_checkmem':
  "checkmem' param (\<lambda>_. calc) = checkmem param calc"
  unfolding checkmem'_def checkmem_def ..
end

locale dp_consistency_reader = reader_mem_defs where lookup_mem=lookup_mem for lookup_mem :: "'mem \<Rightarrow> 'k \<Rightarrow> 'v option Heap"+
  fixes dp :: "'k \<Rightarrow> 'v"
    and inv_mem :: "'mem \<Rightarrow> bool"
  assumes reader_heap: "\<And>mem. inv_mem mem \<Longrightarrow> dp_consistency_heap mem P_mem update_mem lookup_mem"
begin

context
  fixes mem :: 'mem
  assumes inv_mem: "inv_mem mem"
begin

interpretation h: dp_consistency_heap
  apply (rule reader_heap)
  apply (rule inv_mem)
  done

abbreviation "h_crel_vs \<equiv> h.crel_vs"
(*lemmas h_crel_vs_def = h.crel_vs_def*)
lemmas h_crel_vs_checkmem_tupled = h.crel_vs_checkmem_tupled
lemmas h_checkmem_checkmem' = h.checkmem_checkmem'

abbreviation "h_consistentDP \<equiv> h.consistentDP"
lemmas h_consistentDP_intro =  h.consistentDP_intro
lemmas h_crel_vs_return_ext = h.crel_vs_return_ext
lemmas h_crel_vs_return = h.crel_vs_return
lemmas h_transfer'_return = h.transfer'_return
lemmas h_bind_transfer = h.bind_transfer
lemmas h_fun_app_lifted_transfer = h.fun_app_lifted_transfer
lemmas h_crel_vs_fun_app = h.crel_vs_fun_app

end


context includes lifting_syntax heap_reader_monad_syntax begin

definition crel_vs where
  "crel_vs R v r \<equiv> \<forall>mem. inv_mem mem \<longrightarrow> h_crel_vs mem R v (run_readerT r mem)"

abbreviation rel_fun_lifted :: "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c =='mem\<Longrightarrow> 'd) \<Rightarrow> bool" (infixr "===>\<^sub>T" 55) where
  "rel_fun_lifted R R' \<equiv> R ===> crel_vs R'"

lemma rel_fun_lifted_heap:
  "(R0 ===>\<^sub>T R1) f g \<longleftrightarrow> (\<forall>mem. inv_mem mem \<longrightarrow> (R0 ===> h_crel_vs mem R1) f (\<lambda>x. run_readerT (g x) mem))"
  unfolding crel_vs_def by (fastforce dest: rel_funD)

thm h_crel_vs_checkmem_tupled
lemma crel_vs_checkmem_tupled:
  "\<lbrakk>v = dp param; is_equality R; Transfer.Rel (crel_vs R) v s\<rbrakk> \<Longrightarrow>
    Transfer.Rel (crel_vs R) v (checkmem param s)"
  using h_crel_vs_checkmem_tupled h_checkmem_checkmem'
  unfolding crel_vs_def checkmem_def checkmem'_def Rel_def
  by auto

term h_consistentDP
definition "consistentDP dp\<^sub>T \<equiv> \<forall>mem. inv_mem mem \<longrightarrow> h_consistentDP mem (\<lambda>x. run_readerT (dp\<^sub>T x) mem)"

thm h_consistentDP_intro
lemma consistentDP_intro:
  "consistentDP dp\<^sub>T"
  if "(\<And>param. Transfer.Rel (crel_vs op =) (dp param) (dp\<^sub>T param))"
  using that h_consistentDP_intro 
  unfolding consistentDP_def crel_vs_def Rel_def
  by auto

thm h_crel_vs_return_ext
lemma crel_vs_return_ext:
  "Transfer.Rel R x y \<Longrightarrow> Transfer.Rel (crel_vs R) x \<langle>y\<rangle>"
  using h_crel_vs_return_ext
  unfolding crel_vs_def Rel_def return_def_alt
  by fastforce

thm h_crel_vs_return
lemma crel_vs_return:
  "Transfer.Rel R x y \<Longrightarrow> Transfer.Rel (crel_vs R) (Wrap x) \<langle>y\<rangle>"
  unfolding Wrap_def by (fact crel_vs_return_ext)

thm h_transfer'_return
lemma return_transfer[transfer_rule]:
  "(R ===>\<^sub>T R) Wrap Heap_Reader_Monad.return"
  using h_transfer'_return 
  unfolding crel_vs_def return_def_alt
  by (fastforce dest: rel_funD)

thm h_bind_transfer
lemma bind_transfer[transfer_rule]:
  "(crel_vs R0 ===> (R0 ===>\<^sub>T R1) ===> crel_vs R1) (\<lambda>v f. f v) (op \<bind>)"
  unfolding rel_fun_lifted_heap
  unfolding crel_vs_def Heap_Reader_Monad.bind_def
  using h_bind_transfer  
  by (fastforce dest: rel_funD)

thm h_fun_app_lifted_transfer
lemma fun_app_lifted_transfer[transfer_rule]:
  "(crel_vs (R0 ===>\<^sub>T R1) ===> crel_vs R0 ===> crel_vs R1) App (op .)"
  unfolding App_def Heap_Reader_Monad_Ext.fun_app_lifted_def
  by transfer_prover

thm h_crel_vs_fun_app
lemma crel_vs_fun_app:
  "\<lbrakk>Transfer.Rel (crel_vs R0) x x\<^sub>T; Transfer.Rel (crel_vs (R0 ===>\<^sub>T R1)) f f\<^sub>T\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R1) (App f x) (f\<^sub>T . x\<^sub>T)"
  unfolding Rel_def using fun_app_lifted_transfer[THEN rel_funD, THEN rel_funD] .

end

context includes heap_reader_monad_syntax begin

thm if_cong
lemma ifT_cong:
  assumes "b = c" "c \<Longrightarrow> x = u" "\<not>c \<Longrightarrow> y = v"
  shows "Heap_Reader_Monad_Ext.if\<^sub>T \<langle>b\<rangle> x y = Heap_Reader_Monad_Ext.if\<^sub>T \<langle>c\<rangle> u v"
  unfolding Heap_Reader_Monad_Ext.if\<^sub>T_def
  unfolding Heap_Reader_Monad.bind_left_identity
  using if_cong[OF assms] .

lemma return_app_return_cong:
  assumes "f x = g y"
  shows "\<langle>f\<rangle> . \<langle>x\<rangle> = \<langle>g\<rangle> . \<langle>y\<rangle>"
  unfolding Heap_Reader_Monad_Ext.return_app_return_meta assms ..

lemmas [fundef_cong] =
  return_app_return_cong
  ifT_cong
end (* syntaxes *)
end (* locale dp_consistency_reader *)

dp_fun comp\<^sub>T: comp monadifies (heap_reader) comp_def
thm comp\<^sub>T'.simps
lemma (in dp_consistency_reader) shows comp\<^sub>T_transfer[transfer_rule]:
  "crel_vs ((R1 ===>\<^sub>T R2) ===>\<^sub>T (R0 ===>\<^sub>T R1) ===>\<^sub>T (R0 ===>\<^sub>T R2)) comp comp\<^sub>T"
  apply dp_combinator_init
  subgoal premises IH [transfer_rule] thm IH by dp_unfold_defs transfer_prover
  done

dp_fun map\<^sub>T: map monadifies (heap_reader) list.map
lemma (in dp_consistency_reader) map\<^sub>T_transfer[transfer_rule]:
  "crel_vs ((R0 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T list_all2 R1) map map\<^sub>T"
  apply dp_combinator_init
  apply (erule list_all2_induct)
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  done

dp_fun fold\<^sub>T: fold monadifies (heap_reader) fold.simps
lemma (in dp_consistency_reader) fold\<^sub>T_transfer[transfer_rule]:
  "crel_vs ((R0 ===>\<^sub>T R1 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T R1 ===>\<^sub>T R1) fold fold\<^sub>T"
  apply dp_combinator_init
  apply (erule list_all2_induct)
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  subgoal premises [transfer_rule] by dp_unfold_defs transfer_prover
  done

context includes heap_reader_monad_syntax begin

thm map_cong
lemma mapT_cong:
  assumes "xs = ys" "\<And>x. x\<in>set ys \<Longrightarrow> f x = g x"
  shows "map\<^sub>T . \<langle>f\<rangle> . \<langle>xs\<rangle> = map\<^sub>T . \<langle>g\<rangle> . \<langle>ys\<rangle>"
  unfolding map\<^sub>T_def 
  unfolding assms(1)
  using assms(2) by (induction ys) (auto simp: Heap_Reader_Monad_Ext.return_app_return_meta)

thm fold_cong
lemma foldT_cong:
  assumes "xs = ys" "\<And>x. x\<in>set ys \<Longrightarrow> f x = g x"
  shows "fold\<^sub>T . \<langle>f\<rangle> . \<langle>xs\<rangle> = fold\<^sub>T . \<langle>g\<rangle> . \<langle>ys\<rangle>"
  unfolding fold\<^sub>T_def
  unfolding assms(1)
  using assms(2) by (induction ys) (auto simp: Heap_Reader_Monad_Ext.return_app_return_meta)

lemma abs_unit_cong:
  (* for lazy checkmem *)
  assumes "x = y"
  shows "(\<lambda>_::unit. x) = (\<lambda>_. y)"
  using assms ..

lemmas [fundef_cong] =
  dp_consistency_reader.return_app_return_cong
  dp_consistency_reader.ifT_cong
  mapT_cong
  foldT_cong
  abs_unit_cong
end

context dp_consistency_reader begin
context includes lifting_syntax heap_reader_monad_syntax begin

named_theorems dp_match_rule

thm if_cong
lemma if\<^sub>T_cong2:
  assumes "Rel op= b c" "c \<Longrightarrow> Rel (crel_vs R) x x\<^sub>T" "\<not>c \<Longrightarrow> Rel (crel_vs R) y y\<^sub>T"
  shows "Rel (crel_vs R) (if (Wrap b) then x else y) (Heap_Reader_Monad_Ext.if\<^sub>T \<langle>c\<rangle> x\<^sub>T y\<^sub>T)"
  using assms unfolding Heap_Reader_Monad_Ext.if\<^sub>T_def bind_left_identity Rel_def Wrap_def
  by (auto split: if_split)

lemma map\<^sub>T_cong2:
  assumes
    "is_equality R"
    "Rel R xs ys"
    "\<And>x. x\<in>set ys \<Longrightarrow> Rel (crel_vs S) (f x) (f\<^sub>T' x)"
  shows "Rel (crel_vs (list_all2 S)) (App (App map (Wrap f)) (Wrap xs)) (map\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  unfolding map\<^sub>T_def
  unfolding Heap_Reader_Monad_Ext.return_app_return_meta
  unfolding assms(2)[unfolded Rel_def assms(1)[unfolded is_equality_def]]
  using assms(3)
  unfolding Rel_def Wrap_def App_def
  apply (induction ys)
  subgoal premises by (dp_unfold_defs (heap_reader) map) transfer_prover
  subgoal premises prems for a ys
  apply (dp_unfold_defs (heap_reader) map)
    apply (unfold Heap_Reader_Monad_Ext.return_app_return_meta Wrap_App_Wrap)
    supply [transfer_rule] =
      prems(2)[OF list.set_intros(1)]
      prems(1)[OF prems(2)[OF list.set_intros(2)], simplified]
    by transfer_prover
  done

lemma fold\<^sub>T_cong2:
  assumes
    "is_equality R"
    "Rel R xs ys"
    "\<And>x. x\<in>set ys \<Longrightarrow> Rel (crel_vs (S ===> crel_vs S)) (f x) (f\<^sub>T' x)"
  shows
    "Rel (crel_vs (S ===> crel_vs S)) (fold f xs) (fold\<^sub>T . \<langle>f\<^sub>T'\<rangle> . \<langle>ys\<rangle>)"
  unfolding fold\<^sub>T_def
  unfolding Heap_Reader_Monad_Ext.return_app_return_meta
  unfolding assms(2)[unfolded Rel_def assms(1)[unfolded is_equality_def]]
  using assms(3)
  unfolding Rel_def
  apply (induction ys)
  subgoal premises by (dp_unfold_defs (heap_reader) fold) transfer_prover
  subgoal premises prems for a ys
    apply (dp_unfold_defs (heap_reader) fold)
    apply (unfold Heap_Reader_Monad_Ext.return_app_return_meta Wrap_App_Wrap)
    supply [transfer_rule] =
      prems(2)[OF list.set_intros(1)]
      prems(1)[OF prems(2)[OF list.set_intros(2)], simplified]
    by transfer_prover
  done

lemma refl2:
  "is_equality R \<Longrightarrow> Rel R x x"
  unfolding is_equality_def Rel_def by simp

lemma rel_fun2:
  assumes "is_equality R0" "\<And>x. Rel R1 (f x) (g x)"
  shows "Rel (rel_fun R0 R1) f g"
  using assms unfolding is_equality_def Rel_def by auto

lemma crel_vs_return_app_return:
  assumes "Rel R (f x) (g x)"
  shows "Rel R (App (Wrap f) (Wrap x)) (\<langle>g\<rangle> . \<langle>x\<rangle>)"
  using assms unfolding Heap_Reader_Monad_Ext.return_app_return_meta Wrap_App_Wrap .

thm option.case_cong[no_vars]
lemma option_case_cong':
"Rel op= option' option \<Longrightarrow>
(option = None \<Longrightarrow> Rel R f1 g1) \<Longrightarrow>
(\<And>x2. option = Some x2 \<Longrightarrow> Rel R (f2 x2) (g2 x2)) \<Longrightarrow>
Rel R (case option' of None \<Rightarrow> f1 | Some x2 \<Rightarrow> f2 x2)
(case option of None \<Rightarrow> g1 | Some x2 \<Rightarrow> g2 x2)"
  unfolding Rel_def by (auto split: option.split)

thm prod.case_cong[no_vars]
lemma prod_case_cong': fixes prod prod' shows
"Rel op= prod prod' \<Longrightarrow>
(\<And>x1 x2. prod' = (x1, x2) \<Longrightarrow> Rel R (f x1 x2) (g x1 x2)) \<Longrightarrow>
Rel R (case prod of (x1, x2) \<Rightarrow> f x1 x2)
(case prod' of (x1, x2) \<Rightarrow> g x1 x2)"
  unfolding Rel_def by (auto split: prod.splits)

lemmas [dp_match_rule] = prod_case_cong' option_case_cong'


lemmas [dp_match_rule] =
  crel_vs_return_app_return

lemmas [dp_match_rule] =
  map\<^sub>T_cong2
  fold\<^sub>T_cong2
  if\<^sub>T_cong2

lemmas [dp_match_rule] =
  crel_vs_return
  crel_vs_fun_app
  refl2
  rel_fun2

(*
lemmas [dp_match_rule] =
  crel_vs_checkmem_tupled
*)

end (* context lifting_syntax *)
end (* context dp_consistency *)

locale dp_consistency_reader_default =
  fixes bound :: "'k :: {index, heap} bound" 
    and dp :: "'k \<Rightarrow> 'v :: heap" 
begin

interpretation idx: bounded_index bound .

print_locale dp_consistency_reader
sublocale dp_consistency_reader
  where P_mem="\<lambda> mem heap. Array.length heap mem = idx.size"
    and lookup_mem="mem_lookup idx.size idx.checked_idx"
    and update_mem="mem_update idx.size idx.checked_idx"
    and dp=dp
    and inv_mem="\<lambda>_. True"
  apply (rule dp_consistency_reader.intro)
  apply (rule dp_consistency_heap_default.dp_consistency_heap)
  done

abbreviation "inited dp\<^sub>T x \<equiv> (mem_empty idx.size :: 'v option array Heap) \<bind> run_readerT (dp\<^sub>T x)"

lemma memoized:
  "dp x = result_of (inited dp\<^sub>T x) Heap.empty"
  if "consistentDP dp\<^sub>T"
  (*unfolding inited_def*)
  apply (rule dp_consistency_heap_default.memoized_empty)
  subgoal using that unfolding consistentDP_def by auto
  apply (rule refl)
  done

end (* locale dp_consistency_reader_default *)

locale dp_consistency_reader_array_pair'_def =
  fixes bound :: "'k2 :: {index, heap} bound"
    and dp :: "'k \<Rightarrow> 'v::heap"
  fixes key1 :: "'k \<Rightarrow> ('k1 :: heap)" and key2 :: "'k \<Rightarrow> 'k2"
    and k1 k2 :: "'k1"
begin

sublocale idx: bounded_index bound .

abbreviation "inited dp\<^sub>T x \<equiv> (init_state idx.size k1 k2 \<bind> run_readerT (dp\<^sub>T x))"

end

locale dp_consistency_reader_array_pair' =
  dp_consistency_reader_array_pair'_def
  where bound=bound and dp=dp and k1=k1
  for bound::"'k2::{heap,index} bound" and dp::"'k \<Rightarrow> ('v::heap)" and k1::"'k1::heap"+
  assumes keys_injective: "\<forall>k k'. key1 k = key1 k' \<and> key2 k = key2 k' \<longrightarrow> k = k'"
      and keys_neq: "k1 \<noteq> k2"
begin

print_locale dp_consistency_reader
sublocale dp_consistency_reader
  where inv_mem="curry_mem inv_distinct"
    and P_mem="pair_inv_pair idx.size idx.checked_idx key1 key2"
    and lookup_mem="curry_mem (lookup_pair idx.size idx.checked_idx key1 key2)"
    and update_mem="curry_mem (update_pair idx.size idx.checked_idx key1 key2)"
    and dp=dp
  apply (rule dp_consistency_reader.intro)
  apply (rule dp_consistency_heap.intro)
  apply safe
  apply (rule heap_correct_pairI)
  subgoal unfolding curry_mem_beta .
  subgoal using idx.checked_idx_injective .
  subgoal using keys_injective .
  done

context
  fixes mem :: " 'k1 ref \<times> 'k1 ref \<times> 'v option array ref \<times> 'v option array ref"
  assumes mem_is_init: "mem = result_of (init_state idx.size k1 k2) Heap.empty"
begin
print_locale dp_consistency_heap_array_pair'
interpretation h: dp_consistency_heap_array_pair'
  where size=idx.size and to_index=idx.checked_idx
  apply standard
     apply (fact mem_is_init)
    apply (fact idx.checked_idx_injective)
   apply (fact keys_injective)
  apply (fact keys_neq)
  done
lemmas h_dp_consistency_heap_array_pair' = h.dp_consistency_heap_array_pair'_axioms
end

lemma memoized:
  "dp x = result_of (inited dp\<^sub>T x) Heap.empty"
  if "consistentDP dp\<^sub>T"
    (*unfolding inited_def*)
  apply (rule dp_consistency_new'.memoized_empty[OF dp_consistency_heap_array_pair'.dp_consistency_new'[OF h_dp_consistency_heap_array_pair']])
   apply (rule refl)
  apply (unfold dp_consistency_heap_array_pair'.inv_pair'_def[OF h_dp_consistency_heap_array_pair', OF refl])
  apply (fold curry_mem_def)
  apply (fold pair_inv_pair_def)
  apply (rule that[unfolded consistentDP_def, rule_format])
  apply (rule result_of_init_state_distinct)
  done

end


lemmas [code_unfold] =
  reader_mem_defs.checkmem_checkmem'[symmetric]

lemmas [code] =
  reader_mem_defs.checkmem'_def
  heap_mem_defs.checkmem'_def

lemmas [code_unfold] =
  map\<^sub>T_def
(*
lemmas [code] =
  dp_consistency_reader_array_pair'_def.inited_def

lemmas [code] =
  dp_consistency_reader_default.inited_def
*)
end (* theory *)