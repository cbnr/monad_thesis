theory Heap_Reader_Monad_Ext
  imports "./Heap_Reader_Monad"
begin

definition fun_app_lifted :: "('M,'a \<Rightarrow> ('M, 'b) readerT) readerT \<Rightarrow> ('M,'a) readerT \<Rightarrow> ('M,'b) readerT" where
  "fun_app_lifted f\<^sub>T x\<^sub>T \<equiv> do { f \<leftarrow> f\<^sub>T; x \<leftarrow> x\<^sub>T; f x }"

bundle heap_reader_monad_syntax begin

notation fun_app_lifted (infixl "." 999)
type_synonym ('a,'M,'b) fun_lifted = "'a \<Rightarrow> ('M,'b) readerT" ("_ ==_\<Longrightarrow> _" [3,1000,2] 2)
type_synonym ('a,'b) dpfun = "'a ==('a\<rightharpoonup>'b)\<Longrightarrow> 'b" (infixr "\<Rightarrow>\<^sub>T" 2)
type_notation readerT ("[_| _]\<^sub>r")

notation Heap_Reader_Monad.return ("\<langle>_\<rangle>")
notation (ASCII) Heap_Reader_Monad.return ("(#_#)")
notation Transfer.Rel ("Rel")

end

context includes heap_reader_monad_syntax begin

qualified lemma return_app_return:
  "\<langle>f\<rangle> . \<langle>x\<rangle> = f x"
  unfolding fun_app_lifted_def bind_left_identity ..

qualified lemma return_app_return_meta:
  "\<langle>f\<rangle> . \<langle>x\<rangle> \<equiv> f x"
  unfolding return_app_return .

qualified definition if\<^sub>T :: "('s, bool) readerT \<Rightarrow> ('s, 'a) readerT \<Rightarrow> ('s, 'a) readerT \<Rightarrow> ('s, 'a) readerT" where
  "if\<^sub>T b\<^sub>T x\<^sub>T y\<^sub>T \<equiv> do {b \<leftarrow> b\<^sub>T; if b then x\<^sub>T else y\<^sub>T}"
end

end