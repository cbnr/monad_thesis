theory Outer_Heap
  imports "HOL-Imperative_HOL.Imperative_HOL"
begin

locale Outer_Monad
begin

type_synonym 'a state1 = "'a Heap"
type_synonym ('M, 'a) state = "'a state1"

definition return :: "'a \<Rightarrow> 'a state1" where
  "return \<equiv> Heap_Monad.return"

definition bind :: "'a state1 \<Rightarrow> ('a \<Rightarrow> 'b state1) \<Rightarrow> 'b state1" where
  "bind \<equiv> Heap_Monad.bind"

definition app :: "('a \<Rightarrow> 'b state1) state1 \<Rightarrow> 'a state1 \<Rightarrow> 'b state1" where
  "app f x \<equiv> bind f (\<lambda>f'. bind x (\<lambda>x'. f' x'))"

definition effect :: "['a state1, heap, heap, 'a] \<Rightarrow> bool" where
  "effect \<equiv> Heap_Monad.effect"

lemma bind_assoc:
  "bind (bind s k0) k1 = bind s (\<lambda>a. bind (k0 a) k1)"
  unfolding bind_def by (fact Heap_Monad.bind_bind)

lemma return_bind:
  "bind (return v) k = k v"
  unfolding return_def bind_def by (fact Heap_Monad.return_bind)

lemma bind_return:
  "bind s return = s"
  unfolding return_def bind_def
  by (fact Heap_Monad.bind_return)

lemma return_app_return:
  "app (return f) (return x) = f x"
  unfolding app_def
  unfolding return_bind
  ..

lemma effect_bind_elim:
  assumes "effect (bind x f) M Mv v"
  obtains Mx x' where "effect x M Mx x'" "effect (f x') Mx Mv v"
  apply (insert assms)
  unfolding bind_def effect_def
  using effect_bindE
  .

lemma effect_app_elim:
  assumes "effect (app f x) M Mv v"
  obtains f' Mf x' Mx where "effect f M Mf f'" "effect x Mf Mx x'" "effect (f' x') Mx Mv v"
  apply (insert assms)
  unfolding app_def
  apply (elim effect_bind_elim)
  .

end (* Outer_State *)


bundle outer_monad_syntax
begin

(*
adhoc_overloading
  Monad_Syntax.bind Outer_Monad.bind
*)

notation Outer_Monad.return ("\<langle>_\<rangle>")
notation Outer_Monad.app (infixl "." 999)

end (* outer_monad_syntax *)

end (* theory *)