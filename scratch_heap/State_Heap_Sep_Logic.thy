theory State_Heap_Sep_Logic
  imports
    "../Pure_Monad"
    "IICF/IICF"
begin

locale heap_mem_defs =
  fixes is_map :: "('k \<Rightarrow> 'v option) \<Rightarrow> assn"
    and lookup :: "'k \<Rightarrow> 'v option Heap"
    and update :: "'k \<Rightarrow> 'v \<Rightarrow> unit Heap"
begin

definition checkmem :: "'k \<Rightarrow> 'v Heap \<Rightarrow> 'v Heap" where
  "checkmem param calc \<equiv>
    Heap_Monad.bind (lookup param) (\<lambda> x.
    case x of
      Some x \<Rightarrow> return x
    | None \<Rightarrow> Heap_Monad.bind calc (\<lambda> x.
        Heap_Monad.bind (update param x) (\<lambda> _.
        return x
      )
    )
  )
  "

end

locale heap_mem_correct = heap_mem_defs +
  assumes lookup_correct: "<is_map m> lookup k <\<lambda> r. \<exists>\<^sub>A m'. is_map m' * \<up>(m' \<subseteq>\<^sub>m m \<and> r = m k)>\<^sub>t"
  assumes update_correct: "<is_map m> update k v <\<lambda>_. \<exists>\<^sub>A m'. is_map m' * \<up>(m' \<subseteq>\<^sub>m m(k\<mapsto>v))>\<^sub>t"

locale dp_consistency_heap =
  heap_mem_correct is_map lookup update
  for is_map :: "('k \<Rightarrow> 'v option) \<Rightarrow> assn" and lookup and update +
  fixes dp :: "'k \<Rightarrow> 'v"
begin

context
  includes lifting_syntax
begin

definition cmem :: "assn" where
  "cmem \<equiv> \<exists>\<^sub>A m. is_map m * \<up> (\<forall>param\<in>dom m. m param = Some (dp param))"

definition crel_vs :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'b Heap \<Rightarrow> bool" where
  "crel_vs R v s \<equiv> <cmem> s <\<lambda> v'. cmem * \<up>(R v v')>\<^sub>t"

abbreviation rel_fun_lifted ::
  "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c \<Rightarrow> 'd Heap) \<Rightarrow> bool"
  (infixr "===>\<^sub>T" 55) where
  "rel_fun_lifted R R' \<equiv> R ===> crel_vs R'"
term 0 (**)

definition consistentDP :: "('k \<Rightarrow> 'v Heap) \<Rightarrow> bool" where
  "consistentDP \<equiv> (op = ===> crel_vs op =) dp"
term 0 (**)

lemma crel_vs_return:
  "\<lbrakk>Transfer.Rel R x y\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R) x (Heap_Monad.return y)"
  unfolding  Wrap_def Rel_def crel_vs_def by sep_auto

lemma bind_transfer[transfer_rule]:
  "(crel_vs R0 ===> (R0 ===>\<^sub>T R1) ===> crel_vs R1) (\<lambda>v f. f v) (op \<bind>)"
  unfolding rel_fun_def crel_vs_def
  apply clarsimp
  subgoal premises prems for x y f g
    thm prems[rule_format]
    by (sep_auto heap: prems[rule_format])
  done

lemma crel_vs_lookup:
  "crel_vs (\<lambda> v v'. case v' of None \<Rightarrow> True | Some v' \<Rightarrow> v = v' \<and> v = dp param) (dp param) (lookup param)"
  unfolding crel_vs_def cmem_def
  apply sep_auto
  apply (rule cons_post_rule[OF lookup_correct])
  apply sep_auto
  apply (force dest: map_leD split: option.split)+
  done

lemma crel_vs_update:
  "crel_vs op = () (update param (dp param))"
  unfolding crel_vs_def cmem_def
  apply sep_auto
  apply (rule cons_post_rule[OF update_correct])
  apply sep_auto
  by (metis (no_types, lifting) domI map_leD map_upd_Some_unfold)

private lemma crel_vs_bind_eq:
  "\<lbrakk>crel_vs op = v s; crel_vs R (f v) (sf v)\<rbrakk> \<Longrightarrow> crel_vs R (f v) (s \<bind> sf)"
  unfolding rel_fun_def crel_vs_def
  subgoal premises prems
    by (sep_auto heap: prems)
  done

private lemma crel_vs_checkmem:
  "\<lbrakk>is_equality R; Transfer.Rel (crel_vs R) (dp param) s\<rbrakk>
  \<Longrightarrow> Transfer.Rel (crel_vs R) (dp param) (checkmem param s)"
  unfolding checkmem_def Rel_def is_equality_def
  by (auto 4 5 intro:
        crel_vs_lookup crel_vs_update crel_vs_return[unfolded Rel_def] crel_vs_bind_eq
        bind_transfer[unfolded rel_fun_def, rule_format]
      split: option.split_asm
     )

end (* Lifiting Syntax *)

end (* DP Consistency *)

end (* Theory *)