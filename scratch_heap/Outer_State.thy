theory Outer_State
  imports Main Monad_Syntax "Inner_Monad"
begin

locale Outer_Monad
begin

datatype ('M, 'a) state = State (runState: "'M \<Rightarrow> ('a \<times> 'M) Inner_Monad.type")

definition return :: "'a \<Rightarrow> ('M, 'a) state" where
  "return a \<equiv> State (\<lambda>M. Inner_Monad.return (a, M))"

definition bind :: "('M, 'a) state \<Rightarrow> ('a \<Rightarrow> ('M, 'b) state) \<Rightarrow> ('M, 'b) state" where
  "bind s k \<equiv> State (\<lambda>M. Inner_Monad.bind (runState s M) (\<lambda>(a, M'). runState (k a) M'))"

definition app :: "('M, 'a \<Rightarrow> ('M, 'b) state) state \<Rightarrow> ('M, 'a) state \<Rightarrow> ('M, 'b) state" where
  "app f x \<equiv> bind f (\<lambda>f'. bind x (\<lambda>x'. f' x'))"

definition get :: "('M, 'M) state" where
  "get \<equiv> State (\<lambda>M. Inner_Monad.return (M, M))"

definition put :: "'M \<Rightarrow> ('M, unit) state" where
  "put M \<equiv> State (\<lambda>_. Inner_Monad.return ((), M))"

definition effect :: "[('M, 'a) state, 'M, 'M, 'a] \<Rightarrow> bool" where
  "effect x M M' y \<equiv> runState x M = Inner_Monad.return (y, M')"

lemma bind_assoc:
  "bind (bind s k0) k1 = bind s (\<lambda>a. bind (k0 a) k1)"
  unfolding bind_def
  unfolding state.sel
  unfolding Inner_Monad.bind_assoc
  unfolding prod.case_distrib
  unfolding prod.case_distrib[of "\<lambda>f. f _"]
  ..

lemma return_bind:
  "bind (return v) k = k v"
  unfolding return_def bind_def
  apply (rule state.expand)
  unfolding state.sel
  unfolding Inner_Monad.return_bind
  unfolding prod.case
  ..

lemma bind_return:
  "bind s return = s"
  unfolding return_def bind_def
  apply (rule state.expand)
  unfolding state.sel
  unfolding case_prod_eta
  unfolding Inner_Monad.bind_return
  ..

lemma return_app_return:
  "app (return f) (return x) = f x"
  unfolding app_def
  unfolding return_bind
  ..

lemma effect_bind_elim:
  assumes "effect (bind x f) M Mv v"
  obtains Mx x' where "effect x M Mx x'" "effect (f x') Mx Mv v"
  apply (insert assms)
  unfolding effect_def
  unfolding bind_def
  unfolding state.sel
  apply (erule Inner_Monad.bind_eq_return_elim)
  apply (split prod.split_asm)
  apply clarify
  .

lemma effect_app_elim:
  assumes "effect (app f x) M Mv v"
  obtains f' Mf x' Mx where "effect f M Mf f'" "effect x Mf Mx x'" "effect (f' x') Mx Mv v"
  apply (insert assms)
  unfolding app_def
  apply (elim effect_bind_elim)
  .


end (* Outer_State *)

bundle outer_monad_syntax
begin

adhoc_overloading
  Monad_Syntax.bind Outer_Monad.bind

notation Outer_Monad.return ("\<langle>_\<rangle>")
notation Outer_Monad.app (infixl "." 999)

end

end (* theory *)