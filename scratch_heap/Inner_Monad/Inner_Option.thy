theory Inner_Option
  imports Main "HOL-Library.Monad_Syntax"
begin

locale Inner_Monad
begin

type_synonym 'a type = "'a option"

definition return where
  "return \<equiv> Some"

definition bind where
  "bind \<equiv> Option.bind"

lemma return_bind:
  "bind (return x) f = f x"
  unfolding bind_def return_def by (fact Option.bind_lunit)

lemma bind_return:
  "bind x return = x"
  unfolding bind_def return_def by (fact bind_runit)

definition app where
  "app f x = bind f (\<lambda>f'. bind x (\<lambda>x'. f' x'))"

lemma return_app_return:
  "app (return f) (return x) = f x"
  unfolding app_def return_bind ..

lemma bind_assoc:
  "bind (bind x f) g = bind x (\<lambda>x'. bind (f x') g)"
  unfolding bind_def by (fact Option.bind_assoc)

lemma bind_eq_return_elim:
  assumes "bind x f = return z"
  obtains y where "x = return y" "f y = return z"
  apply (insert assms)
  unfolding bind_def return_def
  unfolding bind_eq_Some_conv
  apply (elim exE conjE)
  .

end (* Inner_Monad *)

bundle Inner_Monad_Syntax
begin

adhoc_overloading
  Monad_Syntax.bind Inner_Monad.bind

notation Inner_Monad.return ("\<langle>_\<rangle>")
notation Inner_Monad.app (infixl "." 999)

end (* Inner_Monad_Syntax *)

end (* Theory *)