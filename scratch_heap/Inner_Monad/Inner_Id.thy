theory Inner_Id
  imports Main "HOL-Library.Monad_Syntax"
begin

locale Inner_Monad
begin

type_synonym 'a type = 'a

definition return where
  "return x \<equiv> x"

definition bind where
  "bind x f \<equiv> f x"

lemma return_bind:
  "bind (return x) f = f x"
  unfolding bind_def return_def ..

lemma bind_return:
  "bind x return = x"
  unfolding bind_def return_def ..

definition app where
  "app f x = bind f (\<lambda>f'. bind x (\<lambda>x'. f' x'))"

lemma return_app_return:
  "app (return f) (return x) = f x"
  unfolding app_def return_bind ..

lemma bind_assoc:
  "bind (bind x f) g = bind x (\<lambda>x'. bind (f x') g)"
  unfolding bind_def ..

end (* Inner_Monad *)

bundle Inner_Monad_Syntax
begin

adhoc_overloading
  Monad_Syntax.bind Inner_Monad.bind

notation Inner_Monad.return ("\<langle>_\<rangle>")
notation Inner_Monad.app (infixl "." 999)

end (* Inner_Monad_Syntax *)

end (* Theory *)