theory DP_CRelVS
  imports  "./DP_Lifting"
begin

definition lift_p :: "(heap \<Rightarrow> bool) \<Rightarrow> 'a Heap \<Rightarrow> bool" where
  "lift_p P f =
    (\<forall> h h' v. P h \<longrightarrow> execute f h = Some (v, h') \<longrightarrow> P h')"

lemma lift_p_dest:
  assumes "lift_p heapP f" "heapP heap" "execute f heap = Some (v, heap')"
  shows "heapP heap'"
  using assms unfolding lift_p_def by blast

locale mem_correct = mem_defs lookup for lookup :: "'param \<Rightarrow> _" +
  fixes paramP :: "'param \<Rightarrow> bool"
    and heapP :: "heap \<Rightarrow> bool"
  assumes lookup_inv: "lift_p heapP (lookup k)" and update_inv: "lift_p heapP (update k v)"
  assumes
    lookup_correct: "\<lbrakk>heapP m; paramP param\<rbrakk> \<Longrightarrow> success (lookup k) m \<and> map_of (snd (the (execute (lookup k) m))) \<subseteq>\<^sub>m (map_of m)"
      and
    update_correct: "\<lbrakk>heapP m; paramP param\<rbrakk> \<Longrightarrow> success (update k v) m \<and> map_of (snd (the (execute (update k v) m))) \<subseteq>\<^sub>m (map_of m)(k \<mapsto> v)"
  (* assumes correct: "lookup (update m k v) \<subseteq>\<^sub>m (lookup m)(k \<mapsto> v)" *)

locale dp_consistency =
  mem_correct where lookup=lookup for lookup :: "'param \<Rightarrow> 'result option Heap" +
  fixes dp :: "'param \<Rightarrow> 'result"
begin

context
  includes lifting_syntax
begin

definition cmem :: "heap \<Rightarrow> bool" where
  "cmem mem \<equiv> \<forall>param. paramP param \<longrightarrow> param\<in>dom (map_of mem) \<longrightarrow> map_of mem param = Some (dp param)"

definition memP :: "heap \<Rightarrow> bool" where
  "memP mem \<equiv> cmem mem \<and> heapP mem"

definition crel_vs :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'b Heap \<Rightarrow> bool" where
  "crel_vs R v s \<equiv> \<forall>mem mem' v'. memP mem \<longrightarrow> success s mem \<and> (execute s mem = Some (v', mem') \<longrightarrow> R v v' \<and> memP mem')"

abbreviation rel_fun_lifted :: "('a \<Rightarrow> 'c \<Rightarrow> bool) \<Rightarrow> ('b \<Rightarrow> 'd \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('c \<Rightarrow>\<^sub>T 'd) \<Rightarrow> bool" (infixr "===>\<^sub>T" 55) where
  "R ===>\<^sub>T R' \<equiv> R ===> crel_vs R'"
term 0 (**)


definition consistentDP :: "('param \<Rightarrow>\<^sub>T 'result) \<Rightarrow> bool" where
  "consistentDP \<equiv> (op = ===>\<^sub>T op =) dp"
term 0 (**)
  
  (* cmem *)
lemma cmem_intro:
  assumes "\<And>param v. paramP param \<Longrightarrow> map_of mem param = Some v \<Longrightarrow> v = (dp param)"
  shows "cmem mem"
  using assms unfolding cmem_def by auto

lemma cmem_elim1:
  assumes "cmem mem" "paramP param" "execute (lookup param) mem = Some (Some v, mem')"
  shows "dp param = v"
  using assms unfolding cmem_def map_of_def by auto

lemma cmem_elim:
  assumes "cmem mem" "paramP param" "map_of mem param = Some v"
  shows "dp param = v"
  using assms unfolding cmem_def by auto
term 0 (**)
  
  (* crel_vs *)
lemma crel_vs_intro:
  assumes "\<And>mem mem' v'. memP mem \<Longrightarrow> success v\<^sub>T mem \<and> (execute v\<^sub>T mem = Some (v', mem') \<longrightarrow> R v v' \<and> memP mem')"
  shows "crel_vs R v v\<^sub>T"
  using assms unfolding crel_vs_def by auto
  
lemma crel_vs_elim:
  assumes "crel_vs R v v\<^sub>T" "memP mem"
  obtains v' mem' where "execute v\<^sub>T mem = Some (v', mem')" "R v v'" "memP mem'"
(*  using assms unfolding crel_vs_def success_def apply auto*)
  using assms unfolding crel_vs_def by (blast elim: successE)

lemma crel_vs_dest:
  assumes "crel_vs R v v\<^sub>T" "memP mem" "execute v\<^sub>T mem = Some (v', mem')"
  shows "R v v'" "memP mem'"
  using assms by (auto elim: crel_vs_elim)

  (* consistentDP *)
lemma consistentDP_intro:
  assumes "\<And>param. Transfer.Rel (crel_vs op=) (dp param) (dp\<^sub>T param)"
  shows "consistentDP dp\<^sub>T"
  using assms unfolding consistentDP_def Rel_def by blast
  
lemma crel_vs_return:
  "\<lbrakk>Transfer.Rel R x y\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R) x (return y)"
  unfolding Rel_def by (auto intro: crel_vs_intro success_returnI simp: execute_return)
term 0 (**)

lemma cmem_mono:
  "cmem mem'" if "cmem mem" "map_of mem' \<subseteq>\<^sub>m map_of mem"
  using that unfolding cmem_def dom_def map_le_def by auto

  (* Low level operators *)
lemma cmem_upd:
  "memP M'" if "memP M" "paramP param" "execute (update param (dp param)) M = Some (v, M')"
  using that(1)
  unfolding memP_def
  apply auto
  subgoal
    apply (drule
        update_correct[OF _ that(2), of M param "dp param",
        unfolded that(3),
        simplified])
    apply safe
    apply (rule cmem_intro)
    unfolding map_le_def dom_def
    apply (auto split: if_splits)
    apply -

    apply (drule cmem_elim)
      apply assumption
    
    using update_correct[OF _ that(2), of M param "dp param"] that(3)
    apply auto
    using cmem_mono

  term 0 (*
  apply (rule cmem_intro)
  using update_correct
  using update_correct[of M param "dp param", OF that(2)] that cmem_elim[OF that(1)]

lemma crel_vs_bind_eq:
  "\<lbrakk>crel_vs op = v s; crel_vs R (f v) (sf v)\<rbrakk> \<Longrightarrow> crel_vs R (f v) (s \<bind> sf)"
  unfolding bind_def rel_fun_def by (fastforce intro: crel_vs_intro elim: crel_vs_elim split: prod.split)
term 0 (**)

term crel_vs

lemma memP_lookup:
  "memP mem'" if "memP mem" "paramP param" "effect (lookup param) mem mem' v"
  using that lookup_inv unfolding memP_def
  by (auto elim: effectE dest: lift_p_dest lookup_correct cmem_mono)

lemma crel_vs_lookup:
  "crel_vs (\<lambda> v v'. case v' of None \<Rightarrow> True | Some v' \<Rightarrow> v = v' \<and> v = dp param) (dp param) (lookup param)"
  if "paramP param"
  apply (rule crel_vs_intro)
  using that lookup_inv by (auto intro: crel_vs_intro simp: memP_def elim: effectE cmem_elim dest: cmem_mono lookup_correct lift_p_dest split: option.split)

lemma bind_transfer[transfer_rule]:
  "(crel_vs R0 ===> (R0 ===>\<^sub>T R1) ===> crel_vs R1) (\<lambda>v f. f v) (op \<bind>)"
  apply (intro rel_funI)
  apply (rule crel_vs_intro)
  apply safe
     apply (erule crel_vs_elim)
     apply assumption
    apply (drule rel_funD)
     apply assumption
    apply (erule crel_vs_elim)
     apply assumption back
    apply (rule success_bind_effectI)
     apply assumption
    apply (rule effect_success)
    apply assumption
   apply (erule crel_vs_elim)
    apply assumption
   apply (erule effect_bindE)
   apply (frule effect_deterministic)
    apply assumption back
  apply auto
   apply (drule rel_funD)
    apply assumption
   apply (erule crel_vs_elim)
    apply assumption back
   apply (drule effect_deterministic(2)) back
    apply assumption
  apply auto
   apply (drule effect_deterministic(1)) back back
    apply assumption
  apply auto
  apply (erule effect_bindE)
  apply (erule crel_vs_elim)
   apply assumption
  apply (drule rel_funD)
   apply assumption
  apply (frule effect_deterministic(2)) back back
   apply assumption
  apply auto
  apply (frule effect_deterministic(1)) back back
   apply assumption apply auto
  apply (erule crel_vs_elim)
   apply assumption back
  apply (frule effect_deterministic(2)) back back
   apply auto
  done


  
lemma crel_vs_update:
  "crel_vs op = () (update param (dp param))"
  by (auto intro: cmem_upd crel_vs_intro P_upd)

lemma crel_vs_checkmem:
  "\<lbrakk>is_equality R; Transfer.Rel (crel_vs R) (dp param) s\<rbrakk>
  \<Longrightarrow> Transfer.Rel (crel_vs R) (dp param) (checkmem param s)"
  unfolding checkmem_def Rel_def is_equality_def
  by (rule bind_transfer[unfolded rel_fun_def, rule_format, OF crel_vs_lookup])
     (auto 4 3 intro: crel_vs_lookup crel_vs_update crel_vs_return[unfolded Rel_def] crel_vs_bind_eq
               split: option.split_asm
     )

  (** Transfer rules **)
  (* Basics *)
lemma return_transfer[transfer_rule]:
  "(R ===>\<^sub>T R) (\<lambda>x. x) return"
  unfolding id_def rel_fun_def by (metis crel_vs_return Rel_def)

lemma fun_app_lifted_transfer[transfer_rule]:
  "(crel_vs (R0 ===>\<^sub>T R1) ===> crel_vs R0 ===> crel_vs R1) (\<lambda> f x. f x) (op .)"
  unfolding fun_app_lifted_def by transfer_prover
    
lemma crel_vs_fun_app:
  "\<lbrakk>Transfer.Rel (crel_vs (R0 ===>\<^sub>T R1)) f f\<^sub>T; Transfer.Rel (crel_vs R0) x x\<^sub>T\<rbrakk> \<Longrightarrow> Transfer.Rel (crel_vs R1) (f x) (f\<^sub>T . x\<^sub>T)"
  unfolding Rel_def using fun_app_lifted_transfer[THEN rel_funD, THEN rel_funD] .

  (* HOL *)
lemma if_transfer[transfer_rule]:
  "(crel_vs op = ===> crel_vs R ===> crel_vs R ===> crel_vs R) If if\<^sub>T"
  unfolding if\<^sub>T_def by transfer_prover
  
lemma comp_transfer[transfer_rule]:
  "crel_vs ((R1 ===>\<^sub>T R2) ===>\<^sub>T (R0 ===>\<^sub>T R1) ===>\<^sub>T (R0 ===>\<^sub>T R2)) comp comp\<^sub>T"
  unfolding comp_def comp\<^sub>T_def fun_app_lifted_def by transfer_prover
term 0 (**)

  (* Combinator *)
lemma map_transfer[transfer_rule]:
  "crel_vs ((R0 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T list_all2 R1) map map\<^sub>T"
proof -
  have [transfer_rule]: "((R0 ===>\<^sub>T R1) ===> (list_all2 R0 ===>\<^sub>T list_all2 R1)) map map\<^sub>T'"
    apply ((rule rel_funI)+, induct_tac rule: list_all2_induct, assumption; unfold list.map map\<^sub>T'.simps)
    subgoal premises [transfer_rule] by transfer_prover
    subgoal premises [transfer_rule] by transfer_prover
    done
  show ?thesis
    unfolding map\<^sub>T_def by transfer_prover
qed
  
lemma fold_transfer[transfer_rule]:
  "crel_vs ((R0 ===>\<^sub>T R1 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T R1 ===>\<^sub>T R1) fold fold\<^sub>T"
proof -
  have [transfer_rule]: "((R0 ===>\<^sub>T R1 ===>\<^sub>T R1) ===> list_all2 R0 ===>\<^sub>T R1 ===>\<^sub>T R1) fold fold\<^sub>T'"
    apply ((rule rel_funI)+, induct_tac rule: list_all2_induct, assumption; unfold fold.simps fold\<^sub>T'.simps id_def)
    subgoal premises [transfer_rule] by transfer_prover
    subgoal premises [transfer_rule] by transfer_prover
    done
  show ?thesis
    unfolding fold\<^sub>T_def by transfer_prover
qed

thm fold_transfer
lemma fold1_transfer[transfer_rule]:
  "crel_vs ((R0 ===>\<^sub>T R1 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T R1 ===>\<^sub>T R1) fold fold1\<^sub>T"
  unfolding List.fold_def[abs_def] fold1\<^sub>T_def
  by transfer_prover

thm map_transfer
lemma map1_transfer[transfer_rule]:
  "crel_vs ((R0 ===>\<^sub>T R1) ===>\<^sub>T list_all2 R0 ===>\<^sub>T list_all2 R1) map map1\<^sub>T"
  unfolding List.map_rec map1\<^sub>T_def
  by transfer_prover

end (* Lifting Syntax *)

end (* Consistency *)
end (* Theory *)