theory Memory_Heap_2
  imports State_Heap_Sep_Logic
begin

paragraph \<open>Unused\<close>

text \<open>Connection between Hoare triples and success\<close>

lemma hoare_triple_success_empty:
  "success c heap" if "<emp> c <Q>\<^sub>t"
proof -
  obtain \<sigma>' r where run: "run c (Some heap) \<sigma>' r"
    by (rule run_complete)
  from hoare_tripleD[OF that mod_emp_simp this] show ?thesis
    apply -
    apply (erule run_effectE[OF run])
    apply (rule effect_success)
    apply simp
    done
qed

lemma hoare_triple_success:
  "success c heap" if "<P> c <Q>\<^sub>t" and "(heap, as) \<Turnstile> P"
proof -
  obtain \<sigma>' r where run: "run c (Some heap) \<sigma>' r"
    by (rule run_complete)
  from hoare_tripleD[OF that this] show ?thesis
    apply -
    apply (erule run_effectE[OF run])
    apply (rule effect_success)
    apply simp
    done
qed

lemma
  "(heap, as) \<Turnstile> p \<mapsto>\<^sub>r xa * q \<mapsto>\<^sub>r x \<Longrightarrow> p \<noteq> q"
  by (auto simp del: unequal)



paragraph \<open>Line memory\<close>

definition injective :: "nat \<Rightarrow> ('k \<Rightarrow> nat) \<Rightarrow> bool" where
  "injective size to_index \<equiv> \<forall> a b.
      to_index a = to_index b
    \<and> to_index a < size
    \<and> to_index b < size
    \<longrightarrow> a = b"
  for size to_index

context
  fixes size :: nat
    and to_index :: "('k2 :: heap) \<Rightarrow> nat"
begin

definition
  "mem_empty = (Array.new size (None :: ('v :: heap) option))"

lemma mem_empty_spec:
  "<emp> mem_empty <\<lambda> r. is_array (replicate size None) r>"
  unfolding mem_empty_def is_array_def by sep_auto

context
  fixes mem :: "('v :: heap) option array"
begin

definition
  "mem_lookup k = (let i = to_index k in
    if i < size then Array.nth mem i else return None
  )"

definition
  "mem_update k v = (let i = to_index k in
    if i < size then (Array.upd i (Some v) mem \<bind> (\<lambda> _. return ()))
    else return ()
  )
  "

lemma mem_lookup_spec:
  "<is_array l mem>
    mem_lookup k
  <\<lambda> r. is_array l mem * \<up>(r = (let i = to_index k in if i < size then l ! i else None)) >"
  if "length l = size"
  using that unfolding mem_lookup_def is_array_def by sep_auto

definition
  "mem_of_line m = (\<exists>\<^sub>A l. is_array l mem * \<up>(length l = size) *
    \<up>(m = (\<lambda>k. let i = to_index k in if i < size then l ! i else None))
  )"

lemma mem_lookup_spec':
  "<mem_of_line m>
    mem_lookup k
  <\<lambda>r. mem_of_line m * \<up>(r = m k)>"
  unfolding mem_of_line_def mem_lookup_def is_array_def by sep_auto

lemma mem_lookup_spec2:
  "<mem_of_line m>
    mem_lookup k
  <\<lambda>r. \<exists>\<^sub>A m'. mem_of_line m' * \<up>(m' \<subseteq>\<^sub>m m \<and> r = m k)>\<^sub>t"
  unfolding mem_of_line_def mem_lookup_def is_array_def by sep_auto

context assumes injective: "injective size to_index"
begin

lemma mem_update_spec1:
  "<mem_of_line m>
    mem_update k v
  <\<lambda>r. mem_of_line (m(k\<mapsto>v))>"
  if "to_index k < size"
  using that unfolding mem_of_line_def mem_update_def is_array_def
  by (sep_auto simp: Let_def) (rule ext, simp, metis injective injective_def nth_list_update_neq)

lemma mem_update_spec2:
  "<mem_of_line m>
    mem_update k v
  <\<lambda>r. mem_of_line (if to_index k < size then m(k\<mapsto>v) else m)>"
  unfolding mem_of_line_def mem_update_def is_array_def
  apply (sep_auto simp: Let_def)
   apply (rule ext, simp, metis injective injective_def nth_list_update_neq)
  apply sep_auto
  done

lemma mem_update_spec3:
  "<mem_of_line m>
    mem_update k v
  <\<lambda>r. \<exists>\<^sub>A m'. mem_of_line m' * \<up>(m' \<subseteq>\<^sub>m m(k\<mapsto>v))>\<^sub>t"
  using injective
  by (sep_auto heap: mem_update_spec2) (auto simp: map_le_def mem_of_line_def injective_def)

interpretation heap_mem_correct mem_of_line mem_lookup mem_update
  apply standard
   apply (rule mem_lookup_spec2)
  apply (rule mem_update_spec3)
  done

lemmas mem_heap_correct = heap_mem_correct_axioms

end (* Injectivity *)

end (* Fixed array *)

lemma mem_empty_spec':
  "<emp> mem_empty <\<lambda>r. mem_of_line r empty>"
  unfolding mem_empty_def mem_of_line_def is_array_def Let_def by sep_auto



paragraph \<open>Pair memory\<close>

definition
  "alloc_pair a b \<equiv> do {
    r1 \<leftarrow> ref a;
    r2 \<leftarrow> ref b;
    return (r1, r2)
  }"

definition
  "init_state_inner k1 k2 m1 m2 \<equiv>  do {
    (k_ref1, k_ref2) \<leftarrow> alloc_pair k1 k2;
    (m_ref1, m_ref2) \<leftarrow> alloc_pair m1 m2;
    return (k_ref1, k_ref2, m_ref1, m_ref2)
  }
  "

definition
  "init_state k1 k2 \<equiv> do {
    m1 \<leftarrow> mem_empty;
    m2 \<leftarrow> mem_empty;
    init_state_inner k1 k2 m1 m2
  }"

lemma init_state_spec:
  "<emp> init_state k1 k2 <\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
    \<exists>\<^sub>A m1 m2.
        m_ref1 \<mapsto>\<^sub>r m1 * mem_of_line m1 Map.empty
      * m_ref2 \<mapsto>\<^sub>r m2 * mem_of_line m2 Map.empty
      * k_ref1 \<mapsto>\<^sub>r k1
      * k_ref2 \<mapsto>\<^sub>r k2
  >\<^sub>t"
  unfolding init_state_def init_state_inner_def is_array_def alloc_pair_def
  by (sep_auto heap: mem_empty_spec')

(* Not needed anylonger *)
(*
definition
  "inv_distinct k_ref1 k_ref2 m_ref1 m_ref2 \<equiv>
     m_ref1 =!= m_ref2 \<and> m_ref1 =!= k_ref1 \<and> m_ref1 =!= k_ref2 \<and> m_ref2 =!= k_ref1
   \<and> m_ref2 =!= k_ref2 \<and> k_ref1 =!= k_ref2
  "

lemma init_state_spec':
  "<emp> init_state k1 k2 <\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
    \<up>(inv_distinct k_ref1 k_ref2 m_ref1 m_ref2)
  >\<^sub>t"
  unfolding init_state_def init_state_inner_def is_array_def alloc_pair_def mem_empty_def
  unfolding inv_distinct_def
  apply sep_auto
  oops
*)

context
  fixes key1 :: "'k \<Rightarrow> ('k1 :: heap)" and key2 :: "'k \<Rightarrow> 'k2"
    and m_ref1 m_ref2 :: "('v :: heap) option array ref"
    and k_ref1 k_ref2 :: "('k1 :: heap) ref"
begin

definition
  "is_pair_mem M \<equiv> \<exists>\<^sub>A k1 k2 m1 m2 M1 M2.
    mem_of_line m1 M1 * mem_of_line m2 M2 *
    k_ref1 \<mapsto>\<^sub>r k1 * k_ref2 \<mapsto>\<^sub>r k2
    * m_ref1 \<mapsto>\<^sub>r m1 * m_ref2 \<mapsto>\<^sub>r m2 
    * \<up> ((\<forall> k \<in> dom M1. \<exists> k'. key1 k' = k1 \<and> key2 k' = k) \<and>
    (\<forall> k \<in> dom M2. \<exists> k'. key1 k' = k2 \<and> key2 k' = k) \<and> k1 \<noteq> k2
    \<and> M = (\<lambda> k. if key1 k = k2 then M2 (key2 k) else if key1 k = k1 then M1 (key2 k) else None))
  "

text \<open>We assume that look-ups happen on the older row, so this is biased towards the second entry.\<close>
definition
  "lookup_pair k = do {
    let k' = key1 k;
    k2 \<leftarrow> !k_ref2;
    if k' = k2 then
      do {
        m2 \<leftarrow> !m_ref2;
        mem_lookup m2 (key2 k)
      }
    else
      do {
      k1 \<leftarrow> !k_ref1;
      if k' = k1 then
        do {
          m1 \<leftarrow> !m_ref1;
          mem_lookup m1 (key2 k)
        }
      else
        return None
    }
  }
"

text \<open>We assume that updates happen on the newer row, so this is biased towards the first entry.\<close>
definition
  "update_pair k v = do {
    let k' = key1 k;
      k1 \<leftarrow> !k_ref1;
      if k' = k1 then do {
        m \<leftarrow> !m_ref1;
        mem_update m (key2 k) v
      }
      else do {
        k2 \<leftarrow> !k_ref2;
        if k' = k2 then do {
          m \<leftarrow> !m_ref2;
          mem_update m (key2 k) v
        }
        else do {
          do {
            k1 \<leftarrow> !k_ref1;
            m \<leftarrow> mem_empty;
            m1 \<leftarrow> !m_ref1;
            k_ref2 := k1;
            k_ref1 := k';
            m_ref2 := m1;
            m_ref1 := m
          }
        ;
        m \<leftarrow> !m_ref1;
        mem_update m (key2 k) v
      }
    }
   }
   "

lemma lookup_pair_correct:
  "<is_pair_mem m>
    lookup_pair k
  <\<lambda>r. \<exists>\<^sub>A m'. is_pair_mem m' * \<up>(m' \<subseteq>\<^sub>m m \<and> r = m k)>\<^sub>t"
  unfolding is_pair_mem_def lookup_pair_def
  apply (sep_auto heap: mem_lookup_spec2)
     apply (simp add: domIff map_leD; fail)
  subgoal
    by sep_auto (auto simp: domIff map_le_def)
   apply (sep_auto heap: mem_lookup_spec2 simp: domIff map_leD)+
  subgoal
    by (auto simp: domIff map_le_def)
   apply (sep_auto heap: mem_lookup_spec2)+
  done

context
  assumes injective:
    "injective size to_index"
    "\<forall> k k'. key1 k = key1 k' \<and> key2 k = key2 k' \<longrightarrow> k = k'"
begin

lemma update_pair_correct:
  "<is_pair_mem m>
    update_pair k v
  <\<lambda>r. \<exists>\<^sub>A m'. is_pair_mem m' * \<up>(m' \<subseteq>\<^sub>m m(k\<mapsto>v))>\<^sub>t"
  unfolding update_pair_def is_pair_mem_def
  supply [intro] = injective(1)
  supply [sep_heap_rules] = mem_update_spec3 mem_empty_spec'
  apply sep_auto
  subgoal
    by (auto 4 3 dest: map_leD simp: domIff split: if_split_asm)
   apply sep_auto
  subgoal
    using injective(2) by (auto 4 3 dest: map_leD intro: map_leI simp: domIff split: if_split_asm)
  apply sep_auto
  subgoal
    by (auto 4 3 dest: map_leD simp: domIff split: if_split_asm)
  subgoal
    using injective(2) by (auto 4 3 dest: map_leD intro: map_leI simp: domIff split: if_split_asm)
  apply sep_auto
  subgoal
    by (auto 4 3 dest: map_leD split: if_split_asm)
  subgoal
    using injective(2) by (auto 4 3 dest: map_leD intro: map_leI simp: domIff split: if_split_asm)
  done

interpretation heap_mem_correct is_pair_mem lookup_pair update_pair
  by (standard; rule lookup_pair_correct update_pair_correct)+

end (* Context for injectivity *)

end (* Context for fixed memory *)

(* TODO: Where does the type parameter come from? *)
lemma init_state_spec':
  "<emp> init_state k1 k2 <\<lambda> (k_ref1, k_ref2, m_ref1, m_ref2).
    is_pair_mem TYPE('k1 :: heap) key1 key2 m_ref1 m_ref2 k_ref1 k_ref2 empty
  >\<^sub>t" if "k1 \<noteq> k2"
  using that unfolding is_pair_mem_def by (sep_auto heap: init_state_spec)

end (* Key functions & Size *)

end (* Theory *)