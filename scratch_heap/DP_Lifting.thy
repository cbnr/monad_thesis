theory DP_Lifting
  imports Main "HOL-Imperative_HOL.Imperative_HOL"
begin

  (* Types *)
type_synonym ('a, 'b) fun_lifted = "'a \<Rightarrow> 'b Heap" ("_ \<Rightarrow>\<^sub>T _" [3,2] 2)
notation return ("\<langle>_\<rangle>")

definition fun_app :: "('a \<Rightarrow> 'b Heap) Heap \<Rightarrow> 'a Heap \<Rightarrow> 'b Heap" (infixl "." 999) where
  "f\<^sub>T . x\<^sub>T \<equiv> do {f \<leftarrow> f\<^sub>T; x \<leftarrow> x\<^sub>T; f x}"

lemma return_app_return:
  "\<langle>f\<rangle> . \<langle>x\<rangle> = f x"
  unfolding fun_app_def return_bind ..

locale mem_defs =
  fixes lookup :: "'param \<Rightarrow> 'result option Heap"
    and update :: "'param \<Rightarrow> 'result \<Rightarrow> unit Heap"
begin

definition map_of :: "heap \<Rightarrow> ('param \<rightharpoonup> 'result)" where
  "map_of heap k \<equiv> execute (lookup k) heap \<bind> fst"

definition checkmem :: "'param \<Rightarrow> 'result Heap \<Rightarrow> 'result Heap" where
  "checkmem param calc \<equiv> do {
    x \<leftarrow> lookup param;
    case x of
      Some x \<Rightarrow> \<langle>x\<rangle>
    | None \<Rightarrow> do {
        x \<leftarrow> calc;
        update param x;
        \<langle>x\<rangle>
      }
  }"

(*
definition map_of where
  "map_of heap k = fst (the (execute (lookup k) heap))"
*)

definition checkmem' :: "'param \<Rightarrow> (unit \<Rightarrow> 'result Heap) \<Rightarrow> 'result Heap" where
  "checkmem' param calc \<equiv> do {
    x \<leftarrow> lookup param;
    case x of
      Some x \<Rightarrow> \<langle>x\<rangle>
    | None \<Rightarrow> do {
        x \<leftarrow> calc ();
        update param x;
        \<langle>x\<rangle>
      }
  }"

lemma checkmem_checkmem':
  "checkmem' param (\<lambda>_. calc) = checkmem param calc"
  unfolding checkmem'_def checkmem_def ..

end (* Mem Defs *)

context
  includes lifting_syntax
begin

  (* HOL *)

definition if\<^sub>T :: "bool Heap \<Rightarrow> 'a Heap \<Rightarrow> 'a Heap \<Rightarrow> 'a Heap" where
  "if\<^sub>T b\<^sub>T x\<^sub>T y\<^sub>T \<equiv> do {b \<leftarrow> b\<^sub>T; if b then x\<^sub>T else y\<^sub>T}"

thm comp_def
definition comp\<^sub>T :: "(('b \<Rightarrow>\<^sub>T 'c) \<Rightarrow>\<^sub>T ('a \<Rightarrow>\<^sub>T 'b) \<Rightarrow>\<^sub>T ('a \<Rightarrow>\<^sub>T 'c)) Heap" where
  "comp\<^sub>T \<equiv> \<langle>\<lambda>f. \<langle>\<lambda>g. \<langle>\<lambda>x. \<langle>f\<rangle> . (\<langle>g\<rangle> . \<langle>x\<rangle>)\<rangle>\<rangle>\<rangle>"
  
primrec fold\<^sub>T' :: "('a \<Rightarrow>\<^sub>T 'b \<Rightarrow>\<^sub>T 'b) \<Rightarrow> 'a list \<Rightarrow>\<^sub>T 'b \<Rightarrow>\<^sub>T 'b" where
  "(fold\<^sub>T' f) [] = \<langle>\<lambda>x. \<langle>x\<rangle>\<rangle>"
| "(fold\<^sub>T' f) (x#xs) = comp\<^sub>T . ((fold\<^sub>T' f) xs) . (\<langle>f\<rangle> . \<langle>x\<rangle>)"

lemma
  "(fold\<^sub>T' f) (x#xs) = comp\<^sub>T . (\<langle>fold\<^sub>T' f\<rangle> . \<langle>xs\<rangle>) . (\<langle>f\<rangle> . \<langle>x\<rangle>)"
  unfolding fold\<^sub>T'.simps(2) return_app_return ..
    
definition fold\<^sub>T :: "(('a \<Rightarrow>\<^sub>T 'b \<Rightarrow>\<^sub>T 'b) \<Rightarrow>\<^sub>T 'a list \<Rightarrow>\<^sub>T 'b \<Rightarrow>\<^sub>T 'b) Heap" where
  "fold\<^sub>T \<equiv> \<langle>\<lambda> f\<^sub>T. \<langle>fold\<^sub>T' f\<^sub>T\<rangle>\<rangle>"

thm List.fold_def
definition fold1\<^sub>T where
  "fold1\<^sub>T \<equiv> \<langle>\<lambda>f. \<langle>\<lambda>xs. \<langle>rec_list  \<langle>\<lambda>f. \<langle>\<lambda>x. \<langle>id x\<rangle>\<rangle>\<rangle> (\<lambda>x xs a. \<langle>\<lambda>f. comp\<^sub>T . (a . \<langle>f\<rangle>) . (f x)\<rangle>)\<rangle> . \<langle>xs\<rangle> . \<langle>f\<rangle>\<rangle>\<rangle>"

thm List.map_rec
definition map1\<^sub>T where
  "map1\<^sub>T \<equiv> \<langle>\<lambda>f. \<langle>\<lambda>xs. \<langle>rec_list \<langle>[]\<rangle> (\<lambda>x xs a. \<langle>\<lambda>y. \<langle>\<lambda>ys. \<langle>y#ys\<rangle>\<rangle>\<rangle> . (f x) . a)\<rangle> . \<langle>xs\<rangle>\<rangle>\<rangle> "

end (* locale mem_def *)
end (* theory *)